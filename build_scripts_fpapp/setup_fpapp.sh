#!/bin/sh
# Create folders.
[ -e /home/oarcelus/fpapp ] && rm -r /home/oarcelus/fpapp
mkdir -p /home/oarcelus/fpapp/opt
mkdir -p /home/oarcelus/fpapp/usr/share/applications
mkdir -p /home/oarcelus/fpapp/usr/share/icons/hicolor/scalable/apps
mkdir -p /home/oarcelus/fpapp/usr/share/mime/packages
mkdir -p /home/oarcelus/fpapp/usr/share/icons/hicolor/scalable/mimetypes

# Copy files (change icon names, add lines for non-scaled icons)
cp -r /home/oarcelus/fullprofapp/src/dist/fpapp /home/oarcelus/fpapp/opt/
cp ./fpapp.desktop /home/oarcelus/fpapp/usr/share/applications
cp /home/oarcelus/fullprofapp/src/dist/fpapp/fpgui/icons/logo.svg /home/oarcelus/fpapp/usr/share/icons/hicolor/scalable/apps/logo.svg
cp ./fpapp-mime-type.xml /home/oarcelus/fpapp/usr/share/mime/packages
cp /home/oarcelus/fullprofapp/src/dist/fpapp/fpgui/icons/logo.svg /home/oarcelus/fpapp/usr/share/icons/hicolor/scalable/mimetypes/fpapp-mime-type-fpapp-project-file-mime-type.svg

# Change permissions
find /home/oarcelus/fpapp/opt/fpapp -type f -exec chmod 644 -- {} +
find /home/oarcelus/fpapp/opt/fpapp -type d -exec chmod 755 -- {} +
find /home/oarcelus/fpapp/usr/share -type f -exec chmod 644 -- {} +
chmod +x /home/oarcelus/fpapp/opt/fpapp/fpapp

fpm -C /home/oarcelus/fpapp -s dir -t deb -n "fpapp" -v 1.2.7 --after-install ./postinst.sh --deb-compression gz -p ./fpapp.deb

