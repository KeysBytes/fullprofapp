import sys
# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'FullProfAPP'
copyright = '2023, Oier Arcelus'
author = 'Oier Arcelus'
release = '1.2.10'
version = '1.2.10'
license = 'GNU General Public License version 3 (GPLv3)'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.napoleon",
    "sphinx.ext.mathjax",
    "sphinx.ext.autodoc",
    "sphinxcontrib.email",
    "sphinx_design",
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
email_automode = True

numfig = True
charset_type = "utf-8"

html_css_files = [
    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css",
    "custom.css",
    "css/colors.css",
]
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_logo = "_static/splash.png"
