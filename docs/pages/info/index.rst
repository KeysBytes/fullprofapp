.. FullProfAPP documentation master file, created by
   sphinx-quickstart on Thu Jun 29 10:42:58 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Links:
.. _latest version: https://www.ill.eu/sites/fullprof/php/downloads.html
.. _CIC energiGUNE: https://cicenergigune.com/en/fullprof-app
.. _ILL: https://www.ill.eu/sites/fullprof/php/downloads.html
.. _here: https://www.gnu.org/licenses/gpl-3.0.en.html

Introduction
============

.. _installation:

Installation
------------
Installing `FullProfAPP` is really simple. Follow these steps and you will be running your refinements in no time!

#. Download the `latest version`_ of `FullProf Suite` and install it in your computer.

#. Download the adequate `FullProfAPP` installer for your operating system. At the moment only Windows and Linux machines are supported. You can find the download links in `CIC energiGUNE`_'s and `ILL`_'s websites.

   * **Windows users**: Click on the download link, execute the ``fpapp_windows_x86_installer.exe`` file, and follow the instructions

   * **Linux users (Ubuntu/Debian)**: Click on the download link, open a shell session, ``cd`` to the download folder, and run the following command:

      .. code-block:: shell-session

         $ sudo dpkg -i fpapp_linux_installer.deb

     After the installation is complete, you will find the executable ``fpapp`` in the ``/opt/fpapp/`` folder. It is recommended that you additionally include this folder in the ``PATH`` environmental variable.

#. Run `FullProfAPP`!

.. warning::

   * `FullProfAPP` operates by routinely executing ``CIFs_to_PCR`` and ``fp2k`` programs. Make sure that these programs are visible to `FullProfAPP` and included in the ``PATH`` of your operating system. You can check this by opening a session of Windows or Linux terminal, and trying to execute both programs by typing ``CIFs_to_PCR`` and ``fp2k``.

   * Make sure that you install the last version of `FullProf Suite`.

   * There might be cases where running a freshly installed `FullProfAPP` window fails on initialization. Specially if you are installing a new version and you did not properly uninstall the older version. In this case, make sure you got to the installation folder and you manually delete all the contents that may have not been properly deleted. And example of such error can be the following

   .. figure:: ./img/brotli.png
      :width: 60%
      :alt: framework
      :align: center

      Initialization error, module ``brotli`` is not used by the current version of `FullProfAPP` but the module libraries were not correctly removed from the operating system.

License
-------
This software is distributed under the **GNU General Public License version 3 (GPLv3)**. For more information about the licensing terms go `here`_

Acknowledgements
----------------
The software is developed in the frame of a collaboration between CIC energiGUNE (Spain: Oier Arcelus, Montse Casas-Cabanas, Javier Carrasco, Marine Reynaud), ICMAB-CSIC (Spain: Oier Arcelus, M. Rosa Palacín, Carlos Frontera, Ashley Black), Institute Laue-Lagevin (France: Juan Rodríguez-Carvajal, Nebil A. Katcho, and ALBA Synchrotron (Spain: François Fauth).

This project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement No 957189. The project is part of BATTERY 2030+, the large scale European research initiative for inventing the sustainable batteries of the future.

Contact
-------
During your use of `FullProfAPP` you may encounter some unexpected behavior or bugs, you might also miss some functionality that you would like to be implemented. If this is the case,
please contact us to the following email address. Having direct feedback from users is extremely important for us to keep improving the program.

:email:`fullprofapp@cicenergigune.com`
