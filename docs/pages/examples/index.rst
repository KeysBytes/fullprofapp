.. FullProfAPP documentation master file, created by
   sphinx-quickstart on Thu Jun 29 10:42:58 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Links:
.. _OPTIMADE: https://www.optimade.org/
.. _optimade-python-tools: https://www.optimade.org/optimade-python-tools/latest/getting_started/client/
.. _search match: http://www.cristal.org/smrr/results/
.. _GitLab: https://gitlab.com/d7081/fullprofapp/-/tree/main/

.. include:: ../../.special.rst

Getting Started
===============

In the following sections we will introduce several practical use cases of `FullProfAPP`, from the simple task of loading and visualizing your data, to performing sequential refinements on `operando` data.

The first step is to download the example files from the `GitLab`_ repository

Select Project Folder
---------------------

To select a working directory for your project follow these steps

#. Click on `File`
#. Click on `Select Project Folder`
#. Select the working directory, to follow the next examples select the ``examples/EgyptMakeup`` folder in the example files.

See how the :ref:`File System <FileSys>` and :ref:`Terminal <Term>` tabs are updated.

Load Experimental Data
----------------------

Follow the steps

#. Click on `Pattern`
#. Click on `Import Pattern`.
#. Open the file explorer by clicking `Browse` on the **Pattern File** section and select the ``EgyptMakeUp.xys`` file
#. Select the file format by expanding the **File Format** list. Select the ``xys/xye/xy`` option
#. Open the file explorer by clicking `Browse` on the **IRF File** section and select the ``LURE.irf`` file
#. Select the `X-Rays` option on **Radiation** and the `Debye-Scherrer / Bragg-Brentano` on the **Geometry** section
#. Click on `Add Files`.

Click `OK` to load the patterns into the app. You should see the pattern plotted in the :ref:`Graphics Area <GraphArea>` as well as some additional options enabled, such as the :ref:`Graphics Toolbar <GraphToolbar>` and the :ref:`Automatic Background <AutoBack>` (:ref:`Peak Detection <PeakDetect>`) options. Additionally, the `Pattern List` and the `Load .pcr(s)` buttons from the :ref:`Automatic Refinement Protocol <AutoRef>` and the :ref:`Manual Refinement <ManualRef>` windows are also enabled. Meaning that at this points you can already start loading existing PCR files to perform Rietveld refinements. However, let's continue with our example.

Detecting and Editing Background and Peaks
------------------------------------------

Once the experimental data is loaded into the app, you can try to extract information about the background, and the peak positions and intensities.

To fit the background follow the steps

#. Click on `Pattern`
#. Click on `Automatic Background`

You should see the current fit for the background plotted along with the experimental patterns. On top of this, a small widget containing two sliders (`log_lambda` and `log_p`) pops up. Changing the values of any of the sliders changes the fit of the background.

.. figure:: ./img/example_back.png
         :width: 80%
         :alt: framework
         :align: center

         Background fit example

After fitting the background to the experimental pattern, you can additionally extract peak positions and intensities with the following steps

#. Click on `Pattern`
#. Click on `Peak Detection`

Again, you should see the peak positions and intensities plotted with the experimental patterns and background. You can use the `alpha` and `width` sliders to chose the best parameter combination. Notice that the peak positions are also shown below.

.. figure:: ./img/example_peak.png
         :width: 80%
         :alt: framework
         :align: center

         Peak detection example

.. note::

   The fitting of the background or the detection of peak positions and intensities happens for all patterns that appear as **checked** in the `Pattern Checks` menu of the :ref:`Graphics Toolbar <GraphToolbar>`  (see this :ref:`Note <ChecksNote>`)

The background points (peaks) can be edited by toggling the `Edit Background` (`Edit Peaks`) modes in the `Pattern` menu. See the :ref:`Interacting With Graphs <InteractGraphs>` sections for more details.

Downloading CIF Files from Online Databases
-------------------------------------------

`FullProfAPP` allows you to download crystallographic models as CIF files. It uses OPTIMADE_, an API with a common specification for several materials databases.

.. note::

   The current implementation of `FullProfAPP` only uses `OPTIMADE`_ to obtain the phase identifiers for the corresponding databases. Then, these identifiers are downloaded using database specific API's. The current version of the program is only compatible with the Crystallographic Open Database (COD). We realize that this workaround is not optimal, and we plan to improve the situation using the recently developed client of the `optimade-python-tools`_, to allow users to export CIF files from a variety of databases.

To search for materials in databases, follow these steps

#. Click on `Materials`
#. Click on `Search in Remote Database`

You should see a dialog window pop up. Inside, a periodic table of elements is shown. Each button corresponds to an element and can be toggled to three different states

* :gray:`Grey`: The queried compounds do NOT contain this element
* :goldenrod:`Yellow`: The queried compounds OPTIONALLY contain this element
* :green:`Green`: The queried compounds MUST contain this element

Lets try the following query. :green:`{Pb}` indicating that all the downloaded phases must contain lead. And :goldenrod:`{C, O, S, Cl}`, indicating that the phases may optionally contain any of those phases. The window should look like this

.. figure:: ./img/example_periodic.png
         :width: 80%
         :alt: framework
         :align: center

         Database querying window example

Click `OK` to initiate the download process.

.. note::

   * Be patient when starting the download. The querying process with  `OPTIMADE` is not implemented to be concurrent with `FullProfAPP`'s event loop and the GUI will be blocked for some time while the phase identifiers are retrieved. After that, you will see a progress bar appear in the bottom-right side of the app's main window showing you the downloading progress

   * It may happen that `OPTIMADE` fails to return the queried identifiers. Normally, this is due to the target database being down or not reachable from your network. It may also happens that the materials identifiers are correctly extracted but the specific requests to the database API fail. In this case you will see that the :ref:`Queried Materials <QueryMat>` tab is empty.

Once all CIF files are downloaded, you will see around 120 entries in the :ref:`Queried Materials <QueryMat>` tab. You can inspect the items to see that, indeed, all downloaded phases contain :green:`{Pb}`. You may have also received a warning telling you that some CIF files failed to be parsed and that they would not be loaded into the program. Notice also that a new folder has been created in the working directory called ``Downloaded_CIFs``, where all the CIF files are stored.

.. _simulate:

Simulate Powder Patterns
------------------------

Here we describe how to use `FullProfAPP` to simulate diffraction patterns of materials. The :ref:`Queried Materials <QueryMat>` tab has two buttons `Submit Selection` and `Simulate Selection`. To run a simulation, select the one or more rows in the materials table and click `Simulate Selection`. You will see that the focus is changed to the :ref:`Terminal <Term>` where the information about the execution of ``fp2k`` is printed. When the simulations is finished, a pop up window appears showing the results of the simulations together with the experimental pattern that is currently selected in the :ref:`Graphics Toolbar <GraphToolbar>`.

This popup window contains a few extra features for you to visually adjust some parameters if needed.

* `Scale Factor sliders`: Each crystallographic phase included in the simulations has an associated slider that allows to change the intensities of the profiles for that phase.
* `Zero Correction slider`: This slider alters the value of the constant :math:`2\theta` shift on the simulated patterns.
* `HKL Gen`: Each crystallographic phase has an associated button that allows to recalculate the positions of the `hkl` reflections for varying cell paramters.

.. figure:: ./img/simulation.png
         :width: 100 %
         :alt: framework
         :align: center

         Simulation window of two phases 'cod-1539418' and 'cod-9005510'. The sliders associated to scale factor, zero correction, and the `HKL Gen` pop up window of 'cod-1539418' appear on screen.

Additionally, you can see two extra buttons in the figure above.

* `Rerun Simulation`: Run another simulation considering the newly selected scale factors, zero correction, and cell parameters. Notice that when playing with the sliders of the `HKL Gen` pop up window, only the `hkl` reflections change, not the profile. On the other hand, rerunning the simulation will also update the profile.
* `Generate CIF file`: Duplicate the CIF file(s) that were used in the simulation. The new CIF files will contain the values of the cell parameters that were manually selected with the `HKL Gen` functionality.

The results of the simulations are saved in folders named ``SIMULATE###``.

At this point you can start playing with the sliders. Do you see any way of adjusting the available parameters by hand? Are you able to find some of the phases that are present in the samples out of the 120 phases that you downloaded from the COD?

You may have already noticed that this is not a smart way of going around this problem. Indeed, many packages (most of them commercial) exist that perform accurate `search match`_ identification based on sample peak positions and intensities. However, did you know that this is also possible to do using only Rietveld refinements? See the next section for more information.

Run Full-Profile Phase Searches
-------------------------------

`FullProfAPP` offers the possibility of performing phase searches in order to discriminate the crystallographic phases that are potentially present in your samples from the ones that are not. To test this functionality, we will keep using the Egyptian Makeup example. Follow the steps

#. Copy the CIF files from ``examples/EgyptMakeup/ICSD`` to ``examples/EgyptMakeup/Downloaded_CIFs``.
#. Click on `Materials`.
#. Click on `Search in Local Database` and select the ``examples/EgyptMakeup/Downloaded_CIFs`` folder. You should now have five additional entries in the :ref:`Queried Materials <QueryMat>` table.
#. Check for bad entries and clean them. For example, empty fields, low symmetry entries (P1, P -1, ...), <NA> entries, strange spacegroup symbols, etc..
#. Select the whole table and click `Submit Selection`. You will see the :ref:`Selected Materials <SelMat>` tab become visible with a table that contains all the selected phases.
#. Go to the :ref:`Full-Profile Phase Search Protocol <LuteProt>` window and select the following parameters

   .. figure:: ./img/luteparams.png
            :width: 70%
            :alt: framework
            :align: center

            Example parameters of the :ref:`Full-Profile Phase Search Protocol <LuteProt>` window

   .. note::

      Notice that we excluded the 40 - 90 :math:`2\theta` region. This is because the reflections at high angles are less relevant when performing phase searches.

   .. warning::

      Before going to the next step, we warn you that the results of the phase search are quite sensitive to the selection of the background points, specially for the minority phases. In principle, for this example the default parameters of the :ref:`Automatic Background <AutoBack>` should work just fine, but it is always a good idea to double check your results.

#. Click `Run: FP Search` to start the search. You will see a pop up window that shows the progress of the phase search protocol. It looks like this.

   .. figure:: ./img/searchprocess.png
            :width: 80%
            :alt: framework
            :align: center

            Progress window for the :ref:`Full-Profile Phase Search Protocol <LuteProt>`

   The progress window has the following sections

   * **Scanned Phases (S1)**: Table with all the phases that are being considered during the scanning cycle.

   * **Detected Phases (S2)**: Table with the phases that have achieved the highest Figure of Merit (FoM) at the end of the scanning cycle.

   * **FoM Graph**: It plots the value of the FoM with respect to the phase index.

   * :math:`R_{wp}` **Graph**: It plots the weighted profile factor (:math:`R_{wp}`) each time a phase is added to the list of detected phases.

   .. note::

      The :math:`R_{wp}` should have a decreasing tendency with the subsequent detection of new phases. If you see that adding new phases does not contribute to decreasing :math:`R_{wp}` (or that it even increases its value), it means that the phase search process is failing to identify all the phases correctly (even if it is possible that some of the detected phases are correct).

   The progress window has a color code that tells you which is the status of the scan and quantification cycles:

   * :grey:`White`: The highlighted phases are not analyzed yet
   * :goldenrod:`Yellow`: The highlighted phases have been already analyzed
   * :green:`Green`: The highlighted phase has achieved the maximum FoM of the scanning cycle. If this color appears in the S2 table, the phase search process has been successful.
   * :red:`Red`: The highlighted phases have resulted in a refinement error or their weight fractions are below a threshold (`f_S1`). In any of those cases, the phases are eliminated and not considered for subsequent scanning cycles.

If the full-profile phase search finishes correctly, you should see the :ref:`Results <ResMat>` and the :ref:`Prf Data <prfdat>` tabs appear with a summary of the phases and the resulting refined profiles. Do you get something similar to this?

.. figure:: ./img/egyptprf.png
            :width: 100%
            :alt: framework
            :align: center

            Refined profiles of the EgyptMakeUp example

If the answer is yes, you've solved a 4000 year old Egyptian mystery! 𓍛 𓏇 𓈖𓄿, 𓀊!

.. note::

   * While it is possible to perform phase searches in large sets of candidate phases, Rietveld refinements are more computationally demanding than traditional search-match runs. `FullProfAPP` implements a multiprocessing job scheduler that allows to speed up the phase search process by a factor of 4 with respect to older versions.
   * Still, brute forcing a full-profile phase search might not be the most sensible option, especially when dealing with synthetic samples. Instead, a pre-screening of potential phases with traditional search match methods is probably desirable, to then perform full-profile searches on a reduced set of candidate phases.

Sequential Refinement of Phase Separating LiFePO\ :sub:`4` - FePO\ :sub:`4`
---------------------------------------------------------------------------
In this section we will learn how to interact with the :ref:`Automatic Refinement Protocol <AutoRef>` window. This example features a synchrotron `operando` study of a Li-ion battery performing subsequent charge/discharge cycles. This battery contains LiFePO\ :sub:`4` as the positive electrode active material. Under normal operation conditions, LiFePO\ :sub:`4` is known to be a phase separating material. Thus, we expect that the pattern series will contain both LiFePO\ :sub:`4` and FePO\ :sub:`4` phases with varying weight fractions, manifested as the appearing and dissapearing of their corresponding diffraction peaks. The first thing to start with this examples is to load the experimental patterns. Follow the steps

#. Click on `File/Select Project Folder` and select the ``examples/LiFePO4`` directory
#. Click on `Pattern/Import Pattern`
#. In the dialog window click on `Browse` on the **Pattern File** section and select all the files in the ``examples/LiFePO4/LiFePO4_14LFPKa`` folder
#. Select the ``xys/xye/xy`` option in the **File Format (.*)** section
#. Click on `Browse` on the **IRF File** section and select the ``IRF_NOTOS_July2022_MR.irf`` file
#. Select the `X-Rays` option on **Radiation** and the `Debye-Scherrer / Bragg-Brentano` on the **Geometry** section
#. Click on `Add Files`.

Press the `OK` button to load the pattern series onto the app. Look at the patterns plotted in the :ref:`Graphics Area <GraphArea>`, explore them using the :ref:`Graphics Toolbar <GraphToolbar>`, you can see how new peaks appear while others dissapear with varying intensities. You can have a global view of the whole pattern series by clicking on the `Contour Plot` button

.. figure:: ./img/lfpcontour.png
         :width: 90%
         :alt: framework
         :align: center

         Contour plot of the loaded patterns

Looking at the data you can see some strange noisy signals for the first two patterns `14LFPka_f00001` and `14LFPka_f00002`, which will be ignored in this example.

.. note::

   Since the objective of the example is to perform a sequential refinement, recall the three points from the information given about the :ref:`Sequential Mode <SeqRef>` check box of the :ref:`Automatic Refinement Protocol <AutoRef>` window.

Taking the previous note into consideration, we will set the pattern `14LFPka_f00003` as **checked**, while the rest will remain **unchecked**. Notice the changes in the :ref:`Automatic Refinement Protocol <AutoRef>` window. The `Pattern List` now shows a single item referring to the **checked** item. By further checking the :ref:`Sequential Mode <SeqRef>` box, you are telling `FullProfAPP` to perform a sequential refinement of the whole pattern series starting in `14LFPka_f00003` and finishing in `14LFPka_f000030`.

But first, we should define a better background for our **checked** pattern. Follow the steps

#. Click on `Pattern`
#. Click on `Automatic Background`

You may have to spend some time manually editing the background points, especially in the 10 to 20 :math:`2\theta` region, where the automatic background is not able to find a good fit for the pattern. To edit the background points remember to activate the `Edit Background` mode and use the editing :ref:`commands <EditBack>`. You should obtain something like this

.. figure:: ./img/lfp_background.png
         :width: 90%
         :alt: framework
         :align: center

         Background fit of the pattern

Once you are happy with the fit of the background signal, it is time to download the candidate crystallographic phases from the COD. Follow the steps

#. Click on `Materials`
#. Click on `Search in Remote Database`
#. On the pop up window do the following selection :green:`{Fe, P, O}` and :goldenrod:`{Li}` and click `OK`. The selection should look like this

.. figure:: ./img/example_lfp_periodic.png
         :width: 70%
         :alt: framework
         :align: center

         Database query example

Once the download is completed, we invite you to `simulate`_ some patterns for the LiFePO\ :sub:`4` and FePO\ :sub:`4` structural models to visually inspect how well they fit as initial guesses. At the moment of writting this example, we found the CIF files with identifiers `cod-1101111` (LiFePO\ :sub:`4`) and `cod-1525576` (FePO\ :sub:`4`) to have initial values that are close enough for this example. Alternatively, you can try to use the :ref:`Full-Profile Phase Search Protocol <LuteProt>` window first.

Additionally, we download the CIF files corresponding to aluminum (`cod-4313206`), as our pattern additionally contains signals that come from the current collector of the battery. You can quickly see by simulating the aluminum pattern that the positions of the reflections are close enough, but the peak intensities are not, even when varying the scale factors. This is because the aluminum is highly textured and the powder diffraction condition is not met (some crystallographic planes are over-represented in the data collection). This example will also show you how to treat these specific peaks.

After downloading the crystallographic model phases, we must assign them to our **checked** patterns. Follow the steps

#. Select the `cod-4313206`,  `cod-1101111`, and `cod-1525576` entries on the :ref:`Queried Materials <QueryMat>` table
#. Click on the `Submit Selection` window. See the updates on the :ref:`Selected Materials <SelMat>` tab.
#. Go to the :ref:`Automatic Refinement Protocol <AutoRef>` window and click the :fas:`plus` button. The `Pattern Phases` tree below is updated. Expand the tree item to verify that the three model phases have been linked to the `14LFPka_f00003` pattern.

Now that `FullProfAPP` knows which patterns to treat and which crystallographic models to use, we are ready to define our refinement workflow. Remeber that the refinement :ref:`workflow <WorkFlw>` is defined by checking the items in the `Refinement Parameters` tree and adding the workflow steps by clicking on the :fas:`angle-double-right` button. Follow the steps to define the workflow

#. Check `Scale Factors` for `cod-1101111` and `cod-1525576`. ``Mouse Right Click`` on `cod-4313206` and select the `Profile Matching (Constant Scale Factor)` option. Click :fas:`angle-double-right`.
#. Check `Zero`. Click :fas:`angle-double-right`.
#. Check `Background`. Click :fas:`angle-double-right`.
#. Check `Cell Parameters` for `cod-1101111`, `cod-1525576`, and `cod-4313206`. Click :fas:`angle-double-right`.
#. Check `Ustrain` for `cod-1101111`, `cod-1525576`, and `cod-4313206`. Click :fas:`angle-double-right`.
#. Check `Ysize` for `cod-1101111`, `cod-1525576`, and `cod-4313206`. Click :fas:`angle-double-right`.

Notice the 5 step workflow that we defined in the `Workflow tree` panel. Expand the tree items and verify that indeed that workflow that you set up is the one we show above.

.. note::

   Have you noticed what we did with the aluminum (`cod-4313206`)? Remember that the aluminum in this sample is highly textured and is not well suited to be refined using the Rietveld method. Instead, we set it to be refined using the LeBail method by selecting the `Profile Matching (Constant Scale Factor)` option. 

.. warning::

   Remember that when using the `Profile Matching` mode you should never refine site parameters (atomic positions, temperature factors, occupations). Additionally, if you use the `Constant Scale Factor` mode, you should not refine the scale factors either.

Lastly, we will exclude the 0.0 - 11.5 :math:`2\theta` region from the refinement process. In short, the :ref:`Automatic Refinement Protocol <AutoRef>` window should look like this.

.. figure:: ./img/lfp_auto_window.png
         :width: 90%
         :alt: framework
         :align: center

         :ref:`Automatic Refinement Protocol <AutoRef>` window setup

Once all the parameters are set, `FullProfAPP` is completely setup to perform the sequential refinement.

Click on the `Run FP: Workflow` button to start the refinement. You will see the focus change to the :ref:`Terminal <Term>` tab.

When the first pattern has been refined, two additional tabs (:ref:`Prf Data <prfdat>` and :ref:`Summary <prfdat>`) will become visible. Move around with the :ref:`Graphics Toolbar <GraphToolbar>` to trigger the signals for plotting the refined profiles in the :ref:`Prf Data <prfdat>` tab. The :ref:`Summary <prfdat>` tab will update at the end of each pattern's refinement. Notice the additional parameter tree to the right of the plot area. Check any item in the parameter tree to plot the value of the corresponding parameters against the pattern number. For example, check the `weight fractions` items for `cod-1101111` and `cod-1525576` to inspect the evolution of the weight fractions of LiFePO\ :sub:`4` and FePO\ :sub:`4` along the pattern series.

.. figure:: ./img/lfp_results.png
         :width: 90%
         :alt: framework
         :align: center

         Results of the refinement process

Nice! We have just easily refined a pattern series comming from an `operando` synchrotron measurement of a Li-ion battery! 🔋 Which has saved us a lot of time in comparison to the prospect of doing the same thing pattern by pattern.

.. warning::

   The refinement workflow and the set-up of the sequential refinement is case specific, and this example does not show a universal approach to treat all kinds of sequential data. Additionally, double checking and taking care that all the generated PCR files have paramers that make sense is the responsability of the user. `FullProfAPP` does its best to prevent the refinement process from diverging, but does not automatically handle non-sensical refinement values. That is why the user has complete access to the files generated by ``fp2k`` in the working directory (folders named ``FPSEQ###`` in this specific example).
   
   For example, a quick look into the files generated in this example shows that some of the microstrain and crystallite size parameters take negative values. This is not correct, and the user is responsible for fixing this issue to improve upon the current state of the refinement.

Sequential Refinement of a LiMn\ :sub:`0.333` Ni\ :sub:`0.333` Co\ :sub:`0.333` O\ :sub:`2` Solid Solution 
----------------------------------------------------------------------------------------------------------
This example features a synchrotron `operando` study of a Li-ion battery performing subsequent charge/discharge cycles. In this case, the `operando` cell contains LiMn\ :sub:`0.333` Ni\ :sub:`0.333` Co\ :sub:`0.333` O\ :sub:`2` (NMC111) as the positive electrode active material. This material has a solid solution mechanism under normal operating conditions, where all intermediate Li compositions are thermodinamically stable with respect to the fully lithiated and delithiated phases. Thus, we expect that the pattern series will only contain a single NMC111 phase, where diffraction peaks will evolve according to the changes in the crystal structure of the material. This is in contrast to the previous LiFePO\ :sub:`4` example, where the diffraction peaks of each phases where relatively unchanged but their relative intensities varied according to the constituent phase fractions.

Following the same steps as in previous examples, the first thing is to load the experimental patterns into the app.

#. Click on `File/Select Project Folder` and select the ``examples/NMC111`` directory
#. Click on `Pattern/Import Pattern`
#. In the dialog window click on `Browse` on the **Pattern File** section and select all the files in the ``examples/NMC111/IS22NMCapd`` folder
#. Select the ``xys/xye/xy`` option in the **File Format (.*)** section
#. Click on `Browse` on the **IRF File** section and select the ``IRF_NOTOS_July2022_MR.irf`` file
#. Select the `X-Rays` option on **Radiation** and the `Debye-Scherrer / Bragg-Brentano` on the **Geometry** section
#. Click on `Add Files`.
#. Press the `OK` button to load the pattern series onto the app.

Look at the patterns plotted in the :ref:`Graphics Area <GraphArea>`, explore them using the :ref:`Graphics Toolbar <GraphToolbar>`. Do you see the behaviour that we where describing earlier? You can have a global view of the whole pattern series by clicking on the `Contour Plot` button-

.. figure:: ./img/nmccontour.png
         :width: 90%
         :alt: framework
         :align: center

         Contour plot of the loaded patterns

Given that the objective of the exercise is to perform a sequential refinement of the pattern series, we will set the pattern `IS22NMCa_f00039.xye` as **checked**, while the rest will remain **unchecked**. Notice again the changes in the :ref:`Automatic Refinement Protocol <AutoRef>` window. The `Pattern List` now shows a single item referring to the **checked** item. By further checking the :ref:`Sequential Mode <SeqRef>` box, you are telling `FullProfAPP` to perform a sequential refinement of the whole pattern series starting in `IS22NMCa_f00039.xye` and finishing in `IS22NMCa_f00060.xye`.

The next step is to define a background for our **checked** pattern. Follow the steps

#. Click on `Pattern`
#. Click on `Automatic Background`

In this case the default parameters give a good enough fit for the background, but you are free to change them to your liking.

.. figure:: ./img/nmc_background.png
         :width: 90%
         :alt: framework
         :align: center

         Background fit of the pattern

Once the background is fitted, we must download the CIF files of the material models from the crystallographic databases. Follow the steps

#. Click on `Materials`
#. Click on `Search in Remote Database`
#. On the pop up window do the following selection :green:`{Li, Mn, Ni, Co}` and click `OK`. The selection should look like this:

.. figure:: ./img/example_nmc_periodic.png
         :width: 70%
         :alt: framework
         :align: center

         Database query example

When the download has been completed we must check whether the CIF files have starting parameters that are good enough for the refinement. For this we will `simulate`_ some patterns using the downloaded CIF files. The file with identifier `cod-4002443` is an appropriate material model for NMC111. Additionally, the diffraction patterns contain two extra signals. The peaks around **27.9º**, **32.35º**, and **46.4º** :math:`2\theta` correspond to the aluminum foil, which is highly textured and is not well suited to be refined using the Rietveld method. Instead, just as what we did in the previous LiFePO\ :sub:`4` example, we will treat those peaks using the `Profile Matching (Constant Scale Factor)` option. Then, we must download the CIF file for Al as well and look for the `cod-4313206` identifier. Similarly, the peaks around **33.1º**, **38.0º**, and **50.2º** :math:`2\theta` correspond to two berillium windows that are placed at both ends of the operando cell. In this case, the berillium peaks do not overlap with the NMC111 peaks, and we will just exclude them from the refinement process. 

Then, we must assign the downloaded phases to our **checked** patterns. Follow the steps

#. Select the `cod-4313206` and  `cod-4002443` entries on the :ref:`Queried Materials <QueryMat>` table
#. Click on the `Submit Selection` window. See the updates on the :ref:`Selected Materials <SelMat>` tab.
#. Go to the :ref:`Automatic Refinement Protocol <AutoRef>` window and click the :fas:`plus` button. The `Pattern Phases` tree below is updated. Expand the tree item to verify that the model phases have been linked to the `IS22NMCa_f00039.xye` pattern.

At this point we are ready to select the refinement workflow for our pattern series. Follow the steps

#. Check `Scale Factors` for `cod-4002443`. ``Mouse Right Click`` on `cod-4313206` and select the `Profile Matching (Constant Scale Factor)` option. Click :fas:`angle-double-right`.
#. Check `Zero`. Click :fas:`angle-double-right`.
#. Check `Background`. Click :fas:`angle-double-right`.
#. Check `Cell Parameters` for `cod-4002443` and `cod-4313206`. Click :fas:`angle-double-right`.
#. Check `Ustrain` for `cod-4002443` and `cod-4313206`. Click :fas:`angle-double-right`.
#. Check `Ysize` for `cod-4002443` and `cod-4313206`. Click :fas:`angle-double-right`. 

Last we will need to exclude the regions were the berillium signals appear. Using the :ref:`Excluded Regions <excluded regions>` widget exclude the following regions:

* **0.0º - 10.0º**
* **32.0º - 33.8º**
* **36.0º - 40.0º**
* **49.8º - 60.0º**

Click on the `Run FP: Workflow` button to start the refinement. You will see the focus change to the :ref:`Terminal <Term>` tab, as well as two additional tabs (:ref:`Prf Data <prfdat>` and :ref:`Summary <prfdat>`) that will allow you to inspect the results of the refinement process.

Inspect the refinement results. Is the refinement satisfactory? The evolution of the **c** axis in the :ref:`Summary <prfdat>` tab looks good and follows the behaviour that we would expect from a charge/discharge cycle of NMC111. However, looking at the fitting of the experimental patterns we can see that the results are not so good.  

#. The diffraction peaks become strongly anisotropic along the pattern series. This can be explained by the fact the volume change of NMC111 happens preferentially along the **c** axis, thus accumulating strains along particular directions.
#. The **003** reflection is not completely well refined.

.. figure:: ./img/nmc_wrong_prf.png
         :width: 90%
         :alt: framework
         :align: center

         `IS22NMCa_f00050.xye` pattern showing the strong anisotropy on the broadening of the diffraction peaks.

To improve upon the previous refinement follow the steps we will need to use a new refinement workflow, this time accounting for the anysotropic strains. Follow the steps

#. Check `Scale Factors` for `cod-4002443`. ``Mouse Right Click`` on `cod-4313206` and select the `Profile Matching (Constant Scale Factor)` option. Click :fas:`angle-double-right`.
#. Check `Zero`. Click :fas:`angle-double-right`.
#. Check `Background`. Click :fas:`angle-double-right`.
#. Check `Cell Parameters` for `cod-4002443` and `cod-4313206`. Click :fas:`angle-double-right`.
#. Check `Ustrain` for `cod-4002443` and `cod-4313206`. Click :fas:`angle-double-right`.
#. Check `Ysize` for `cod-4002443` and `cod-4313206`. Click :fas:`angle-double-right`.
#. Check `Strain Anisotropy/cod-4002443/S_112`. Click :fas:`angle-double-right`.
#. Check `Strain Anisotropy/cod-4002443/S_004`. Click :fas:`angle-double-right`.
#. Check `Strain Anisotropy/cod-4002443/S_400`. Click :fas:`angle-double-right`.

We will leave the excluded regions unchanged from the previous refinement.

Click on the `Run FP: Workflow` to start the more complex 9 step refinement. Check the new results in the :ref:`Prf Data <prfdat>` and :ref:`Summary <prfdat>` tabs.

.. figure:: ./img/nmc_right_prf.png
         :width: 90%
         :alt: framework
         :align: center

         Evolution of the **c** axis parameter and the fitting of the anisotropic peaks of the `IS22NMCa_f00050.xye` pattern.

As you can see, while the evolution of the **c** axis parameter is similar in both cases (with and without anisotropic strains), the quality of the fit has increased significantly.

Once again, congratulations! Most battery chemistries operate with either of the mechanisms (or a combination of both) that we have introduced here for the cases of LiFePO\ :sub:`4` (phase separating) and NMC111 (solid solution). You are now equiped to do SCIENCE 🧪 with all kinds of materials for energy storage 🔋!

.. note::

   **Extra Exercises**
      * Try to include both berillium windows in the refinement (Hint: Each window has its own zero-shifts)
      * Improve the refinement of the NMC111 sample. You can see that the **003** still is not well refined. This happens at the end of the charge (delithiation). Is there some additional signals coming from another phase (H2, H3, O1, rock-salt, spinel...)?.