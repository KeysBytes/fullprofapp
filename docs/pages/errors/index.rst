Sources of errors
=================

This section gathers some of the lessons learned in the 2nd FullProf School 2023 (2-6 October) when exposing the app to a larger variety of data. These are some possible sources of errors when using `FullProfAPP`.

.. warning::

   * We have found that there are problems when working with very long filenames or in directories that are too far from ``C:`` (meaning that the paths are very long). This should not be a problem anymore because the execution of ``fp2k`` is now done using relative paths. Still this could be a problem if filenames are too long.

      * SOLUTION: Work close to ``C:`` and try to use short filenames.

   * We found that for those that did not have an administrator access to their computers or did not install `FullProf Suite` or `FullProfAPP` as administrators had troubles. The problem lied in some cases that ``fp2k`` could not find in the particular user's session PATH. In other cases ``fp2k`` had no permissions to write files to the computer.

      * SOLUTION: Some people managed to make it work by executing `FullProfAPP` in administrator mode. Otherwise you should contact IT to set up the installation of `FullProf Suite` and `FullProfAPP` correctly. 

   * For some people, doing simulations with the `Simulate Selection <QueryMat>` button did not work, even if they could perform Rietveld refinements in the normal way. This is because when the radiation wavelength is very small, if the IRF file does not explicitly specify contain the :math:`2\theta` range through the **THRG** keyword, a PCR file with a large :math:`2\theta` range will be prepared. In such cases the maximum number of reflections allowed by ``fp2k`` will be reached.

      * SOLUTION: Increase the ``MaxREFLX`` keyword in the ``fullprof.dim`` file in the installation folder, or make sure that the **THRG** keyword is always included in your IRF files matching the range of your experimental data.

   * Sometimes `FullProfAPP` gives an error message telling the user that it failed to read the SUM files when gathering the refinement results and that this is caused by a bad refinement. However, the refienement seemed to be perfectly fine.

      * SOLUTION: We stil did not investigate this bug. Restarting `FullProfAPP` gets rid of this annoying message.

   * The Linux version is unstable when downloading CIF files from databases. Depends on distribution.

   * In Linux you may find an error message of the type ``qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.``.

      * SOLUTION: Install the following library ``sudo apt-get install libxcb-xinerama0``
    