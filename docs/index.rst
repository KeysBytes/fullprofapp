.. FullProfAPP documentation master file, created by
   sphinx-quickstart on Thu Jun 29 10:42:58 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Links:

.. _BIG_MAP: https://www.big-map.eu/
.. _BIG_MAP_APPSTORE: https://big-map.github.io/big-map-registry/
.. _FULLPROF_SUITE: https://www.ill.eu/sites/fullprof/
.. _CIC_ENERGIGUNE: https://cicenergigune.com/en/fullprof-app

FullProfAPP
===========

Welcome to the `FullProfAPP` documentation. Here you will find all the information that you will need to install and
run the software. We hope you find this program useful for your powder pattern refinements!

With this application you will be able to perform various tasks:

* Visualize and process your powder diffraction data, background (peak) detection and editing, refinement results, etc.
* Download crystallographic (CIF) files from the Crystallographic Open Database (COD).
* Simulate powder pattern profiles and manually vary parameters (scale factors, cell parameters, etc.) for preliminary visual checks on your data
* Perform full-profile phase search analysis on the downloaded CIF files using the Rietveld method
* Perform automated Rietveld refinements by selecting flexible workflows

.. toctree::
   :maxdepth: 5
   :glob:
   :caption: CONTENTS

   ./pages/info/*
   ./pages/gui/*
   ./pages/examples/*
   ./pages/errors/*
   ./pages/changelog/*
   