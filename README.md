# FullProfAPP

This repository contains the documents and applications to be developed for the project: **FullProfAPP, a new tool for autonomous, high-throughput powder diffraction data treatment**. The development of the FullProfAPP is carried out in the frame of a collaboration between CIC energiGUNE (Spain; Oier Arcelus, Montse Casas-Cabanas, Javier Carrasco, Marine Reynaud), ICMAB-CSIC (Spain; Oier Arcelus, M. Rosa Palacín, Carlos Frontera, Ashley Black), Institut Laue Langevin (France; Juan Rodríguez Carvajal, Nebil Katcho) and ALBA Synchrotron (Spain; François Fauth).

## Acknowledgement
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 957189. The project is part of BATTERY 2030+, the large-scale European research initiative for inventing the sustainable batteries of the future.

## LICENSE
This software is distributed under the **GNU General Public License version 3 (GPLv3)**. For more information about the licensing terms read the LICENSE file, or go [here](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Documentation
An extensive documentation with installation instructions, software description, and examples is available in  

## Contact
CIC energiGUNE

fullprofapp@cicenergigune.com
