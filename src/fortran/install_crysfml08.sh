#!/bin/sh

installdir='/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/crysfml_python'
currentdir=${pwd}
crysfml08dir='/home/oarcelus/CrysFML2008/build/ifort_release'

cd ${installdir}
ifort -c -fpic -fpp '/home/oarcelus/fullprofapp/src/fortran/src/forpy_mod.f90'
ifort -c -fpic '/home/oarcelus/fullprofapp/src/fortran/src/Simil_mod.f90' -I ${crysfml08dir}/Src08/crysfml_modules/
ifort -c -fpic -fpp '/home/oarcelus/fullprofapp/src/fortran/src/crysfml08_forpy.f90' -I ${crysfml08dir}/Src08/crysfml_modules/

ifort -shared -o crysfml08_forpy.so crysfml08_forpy.o Simil_mod.o forpy_mod.o -L ${crysfml08dir}/LibC/ ${crysfml08dir}/LibC/libcrysfml.a

cd ${currentdir}
