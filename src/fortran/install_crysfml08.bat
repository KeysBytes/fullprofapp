@echo off
set installdir=C:\Users\oarcelus\fullprofapp\.venv\Lib\site-packages\crysfml_python
set CRYSFML08_DIR=C:\CrysFML2008\ifort_release\include
set CRYSFML08_LIB=C:\CrysFML2008\ifort_release\lib\libCrysFML08.a
set LIBPYTHON=C:\Users\oarcelus\AppData\Local\Programs\Python\Python310\libs\python310.lib

set currentdir=%cd%
cd %installdir%
ifort.exe  /c /fpp "C:\Users\oarcelus\fullprofapp\src\fortran\src\forpy_mod.f90"
echo FORPY_MOD_INSTALLED
ifort.exe  /c "C:\Users\oarcelus\fullprofapp\src\fortran\src\Simil_mod.f90" /I %CRYSFML08_DIR%
echo SIMIL_MOD_INSTALLED
ifort.exe  /c /fpp "C:\Users\oarcelus\fullprofapp\src\fortran\src\crysfml08_forpy.f90" /I %CRYSFML08_DIR%
echo CRYSFML08_MOD_INSTALLED
link forpy_mod.obj Simil_mod.obj crysfml08_forpy.obj /out:"crysfml08_forpy.dll" /libpath:%CRYSFML08_DIR% /dll %LIBPYTHON% %CRYSFML08_LIB%
del "crysfml08_forpy.pyd"
rename "crysfml08_forpy.dll" "crysfml08_forpy.pyd"
cd %currentdir%
pause