@echo off
set installdir=C:\Users\oarcelus\fullprofapp\.venv\Lib\site-packages\crysfml_python
set CRYSFML_DIR=C:\crysfml\build\ifort_win_fpm\
set CRYSFML_LIB=C:\crysfml\build\ifort_win_fpm\CrysFML\libCrysFML.a
set LIBPYTHON=C:\Users\oarcelus\AppData\Local\Programs\Python\Python310\libs\python310.lib

set currentdir=%cd%
cd %installdir%
ifort.exe  /c /fpp "C:\Users\oarcelus\fullprofapp\src\fortran\src\forpy_mod.f90"
ifort.exe  /c /fpp "C:\Users\oarcelus\fullprofapp\src\fortran\src\crysfml_forpy.f90" /I %CRYSFML_DIR%
link forpy_mod.obj crysfml_forpy.obj /out:"crysfml_forpy.dll" /libpath:%CRYSFML_DIR% /dll %LIBPYTHON% %CRYSFML_LIB%
del "crysfml_forpy.pyd"
rename "crysfml_forpy.dll" "crysfml_forpy.pyd"
cd %currentdir%
pause
