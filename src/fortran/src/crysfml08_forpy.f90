module crysfml08_forpy
    use forpy_mod
    use iso_c_binding
   	use Simil_Powder_Pattern,  only : Similarity_of_Powder_Patterns
    use CFML_GlobalDeps

    implicit none

    type(PythonModule), save :: mod_def
    type(PythonMethodTable), save :: method_table

    contains

    function PyInit_crysfml08_forpy() bind(c, name="PyInit_crysfml08_forpy") result(m)
        !!DEC$ ATTRIBUTES DLLEXPORT :: PyInit_crysfml08_forpy
        !This line only for Ifort compilation on Windows (delete if compiling in linux)
        type(c_ptr) :: m
        m = init()
    end function PyInit_crysfml08_forpy

    function init() result(m)
        type(c_ptr) :: m
        integer :: ierror

        ierror = forpy_initialize()

        call method_table%init(1)

        call method_table%add_method("simil_pow", &
                                     "Calculate DeGelder similarity between calculated patterns of two crystall structures", &
                                     METH_VARARGS, &
                                     c_funloc(simil_pow))

        m = mod_def%init("crysfml08_forpy", "CrysFML08 routines to be used in python", method_table)

    end function init

    function simil_pow(self_ptr, args_ptr) result(r) bind(c)

      ! Calculate HKL reflections given structural information
      type(c_ptr), value :: self_ptr
      type(c_ptr), value :: args_ptr
      type(c_ptr) :: r

      !Declare variables
		  integer                                           :: i, ierror, size1, size2
      character(len=:)      , dimension(:), allocatable :: filcod
		  character(len=:)                    , allocatable :: file1c, file2c
		  real                  , dimension(3)              :: var
		  real                                              :: hofmann, degelder
		
      !Forpy types
      type(object) :: item, file1, file2
      type(tuple) :: args, ret
		  type(list) :: py_files, py_vars

      call clear_error()
      ierror = 0

      call unsafe_cast_from_c_ptr(args, args_ptr)

      ierror = args%getitem(item, 0)
      ierror = cast(py_files, item)
      ierror = args%getitem(item, 1)
      ierror = cast(py_vars, item)
		
		  ! Read CIF filenames
	    ierror = py_files%getitem(file1, 0)
		  ierror = py_files%getitem(file2, 1)
		  ierror = cast(file1c, file1)
		  ierror = cast(file2c, file2)
		
		  ! Allocate filename object
		  size1 = len(file1c)
		  size2 = len(file2c)
		  allocate(character(len=max(size1, size2)) :: filcod(2))
		  filcod(1) = file1c
		  filcod(2) = file2c
		
		  ! Read vars
		  ierror = py_vars%getitem(var(1), 0)
		  ierror = py_vars%getitem(var(2), 1)
		  ierror = py_vars%getitem(var(3), 2)

		  call Similarity_of_Powder_Patterns(filcod, var, hofmann, degelder)
      ierror = err_cfml%ierr
      if (ierror == 0) then
		ierror = tuple_create(ret, 3)
        ierror = ret%setitem(0, 0)
        ierror = ret%setitem(1, trim(err_cfml%msg))
        ierror = ret%setitem(2, degelder)
		  else
		    ierror = tuple_create(ret, 2)
        ierror = ret%setitem(0, err_cfml%ierr)
        ierror = ret%setitem(1, trim(err_cfml%msg))
      end if

		  r = ret%get_c_ptr()
        
    end function simil_pow

end module crysfml08_forpy
