 !!----
 !!---- Module:  Simil_Powder_Patterns
 !!----
 !!---- Simple method for comparing powder patterns by reading CIF files
 !!----
 !!---- Default Powder Diffraction Pattern
 !!----  stlmax=0.4; PPC%Thmin=2.00; PPC%U=0.0002; PPC%V=-0.0002; PPC%W=0.012; PPC%LAMBDA=1.54056; PPC%X=0.0015
 !!---- PPC%step=0.05; PPC%Ls=1900.0;  PPC%bkg=0.0; PPC%job=0
 !!----
 Module Simil_Powder_Pattern
    !---- Use Modules ----!
    use CFML_GlobalDeps,       only: to_Deg, cp, Err_CFML
    use CFML_Maths,            only: locate
    use CFML_Atoms,            only: AtList_Type,Write_Atom_List
    use CFML_Reflections,      only: RefList_Type, Srefl_type, Initialize_RefList, Gener_Reflections
    use CFML_DiffPatt,         only: DiffPat_Type, DiffPat_E_Type, allocate_pattern
    use CFML_Profiles,         only: PseudoVoigt
    use CFML_Metrics,          only: Cell_G_type
    Use CFML_gSpaceGroups,     only: SpG_Type,Write_SpaceGroup_Info
    Use CFML_Structure_Factors,only: Structure_Factors,Init_Structure_Factors, &
                                     SF_init_opMatTr, SF_Clear_init_symOP
     use CFML_IOForm,          only: Read_Xtal_Structure
    !---- Variables ----!
    implicit none

    private

    public  :: Similarity_of_Powder_Patterns,calc_powder_pattern, PowPat_Similarity, Write_PRF, PowPat_WCC
    private :: TCH

    Type, public :: PowPat_CW_Conditions
       character(len=140) :: title
       integer            :: job
       real(kind=cp)      :: Lambda, U, V, W, X, Ls
       real(kind=cp)      :: Thmin, Thmax, step
       real(kind=cp)      :: scalef,bkg
    End Type PowPat_CW_Conditions

 Contains
    !!----
    !!---- Pure Subroutine TCH(Hg,Hl,Fwhm,Eta)
    !!----
    !!---- Calculation of eta and FWHM of the pV-function for the
    !!---- T-C-H representation.
    !!----
    !!
    Pure Subroutine TCH(Hg,Hl,Fwhm,Eta)
       !---- Arguments ----!
       real, intent(in)  :: hg
       real, intent(in)  :: hl
       real, intent(out) :: fwhm
       real, intent(out) :: eta

       !---- Variables ----!
       real, parameter :: o1= 2.69269, o2=2.42843, o3=4.47163, o4= 0.07842
       real, parameter :: e1= 1.36603, e2=0.47719, e3=0.11116
       real            :: ctl, tlr

       ! There is no exception handling because it is supposed to be
       ! perfomed before calling TCH
       ctl=hg**5.0+o1*hg**4.0*hl+o2*hg**3.0*hl**2.0+o3*hg**2.0*hl**3.0+  &
           o4*hg*hl**4.0+hl**5.0
       fwhm=ctl**0.2
       tlr = hl/fwhm
       eta = max(1.0e-06, e1*tlr-e2*tlr*tlr+e3*tlr**3.0)  !eta
    End Subroutine TCH

    Subroutine Calc_Powder_Pattern(Ppc,Hkl,Pat)
       !---- Argument ----!
       Type(PowPat_CW_Conditions),     intent(in)  :: PPC
       type(RefList_Type),             intent(in)  :: hkl
       Type(DiffPat_E_Type),           intent(out) :: Pat

       !--- Local Variables ----!
       integer :: i,j,npts,i1,i2
       real    :: Intens,Bragg,Hl,Hg, ss,cs,tt,th1,th2,LorentzF, Y,eta,fwhm,chw

       npts=(PPC%Thmax-PPC%Thmin)/PPC%step + 1.02
       call allocate_pattern(Pat,npts) !Initializes the intensities to zero
       Pat%Title=adjustl(Trim(PPC%title))
       i=len_trim(Pat%Title)
       write(unit=Pat%Title(i+2:),fmt="(a,f7.4,f7.1)") " => lambda,Lsize: ", &
                  PPC%Lambda,PPC%Ls

       Pat%ScatVar="2-Theta"
       Pat%instr="Calculated Pattern"
       Pat%xmin= PPC%Thmin
       Pat%xmax= PPC%Thmax
       Pat%ymin= 0.0
       Pat%ymax=0.0
       Pat%scal=1.0
       Pat%monitor=0.0
       Pat%step=PPC%step
       Pat%Tsample=300.0
       Pat%Tset=300.0
       Pat%npts=npts
       Pat%ct_step=.true.
       Pat%wave=[PPC%Lambda,PPC%Lambda,0.0,0.0,0.0]
       chw=15.0
       do i=1,npts
          Pat%x(i)=Pat%xmin+real(i-1)*Pat%step
       end do
       Y= to_deg*PPC%Lambda/PPC%Ls
       Select Type(ref => hkl%ref)
        type is (Srefl_type)
         do i=1,hkl%nref
            ss=PPC%Lambda*ref(i)%S
            cs=sqrt(abs(1.0-ss*ss))
            tt=ss/cs
            LorentzF=0.5/(ss*ss*cs)
            Bragg=2.0*asind(ss)
            HG=sqrt(tt*( PPC%U*tt + PPC%V) + PPC%W)
            HL=PPC%X*tt + Y/cs
            call TCH(hg,hl,fwhm,eta)
            Select Case(nint(eta*10.0))
               Case(:2)
                  chw=25.0
               case(3:5)
                  chw=45.0
               case(6:7)
                  chw=60.0
               case(8:)
                  chw=90.0
            End Select

            th1=Bragg-chw*fwhm
            th2=Bragg+chw*fwhm
            i1=Locate(Pat%x,th1,npts)
            i2=Locate(Pat%x,th2,npts)
            i1=max(i1,1)
            i2=min(i2,npts)
            Intens= LorentzF *ref(i)%mult * ref(i)%Fc**2 * PPC%Scalef
            do j=i1,i2
               Pat%ycalc(j)=Pat%ycalc(j)+ PseudoVoigt( Pat%x(j)-Bragg, [fwhm,eta ] ) * Intens
            end do
         end do
         Pat%ymax=maxval(Pat%ycalc)
         Pat%ymin=minval(Pat%ycalc)
       End Select

    End Subroutine Calc_Powder_Pattern

    Function PowPat_Similarity(Pat1,Pat2) result(similarity)
      Type(DiffPat_E_Type), intent(in) :: Pat1,Pat2
      real(kind=cp) :: similarity
      integer :: i,j,n
      real(kind=cp) :: sum1,sum2,sim
      real(kind=cp), dimension(Pat1%npts) :: integ1,integ2
      n=Pat1%npts
      sum1=sum(Pat1%ycalc(1:n))
      sum2=sum(Pat2%ycalc(1:n))
      integ1=0.0_cp
      integ2=0.0_cp
      do i=1,n
        integ1(i) = sum(Pat1%ycalc(1:i))/sum1
        integ2(i) = sum(Pat2%ycalc(1:i))/sum2
      end do
      similarity= 100.0*(1.0 - sum(abs(integ1-integ2))/real(n))
    End Function PowPat_Similarity

    ! Calculation of the Cross-correlation function
    Function cross_corr(f,g) result(fg) !
      real(kind=cp), dimension(:), intent(in) :: f
      real(kind=cp), dimension(:), intent(in) :: g

      real(kind=cp), dimension(:), allocatable  :: fg,fm,gm
      integer :: i ,j, nf, ng, n
      nf=size(f); ng=size(g)
      n= min(ng,nf)
      allocate(fg(-n+1:n-1),fm(-n+1:n-1),gm(-n+1:n-1))
      fm(-n+1:-1)=0.0_cp; fm(0:n-1)=1000.0*f(1:n)/sum(f(1:n)) !Normalization by 1.0/area x 1000
      gm(-n+1:-1)=0.0_cp; gm(0:n-1)=1000.0*g(1:n)/sum(g(1:n))

      do i=-n+1,n-1
        fg(i) = 0.0_cp
        do j=-n+1,n-1
           if(j+i < -n+1) cycle
           if(j+i > n-1) cycle
           fg(i)=fg(i) + fm(j) * gm(j+i)
        end do
      end do
    End Function cross_corr

    ! Similarity between two powder patterns based on weighted cross-correlation
    ! function between two patterns. The similarity is between 0 and 100 and it is
    ! obtained as a single value after weigthed integration with a triangular function of base -L,L.
    !
    Function PowPat_WCC(Pat1,Pat2,L,prt) result(similarity)
      Type(DiffPat_E_Type), intent(in) :: Pat1,Pat2
      integer,              intent(in) :: L
      logical, optional,    intent(in) :: prt
      real(kind=cp) :: similarity
      integer :: i,j,n,lc
      real(kind=cp) :: sum11,sum22,sum12, sm, w, ttheta
      real(kind=cp), dimension(-Pat1%npts+1:Pat1%npts-1) :: c12,c11,c22

      c11=cross_corr(Pat1%ycalc,Pat1%ycalc)
      c22=cross_corr(Pat2%ycalc,Pat2%ycalc)
      c12=cross_corr(Pat1%ycalc,Pat2%ycalc)
      sum11=0.0 ; sum22=0.0 ; sum12=0.0
      sm=1.0/real(L)
      do i=-L,L
        w=1.0 - abs(real(i)*sm)  !Triangular weight
        sum11=sum11+ w * c11(i)
        sum22=sum22+ w * c22(i)
        sum12=sum12+ w * c12(i)
      end do
      similarity= 100.0*sum12/sqrt(sum11*sum22)

      if(present(prt)) then
        if(prt) then
          n=Pat1%npts
          open(newunit=lc,file="correlations.xys", status="replace",action="write")
          write(lc,"(a)") "! Multi-column correlations  c12, c11, c22"
          write(lc,"(a)") "! -2theta_max  -  2theta_max"
          write(lc,"(a,2f12.5)") "!", (-n+1)*Pat1%step, (n-1)*Pat1%step
          write(lc,"(a)") "!"
          write(lc,"(a)") "!   Testing correlation functions"
          write(lc,"(a)") "!"
          do i=-n+1,n-1
            ttheta= i*Pat1%step
            write(lc,"(4f18.5)") ttheta,c12(i),c11(i),c22(i)
          end do
          close(unit=lc)
        end if
      end if

    End Function PowPat_WCC

    Subroutine Write_PRF(fileprf,lambda,Pat,hkl)
       character(len=*),        intent(in)  :: fileprf
       real(kind=cp),           intent(in)  :: lambda
       Type(DiffPat_E_Type),    intent(in)  :: Pat
       Type(RefList_Type),      intent(in)  :: hkl

       integer :: i,j,i_prf
       character(len=*),parameter :: tb=char(9)
       character (len=50) :: forma1
       real :: ymax,scl

       open(newunit=i_prf,file=trim(fileprf),status="replace",action="write")
       ymax=Pat%ymax
       scl=1.0
       do
         if(ymax < 1.0e6) exit !on exit we have the appropriate value of scl
         scl=scl*0.1
         ymax=ymax*scl
       end do
       if(ymax < 100.0) then
        forma1='(f12.4,4(a,f8.4))'
       else if(ymax < 1000.0) then
        forma1='(f12.4,4(a,f8.3))'
       else if(ymax < 10000.0) then
        forma1='(f12.4,4(a,f8.2))'
       else if(ymax < 100000.0) then
        forma1='(f12.4,4(a,f8.1))'
       else
        forma1='(f12.4,4(a,f8.0))'
       end if

       !write(*,"(3f12.4,a,f12.4,a)") ymaxini, ymax, scl, " Pat%ymax * scl:",ymaxini*scl, " Format: "//trim(forma1)
       write(i_prf,'(a)') trim(Pat%Title)  !//"  CELL:"
                                !  N_phases, N_points, Lamda1,Lambda2,zero,shift1,shif2,Ixunit
       write(i_prf,'(I3,I7,5f12.5,i5)')1,Pat%npts,lambda,lambda,0.0,0.0,0.0,0
       write(i_prf,'(17i6)')  hkl%Nref, 0 , 0
       write(i_prf,'(15a)')' 2Theta',tb,'Yobs',tb,'Ycal',tb,  &
        'Yobs-Ycal',tb,'Backg',tb,'Posr',tb,'(hkl)',tb,'K'
       !dd=(y(i,n_pat)-yc(i,n_pat))*scl
       do  i=1,Pat%npts
         write(i_prf,forma1) Pat%x(i),tb,Pat%ycalc(i)*scl,tb,Pat%ycalc(i)*scl,tb, -ymax/4.0,tb,0.0
       end do
       !Writing reflections
       do j=1,hkl%Nref
         !iposr=-(k-1)*ideltr
           write(i_prf,'(f12.4,9a,i8,a,3i3,a,2i3)')  2.0*asind(hkl%Ref(j)%s*Lambda), &
               tb,'        ',tb,'        ',tb,'        ',  &
               tb,'        ',tb,0, tb//'(',hkl%Ref(j)%h,')'//tb,hkl%Ref(j)%imag,1
       end do
      close(unit=i_prf)
    End Subroutine Write_PRF

    Subroutine Similarity_of_Powder_Patterns(filcod,var,simil1,simil2)
     character(len=*), dimension(2), intent(in out) :: filcod
     real(kind=cp),    dimension(3), intent(in)     :: var
     real(kind=cp),                 intent(out)     :: simil1,simil2
     !---- Variables ----!
     real(kind=cp)          :: stlmax
     character(len=132)     :: powfile

     class(Cell_G_Type), allocatable   :: cell
     class(SPG_Type), allocatable      :: SpG
     Type(AtList_Type)                 :: A
     Type(RefList_Type)                :: hkl
     Type(DiffPat_E_Type),dimension(2) :: Pat
     Type(PowPat_CW_Conditions)        :: PPC

     integer   :: ip, i,ier,lp, Lwidth
     Logical   :: esta


     stlmax=0.4; PPC%Thmin=2.00; PPC%U=0.0002; PPC%V=-0.0002; PPC%W=0.012; PPC%LAMBDA=1.54056; PPC%X=0.0015
     PPC%step=0.05; PPC%bkg=0.0; PPC%job=0
     PPC%Ls=var(1)
     stlmax=var(2)
     Lwidth=nint(var(3))
     do ip=1,2
        write(unit=*,fmt=*) " "
        i=index(filcod(ip),".cif")
        if(i /= 0) then
          filcod(ip)=filcod(ip)(1:i-1)
        end if
        inquire(file=trim(filcod(ip))//".cif",exist=esta)
        if (esta) then
          call Read_Xtal_Structure(trim(filcod(ip))//".cif",Cell,SpG,A)
          call SF_Clear_init_symOP()
          call SF_init_opMatTr(SpG)
        else
          write(*,*) " The file "//trim(filcod(ip))//".cif"//" does not exist!"
          stop
        end if

        if (err_CFML%Ierr == 0) then
          ! Calculate a default Powder Diffraction Pattern
          PPC%Title="Powder Pattern of Structure provided in: "//trim(filcod(ip))//".cif"
          PPC%Thmax= int(2.0*asind(stlmax*1.54056))
          powfile=trim(filcod(ip))//".xys"

          PPC%title=Trim(PPC%title)//"; X-RAYS: "

          call Gener_Reflections(Cell,0.0,stlmax,hkl,SpG,Unique=.true.,Friedel=.true.,Ref_typ='srefl')
          call Init_Structure_Factors(hkl,A,Spg,mode="XRA",lambda=PPC%lambda)
          call Structure_Factors(hkl,A,SpG,mode="XRA",lambda=PPC%lambda)

          if (err_CFML%Ierr == 0) then
            PPC%Scalef=cell%RVol !Scaled by the reciprocal cell volume
            call Calc_powder_pattern(PPC,hkl,Pat(ip))
          end if
        end if
     end do
     if (err_CFML%Ierr == 0) then
       simil1=PowPat_Similarity(Pat(1),Pat(2))
       simil2=PowPat_WCC(Pat(1),Pat(2),Lwidth)
     else
       simil1=0.0
       simil2=0.0
      end if
    End Subroutine Similarity_of_Powder_Patterns

  End Module Simil_Powder_Pattern

