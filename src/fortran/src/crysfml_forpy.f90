module crysfml_forpy
    use forpy_mod
    use iso_c_binding
    use CFML_BVS_Energy_Calc, only: Atoms_Conf_List_Type, Cost_BVS, Allocate_Atoms_Conf_List, Deallocate_Atoms_Conf_List
    use CFML_Crystal_Metrics, only: Err_Crys, Crystal_Cell_Type, Set_Crystal_Cell, Get_Cryst_Family, Change_Setting_Cell
    use CFML_Crystallographic_Symmetry, only: Space_Group_Type, NS_Space_Group_Type, Set_SpaceGroup, &
                                              Setting_Change, Err_Symm
    use CFML_GlobalDeps, only: cp
    use CFML_Reflections_Utilities, only: HKL_Gen, Get_MaxNumRef, Reflect_Type, HKL_Equiv_List
    use CFML_IO_Formats, only: Read_CIF_Atom, Read_CIF_Cell, Err_Form
    use CFML_Atom_TypeDef, only: Atom_List_Type, Multi, Atom_Type, Atoms_Cell_Type, Allocate_Atoms_Cell
	use CFML_Scattering_Chemical_Tables, only: Get_Ionic_Radius
	use CFML_Geometry_Calc, only: Coord_Info, Set_TDist_Coordination, Allocate_Coordination_Type, Calc_Dist_Angle_Sigma

    implicit none

    type(PythonModule), save :: mod_def
    type(PythonMethodTable), save :: method_table

    type(Crystal_Cell_Type) :: cell
    type(Space_Group_Type) :: spg

    contains

    function PyInit_crysfml_forpy() bind(c, name="PyInit_crysfml_forpy") result(m)
        !!DEC$ ATTRIBUTES DLLEXPORT :: PyInit_crysfml_forpy
        !This line only for Ifort compilation on Windows (delete if compiling in linux)
        type(c_ptr) :: m
        m = init()
    end function PyInit_crysfml_forpy

    function init() result(m)
        type(c_ptr) :: m
        integer :: ierror

        ierror = forpy_initialize()

        call method_table%init(8)

        call method_table%add_method("gen_hkl", &
                                     "Generate HKL reflections", &
                                     METH_VARARGS, &
                                     c_funloc(gen_hkl))

        call method_table%add_method("get_equiv_hkl", &
                                     "Generate all the equivalent reflections of a give HKL", &
                                     METH_VARARGS, &
                                     c_funloc(get_equiv_hkl))

        call method_table%add_method("get_crystal_system", &
                                     "Get Crystal System from H-M symbol", &
                                     METH_VARARGS, &
                                     c_funloc(get_crystal_system))

        call method_table%add_method("get_crystal_family", &
                                     "Get Crystal Family from Cell Parameters", &
                                     METH_VARARGS, &
                                     c_funloc(get_crystal_family))
									 
		call method_table%add_method("get_crystal_volume", &
                                     "Get Cell Volume from Cell Parameters", &
                                     METH_VARARGS, &
                                     c_funloc(get_crystal_volume))

        call method_table%add_method("transform_setting_R_to_H", &
                                     "Transform Rhomborhedral to Hexagonal setting on Trigonal systems", &
                                     METH_VARARGS, &
                                     c_funloc(transform_setting_R_to_H))

        call method_table%add_method("calculate_gii", &
                                     "Calculate the global stability index of a crystal structure", &
                                     METH_VARARGS, &
                                     c_funloc(calculate_gii))
		
		call method_table%add_method("get_laue_num", &
                                     "Get the laue number for size and strain models in fullprof", &
                                     METH_VARARGS, &
                                     c_funloc(get_laue_num))

        m = mod_def%init("crysfml_forpy", "CrysFML routines to be used in python", method_table)

    end function init

    function gen_hkl(self_ptr, args_ptr) result(r) bind(c)

        ! Calculate HKL reflections given structural information
        type(c_ptr), value :: self_ptr
        type(c_ptr), value :: args_ptr
        type(c_ptr) :: r

        !Parameters
        logical, parameter :: friedel = .true.

        !Declare variables
        integer :: ierror, nref, n, ierr
        real(kind=cp) :: s_min, s_max
        real, dimension(:), pointer :: p_cell
        character(len=:), allocatable :: spg_str
        integer, dimension(:, :), allocatable :: hkl_vals
        real, dimension(:), allocatable :: s_vals

        !Forpy types
        type(object) :: item
        type(tuple) :: args, ret
        type(ndarray) :: py_cell, py_hkl, py_s
        type(Reflect_Type), dimension(:), allocatable :: hkl

        call unsafe_cast_from_c_ptr(args, args_ptr)

        ierror = args%getitem(item, 0)
        ierror = cast(py_cell, item)
        ierror = py_cell%get_data(p_cell)
        ierror = args%getitem(item, 1)
        ierror = cast(spg_str, item)
        ierror = args%getitem(item, 2)
        ierror = cast(s_min, item)
        ierror = args%getitem(item, 3)
        ierror = cast(s_max, item)

        call Set_Crystal_Cell(p_cell(1:3), p_cell(4:6), cell)
        call Set_Spacegroup(spg_str, spg)

        nref = Get_MaxNumRef(s_max, cell%CellVol, s_min)
        allocate(hkl(nref), stat=ierr)
        if (ierr /= 0) then
            ierror = tuple_create(ret,1)
            ierror = ret%setitem(0,ierr)
            r = ret%get_c_ptr()
            return
        end if

        call HKL_Gen(cell, spg, friedel, s_min, s_max, nref, hkl)

        allocate(hkl_vals(3, nref), s_vals(nref), stat=ierr)
        if (ierr /= 0) then
            ierror = tuple_create(ret,1)
            ierror = ret%setitem(0,ierr)
            r = ret%get_c_ptr()
            return
        end if

        do n = 1, nref
            hkl_vals(:, n) = hkl(n)%h(:)
            s_vals(n) = hkl(n)%s
        end do

        !Create NumPy arrays
        ierror = ndarray_create(py_hkl, hkl_vals)
        ierror = ndarray_create(py_s, s_vals)

        !Return arrays
        ierror = tuple_create(ret, 2)
        ierror = ret%setitem(0, py_hkl)
        ierror = ret%setitem(1, py_s)
        r = ret%get_c_ptr()

    end function gen_hkl

    function get_equiv_hkl(self_ptr, args_ptr) result(r) bind(c)

        type(c_ptr), value :: self_ptr
        type(c_ptr), value :: args_ptr
        type(c_ptr) :: r

        ! Variables
        integer :: ierror, mul, ierr
        character(len=:), allocatable :: spg_str
        integer, dimension(:), pointer :: hkl
        integer, dimension(:,:), allocatable :: hkl_list

        ! Forpy types
        type(object) :: item
        type(tuple) :: args, ret
        type(ndarray) :: py_hkl, py_hkl_list

        call unsafe_cast_from_c_ptr(args, args_ptr)

        ierror = args%getitem(item, 0)
        ierror = cast(py_hkl, item)
        ierror = py_hkl%get_data(hkl)
        ierror = args%getitem(item, 1)
        ierror = cast(spg_str, item)

        call Set_Spacegroup(spg_str, spg)

        allocate(hkl_list(3, spg%numops*2), stat=ierr)
        if (ierr /= 0) then
            ierror = tuple_create(ret,1)
            ierror = ret%setitem(0,ierr)
            r = ret%get_c_ptr()
            return
        end if

        call HKL_Equiv_List(hkl(1:3), spg, .true., mul, hkl_list)

        !Create NumPy arrays
        ierror = ndarray_create(py_hkl_list, hkl_list)

        !Return arrays
        ierror = tuple_create(ret, 2)
        ierror = ret%setitem(0, py_hkl_list)
        ierror = ret%setitem(1, mul)
        r = ret%get_c_ptr()

    end function get_equiv_hkl

    function get_crystal_system(self_ptr, args_ptr) result(r) bind(c)
        ! Recover crystal system from H-M symbol
        type(c_ptr), value :: self_ptr
        type(c_ptr), value :: args_ptr
        type(c_ptr) :: r

        ! Variables
        integer :: ierror
        character(len=:), allocatable :: spg_str

        ! Forpy types
        type(object) :: item
        type(tuple) :: args, ret

        call unsafe_cast_from_c_ptr(args, args_ptr)

        ierror = args%getitem(item, 0)
        ierror = cast(spg_str, item)

        call Set_Spacegroup(spg_str, spg)

        ! Return Crystal System
        ierror = tuple_create(ret, 1)
        ierror = ret%setitem(0, spg%CrystalSys)
        r = ret%get_c_ptr()

    end function get_crystal_system

    function get_crystal_family(self_ptr, args_ptr) result(r) bind(c)
        ! Recover crystal family from cell parameters
        type(c_ptr), value :: self_ptr
        type(c_ptr), value :: args_ptr
        type(c_ptr) :: r

        ! Variables
        integer :: ierror
        real, dimension(:), pointer :: p_cell
        character(len=12) :: family, system
        character(len=1) :: symbol

        ! Forpy types
        type(object) :: item
        type(tuple) :: args, ret
        type(ndarray) :: py_cell

        call unsafe_cast_from_c_ptr(args, args_ptr)

        ierror = args%getitem(item, 0)
        ierror = cast(py_cell, item)
        ierror = py_cell%get_data(p_cell)

        call Set_Crystal_Cell(p_cell(1:3), p_cell(4:6), cell)
        call Get_Cryst_Family(cell, family, symbol, system)

        ! Return Crystal Info
        ierror = tuple_create(ret, 3)
        ierror = ret%setitem(0, family)
        ierror = ret%setitem(1, symbol)
        ierror = ret%setitem(2, system)
        r = ret%get_c_ptr()

    end function get_crystal_family
	
	function get_crystal_volume(self_ptr, args_ptr) result(r) bind(c)
        ! Recover crystal family from cell parameters
        type(c_ptr), value :: self_ptr
        type(c_ptr), value :: args_ptr
        type(c_ptr) :: r
		
		! Variables
        integer :: ierror
        real, dimension(:), pointer :: p_cell

        ! Forpy types
        type(object) :: item
        type(tuple) :: args, ret
        type(ndarray) :: py_cell

        call unsafe_cast_from_c_ptr(args, args_ptr)

        ierror = args%getitem(item, 0)
        ierror = cast(py_cell, item)
        ierror = py_cell%get_data(p_cell)

        call Set_Crystal_Cell(p_cell(1:3), p_cell(4:6), cell)
		
		! Return Crystal Info
        ierror = tuple_create(ret, 1)
        ierror = ret%setitem(0, cell%cellvol)
        r = ret%get_c_ptr()
		
	end function get_crystal_volume
		
    function transform_setting_R_to_H(self_ptr, args_ptr) result(r) bind(c)
        ! Transforms setting of Trigonal crystal systems, from Rhombrohedral to Hexagonal
        ! Here sometimes some Trigonal Space Group H-M symbols are defined with :R or :H modifiers
        ! So this should be only applied for the :R modifiers
        type(c_ptr), value :: self_ptr
        type(c_ptr), value :: args_ptr
        type(c_ptr) :: r

         ! Parameters
        real(kind=cp), dimension(3, 3), parameter :: transform = reshape((/-1.0,1.0,1.0,1.0,0.0,1.0,0.0,-1.0,1.0/), shape(transform))
        real(kind=cp), dimension(3), parameter :: orig = (/0.0,0.0,0.0/)

        ! Variables
        integer :: ierror, ierr
        real, dimension(:), pointer :: p_cell
        character(len=:), allocatable :: spg_str
        type(NS_Space_Group_Type) :: ns_spg
        type(Crystal_Cell_Type) :: ncell

        ! Forpy types
        type(object) :: item
        type(tuple) :: args, ret
        type(ndarray) :: py_cell, py_ncell, py_nang

        call unsafe_cast_from_c_ptr(args, args_ptr)

        ierror = args%getitem(item, 0)
        ierror = cast(spg_str, item)
        ierror = args%getitem(item, 1)
        ierror = cast(py_cell, item)
        ierror = py_cell%get_data(p_cell)

        call Set_Spacegroup(spg_str, spg)

        if (spg%CrystalSys /= 'Trigonal') then
            ierror = tuple_create(ret,1)
            ierr = 0
            ierror = ret%setitem(0,ierr)
            r = ret%get_c_ptr()
            return
        end if

        call Setting_Change(transform, orig, spg, ns_spg)
        call Set_Crystal_Cell(p_cell(1:3), p_cell(4:6), cell)
        call Change_Setting_Cell(cell, transform, ncell)

        ! Create new cell arrays
        ierror = ndarray_create(py_ncell, ncell%cell)
        ierror = ndarray_create(py_nang, ncell%ang)

        ! Return Transformation
        ierror = tuple_create(ret, 2)
        ierror = ret%setitem(0, py_ncell)
        ierror = ret%setitem(1, py_nang)
        r = ret%get_c_ptr()

    end function transform_setting_R_to_H

    function calculate_gii(self_ptr, args_ptr) result(r) bind(c)
        ! Calculates the global instability index of a crystal structure
        type(c_ptr), value :: self_ptr
        type(c_ptr), value :: args_ptr
        type(c_ptr) :: r

        ! Variables
        integer :: ierror, ierr, ibeg, iend, maxsize, nat, i, j, icm, lenght, nuq, nan, ncat, nchar, counter, valence, maxcor
        character(len=:), allocatable :: line, symbol, elem, spg_str
        character(len=:), dimension(:), allocatable :: files
		character(len=4), dimension(:), allocatable :: species
		real(kind=cp), dimension(:), allocatable :: radii
		real(kind=cp) :: radius, gii
		real(kind=cp), dimension(6) :: cella, std_cella
		
        type(Atom_List_Type) :: atlist
        type(Atoms_Conf_List_Type) :: atconf
		type(Atoms_Cell_Type) :: atcell

        ! Forpy types
        type(object) :: item, lineobj
        type(tuple) :: args, ret
        type(list) :: py_file

        call unsafe_cast_from_c_ptr(args, args_ptr)

        ierror = args%getitem(item, 0) ! Read CIF string
        ierror = cast(py_file, item)   ! Read CIF string
        ierror = args%getitem(item, 1) ! Read line beggining
        ierror = cast(ibeg, item)      ! Read line beggining
        ierror = args%getitem(item, 2) ! Read line end
        ierror = cast(iend, item)      ! Read line end
        ierror = args%getitem(item, 3) ! Read max size of string
        ierror = cast(maxsize, item)   ! Read max size of string
		ierror = args%getitem(item, 4) ! Read spacegroup string
		ierror = cast(spg_str, item)   ! Read spacegroup string
		
		! Error codes:
		! 0: Failed to read atoms from File
		! 1: Failed to set spacegroup
		! 2: Failed to set crystal cell
		! 3: CIF file does not contain information about valences (run Formal Charges?)
		! 4: Names of species are not common
		! 5: Error calculating radius
		
		! Set spacegroup
		call Set_Spacegroup(spg_str, spg)

		if (Err_Symm) then
			ierror = tuple_create(ret,1)
			ierr = 0
			ierror = ret%setitem(1,ierr)
			r = ret%get_c_ptr()
			return
		end if

		! Read python list of strings to fortran array of strings
        ierror = py_file%len(lenght)
        allocate(character(len=maxsize) :: files(lenght))
        do i = 0, lenght - 1
            ierror = py_file%getitem(lineobj, i)
            ierror = cast(line, lineobj)
            files(i + 1) = line
            deallocate(line)
            call lineobj%destroy
        end do

        ! Read Relevant Information from the CIF files
        call Read_CIF_Atom(files, ibeg, iend, nat, atlist)
		if (Err_Form) then
			ierror = tuple_create(ret,1)
			ierr = 0
			ierror = ret%setitem(0,ierr)
			r = ret%get_c_ptr()
			return
		end if
		ibeg = 1
		call Read_CIF_Cell(files, ibeg, iend, cella, std_cella)
		call Set_Crystal_Cell(cella(1:3), cella(4:6), cell)
		if (Err_Crys) then
			ierror = tuple_create(ret,1)
			ierr = 0
			ierror = ret%setitem(2,ierr)
			r = ret%get_c_ptr()
			return
		end if
		
		! Calculate multiplicities and atoms in the cell
		call Allocate_Atoms_Cell(atlist%natoms, spg%multip, 30.0, atcell)
		call Multi(0, .false., .true., spg, atlist, atcell) ! Maybe its already calculated above

		! Try to get the list of unique species
		allocate(species(atlist%natoms))
		species(1) = atlist%atom(1)%sfacsymb
		nuq = 1
        do i = 2, atlist%natoms
			counter = 0
			do j = 1, nuq
				if (species(j) == atlist%atom(i)%sfacsymb) counter = counter + 1
			end do
				
			if (counter == 0) then
				nuq = nuq + 1
				species(nuq) = atlist%atom(i)%sfacsymb
			end if	
		end do
		
		! Extract information about number of anions, cations, valences, radius...
		nan = 0
		ncat = 0
		allocate(radii(nuq))
		do i = 1, nuq
			symbol = trim(species(i))
			nchar = len(symbol)
			if ('-' == symbol(nchar:nchar)) then
				nan = nan + 1
				read(symbol(nchar-1:nchar-1),*) valence
				valence = -valence
			else if ('+' == symbol(nchar:nchar)) then
			    ncat = ncat + 1
				read(symbol(nchar-1:nchar-1),*) valence
			else
				ierror = tuple_create(ret,1)
				ierr = 0
				ierror = ret%setitem(3,ierr)
				r = ret%get_c_ptr()
				return
			end if
				
			if (nchar == 3) then
				call Get_Ionic_Radius(symbol(1:1), valence, radius)
				if (-0.0001 < radius < 0.0001) then
					ierror = tuple_create(ret,1)
					ierr = 0
					ierror = ret%setitem(5,ierr)
					r = ret%get_c_ptr()
				end if
			else if (nchar == 4) then
				call Get_Ionic_Radius(symbol(1:2), valence, radius)
				if (-0.0001 < radius < 0.0001) then
					ierror = tuple_create(ret,1)
					ierr = 0
					ierror = ret%setitem(5,ierr)
					r = ret%get_c_ptr()
				end if
			else
				ierror = tuple_create(ret,1)
				ierr = 0
				ierror = ret%setitem(4,ierr)
				r = ret%get_c_ptr()
			end if
			
			radii(i) = radius
				
			deallocate(symbol)
		end do
		
		! call Set_TDist_Coordination previously as demanded
		call Allocate_Coordination_Type(atlist%natoms, spg%multip, 3.0, maxcor)
		call Set_TDist_Coordination(maxcor, 3.0, cell, spg, atlist)
		!call Calc_Dist_Angle_Sigma(2.0, 2.0, cell, spg, atlist)
		

        ! Set Parameters ot Atoms_Conf
        call Allocate_Atoms_Conf_List(atlist%natoms, atconf)
		atconf%n_spec = nuq
		atconf%n_anions = nan
		atconf%n_cations = ncat
		atconf%totatoms = atcell%nat
		atconf%species = species(1:nuq)
		atconf%radius = radii
		atconf%atom = atlist%atom
		
		! Maybe is just necessary to have informaton about the unique atoms
		
		do i=1,atconf%natoms
			print*, atconf%atom(i)%ind
		end do
		
		call Cost_BVS(atconf, gii)
		
		print*, gii
		
        r = ret%get_c_ptr()

    end function calculate_gii
	
	function get_laue_num(self_ptr, args_ptr) result(r) bind(c)
        ! Calculates the global instability index of a crystal structure
        type(c_ptr), value :: self_ptr
        type(c_ptr), value :: args_ptr
        type(c_ptr) :: r
		
		! Variables
        integer :: ierror
        character(len=:), allocatable :: spg_str
		real, dimension(:), pointer :: p_cell
		character(len=12) :: family, system, laue
        character(len=1) :: symbol

        ! Forpy types
        type(object) :: item
		type(ndarray) :: py_cell
        type(tuple) :: args, ret

        call unsafe_cast_from_c_ptr(args, args_ptr)

        ierror = args%getitem(item, 0)
        ierror = cast(spg_str, item)
        ierror = args%getitem(item, 1)
        ierror = cast(py_cell, item)
        ierror = py_cell%get_data(p_cell)

        call Set_Spacegroup(spg_str, spg)
		call Set_Crystal_Cell(p_cell(1:3), p_cell(4:6), cell)
        call Get_Cryst_Family(cell, family, symbol, system)


        ! Return Crystal System
        ierror = tuple_create(ret, 3)
        ierror = ret%setitem(0, spg%Laue)
		ierror = ret%setitem(1, spg%CrystalSys)
		ierror = ret%setitem(2, system)
        r = ret%get_c_ptr()

	end function get_laue_num

end module crysfml_forpy
