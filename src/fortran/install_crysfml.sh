#!/bin/sh

installdir='/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/crysfml_python'
currentdir=${pwd}
crysfmldir='/home/oarcelus/crysfml/build/ifort_release'

cd ${installdir}
ifort -c -fpic -fpp '/home/oarcelus/fullprofapp/src/fortran/src/forpy_mod.f90'
ifort -c -fpic -fpp '/home/oarcelus/fullprofapp/src/fortran/src/crysfml_forpy.f90' -I ${crysfmldir}/crysfml_common_modules/
ifort -shared -o crysfml_forpy.so crysfml_forpy.o forpy_mod.o -L ${crysfmldir}/Src/ ${crysfmldir}/Src/libcrysfml.a

cd ${currentdir}
