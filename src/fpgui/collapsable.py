from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc


class CollapsableProtocol(qtw.QWidget):

    def __init__(self, title="", widget: qtw.QWidget = None, parent=None):
        super(CollapsableProtocol, self).__init__(parent)

        self.expandableButton = qtw.QToolButton()
        self.expandableButton.setText(title)
        self.expandableButton.setCheckable(True)
        self.expandableButton.setChecked(False)

        self._widget = widget

        self.pressed = False

        self.expandableButton.setToolButtonStyle(qtc.Qt.ToolButtonTextBesideIcon)
        self.expandableButton.setSizePolicy(qtw.QSizePolicy.Expanding, qtw.QSizePolicy.Fixed)
        self.expandableButton.setArrowType(qtc.Qt.RightArrow)
        self.expandableButton.pressed.connect(self.on_pressed)

        self.expandAnimation = qtc.QParallelAnimationGroup(self)

        self.contentArea = qtw.QScrollArea()
        self.contentArea.setMaximumHeight(0)
        self.contentArea.setMinimumHeight(0)
        self.contentArea.setSizePolicy(qtw.QSizePolicy.Expanding, qtw.QSizePolicy.Minimum)
        self.contentArea.setFrameShape(qtw.QFrame.NoFrame)

        lay = qtw.QVBoxLayout(self)
        lay.setSpacing(0)
        lay.setContentsMargins(0, 0, 0, 0)
        lay.addWidget(self.expandableButton)
        lay.addWidget(self.contentArea)

        self.expandAnimation.addAnimation(
            qtc.QPropertyAnimation(self, b"minimumHeight")
        )
        self.expandAnimation.addAnimation(
            qtc.QPropertyAnimation(self, b"maximumHeight")
        )
        self.expandAnimation.addAnimation(
            qtc.QPropertyAnimation(self.contentArea, b"maximumHeight")
        )

        self.set_content_layout(layout=self._widget.layout())

    def on_pressed(self):
        checked = self.expandableButton.isChecked()
        self.expandableButton.setArrowType(qtc.Qt.DownArrow if not checked else qtc.Qt.RightArrow)
        self.expandAnimation.setDirection(qtc.QAbstractAnimation.Forward if not checked else
                                          qtc.QAbstractAnimation.Backward)

        self.expandAnimation.start()

    def set_content_layout(self, layout):

        lay = self.contentArea.layout()
        del lay
        self.contentArea.setLayout(layout)
        collapsed_height = (self.sizeHint().height() - self.contentArea.maximumHeight())
        content_height = layout.sizeHint().height()
        for i in range(self.expandAnimation.animationCount()):
            animation = self.expandAnimation.animationAt(i)
            animation.setDuration(500)
            animation.setStartValue(collapsed_height)
            animation.setEndValue(collapsed_height + content_height)

        content_animation = self.expandAnimation.animationAt(self.expandAnimation.animationCount() - 1)
        content_animation.setDuration(500)
        content_animation.setStartValue(0)
        content_animation.setEndValue(content_height)

