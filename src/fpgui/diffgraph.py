from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

import qtawesome as qta

import pyqtgraph as pg
import numpy as np
import collections.abc
import pandas as pd
import os
import sys

from typing import Union, List

from fpgui.uifiles.uidiffgraph import Ui_diffGraph

from fpgui.cellparamvary import CellVary
from fpgui.fpautorietveld import TreeModel

from fpfunctions.app_io import PcrIO, SumIO, MicIO, PrfIO
from fpfunctions.signals import DiffractionSignal
import fpfunctions.symbols as symbols

from crysfml_python import crysfml_forpy


class DiffGraph(qtw.QWidget):

    reset_selection = qtc.pyqtSignal(bool)
    select_add = qtc.pyqtSignal(object)
    add_point = qtc.pyqtSignal(object)
    select_drag = qtc.pyqtSignal(float, float, float, float)
    delete = qtc.pyqtSignal(bool)
    point_pos = qtc.pyqtSignal(object)
    x_pos = qtc.pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # load form
        self.ui_diffgraph = Ui_diffGraph()
        self.ui_diffgraph.setupUi(self)

        """ Set style on graphs on the V Layout """
        self.ui_diffgraph.diff_graph.addLegend()

        axis_pen = pg.mkPen(color=(0, 0, 0), width=2)

        self.ui_diffgraph.diff_graph.setLabel('left', '<font size="4">Intensity (counts)</font>')
        self.ui_diffgraph.diff_graph.getAxis('left').setPen(axis_pen)
        self.ui_diffgraph.diff_graph.getAxis('left').setTextPen(axis_pen)

        self.ui_diffgraph.diff_graph.setLabel('bottom', u'<font size="4">2θ (º)</font>')
        self.ui_diffgraph.diff_graph.getAxis('bottom').setPen(axis_pen)
        self.ui_diffgraph.diff_graph.getAxis('bottom').setTextPen(axis_pen)

        self.ui_diffgraph.diff_graph.setLabel('right', ' ')
        self.ui_diffgraph.diff_graph.showAxis('right')
        self.ui_diffgraph.diff_graph.getAxis('right').setPen(axis_pen)
        self.ui_diffgraph.diff_graph.getAxis('right').setStyle(showValues=False)

        self.ui_diffgraph.diff_graph.setLabel('top', ' ')
        self.ui_diffgraph.diff_graph.showAxis('top')
        self.ui_diffgraph.diff_graph.getAxis('top').setPen(axis_pen)
        self.ui_diffgraph.diff_graph.getAxis('top').setStyle(showValues=False)

        self.ui_diffgraph.diff_graph.setBackground((255, 255, 255))

        self.ui_diffgraph.peaks_graph.getAxis('left').setStyle(showValues=False)
        self.ui_diffgraph.peaks_graph.setXLink(self.ui_diffgraph.diff_graph)
        self.ui_diffgraph.peaks_graph.hideAxis('left')
        self.ui_diffgraph.peaks_graph.hideAxis('bottom')
        self.ui_diffgraph.peaks_graph.setBackground((255, 255, 255))

        """ Set viewBox """
        self.ui_diffgraph.diff_graph.plotItem.getViewBox().setMouseMode(pg.ViewBox.RectMode)
        self.ui_diffgraph.diff_graph.setMouseEnabled(x=False, y=False)

        self.ui_diffgraph.peaks_graph.plotItem.getViewBox().setMouseMode(pg.ViewBox.RectMode)
        self.ui_diffgraph.peaks_graph.setMouseEnabled(x=False, y=False)

        self.ui_diffgraph.diff_graph.plotItem.getViewBox().mouseClickEvent = self.click_graph
        self.ui_diffgraph.diff_graph.plotItem.getViewBox().mouseDragEvent = self.drag_graph
        self.ui_diffgraph.diff_graph.keyPressEvent = self.key_pressed
        self.ui_diffgraph.diff_graph.plotItem.getViewBox().hoverEvent = self.get_coords
        self.ui_diffgraph.peaks_graph.plotItem.getViewBox().mouseDragEvent = None
        self.ui_diffgraph.peaks_graph.plotItem.getViewBox().hoverEvent = self.get_xaxis

        self._reflections = None

    def set_reflections(self, reflections):
        self._reflections = reflections

    def click_graph(self, ev):
        """ Redefine some graph click functions
         RightClick : Zoom Back
         LeftClick : Reset Selection
         Ctrl + LeftClick : Select and add to selection
         Ctrl + RightClick : Menu for PNG exports
         Shift + LeftClick : Add Point
         TODO: Behavior is still a bit buggy, cant click directly on symbols, just nearby works."""

        if ev.button() == qtc.Qt.MouseButton.RightButton and not ev.modifiers() & qtc.Qt.ControlModifier \
                and not ev.modifiers() & qtc.Qt.ShiftModifier:
            self.ui_diffgraph.diff_graph.autoRange()
            ev.accept()
        elif ev.button() == qtc.Qt.MouseButton.LeftButton and not ev.modifiers() & qtc.Qt.ControlModifier \
                and not ev.modifiers() & qtc.Qt.ShiftModifier:
            self.reset_selection.emit(True)
            ev.accept()
        elif ev.button() == qtc.Qt.MouseButton.LeftButton and ev.modifiers() & qtc.Qt.ShiftModifier:
            self.ui_diffgraph.diff_graph.scene().items(ev.scenePos())
            vb = self.ui_diffgraph.diff_graph.plotItem.getViewBox()
            mousepoint = vb.mapSceneToView(ev._scenePos)
            point = np.array([mousepoint.x(), mousepoint.y()])
            self.select_add.emit(point)
            ev.accept()

        elif ev.button() == qtc.Qt.MouseButton.LeftButton and ev.modifiers() & qtc.Qt.ControlModifier:
            self.ui_diffgraph.diff_graph.scene().items(ev.scenePos())
            vb = self.ui_diffgraph.diff_graph.plotItem.getViewBox()
            mousepoint = vb.mapSceneToView(ev._scenePos)
            point = np.array([mousepoint.x(), mousepoint.y()])
            self.add_point.emit(point)
            ev.accept()

        else:
            pg.ViewBox.mouseClickEvent(self.ui_diffgraph.diff_graph.plotItem.getViewBox(), ev)

    def drag_graph(self, ev):
        """ Redefine some graph drag functions
        Ctrl + LeftClickDrag : Select Points/s
        """

        if ev.button() == qtc.Qt.MouseButton.LeftButton and ev.modifiers() & qtc.Qt.ControlModifier:
            vb = self.ui_diffgraph.diff_graph.plotItem.getViewBox()
            if ev.isFinish():
                vb.rbScaleBox.hide()

                xfin = vb.mapToView(ev.buttonDownPos()).x()
                xbeg = vb.mapToView(ev.pos()).x()
                yfin = vb.mapToView(ev.buttonDownPos()).y()
                ybeg = vb.mapToView(ev.pos()).y()
                self.select_drag.emit(float(xbeg), float(xfin), float(ybeg), float(yfin))
                ev.accept()
            else:
                vb.updateScaleBox(ev.buttonDownPos(), ev.pos())
                ev.accept()

        else:
            pg.ViewBox.mouseDragEvent(self.ui_diffgraph.diff_graph.plotItem.getViewBox(), ev)

    def key_pressed(self, ev):
        if ev.key() == qtc.Qt.Key_Delete:
            self.delete.emit(True)
            ev.accept()
        else:
            pg.ViewBox.keyPressEvent(self.ui_diffgraph.diff_graph.plotItem.getViewBox(), ev)

        if ev.key() == qtc.Qt.Key_Left:
            state = self.ui_diffgraph.diff_graph.plotItem.getViewBox().state
            xrange = np.array(state['viewRange'][0], dtype=float)
            step = (xrange[1] - xrange[0]) / 100.0

            self.ui_diffgraph.diff_graph.plotItem.getViewBox().setXRange(
                xrange[0] - step, xrange[1] - step, padding=0.0)

        elif ev.key() == qtc.Qt.Key_Right:
            state = self.ui_diffgraph.diff_graph.plotItem.getViewBox().state
            xrange = np.array(state['viewRange'][0], dtype=float)
            step = (xrange[1] - xrange[0]) / 100.0

            self.ui_diffgraph.diff_graph.plotItem.getViewBox().setXRange(
                xrange[0] + step, xrange[1] + step, padding=0.0)

        elif ev.key() == qtc.Qt.Key_Up:
            state = self.ui_diffgraph.diff_graph.plotItem.getViewBox().state
            yrange = np.array(state['viewRange'][1], dtype=float)
            step = (yrange[1] - yrange[0]) / 100.0

            self.ui_diffgraph.diff_graph.plotItem.getViewBox().setYRange(
                yrange[0] + step, yrange[1] + step, padding=0.0)

        elif ev.key() == qtc.Qt.Key_Down:
            state = self.ui_diffgraph.diff_graph.plotItem.getViewBox().state
            yrange = np.array(state['viewRange'][1], dtype=float)
            step = (yrange[1] - yrange[0]) / 100.0

            self.ui_diffgraph.diff_graph.plotItem.getViewBox().setYRange(
                yrange[0] - step, yrange[1] - step, padding=0.0)

        else:
            pg.ViewBox.keyPressEvent(self.ui_diffgraph.diff_graph.plotItem.getViewBox(), ev)

    def get_coords(self, ev):

        try:
            self.ui_diffgraph.diff_graph.scene().items(ev.scenePos())
            vb = self.ui_diffgraph.diff_graph.plotItem.getViewBox()
            mousepoint = vb.mapSceneToView(ev.scenePos())
            point = np.array([mousepoint.x(), mousepoint.y()])
            self.point_pos.emit(point)

            if ev.isExit():
                self.point_pos.emit(None)

        except AttributeError:
            self.point_pos.emit(None)

    def get_xaxis(self, ev):

        try:
            self.ui_diffgraph.peaks_graph.scene().items(ev.scenePos())
            vb = self.ui_diffgraph.peaks_graph.plotItem.getViewBox()
            mousepoint = vb.mapSceneToView(ev.scenePos())
            pos = float(mousepoint.x())
            self.x_pos.emit(pos)

        except AttributeError:
            self.x_pos.emit(None)

    def highlight_reflections(self, pos):

        if pos is not None:
            x_min = pos - 0.3
            x_max = pos + 0.3

            mask1 = self._reflections['x'] < x_min
            mask2 = self._reflections['x'] < x_max
            mask_out = ~(mask1 ^ mask2)
            mask_in = (mask1 ^ mask2)
            out_hkl = self._reflections[mask_out]
            in_hkl = self._reflections[mask_in]

            self.sender().ui_diffgraph.peaks_graph.clear()

            self.sender().ui_diffgraph.peaks_graph.plot(x=out_hkl['x'].to_numpy(), y=-1.0 * out_hkl['ph'].to_numpy(),
                                                        symbolPen=pg.mkPen(color=(0, 204, 0), width=1.5),
                                                        pen=None, symbol=symbols.custom_symbol("|"))
            self.sender().ui_diffgraph.peaks_graph.plot(x=in_hkl['x'].to_numpy(), y=-1.0 * in_hkl['ph'].to_numpy(),
                                                        symbolPen=pg.mkPen(color=(255, 0, 0), width=1.5), pen=None,
                                                        symbol=symbols.custom_symbol("|"))

            if len(in_hkl.columns) != 0:
                hklstr = np.array2string(in_hkl[['h', 'k', 'l']].to_numpy()).replace("[", "").replace("]", "")
                phstr = np.array2string(np.array([in_hkl['ph'].to_numpy()]).T).replace("[", "").replace("]", "")

                splt_lines = zip(hklstr.split('\n'), phstr.split('\n'))
                # horizontal join
                res = '\n'.join([f"{x} --> ph: {y}" for x, y in splt_lines if x and y])

                self.sender().setToolTip(res)

            else:
                self.sender().setToolTip("")

        else:
            self.sender().setToolTip("")
            self.sender().ui_diffgraph.peaks_graph.clear()
            self.sender().ui_diffgraph.peaks_graph.plot(x=self._reflections['x'].to_numpy(),
                                                        y=-1.0 * self._reflections['ph'].to_numpy(),
                                                        symbolPen=pg.mkPen(color=(0, 204, 0), width=1.5), pen=None,
                                                        symbol=symbols.custom_symbol("|"))


class PopUpDiffGraph(DiffGraph):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.button_back = qtw.QToolButton()
        self.button_back.clicked.connect(self.change_back_patt)
        self.button_back.setIcon(qta.icon('ei.arrow-left'))
        self.button_next = qtw.QToolButton()
        self.button_next.clicked.connect(self.change_next_patt)
        self.button_next.setIcon(qta.icon('ei.arrow-right'))

        self.combo_paths = qtw.QComboBox()

        self.npatt = 1

        self.ui_diffgraph.verticalLayout.addLayout(qtw.QHBoxLayout())
        self.ui_diffgraph.verticalLayout.itemAt(2).layout().addWidget(self.combo_paths)
        self.ui_diffgraph.verticalLayout.itemAt(2).layout().addWidget(self.button_back)
        self.ui_diffgraph.verticalLayout.itemAt(2).layout().addWidget(qtw.QLabel('PATT:'))
        self.lineedit = qtw.QLineEdit()
        self.lineedit.setReadOnly(True)
        self.lineedit.setText('0')
        self.lineedit.setSizePolicy(qtw.QSizePolicy.Fixed, qtw.QSizePolicy.Fixed)
        self.lineedit.setFixedWidth(40)
        self.ui_diffgraph.verticalLayout.itemAt(2).layout().addWidget(self.lineedit)
        self.ui_diffgraph.verticalLayout.itemAt(2).layout().addWidget(self.button_next)
        self.enable_buttons()

    def enable_buttons(self):
        ipatt = int(self.lineedit.text())

        if self.npatt == 1:
            self.button_back.setEnabled(False)
            self.button_next.setEnabled(False)
        else:
            if ipatt + 1 >= self.npatt:
                self.button_back.setEnabled(True)
                self.button_next.setEnabled(False)
            elif ipatt <= 0:
                self.button_back.setEnabled(False)
                self.button_next.setEnabled(True)
            else:
                self.button_back.setEnabled(True)
                self.button_next.setEnabled(True)

    def change_back_patt(self):
        ipatt = int(self.lineedit.text())
        self.lineedit.setText(str(ipatt - 1))
        self.enable_buttons()

    def change_next_patt(self):
        ipatt = int(self.lineedit.text())
        self.lineedit.setText(str(ipatt + 1))
        self.enable_buttons()


class SimGraph(qtw.QWidget):

    rerun_params = qtc.pyqtSignal(dict)
    save_cif = qtc.pyqtSignal(int)

    def __init__(self, path, jobid, pattern, info, cifdir, data, rerunparams, rerunsim, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.vlay = None
        self.graph = None
        self.sliders = None
        self.buttons = None
        self.scale_labels = None
        self.cellvary = None
        self.glay = None
        self.vlay1 = None
        self.hlay1 = None
        self.hlay2 = None
        self.hlay3 = None
        self.zero_text = None
        self.slider_zero = None
        self.zerovalue = None
        self.simplots = None
        self.table = None
        self.simpatterns = None
        self.scale_multiplier = None
        self.simreflections = None

        self.info = info
        self.patterns = pattern
        self.cifdir = cifdir
        self.data = data
        self.jobpath = path
        self.jobid = jobid
        self.rerunparams = rerunparams
        self.rerunsim = rerunsim
        self.init_widget()
        self.plot_widget()

    def init_widget(self):

        self.vlay = qtw.QVBoxLayout()
        self.graph = DiffGraph()
        self.vlay.addWidget(self.graph)

        self.sliders = {}
        self.buttons = {}
        self.scale_labels = {}
        self.cellvary = {}

        self.glay = qtw.QGridLayout()

        i = 0
        j = 0
        for identifier in self.info['identifier'].tolist():
            self.hlay1 = qtw.QHBoxLayout()
            self.hlay2 = qtw.QHBoxLayout()
            self.hlay1.addWidget(qtw.QLabel(identifier))
            button = qtw.QPushButton()
            button.setObjectName(identifier)
            button.setText("HKL Gen")
            self.buttons[identifier] = button
            self.hlay1.addWidget(button)
            slider = qtw.QSlider(qtc.Qt.Horizontal)
            slider.setObjectName(identifier)
            slider.setMinimum(0)
            slider.setMaximum(1000)
            self.sliders[identifier] = slider

            self.vlay1 = qtw.QVBoxLayout()

            self.vlay1.addLayout(self.hlay1)
            self.scale_labels[identifier] = qtw.QLabel("Scale: {res:.3E}".format(res=0.1e-4))
            self.hlay2.addWidget(self.scale_labels[identifier])

            self.hlay2.addWidget(slider)
            self.vlay1.addLayout(self.hlay2)
            self.vlay1.setAlignment(qtc.Qt.AlignCenter)

            if i % 6 == 0 and i != 0:
                i = 0
                j += 1

            i += 1

            self.glay.addLayout(self.vlay1, j, i)

            slider.valueChanged.connect(self.scale_change)
            button.pressed.connect(lambda: self.show_hklgen())

            phase = self.info[self.info['identifier'].isin([identifier])]
            self.cellvary[identifier] = CellVary(info=phase)

        self.hlay3 = qtw.QHBoxLayout()
        self.zero_text = qtw.QLabel("Zero Correction: {res:10.4f}".format(res=0.0))
        self.slider_zero = qtw.QSlider(qtc.Qt.Horizontal)
        self.slider_zero.setObjectName("zero_slider")
        self.slider_zero.setMinimum(0)
        self.slider_zero.setMaximum(1000)
        self.slider_zero.setValue(500)
        self.slider_zero.setSliderPosition(500)

        button_rerun = qtw.QPushButton()
        button_rerun.setText("Rerun Simulation")
        button_cif = qtw.QPushButton()
        button_cif.setText("Generate CIF file")

        self.slider_zero.valueChanged.connect(lambda: self.zero_change())
        self.slider_zero.sliderReleased.connect(lambda: self.zero_record())

        button_rerun.pressed.connect(self.pass_info_sim)
        button_cif.pressed.connect(self.save_info_cif)

        self.hlay3.addWidget(self.zero_text)
        self.hlay3.addWidget(self.slider_zero)
        self.hlay3.addWidget(button_rerun)
        self.hlay3.addWidget(button_cif)

        self.vlay.addLayout(self.glay)
        self.vlay.addLayout(self.hlay3)

        self.setLayout(self.vlay)

        self.graph.x_pos.connect(self.graph.highlight_reflections)

    def hex_to_rgb(self, hex: str):
        return tuple(int(hex[i: i+2], 16) for i in (0, 2, 4))

    def plot_widget(self):
        self.simplots = dict()
        self.table = dict()
        self.simpatterns = dict()
        self.scale_multiplier = dict()

        pcr = PcrIO(path=self.jobpath, jobid=self.jobid)
        prf = PrfIO(self.jobpath, self.jobid)
        pcr.read_custom_pcr()
        prf.read_data()

        self.zerovalue = prf.zerovalue
        """ Plot reflections as lines """
        temp_values = prf.reflections
        mask1 = temp_values['x'] < self.patterns.diffraction[0, 0]
        mask2 = temp_values['x'] <= self.patterns.diffraction[-1, 0]
        mask = (mask1 ^ mask2)
        self.simreflections = temp_values[mask]

        self.graph.ui_diffgraph.peaks_graph.plot(x=self.simreflections['x'].to_numpy(),
                                                 y=-1.0 * self.simreflections['ph'].to_numpy(),
                                                 pen=None,
                                                 symbolPen=pg.mkPen(color=(0, 204, 0), width=1.5),
                                                 symbol=symbols.custom_symbol("|"))

        """ Load .sub files (free format) """
        i = 1
        for identifier in self.info['identifier'].tolist():
            filename = os.path.join(self.jobpath, f"{self.jobid}{i}.sub")
            sims = DiffractionSignal.from_file(filename=filename, fmt='free')

            mask1 = sims.diffraction[:, 0] < self.patterns.diffraction[0, 0]
            mask2 = sims.diffraction[:, 0] <= self.patterns.diffraction[-1, 0]
            mask = (mask1 ^ mask2)
            sims.diffraction = sims.diffraction[mask]

            self.simpatterns[identifier] = sims
            self.table[identifier] = i
            self.scale_multiplier[identifier] = (pcr.get_profile_phase(identifier, 'scale') / 0.1e-4)

            i += 1

        self.graph.ui_diffgraph.diff_graph.plot(x=self.patterns.diffraction[:, 0],
                                                y=self.patterns.diffraction[:, 1],
                                                pen=pg.mkPen(color=(0, 85, 255), width=1),
                                                symbolPen=pg.mkPen(color=(0, 85, 255), width=1.5),
                                                symbolSize=1.5, symbolBrush=(0, 85, 255))

        for identifier in self.info['identifier'].tolist():
            color = tuple(np.random.choice(range(256), size=3))
            pen = pg.mkPen(color=color, width=3)
            background_interpolated = np.interp(self.simpatterns[identifier].diffraction[:, 0],
                                                self.patterns.diffraction[:, 0],
                                                self.patterns.background,
                                                left=self.patterns.background[0],
                                                right=self.patterns.background[1])
            self.simplots[identifier] = \
                self.graph.ui_diffgraph.diff_graph.plot(x=self.simpatterns[identifier].diffraction[:, 0],
                                                        y=(self.simpatterns[identifier].diffraction[:, 1] +
                                                           background_interpolated),
                                                        name=identifier,
                                                        pen=pen)

        """ Define slider positions if we are reruning a simulation from previous one """
        if self.rerunsim:
            self.set_slider_pos(self.rerunparams)

        self.graph.set_reflections(self.simreflections)

    def set_slider_pos(self, params: dict):

        for key, value in params.items():
            if 'zero' in key:
                """ Set slider position for zero """
                self.slider_zero.setSliderPosition(self.zero_pos(value))
                self.zero_text.setText("Zero Correction: {res:10.4f}".format(res=value))

            elif 'scale' in key:
                """ Set scale slider positions """
                identifier = key.replace("-scale", "")
                self.sliders[identifier].setSliderPosition(self.value_pos(value/0.1e-4))
                self.scale_labels[identifier].setText("Scale: {res:.3E}".format(res=value))

            elif 'params' in key:
                identifier = key.replace("-params", "")
                self.cellvary[identifier].params = value

            elif 'positions' in key:
                identifier = key.replace("-positions", "")
                self.cellvary[identifier].sliderpos = value
                self.cellvary[identifier].set_slider_pos()

    def pass_info_sim(self):

        values = dict()

        values['zero'] = self.zero_handler(float(self.slider_zero.value()))

        for identifier in self.info['identifier'].tolist():

            values[f"{identifier}-scale"] = 0.1e-4 * self.value_handler(float(self.sliders[identifier].value()))
            values[f"{identifier}-params"] = self.cellvary[identifier].params
            values[f"{identifier}-positions"] = self.cellvary[identifier].sliderpos

        self.rerun_params.emit(values)

    def save_info_cif(self):
        values = dict()

        for identifier in self.info['identifier'].tolist():
            values[f"{identifier}-params"] = self.cellvary[identifier].params
            values[f"{identifier}-positions"] = self.cellvary[identifier].sliderpos

        for key, value in values.items():
            if 'params' in key:
                identifier = key.replace("-params", "")

                local = []

                for item in os.listdir(self.cifdir):
                    if identifier in item:
                        local.append(item.replace('.cif', ''))
                    else:
                        pass

                cif = self.data[identifier]
                cif[cif.keys()[0]]['_cell_length_a'] = value[0]
                cif[cif.keys()[0]]['_cell_length_b'] = value[1]
                cif[cif.keys()[0]]['_cell_length_c'] = value[2]
                cif[cif.keys()[0]]['_cell_angle_alpha'] = value[3]
                cif[cif.keys()[0]]['_cell_angle_beta'] = value[4]
                cif[cif.keys()[0]]['_cell_angle_gamma'] = value[5]

                num = len(local) + 1
                with open(os.path.join(self.cifdir, f"{identifier}-{num}.cif"), 'w') as f:
                    f.write(cif.WriteOut())

        self.save_cif.emit(0)

    def show_hklgen(self):

        identifier = self.sender().objectName()
        zero = self.zero_handler(float(self.slider_zero.value()))

        self.cellvary[identifier].increments.connect(lambda params: self.hklgen(identifier, zero, params))
        self.cellvary[identifier].show()

    def hklgen(self, identifier, zero, params):

        """Convert increments to cell parameters"""
        phase = self.info[self.info['identifier'].isin([identifier])]
        spg = phase.iloc[0]['spacegroup']

        """ Calculate sin(theta)/lambda max and min values """
        th_min = 0.5 * self.patterns.diffraction[0, 0]
        th_max = 0.5 * self.patterns.diffraction[-1, 0]

        th_min = np.deg2rad(th_min)
        th_max = np.deg2rad(th_max)

        if isinstance(self.patterns.wavelength, tuple):
            wav = self.patterns.wavelength[0]
        else:
            wav = self.patterns.wavelength

        s_min = np.sin(th_min) / wav
        s_max = np.sin(th_max) / wav

        r = crysfml_forpy.gen_hkl(params, spg, s_min, s_max)

        """ Eliminate reflections that are recalculated by gen_hkl """
        self.simreflections = self.simreflections[self.simreflections['ph'] != self.table[identifier]]
        """ Add recalculated reflections with phase value equivalent to the one used """
        th = np.arcsin(r[1] * wav)
        two_th = 2.0 * np.rad2deg(th) + zero
        hkl = r[0].T
        newdf = pd.DataFrame({'x': two_th, 'h': hkl[:, 0],
                              'k': hkl[:, 1], 'l': hkl[:, 2],
                              'ph': self.table[identifier]})

        self.simreflections = pd.concat([self.simreflections, newdf])
        self.update_simreflections(self.simreflections)

        self.graph.set_reflections(self.simreflections)
    
    def update_simreflections(self, reflections):

        self.graph.ui_diffgraph.peaks_graph.clear()
        self.graph.ui_diffgraph.peaks_graph.plot(x=reflections['x'].to_numpy(),
                                                 y=-1.0 * reflections['ph'].to_numpy(),
                                                 pen=None,
                                                 symbolPen=pg.mkPen(color=(0, 204, 0), width=1.5),
                                                 symbol=symbols.custom_symbol("|"))

    def zero_record(self):

        values = dict()

        values['zero'] = self.zero_handler(float(self.slider_zero.value()))

        for identifier in self.info['identifier'].tolist():
            values[identifier] = self.value_handler(float(self.sliders[identifier].value()))

        self.simreflections['x'] = self.simreflections['x'] + (values['zero'] - self.zerovalue)
        self.zerovalue = values['zero']
        self.update_simreflections(self.simreflections)
        self.graph.set_reflections(self.simreflections)
        
    def zero_change(self):

        values = dict()

        values['zero'] = self.zero_handler(float(self.slider_zero.value()))
        self.zero_text.setText("Zero Correction: {res:10.4f}".format(res=values['zero']))

        for identifier in self.info['identifier'].tolist():
            values[identifier] = self.value_handler(float(self.sliders[identifier].value()))

        tmp = self.simreflections.copy()
        tmp['x'] = tmp['x'] + (values['zero'] - self.zerovalue)

        for identifier in self.info['identifier'].tolist():
            background_interpolated = np.interp(self.simpatterns[identifier].diffraction[:, 0] + values['zero'],
                                                self.patterns.diffraction[:, 0],
                                                self.patterns.background,
                                                left=self.patterns.background[0],
                                                right=self.patterns.background[1])

            self.simplots[identifier].setData(x=self.simpatterns[identifier].diffraction[:, 0] + values['zero'],
                                              y=(self.simpatterns[identifier].diffraction[:, 1]
                                                 * values[identifier] / self.scale_multiplier[identifier]
                                                 + background_interpolated))

        self.update_simreflections(tmp)

        self.graph.set_reflections(tmp)
        
    def scale_change(self):

        values = dict()

        values['zero'] = self.zero_handler(float(self.slider_zero.value()))

        for identifier in self.info['identifier'].tolist():
            values[identifier] = self.value_handler(float(self.sliders[identifier].value()))

            scale = values[identifier] * 0.1e-4
            self.scale_labels[identifier].setText("Scale: {res:.3E}".format(res=scale))

        for identifier in self.info['identifier'].tolist():
            background_interpolated = np.interp(self.simpatterns[identifier].diffraction[:, 0] + values['zero'],
                                                self.patterns.diffraction[:, 0],
                                                self.patterns.background,
                                                left=self.patterns.background[0],
                                                right=self.patterns.background[1])

            self.simplots[identifier].setData(x=self.simpatterns[identifier].diffraction[:, 0] + values['zero'],
                                              y=(self.simpatterns[identifier].diffraction[:, 1]
                                                 * values[identifier] / self.scale_multiplier[identifier]
                                                 + background_interpolated))

    @staticmethod
    def zero_handler(value):
        vmin = -0.3
        vmax = 0.3

        scaled_value = value * (vmax - vmin) / 1000 + vmin

        return scaled_value

    @staticmethod
    def zero_pos(scaled_value):
        vmin = -0.3
        vmax = 0.3

        value = int((scaled_value - vmin) * 1000 / (vmax - vmin))

        return value

    @staticmethod
    def value_handler(value):
        vmin = np.log(float(0.01))
        vmax = np.log(float(10**5))

        scale_factor = (vmax - vmin)/1000.0

        scaled_value = np.exp(vmin + scale_factor*value)

        return scaled_value

    @staticmethod
    def value_pos(scaled_value):
        vmin = np.log(float(0.01))
        vmax = np.log(float(10 ** 5))

        scale_factor = (vmax - vmin) / 1000.0

        value = int((np.log(scaled_value) - vmin)/scale_factor)

        return value


class FomGraph(DiffGraph):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        item_peaks_graph = self.ui_diffgraph.verticalLayout.itemAt(1)
        widget_peaks = item_peaks_graph.widget()
        self.ui_diffgraph.verticalLayout.removeWidget(widget_peaks)
        self.ui_diffgraph.peaks_graph = None

        self.ui_diffgraph.diff_graph.setLabel('left', 'R-factor')
        self.ui_diffgraph.diff_graph.setLabel('bottom', 'Step')


class ResultsGraph(qtw.QSplitter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.graph = DiffGraph()

        item_peaks_graph = self.graph.ui_diffgraph.verticalLayout.itemAt(1)
        widget_peaks = item_peaks_graph.widget()
        self.graph.ui_diffgraph.verticalLayout.removeWidget(widget_peaks)
        self.graph.ui_diffgraph.peaks_graph = None

        self.tree_dict = dict()
        self.results_storage = dict()
        self.plot_dict = dict()
        self.error = dict()

        self.treemodel = None
        self.warning = None
        self._radiation = None
        self.textit = None
        self._jobids = []

        self.graph.ui_diffgraph.diff_graph.setLabel('left', 'Property')
        self.graph.ui_diffgraph.diff_graph.setLabel('bottom', 'Scan Step')
        self.graph.ui_diffgraph.diff_graph.plotItem.getViewBox().hoverEvent = self.show_key_hint

        self.addWidget(self.graph)

        widget = qtw.QWidget()
        vlay = qtw.QVBoxLayout(widget)
        self.treeview = qtw.QTreeView()
        self.treeview.setAlternatingRowColors(True)
        self.treeview.clicked.connect(self.do_checks)
        self.treeview.clicked.connect(self.clean_top_checks)
        self.treeview.clicked.connect(self.plot_checked_results)

        self.button = qtw.QPushButton('Export Data')
        self.button.clicked.connect(self.export_data)

        self.button1 = qtw.QPushButton('Check All')
        self.button1.setCheckable(True)
        self.button1.clicked.connect(self.check_all_tree)

        vlay.addWidget(self.treeview)

        hlay = qtw.QHBoxLayout()
        hlay.addWidget(self.button)
        hlay.addWidget(self.button1)
        vlay.addLayout(hlay)

        self.addWidget(widget)

        self.setStretchFactor(0, 5)
        self.setStretchFactor(1, 1)

    def show_key_hint(self, ev):
        if self._jobids:
            self.graph.ui_diffgraph.diff_graph.scene().items(ev.scenePos())
            vb = self.graph.ui_diffgraph.diff_graph.plotItem.getViewBox()
            mousepoint = vb.mapSceneToView(ev._scenePos)
            key_index = int(mousepoint.x() + 0.5)
            if self.textit is not None:
                self.graph.ui_diffgraph.diff_graph.removeItem(self.textit)
            self.textit = pg.TextItem(self._jobids[key_index], color=(0, 0, 0))
            self.textit.setAnchor((1, 1))
            font = qtg.QFont("Helvetica", 10)
            self.textit.setFont(font)
            if key_index <= len(self._jobids) - 1:
                posx = float(mousepoint.x())
                posy = float(mousepoint.y())
                if posy is not None and posx is not None:
                    self.textit.setPos(posx, posy)
                    self.graph.ui_diffgraph.diff_graph.addItem(self.textit)

    def set_model_tree(self):
        self.treemodel = TreeModel('Parameters', self.tree_dict)
        self.treeview.setModel(self.treemodel)

    def load_tree_dict(self, tree_dict: dict):
        self.tree_dict = tree_dict
        self.set_model_tree()
        self.init_results_database()

    def initialize_tree_dict(self, pcr_dict: dict):
        pattern_phase = self.get_phases_for_patterns(pcr_dict)
        uq_phases = self.get_unique_phases(pattern_phase)
        at_phases = self.get_atoms_for_phases_pcr(pcr_dict)
        self._radiation = self.get_radiation(pcr_dict)

        self.build_tree_dict(uq_phases, at_phases, pcr_dict)
        self.set_model_tree()
        self.init_results_database()

    def build_tree_dict(self, uniques: list, atoms: dict, pcr_dict: dict):
        self.tree_dict = dict()
        self.tree_dict['Sites'] = dict()

        for i in range(len(self._radiation)):
            self.tree_dict[f"Pattern {i}"] = dict()

            if any(rad == 'xrd' or rad == 'cw' for rad in self._radiation[i]):
                self.tree_dict[f"Pattern {i}"][('Rp', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('Rwp', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('Rexp', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('Chi2', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('R-factor', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('zero', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('sycos', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('sysin', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('weight_fractions', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_a', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_b', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_c', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_alpha', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_beta', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_gamma', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_vol', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('scale_factors', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('u_strain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('x_strain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('v', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('y_size', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('g_size', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('boverall', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('asym1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('asym2', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('asym3', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('asym4', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('phzero', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('phsycos', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('phsysin', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('avg_app_size', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('avg_max_strain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)

            if any(rad == 'tof' for rad in self._radiation[i]):
                self.tree_dict[f"Pattern {i}"][('Rp', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('Rwp', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('Rexp', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('Chi2', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('R-factor', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('zero', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('ddt1', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('ddt2', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('ddt1d', qtc.Qt.Unchecked)] = None
                self.tree_dict[f"Pattern {i}"][('weight_fractions', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_a', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_b', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_c', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_alpha', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_beta', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_gamma', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('cell_vol', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('scale_factors', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('sigma-2', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('sigma-1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('sigma-0', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('sigma-q', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('iso-gstrain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('iso-gsize', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('gamma-2', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('gamma-1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('gamma-0', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('iso-lorstrain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('iso-lorsize', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('alpha0', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('alpha1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('alphaq', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('beta0', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('beta1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('betaq', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('boverall', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('avg_app_size', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.tree_dict[f"Pattern {i}"][('avg_max_strain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)

        self._init_strain_anisotropy(uniques, i, pcr_dict)
        self._init_size_anisotropy(uniques, i, pcr_dict)

        self.tree_dict[f"Sites"][('pos_x', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}
        self.tree_dict[f"Sites"][('pos_y', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}
        self.tree_dict[f"Sites"][('pos_z', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}
        self.tree_dict[f"Sites"][('b_iso', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}
        self.tree_dict[f"Sites"][('occupations', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}

    def _init_strain_anisotropy(self, uniques: list, ipatt: int, pcr_dict: dict):
        for phase in uniques:
            laue = next(pcr.get_laue(phase[0], ipatt) for pcr in pcr_dict.values() if phase[0] in pcr.read_ids())
            if any(pcr.get_profile_phase(phase[0], 'straintag', ipatt) != 0 for pcr in pcr_dict.values()):
                if '-1' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 's400 s040 s004 s220 s202 s022 s211 s121 s112 s310 s301 s130 s103 s013 s031'.split()]
                elif '12/m1' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 's400 s040 s004 s220 s202 s022 s121 s301 s103'.split()]
                elif '112/m' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 's400 s040 s004 s220 s202 s022 s112 s310 s130'.split()]
                elif 'mmm' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 's400 s040 s004 s220 s202 s022'.split()]
                elif '4/m' == laue or '4/mmm' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 's400 s040 s220 s202'.split()]
                elif '-3R' == laue or '-3mR' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 's400 s004 s112 s211'.split()]
                elif '-3' == laue or '-3m1' == laue or '-31m' == laue or '6/m' == laue or '6/mmm' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 's400 s004 s112'.split()]
                elif 'm-3' == laue or 'm-3m' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 's400 s220'.split()]
            
                for param in params:
                    if param in self.tree_dict[f"Pattern {ipatt}"]:
                        if phase not in self.tree_dict[f"Pattern {ipatt}"][param]:
                            self.tree_dict[f"Pattern {ipatt}"][param][phase] = None
                    else:
                        self.tree_dict[f"Pattern {ipatt}"] = self.tree_dict[f"Pattern {ipatt}"] | {param: {phase: None}}
                
                if ('lorentzstr', qtc.Qt.Unchecked) in self.tree_dict[f"Pattern {ipatt}"]:
                    if phase not in self.tree_dict[f"Pattern {ipatt}"][('lorentzstr', qtc.Qt.Unchecked)]:
                        self.tree_dict[f"Pattern {ipatt}"][('lorentzstr', qtc.Qt.Unchecked)][phase] = None
                else:
                    self.tree_dict[f"Pattern {ipatt}"] = self.tree_dict[f"Pattern {ipatt}"] | {('lorentzstr', qtc.Qt.Unchecked): {phase: None}}

    def _init_size_anisotropy(self, uniques: list, ipatt: int, pcr_dict: dict):
        for phase in uniques:
            laue = next(pcr.get_laue(phase[0], ipatt) for pcr in pcr_dict.values() if phase[0] in pcr.read_ids())
            if any(pcr.get_profile_phase(phase[0], 'sizetag', ipatt) != 0 for pcr in pcr_dict.values()):
                if '12/m1' == laue or '112/m' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 'y00 y20 y22+ y22- y40 y42+ y42- y44+ y44-'.split()]      
                elif '-3m1' == laue or '-31m' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 'y00 y20 y40 y43- y60 y63- y66+'.split()]
                elif 'm-3' == laue or 'm-3m' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 'k00 k41 k61 k62 k81'.split()]
                elif 'mmm' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 'y00 y20 y22+ y40 y42+ y44+'.split()]
                elif '6/m' == laue or '6/mmm' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 'y00 y20 y40 y60 y66+ y66-'.split()]
                elif '-3' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 'y00 y20 y40 y43- y43+'.split()]
                elif '4/m' == laue or '4/mmm' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 'y00 y20 y40 y44+ y44- y60 y64+ y64-'.split()]
                elif '-1' == laue:
                    params = [(var, qtc.Qt.Unchecked) for var in 'y00 y20 y21+ y21- y22+ y22-'.split()]
            
                for param in params:
                    if param in self.tree_dict[f"Pattern {ipatt}"]:
                        if phase not in self.tree_dict[f"Pattern {ipatt}"][param]:
                            self.tree_dict[f"Pattern {ipatt}"][param][phase] = None
                    else:
                        self.tree_dict[f"Pattern {ipatt}"] = self.tree_dict[f"Pattern {ipatt}"] | {param: {phase: None}}
                
    def init_results_database(self):
        flat_dict = self.flatten(self.tree_dict)
        self.results_storage = dict()
        self._jobids = []
        for key in flat_dict:
            self.results_storage[key] = []

    def fill_results_database(self, path: Union[str, List[str]]):
        try:
            if isinstance(path, list):
                for folder in path:
                    self.fill_results(path=folder)
            else:
                self.fill_results(path=path)
        except ValueError:
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"Failed at reading results:\n\n"
                                 f"Check .sum/.mic files, this is usually a consequence of a bad refinement")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

    def fill_results(self, path: str):
        files_mic = []
        head, tail = os.path.split(path)
        self._jobids.append(tail)
        for file in os.listdir(path):
            if ".pcr" in file:
                file = file.replace(".pcr", "")
                file_pcr = file
            elif ".sum" in file:
                file = file.replace(".sum", "")
                file_sum = file
            elif ".mic" in file:
                file = file.replace(".mic", "")
                files_mic.append(file)

        res_sum = any('.sum' in string for string in os.listdir(path))
        res_mic = any('.mic' in string for string in os.listdir(path))

        if res_sum:
            pcr_object = PcrIO(path=path, jobid=file_pcr)
            pcr_object.read_custom_pcr()
            sum_object = SumIO(path=path, jobid=file_sum)

            fileresults = sum_object.parse_all_info(pcr_object)
            if fileresults is None:
                self.warning = qtw.QMessageBox()
                self.warning.setIcon(qtw.QMessageBox.Critical)
                self.warning.setText(f"Failed at reading .sum file, no outputs will be plotted.\n\n"
                                     f"Check .sum file, this is usually a consequence of a bad refinement")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                self.warning.exec_()
            else:
                for key in self.results_storage.keys():
                    if key in fileresults:
                        self.results_storage[key].append(fileresults[key])
                    else:
                        if ('avg_app_size' == key[1] or 'avg_max_strain' == key[1]) and res_mic:
                            pass
                        else:
                            self.results_storage[key].append(None)

        if res_mic:
            pcr_object = PcrIO(path=path, jobid=file_pcr)
            pcr_object.read_custom_pcr()
            mic_object = MicIO(path=path, jobid=file_pcr)

            for key in self.results_storage.keys():
                if 'avg_app_size' == key[1]:
                    if pcr_object.check_phase_exists(identifier=key[2]):
                        iphas = pcr_object.read_ids().index(key[2]) + 1
                        ipatt = int(key[0].replace('Pattern', "").strip()) + 1
                        if pcr_object.multipattern:
                            size_val = mic_object.read_fromfile(iphas, ipatt)[1]
                        else:
                            size_val = mic_object.read_fromfile(iphas)[1]

                        if size_val is not None:
                            self.results_storage[key].append(size_val)
                        else:
                            self.results_storage[key].append(None)
                    else:
                        self.results_storage[key].append(None)

                elif 'avg_max_strain' == key[1]:
                    if pcr_object.check_phase_exists(identifier=key[2]):
                        iphas = pcr_object.read_ids().index(key[2]) + 1
                        ipatt = int(key[0].replace('Pattern', "").strip()) + 1
                        if pcr_object.multipattern:
                            strain_val = mic_object.read_fromfile(iphas, ipatt)[2]
                        else:
                            strain_val = mic_object.read_fromfile(iphas)[2]

                        if strain_val is not None:
                            self.results_storage[key].append(strain_val)
                        else:
                            self.results_storage[key].append(None)
                    else:
                        self.results_storage[key].append(None)
    
    def sort_results(self, order: List[str]):
        index_order = [self._jobids.index(val) for val in order if val in self._jobids]
        for key in self.results_storage.keys():
            self.results_storage[key] = [self.results_storage[key][i] for i in index_order]

        self._jobids = [self._jobids[i] for i in index_order]

    def flatten(self, dictionary: collections.abc.MutableMapping, parent_key: list = None):
        """
        Turn a nested dictionary into a flattened dictionary
        :param dictionary: The dictionary to flatten
        :param parent_key: The string to prepend to dictionary's keys
        :return: A flattened dictionary
        """

        items = []
        for key, value in dictionary.items():
            if isinstance(key, tuple):
                new_key = parent_key + [key[0]] if parent_key else [key[0]]
            else:
                new_key = parent_key + [key] if parent_key else [key]

            if isinstance(value, collections.abc.MutableMapping):
                items.extend(self.flatten(value, new_key).items())
            elif isinstance(value, list):
                for k, v in enumerate(value):
                    items.extend(self.flatten({str(k): v}, new_key).items())
            else:
                items.append((tuple(new_key), value))
        return dict(items)

    @staticmethod
    def get_phases_for_patterns(pcr_dict: dict):
        pattern_phase = dict()
        for keys, values in pcr_dict.items():
            pattern_phase[keys] = values.read_ids()

        return pattern_phase

    @staticmethod
    def get_radiation(pcr_dict: dict):
        radiation = []
        jobdict = {0: 'xrd', 1: 'cw', -1: 'tof'}
        npatts = []
        for pcr in pcr_dict.values():
            npatt = pcr.get_patt_num()
            npatts.append(npatt)
            radiation.append([jobdict[pcr.get_flags_global('job', ipatt)] for ipatt in range(npatt)])

        nmax = max(npatts)
        corrected = []
        for rad in radiation:
            if len(rad) < nmax:
                rad.extend(['' for _ in range(nmax - len(rad))])

            corrected.append(rad)

        radiation = corrected
        radiationT = list(zip(*radiation))
        radiation = [set(sublist) for sublist in radiationT]

        return radiation

    @staticmethod
    def get_unique_phases(pattern_phase: dict):
        phases = []
        for vals in pattern_phase.values():
            for phase in vals:
                phases.append(phase)

        phases = [(i, qtc.Qt.Unchecked) for n, i in enumerate(phases) if i not in phases[:n]]

        return phases

    @staticmethod
    def get_atoms_for_phases_pcr(pcr_dict: dict):
        phase_atoms = dict()
        for keys, values in pcr_dict.items():
            phases = values.read_ids()
            for phase in phases:
                atoms = values.read_site_labels(phase)
                atoms = atoms if atoms is not None else []
                atoms = {(atom, qtc.Qt.Unchecked): '' for atom in atoms}
                phase_atoms[(phase, qtc.Qt.Unchecked)] = atoms

        non_duplicate_keys = set(phase_atoms)
        phase_atoms = {key: phase_atoms[key] for key in set(phase_atoms) & non_duplicate_keys}

        return phase_atoms

    def export_all(self, filename: str):
        output = dict()
        output['number'] = range(len(self._jobids))
        output['jobid'] = self._jobids
        if self.results_storage:
            for key, val in self.results_storage.items():
                y = [i[0] if i is not None else None for i in val]
                std_y = [i[1] if i is not None else None for i in val]

                key = [key[0].replace(" ", "-")] + list(key[1:])
                output['__'.join(key)] = y
                output['__'.join(key) + 'std'] = std_y

            df = pd.DataFrame(output)
            df.to_csv(filename + 'out.csv', index=False)


    def check_all_tree(self, check):
        idx = self.treeview.rootIndex()
        idx1 = self.treeview.model().index(0, 0, idx)
        check = qtc.Qt.Checked if check else qtc.Qt.Unchecked
        self.do_all_checks(idx, check)

        self.treeview.clicked.emit(idx1)

    def do_all_checks(self, parent, check):
        model = self.treeview.model()
        for row in range(model.rowCount(parent)):
            child = model.index(row, 0, parent)
            if model.itemFromIndex(child).isCheckable():
                model.itemFromIndex(child).setCheckState(check)
                if model.hasChildren(child):
                    self.do_all_checks(child, check)
            else:
                if model.hasChildren(child):
                    self.do_all_checks(child, check)

    def export_data(self):
        name = qtw.QFileDialog.getSaveFileName()
        d = self.fill_dict_from_view()
        flat_d = self.flatten(dictionary=d)

        output = dict()
        output['number'] = range(len(self._jobids))
        output['jobid'] = self._jobids
        if name[0]:
            for key in flat_d.keys():
                if self.results_storage[key]:
                    y = [i[0] if i is not None else None for i in self.results_storage[key]]
                    std_y = [i[1] if i is not None else None for i in self.results_storage[key]]

                    key = [key[0].replace(" ", "-")] + list(key[1:])
                    output['__'.join(key)] = y
                    output['__'.join(key) + 'std'] = std_y

            df = pd.DataFrame(output)

            with open(name[0], 'w') as f:
                f.write(df.to_string(index=False))

    def plot_checked_results(self):
        self.graph.ui_diffgraph.diff_graph.clear()
        self.plot_dict = dict()
        self.error = dict()

        d = self.fill_dict_from_view()
        flat_d = self.flatten(dictionary=d)

        for key in flat_d.keys():
            color = tuple(np.random.choice(range(256), size=3))
            pen = pg.mkPen(color=color, width=1.5)

            if key in self.results_storage:
                x = np.array([i for i in range(len(self.results_storage[key])) if self.results_storage[key][i] is not None])
                y = np.array([i[0] for i in self.results_storage[key] if i is not None])
                std_y = np.array([i[1] for i in self.results_storage[key] if i is not None])

                self.plot_dict[key] = self.graph.ui_diffgraph.diff_graph.plot(x=x, y=y, name=' '.join(key), pen=pen,
                                                                              symbolPen=pen, symbolSize=3.0,
                                                                              symbolBrush=color)

                self.error[key] = pg.ErrorBarItem(x=x, y=y, top=std_y*0.5, bottom=std_y*0.5, beam=0.2, pen=pen)
                self.graph.ui_diffgraph.diff_graph.addItem(self.plot_dict[key])
                self.graph.ui_diffgraph.diff_graph.addItem(self.error[key])

        self.graph.ui_diffgraph.diff_graph.autoRange()

    def update_plots(self):
        d = self.fill_dict_from_view()
        flat_d = self.flatten(dictionary=d)
        for key in flat_d.keys():
            x = np.array([i for i in range(len(self.results_storage[key])) if self.results_storage[key][i] is not None])
            y = np.array([i[0] for i in self.results_storage[key] if i is not None])
            std_y = np.array([i[1] for i in self.results_storage[key] if i is not None])

            self.plot_dict[key].setData(x=x, y=y)
            self.error[key].setData(x=x, y=y, top=std_y * 0.5, bottom=std_y * 0.5, beam=0.2)

        self.graph.ui_diffgraph.diff_graph.autoRange()

    def fill_dict_from_view(self):
        idx = self.treeview.rootIndex()
        d = self.get_dict_checked(idx, d={})

        return d

    def get_dict_all(self):
        if self.treeview.model() is not None:
            idx = self.treeview.rootIndex()
            return self.get_dict_all_params(idx, d={})

    def get_dict_all_params(self, parent, d):
        model = self.treeview.model()
        for row in range(model.rowCount(parent)):
            child = model.index(row, 0, parent)

            if model.itemFromIndex(child).isCheckable():
                state = model.itemFromIndex(child).checkState()
                if model.hasChildren(child):
                    d[(child.data(), state)] = dict()
                    self.get_dict_all_params(child, d[(child.data(), state)])
                else:
                    d[(child.data(), state)] = ''
            else:
                if model.hasChildren(child):
                    d[child.data()] = dict()
                    self.get_dict_all_params(child, d[child.data()])

        return d

    def get_dict_checked(self, parent, d):
        model = self.treeview.model()
        for row in range(model.rowCount(parent)):
            child = model.index(row, 0, parent)
            if model.itemFromIndex(child).isCheckable():
                if model.itemFromIndex(child).checkState() == qtc.Qt.Checked:
                    if model.hasChildren(child):
                        d[child.data()] = dict()
                        self.get_dict_checked(child, d[child.data()])
                    else:
                        d[child.data()] = ''

                elif model.itemFromIndex(child).checkState() == qtc.Qt.PartiallyChecked:
                    d[child.data()] = dict()
                    self.get_dict_checked(child, d[child.data()])

            else:
                if model.hasChildren(child):
                    d[child.data()] = dict()
                    self.get_dict_checked(child, d[child.data()])

        return d

    def do_checks(self, parent):
        model = self.treeview.model()

        if model.hasChildren(parent) and model.itemFromIndex(parent).isCheckable():
            for row in range(model.rowCount(parent)):
                child = model.index(row, 0, parent)
                if model.itemFromIndex(parent).checkState() == qtc.Qt.Checked:
                    model.itemFromIndex(child).setCheckState(qtc.Qt.Checked)

                elif model.itemFromIndex(parent).checkState() == qtc.Qt.Unchecked:
                    model.itemFromIndex(child).setCheckState(qtc.Qt.Unchecked)

                self.do_checks(child)

    def clean_top_checks(self, parent):
        model = self.treeview.model()
        top = model.parent(parent)

        if model.itemFromIndex(top) is not None:
            checks = [0, 0, 0]
            for row in range(model.rowCount(top)):
                child = model.index(row, 0, top)
                if model.itemFromIndex(child).checkState() == qtc.Qt.Checked:
                    checks[0] += 1
                elif model.itemFromIndex(child).checkState() == qtc.Qt.Unchecked:
                    checks[1] += 1
                else:
                    checks[2] += 1

            if checks[0] == model.rowCount(top) and model.itemFromIndex(top).isCheckable():
                model.itemFromIndex(top).setCheckState(qtc.Qt.Checked)
            elif checks[1] == model.rowCount(top) and model.itemFromIndex(top).isCheckable():
                model.itemFromIndex(top).setCheckState(qtc.Qt.Unchecked)
            elif model.itemFromIndex(top).isCheckable():
                model.itemFromIndex(top).setCheckState(qtc.Qt.PartiallyChecked)

            self.clean_top_checks(top)
