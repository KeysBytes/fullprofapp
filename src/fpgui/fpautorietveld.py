import os
import json
import numpy as np
from typing import List, Set
import collections.abc

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

from fpfunctions.app_io import PcrIO
from fpgui.uifiles.uifpautorietveld import Ui_Form
from fpgui.excludetabwidget import ExcludeTabWidget
from fpgui.pcrparamdialog import PcrDialog
from fpgui.restconstdialog import RestraintDialog, ConstraintDialog, DuplicateDialog, LimitsDialog
from fpgui.custommodels import TreeModel

import qtawesome as qta


class AutoRietveld(qtw.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # load form
        self.ui_autorietveld = Ui_Form()
        self.ui_autorietveld.setupUi(self)

        self.excludedtab = ExcludeTabWidget()
        self.ui_autorietveld.verticalLayout.addWidget(self.excludedtab)

        self.pattern_pcrs = dict()
        self.pattern_phase = dict()
        self.parameter_tree = dict()

        self.workflow_commands = {}
        self.workflow_actions = []
        self.workflow_actions.append(qtw.QAction('Load Workflow'))
        self.workflow_actions.append(qtw.QAction('Save Workflow'))
        self.workflow_actions[0].triggered.connect(self.load_workflow)
        self.workflow_actions[1].triggered.connect(self.save_workflow)

        self.restraint_actions = list()
        self.restraint_fields = dict()
        self.restraint_codes = dict()

        self.constraint_actions = list()
        self.constraint_fields = dict()
        self.constraint_codes = dict()

        self.limit_actions = list()
        self.limit_fields = dict()
        self.limit_codes = dict()

        self.lebail_mode = dict()
        self.lebail_const = dict()

        self.warning = qtw.QMessageBox()

        self.pcrparams = None
        self.menu = None
        self.menu_workflow = None
        self._radiation = None

        self.ui_autorietveld.button_run_fp.setEnabled(False)
        self.ui_autorietveld.button_select_step.setEnabled(False)

        # ToolTips
        self.ui_autorietveld.label_7.setToolTip("Relaxation factor of atomic parameters")
        self.ui_autorietveld.rat.setToolTip("Relaxation factor of atomic parameters")
        self.ui_autorietveld.label_8.setToolTip("Relaxation factor of anisotropic displacements")
        self.ui_autorietveld.ran.setToolTip("Relaxation factor of anisotropic displacements")
        self.ui_autorietveld.label_9.setToolTip("Relaxation factor of profile parameters")
        self.ui_autorietveld.rpr.setToolTip("Relaxation factor of profile parameters")
        self.ui_autorietveld.label_10.setToolTip("Relaxation factor of global parameters")
        self.ui_autorietveld.rgl.setToolTip("Relaxation factor of global parameters")

        # Pattern's
        self.ui_autorietveld.button_minus.setIcon(qta.icon('fa5s.minus'))
        self.ui_autorietveld.button_minus.setToolTip('Eliminate current entry')
        self.ui_autorietveld.button_plus.setIcon(qta.icon('fa5s.plus'))
        self.ui_autorietveld.button_plus.setToolTip('Add selection phases to current entry')
        self.ui_autorietveld.button_minusminus.setIcon(qta.icon('fa5s.minus-square'))
        self.ui_autorietveld.button_minusminus.setToolTip('Eliminate ALL entries')
        self.ui_autorietveld.button_plusplus.setIcon(qta.icon('fa5s.plus-square'))
        self.ui_autorietveld.button_plusplus.setToolTip('Add selection phases to ALL entries')

        # Workflow's
        self.ui_autorietveld.button_select_step.setIcon(qta.icon('fa5s.angle-double-right'))

        # Run params
        self.ui_autorietveld.line_ncy.setValidator(qtg.QIntValidator(1, 100))
        self.ui_autorietveld.line_ste.setValidator(qtg.QIntValidator(1, 100))

        self.ui_autorietveld.rat.setValidator(qtg.QDoubleValidator())
        self.ui_autorietveld.ran.setValidator(qtg.QDoubleValidator())
        self.ui_autorietveld.rpr.setValidator(qtg.QDoubleValidator())
        self.ui_autorietveld.rgl.setValidator(qtg.QDoubleValidator())

        self.ui_autorietveld.tree_parameters.customContextMenuRequested.connect(self.show_context_menu)
        self.ui_autorietveld.tree_parameters.clicked.connect(self.do_checks)
        self.ui_autorietveld.tree_parameters.clicked.connect(self.clean_top_checks)
        self.ui_autorietveld.tree_parameters.clicked.connect(self.clean_relations)
        self.ui_autorietveld.tree_parameters.clicked.connect(self.add_size_strain_zero_relations)
        self.ui_autorietveld.tree_parameters.setContextMenuPolicy(qtc.Qt.CustomContextMenu)

        self.ui_autorietveld.tree_parameters.header().setStyleSheet("QHeaderView {font-weight: bold}")
        self.ui_autorietveld.tree_patterns.header().setStyleSheet("QHeaderView {font-weight: bold}")

        self.ui_autorietveld.tree_workflow.keyPressEvent = self.delete_selected_step
        self.ui_autorietveld.tree_workflow.customContextMenuRequested.connect(self.show_workflow_menu)
        self.ui_autorietveld.tree_workflow.setContextMenuPolicy(qtc.Qt.CustomContextMenu)

        self.ui_autorietveld.button_duplicate.clicked.connect(self.duplicate_atoms)
        self.ui_autorietveld.button_select_step.clicked.connect(self.parameter_to_workflow)

        # Background values
        self.cheb_order = qtw.QLineEdit()
        self.cheb_label = qtw.QLabel('Order')
        self.cheb_order.setValidator(qtg.QIntValidator(1, 23))
        self.cheb_order.setText('5')
        self.cycles_fourier = qtw.QLineEdit()
        self.cycles_label = qtw.QLabel('Cycles')
        self.cycles_fourier.setValidator(qtg.QIntValidator())
        self.cycles_fourier.setText('1')
        self.window_fourier = qtw.QLineEdit()
        self.window_label = qtw.QLabel('Window')
        self.window_fourier.setValidator(qtg.QIntValidator())
        self.window_fourier.setText('2000')

        self.cheb_order.setToolTip("Set the order of the Chebyshev polynomial expansion of the background")
        self.cheb_label.setToolTip("Set the order of the Chebyshev polynomial expansion of the background")
        self.window_fourier.setToolTip("Set the window (in number of data points) to perform the filtering (min Npoints/6)")
        self.window_label.setToolTip("Set the window (in number of data points) to perform the filtering (min Npoints/6)")
        self.cycles_fourier.setToolTip("Apply fourier filtering once every input number of cycles")
        self.cycles_label.setToolTip("Apply fourier filtering once every input number of cycles")

        self.cheb_order.hide()
        self.cheb_label.hide()
        self.window_fourier.hide()
        self.window_label.hide()
        self.cycles_fourier.hide()
        self.cycles_label.hide()

        size = len(self.ui_autorietveld.background_layout)
        self.ui_autorietveld.background_layout.insertWidget(size - 1, self.cheb_order)
        self.ui_autorietveld.background_layout.insertWidget(size, self.cheb_label)
        self.ui_autorietveld.background_layout.insertWidget(size + 1, self.window_fourier)
        self.ui_autorietveld.background_layout.insertWidget(size + 2, self.window_label)
        self.ui_autorietveld.background_layout.insertWidget(size + 3, self.cycles_fourier)
        self.ui_autorietveld.background_layout.insertWidget(size + 4, self.cycles_label)

        self.ui_autorietveld.background_combo.currentIndexChanged.connect(self.background_type_change)

        self.ui_autorietveld.load_pcr.clicked.connect(lambda: self.load_pcr())

        # Background menu is fixed
        self.backg_menu = BackgroundMenu(self.ui_autorietveld.tree_patterns)

        # Command dict
        self._text_commands = {'Scale Factors': 'scalecode', 'Zero': 'zerocode', 'SyCos': 'sycoscode',
                               'SySin': 'sysincode', 'Background': 'backgd', 'Cell Parameters': 'cell',
                               'Ustrain': 'ucode', 'Xstrain': 'xcode', 'V': 'vcode', 'Ysize': 'ycode',
                               'Gsize': 'gszcode', 'Boveralls': 'bovcode', 'Sigma-2': 'sig2code',
                               'Sigma-1': 'sig1code', 'Sigma-0': 'sig0code', 'Sigma-Q': 'sigqcode',
                               'Iso-GStrain': 'isogstraincode', 'Iso-GSize': 'isogsizecode',
                               'Gamma-2': 'gam2code', 'Gamma-1': 'gam1code', 'Gamma-0': 'gam0code',
                               'Iso-LorStrain': 'isolstraincode', 'Iso-LorSize': 'isolsizecode',
                               'Alpha0': 'alpha0code', 'Alpha1': 'alpha1code', 'AlphaQ': 'alphaqcode',
                               'Beta0': 'beta0code', 'Beta1': 'beta1code', 'BetaQ': 'betaqcode',
                               'Asym1': 'asy1code', 'Asym2': 'asy2code', 'Asym3': 'asy3code', 'Asym4': 'asy4code',
                               'X': 'xcode', 'Y': 'ycode', 'Z': 'zcode', 'Biso': 'bisocode',
                               'Occupations': 'occcode', 'Phase Zero': 'zerophcode', 'Phase SyCos': 'sycosphcode',
                               'Phase SySin': 'sysinphcode', 'S_400': 's400code', 'S_040': 's040code', 'S_004': 's004code',
                               'S_220': 's220code', 'S_202': 's202code', 'S_022': 's002code', 'S_211': 's211code',
                               'S_121': 's121code', 'S_112': 's112code', 'S_310': 's310code', 'S_301': 's301code',
                               'S_130': 's130code', 'S_103': 's103code', 'S_013': 's013code', 'S_031': 's031code',
                               'Lorentzian Strain': 'lorentzstrcode',
                               'Y00': 'y00code', 'Y22+': 'y22+code', 'Y22-': 'y22-code', 'Y20': 'y20code', 'Y44+': 'y44+code',
                               'Y44-': 'y44-code', 'Y42+': 'y42+code', 'Y42-': 'y42-code', 'Y40': 'y40code', 'Y43-': 'y43-code',
                               'Y60': 'y60code', 'Y63-': 'y63-code', 'Y66+': 'y66+code', 'Y66-': 'y66-code', 'K00': 'k00code',
                               'K41': 'k41code', 'K61': 'k61code', 'K62': 'k62code', 'K81': 'k81code', 'Y64+': 'y64+code',
                               'Y64-': 'y64-code', 'Y21+': 'y21+code', 'Y21-': 'y21-code', 'Y43+': 'y43+code'}

    def set_radiation(self, parameter_info: List[str]):
        self._radiation = parameter_info

    def duplicate_atoms(self):
        valid = self.check_valid_duplicate()
        if valid:
            model = self.ui_autorietveld.tree_parameters.model()
            index = self.ui_autorietveld.tree_parameters.selectedIndexes()

            names = []
            for ix in index:
                top_name = model.parent(ix).data()
                names.append((top_name, ix.data()))

            names = list(set(names))
            dialog = DuplicateDialog(names=names)
            result = dialog.exec()

            if result == qtw.QDialog.Accepted:
                new_sites = dialog.get_outputs()

                for pattern in self.pattern_pcrs.keys():
                    for site, new in new_sites.items():
                        if self.pattern_pcrs[pattern].check_phase_exists(site[0]):
                            self.pattern_pcrs[pattern].add_site_phase(identifier=site[0], atom=new[0])
                            self.pattern_pcrs[pattern].remove_anisotropic_site(identifier=site[0], atom=new[0])

                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], new[1], 'type')

                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'x')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'x')
                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'y')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'y')
                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'z')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'z')
                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'biso')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'biso')
                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'occ')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'occ')
                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'nt')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'nt')

                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'xcode')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'xcode')
                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'ycode')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'ycode')
                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'zcode')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'zcode')
                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'bisocode')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'bisocode')
                            val = self.pattern_pcrs[pattern].get_site_phase(site[0], site[1], 'occcode')
                            self.pattern_pcrs[pattern].edit_site_phase(site[0], new[0], val, 'occcode')

            self.update_parameters_phases()

    def check_valid_duplicate(self):
        model = self.ui_autorietveld.tree_parameters.model()
        index = self.ui_autorietveld.tree_parameters.selectedIndexes()

        names = []
        for ix in index:
            top_name = model.parent(ix).data()
            names.append(top_name)

        if not names:
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText("Atom selection is empty")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)

            self.warning.exec_()
            return False

        phases = []
        for pattern, pcr in self.pattern_pcrs.items():
            phases = phases + pcr.read_ids()

        """ Check if selected parents are in the list """
        for name in names:
            if name not in phases:
                self.warning.setIcon(qtw.QMessageBox.Critical)
                self.warning.setText("Some Tree selections are not atoms")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                self.warning.exec_()
                return False

        return True
    
    def include_save_menu(self):
        if not self.workflow_commands:
            return False
        else:
            return True
    
    def include_load_menu(self):
        if self.ui_autorietveld.button_select_step.isEnabled():
            return True
        else:
            return False
        
    def show_workflow_menu(self, position):
        self.menu_workflow = qtw.QMenu(self.ui_autorietveld.tree_workflow)
        resload = self.include_load_menu()
        ressave = self.include_save_menu()

        if resload:
            self.menu_workflow.addAction(self.workflow_actions[0])
        if ressave:
            self.menu_workflow.addAction(self.workflow_actions[1])

        self.menu_workflow.exec_(self.ui_autorietveld.tree_workflow.mapToGlobal(position))

    def check_valid_loading_workflow(self, workflow):
        error_phase_not_present = []
        error_atom_label_not_present = []
        flat_workflow = self.flatten_keys(workflow)

        for value in flat_workflow.values():

            # Check if there are phases in workflow file that do not exist in PCR
            if not any(value[2] in pcr.read_ids() for pcr in self.pattern_pcrs.values()) and value[2]:
                error_phase_not_present.append(value[2])
                continue

            # Check if, for existing phases there are crystal labels that do not coindide in PCR
            # TODO: IMPROVE LONG EXPRESSION FOR READING SITE LABELS (IT ORIGINALLY RETURNS 'NONE', MAYBE RETURN [])?
            if value[0] == 'Sites' and value[3] and any(value[2] == phase and not value[3] in (pcr.read_site_labels(phase) if pcr.read_site_labels(phase) is not None else []) for pcr in self.pattern_pcrs.values() for phase in pcr.read_ids()):
                error_atom_label_not_present.append((value[2], value[3]))
                continue

        if error_phase_not_present:
            result = ''.join([f"- {file}\n" for file in set(error_phase_not_present)])
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"The following phases appearing on the workflow file do not coincide with any of the phases that are currently loaded:\n\n"
                                 f"{result} \n"
                                 f"Cannot load the workflow file."
                                 )
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()
            return False
        
        if error_atom_label_not_present:
            result = ''.join([f"- {file}\n" for file in [' -> '.join(val) for val in set(error_atom_label_not_present)]])
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"The following commands in the workflow file target crystal site labels that do not coincide with any of the site labels that are currently loaded:\n\n"
                                 f"{result} \n"
                                 f"Cannot load the workflow file."
                                 )
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()
            return False
        
        # Check that number of phases in workflow is the same, give warning if PCR has more phases
        uq_phases_workflow = set([value[2] for value in flat_workflow.values() if value[2]])
        uq_phases_loaded = set([phase for pcr in self.pattern_pcrs.values() for phase in pcr.read_ids()])
        if len(uq_phases_workflow) < len(uq_phases_loaded):
            result_workflow = ''.join([f"- {file}\n" for file in uq_phases_workflow])
            result_loaded = ''.join([f"- {file}\n" for file in uq_phases_loaded])
            self.warning.setIcon(qtw.QMessageBox.Warning)
            self.warning.setText(f"There are more phases loaded than phases included in the workflow. These are the loaded phases:\n\n{result_loaded} \nAnd these are the phases included in the workflow:\n\n{result_workflow} \n"
                                 f"The refinement of any parameter that is not included in the workflow will happen according what is originally set in the underlying PCR files.\n\n"
                                 f"This is risky, do you want to load the workflow?"
                                 )
            self.warning.setStandardButtons(qtw.QMessageBox.Yes | qtw.QMessageBox.No)
            res = self.warning.exec_()

            if res == qtw.QMessageBox.Yes:
                return True
            else:
                return False
            
        # Check that for each coinciding phase, if PCR has more sites
        loaded_sites_for_phases = {phase: (set(pcr.read_site_labels(phase)) if pcr.read_site_labels(phase) is not None else set()) for pcr in self.pattern_pcrs.values() for phase in pcr.read_ids()}
        workflow_sites_for_phases = {phase: set() for pcr in self.pattern_pcrs.values() for phase in pcr.read_ids()}
        for value in flat_workflow.values():
            if value[0] == 'Sites' and value[3]:
                workflow_sites_for_phases[value[2]].add(value[3])

        differences = {k: v - workflow_sites_for_phases[k] for k, v in loaded_sites_for_phases.items() if v - workflow_sites_for_phases[k]}
        if differences:
            result = ''.join([f"- {k} -> {value}\n" for k, v in differences.items() for value in v])
            self.warning.setIcon(qtw.QMessageBox.Warning)
            self.warning.setText(f"These sites appear in the loaded PCR files but not in the workflow.\n\n{result} \n"
                                 f"The refinement of any parameter that is not included in the workflow will happen according what is originally set in the underlying PCR files.\n\n"
                                 f"This is risky, do you want to load the workflow?"
                                 )
            
            self.warning.setStandardButtons(qtw.QMessageBox.Yes | qtw.QMessageBox.No)
            res = self.warning.exec_()

            if res == qtw.QMessageBox.Yes:
                return True
            else:
                return False

        return True

    def load_workflow(self):
        filename = qtw.QFileDialog.getOpenFileName(self, 'Load Workflow File', os.getcwd(), 'JSON text files (*.json)')
        filename = os.path.normpath(filename[0])

        try:
            with open(filename) as f:
                text = json.loads(f.read())
            res = self.check_valid_loading_workflow(text)
        except Exception as e:
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"Something went wrong when reading the workflow file")
            self.warning.setInformativeText(f"{e}")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()
            return

        if res:
            self.workflow_commands = text
            self.set_model_workflow()

    def save_workflow(self):
        name = qtw.QFileDialog.getSaveFileName(self, 'Save Workflow File', '', "JSON text files (*.json)")
        name = os.path.normpath(name[0])
        with open(name, 'w') as f:
            json_string = json.dumps(self.workflow_commands, indent=2)
            f.write(json_string)

    def show_context_menu(self, position):
        self.menu = qtw.QMenu(self.ui_autorietveld.tree_parameters)
        res_constr = self.include_constraint_menu()
        res_restr = self.include_restraint_menu()
        res_limit = self.include_limit_menu()

        if res_restr:
            if self.restraint_actions:
                for action in self.restraint_actions:
                    self.menu.addAction(action)
            else:
                self.restraint_actions.append(qtw.QAction('New Linear Restraint'))
                self.restraint_actions[0].triggered.connect(self.new_restraint)
                self.menu.addAction(self.restraint_actions[0])

            self.menu.addSeparator()

        if res_constr:
            if self.constraint_actions:
                for action in self.constraint_actions:
                    self.menu.addAction(action)
            else:
                self.constraint_actions.append(qtw.QAction('New Constraint'))
                self.constraint_actions[0].triggered.connect(lambda: self.new_constraint(res_constr))
                self.menu.addAction(self.constraint_actions[0])

            self.menu.addSeparator()
            #self.menu.addAction(qtw.QAction('New Constraint'))

        if res_limit:
            if self.limit_actions:
                for action in self.limit_actions:
                    self.menu.addAction(action)
            else:
                self.limit_actions.append(qtw.QAction('New Limit'))
                self.limit_actions[0].triggered.connect(self.new_limit)
                self.menu.addAction(self.limit_actions[0])

            self.menu.addSeparator()

        self.menu.hovered.connect(self.hover_actions)

        res = self.include_lebail_menu()
        if res:
            self.menu.addSeparator()
            ixs = self.ui_autorietveld.tree_parameters.selectedIndexes()
            for ix in ixs:
                if ix.data() in self.lebail_mode:
                    self.menu.addAction(self.lebail_mode[ix.data()])
                    self.menu.addAction(self.lebail_const[ix.data()])
                else:
                    self.lebail_mode[ix.data()] = qtw.QAction('Profile Matching (Constant Scale Factor)')
                    self.lebail_mode[ix.data()].setObjectName(f'lebail____{ix.data()}')
                    self.lebail_mode[ix.data()].setCheckable(True)
                    self.lebail_mode[ix.data()].triggered.connect(self.check_lebail_modes)
                    self.menu.addAction(self.lebail_mode[ix.data()])
                    self.lebail_const[ix.data()] = qtw.QAction('Profile Matching (Constant Relative Intensities)')
                    self.lebail_const[ix.data()].setObjectName(f'const____{ix.data()}')
                    self.lebail_const[ix.data()].setCheckable(True)
                    self.lebail_const[ix.data()].triggered.connect(self.check_lebail_modes)
                    self.menu.addAction(self.lebail_const[ix.data()])

        self.menu.exec_(self.ui_autorietveld.tree_parameters.mapToGlobal(position))

    def check_lebail_modes(self):
        if 'lebail' in self.sender().objectName():
            idx = self.sender().objectName().split('____')[1]
            if self.lebail_mode[idx].isChecked() and self.lebail_const[idx].isChecked():
                self.lebail_const[idx].setChecked(False)

        elif 'const' in self.sender().objectName():
            idx = self.sender().objectName().split('____')[1]
            if self.lebail_mode[idx].isChecked() and self.lebail_const[idx].isChecked():
                self.lebail_mode[idx].setChecked(False)

    def get_selected_topnames(self):
        ixs = self.ui_autorietveld.tree_patterns.selectedIndexes()
        return list(set([self.get_pattern_top_names(ix) for ix in ixs]))

    def include_constraint_menu(self):
        model = self.ui_autorietveld.tree_parameters.model()
        ixs = self.ui_autorietveld.tree_parameters.selectedIndexes()

        if len(ixs) == 1: return False
        if any(model.itemFromIndex(ix).checkState() != qtc.Qt.Checked for ix in ixs): return False
        if any(model.hasChildren(ix) for ix in ixs): return False

        path_to_top = [list(reversed(self.go_to_top([], ix))) for ix in ixs]
        if any([val[1] == 'Cell Parameters' for val in path_to_top]):
            """ Cell Params are selected to be related """
            if not all([val[1] == 'Cell Parameters' for val in path_to_top]):
                """ Not all selections are Cell Parameters """
                return False
            else:
                if len(set([val[2] for val in path_to_top])) == 1:
                    return 'PEQU'
                else:
                    return False
        else:
            return True

    def include_limit_menu(self):
        model = self.ui_autorietveld.tree_parameters.model()
        ixs = self.ui_autorietveld.tree_parameters.selectedIndexes()

        if any(model.itemFromIndex(ix).checkState() != qtc.Qt.Checked for ix in ixs): return False
        if any(model.hasChildren(ix) for ix in ixs): return False

        return True

    def include_restraint_menu(self):
        model = self.ui_autorietveld.tree_parameters.model()
        ixs = self.ui_autorietveld.tree_parameters.selectedIndexes()

        if len(ixs) == 1: return False
        if any(model.itemFromIndex(ix).checkState() != qtc.Qt.Checked for ix in ixs): return False
        if any(model.hasChildren(ix) for ix in ixs): return False

        path_to_top = [list(reversed(self.go_to_top([], ix))) for ix in ixs]
        if any([val[1] == 'Cell Parameters' for val in path_to_top]):
            return False
        else:
            return True

    def include_lebail_menu(self):
        ixs = self.ui_autorietveld.tree_parameters.selectedIndexes()

        if len(ixs) != 1:
            return False

        data = [ix.data() for ix in ixs]
        uqphases = [item[0] for item in self.get_unique_phases(self.pattern_phase)]

        if all(item in uqphases for item in data):
            return True
        else:
            return False

    def raise_lebail_warning(self, workflow: dict):
        error_commands = []
        for key, val in self.lebail_mode.items():
            if val.isChecked():
                for command in workflow['VARY'].values():
                    if 'Scale Factors' == command[1] and command[2] in key:
                        error_commands.append('Scale Factors')
                    elif 'Biso' == command[1] and command[2] in key:
                        error_commands.append('Biso')
                    elif 'X' == command[1] and command[2] in key:
                        error_commands.append('X')
                    elif 'Y' == command[1] and command[2] in key:
                        error_commands.append('Y')
                    elif 'Z' == command[1] and command[2] in key:
                        error_commands.append('Z')
                    elif 'Occupations' == command[1] and command[2] in key:
                        error_commands.append('Occupations')

        for key, val in self.lebail_const.items():
            if val.isChecked():
                for command in workflow['VARY'].values():
                    if 'Biso' == command[1] and command[2] in key:
                        error_commands.append('Biso')
                    elif 'X' == command[1] and command[2] in key:
                        error_commands.append('X')
                    elif 'Y' == command[1] and command[2] in key:
                        error_commands.append('Y')
                    elif 'Z' == command[1] and command[2] in key:
                        error_commands.append('Z')
                    elif 'Occupations' == command[1] and command[2] in key:
                        error_commands.append('Occupations')

        if error_commands:
            result = ' - \n'.join(set(error_commands))
            self.warning.setIcon(qtw.QMessageBox.Warning)
            self.warning.setText(f"Trying to refine the following parameters in Profile Matching mode:\n"
                                 f"{result} \n"
                                 f"Do you want to continue?"
                                 )
            self.warning.setStandardButtons(qtw.QMessageBox.Yes | qtw.QMessageBox.Cancel)

            res = self.warning.exec_()

            if res == qtw.QMessageBox.Yes:
                return True
            else:
                return False

        else:
            return True

    def new_limit(self):
        model = self.ui_autorietveld.tree_parameters.model()
        index = self.ui_autorietveld.tree_parameters.selectedIndexes()

        dialog = LimitsDialog(model=model, index=index)
        result = dialog.exec()
        if result == qtw.QDialog.Accepted:
            key = f'Parameter Limit {len(self.limit_actions)}'
            action = qtw.QAction(key)
            self.limit_actions.append(action)
            self.limit_fields[key] = dialog.get_outputs()
            res = self.check_valid_limits()

            if not res:
                index = self.limit_actions.index(action)
                self.limit_actions.pop(index)
                self.limit_fields.pop(action.text())
    
    def new_constraint(self, mode):
        model = self.ui_autorietveld.tree_parameters.model()
        index = self.ui_autorietveld.tree_parameters.selectedIndexes()

        if mode == 'PEQU':
            key = f'Constraint {len(self.constraint_actions)}'
            action = qtw.QAction(key)
            self.constraint_actions.append(action)

            path_to_top = [tuple(reversed(self.go_to_top([], ix))) for ix in index][0]
            self.constraint_fields[key] = {path_to_top: 'PEQU'}

        else:
            dialog = ConstraintDialog(model=model, index=index)
            result = dialog.exec()
            if result == qtw.QDialog.Accepted:
                key = f'Constraint {len(self.constraint_actions)}'
                action = qtw.QAction(key)
                self.constraint_actions.append(action)
                self.constraint_fields[key] = dialog.get_outputs()
                res = self.check_valid_constrains()

                if not res:
                    index = self.constraint_actions.index(action)
                    self.constraint_actions.pop(index)
                    self.constraint_fields.pop(action.text())

    def new_restraint(self):
        model = self.ui_autorietveld.tree_parameters.model()
        index = self.ui_autorietveld.tree_parameters.selectedIndexes()

        dialog = RestraintDialog(model=model, index=index)
        result = dialog.exec()
        if result == qtw.QDialog.Accepted:
            key = f'Linear Restraint {len(self.restraint_actions)}'
            self.restraint_actions.append(qtw.QAction(key))
            self.restraint_fields[key] = dialog.get_outputs()

    def check_valid_limits(self):
        paths = []
        for value in self.limit_fields.values():
            for key in value.keys():
                paths.append(key)

        if len(paths) != len(set(paths)):
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText("A single parameter has more than one limit, set unique limits for parameters")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)

            self.warning.exec_()

            return False
        else:
            return True

    def check_valid_constrains(self):
        paths = []
        for value in self.constraint_fields.values():
            for key in value.keys():
                paths.append(key)

        if len(paths) != len(set(paths)):
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText("A single parameter has more than one constrained relation, set constraints with "
                                 "unique parameter sets.")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)

            self.warning.exec_()

            return False
        else:
            return True

    def generate_constraint_codes(self):
        self.constraint_codes = dict()
        if self.constraint_fields:
            code = 299
            for key, value in self.constraint_fields.items():
                self.constraint_codes[key] = dict()

                for tree, coefficient in value.items():
                    if coefficient == 'PEQU':
                        self.constraint_codes[key][tree] = coefficient
                    else:
                        reformat = f"{float(coefficient):.4f}"
                        self.constraint_codes[key][tree] = (str(code), reformat)

                code -= 1

            return True

        else:
            return False

    def generate_restraint_codes(self):
        self.restraint_codes = dict()
        if self.restraint_fields:
            constraint_codes = {}
            if self.constraint_codes:
                for key, val in self.constraint_codes.items():
                    for tree, code in val.items():
                        constraint_codes[tree] = code[0]
                code = min(int(val) for val in constraint_codes.values())
            else:
                code = 299

            restraint_codes = {}
            for key, val in self.restraint_fields.items():
                self.restraint_codes[key] = dict()
                for tree, fields in val.items():
                    if isinstance(tree, tuple):
                        if constraint_codes.get(tree) is not None:
                            self.restraint_codes[key][tree] = (constraint_codes[tree], f"{float(fields):.4f}")
                        elif restraint_codes.get(tree) is not None:
                            self.restraint_codes[key][tree] = (restraint_codes[tree], f"{float(fields):.4f}")
                        else:
                            code -= 1
                            restraint_codes[tree] = code
                            self.restraint_codes[key][tree] = (code, f"{float(fields):.4f}")
                    else:
                        self.restraint_codes[key][tree] = f"{float(fields):.5f}"
            return True

        else:
            return False

    def generate_limit_codes(self):
        self.limit_codes = dict()

        if self.limit_actions:
            restraint_codes = {}
            constraint_codes = {}
            if self.restraint_codes:
                for key, val in self.restraint_codes.items():
                    for tree, code in val.items():
                        if isinstance(tree, tuple):
                            restraint_codes[tree] = code[0]
            if self.constraint_codes:
                for key, val in self.constraint_codes.items():
                    for tree, code in val.items():
                        constraint_codes[tree] = code[0]

            if restraint_codes:
                code = min(int(val) for val in restraint_codes.values())
            elif constraint_codes:
                code = min(int(val) for val in constraint_codes.values())
            else:
                code = 299

            limit_codes = {}
            for key, val in self.limit_fields.items():
                self.limit_codes[key] = dict()
                for tree, fields in val.items():
                    if restraint_codes.get(tree) is not None:
                        self.limit_codes[key][tree] = (restraint_codes[tree], f"{float(fields[-1]):.4f}")
                    elif constraint_codes.get(tree) is not None:
                        self.limit_codes[key][tree] = (constraint_codes[tree], f"{float(fields[-1]):.4f}")
                    elif limit_codes.get(tree) is not None:
                        self.limit_codes[key][tree] = (limit_codes[tree], f"{float(fields[-1]):.4f}")
                    else:
                        code -= 1
                        limit_codes[tree] = code
                        self.limit_codes[key][tree] = (code, f"{float(fields[-1]):.4f}")

            return True

        else:
            return False

    def clean_relations(self):
        idx = self.ui_autorietveld.tree_parameters.rootIndex()
        fix = self.flatten(self.get_dict_unchecked(idx, d={}))

        if self.constraint_fields:
            constraints = self.constraint_fields.copy()
            for key, val in constraints.items():
                for tree in val.keys():
                    if tree in [fixed for fixed in fix.keys()]:
                        self.constraint_fields.pop(key, None)
                        action = [act for act in self.constraint_actions if act.text() == key][0]
                        index = self.constraint_actions.index(action)
                        self.constraint_actions.pop(index)

            self.reorder_keys()

        if self.restraint_fields:
            restraints = self.restraint_fields.copy()
            for key, val in restraints.items():
                for tree in val.keys():
                    if isinstance(tree, tuple) and tree in [fixed for fixed in fix.keys()]:
                        self.restraint_fields.pop(key, None)
                        action = [act for act in self.restraint_actions if act.text() == key][0]
                        index = self.restraint_actions.index(action)
                        self.restraint_actions.pop(index)

            self.reorder_keys()

        if self.limit_fields:
            limits = self.limit_fields.copy()
            for key, val in limits.items():
                for tree in val.keys():
                    if tree in [fixed for fixed in fix.keys()]:
                        self.limit_fields.pop(key, None)
                        action = [act for act in self.limit_actions if act.text() == key][0]
                        index = self.limit_actions.index(action)
                        self.limit_actions.pop(index)

            self.reorder_keys()

    def hover_actions(self, action):
        self.menu.keyPressEvent = lambda ev: self.delete_items(action, ev)

    def delete_items(self, action, ev):
        if ev.key() == qtc.Qt.Key_Delete and 'New' not in action.text():
            if 'Restraint' in action.text():
                index = self.restraint_actions.index(action)
                self.restraint_actions.pop(index)
                self.restraint_fields.pop(action.text())
                self.menu.removeAction(action)
            elif 'Constraint' in action.text():
                index = self.constraint_actions.index(action)
                self.constraint_actions.pop(index)
                self.constraint_fields.pop(action.text())
                self.menu.removeAction(action)
            elif 'Limit' in action.text():
                index = self.limit_actions.index(action)
                self.limit_actions.pop(index)
                self.limit_fields.pop(action.text())
                self.menu.removeAction(action)

            self.reorder_keys()

    def delete_selected_step(self, ev):
        if ev.key() == qtc.Qt.Key_Delete and len(self.ui_autorietveld.tree_workflow.selectedIndexes()) != 0:
            model = self.ui_autorietveld.tree_workflow.model()
            index = self.ui_autorietveld.tree_workflow.selectedIndexes()

            indexes = [idx for idx in index if idx.data() is not None]
            for ix in indexes:
                top = model.parent(ix)
                if model.itemFromIndex(top) is None and 'Step ' in ix.data():
                    self.workflow_commands.pop(ix.data(), None)

            self.reorder_workflow_keys()
            self.set_model_workflow()

    def reorder_workflow_keys(self):
        new_workflow = dict()
        count = 0
        for vals in self.workflow_commands.values():
            new_workflow[f"Step {count}"] = vals
            count += 1

        self.workflow_commands = new_workflow

    def reorder_keys(self):
        new_restraint_actions = []
        new_restraint_fields = dict()
        new_constraint_actions = []
        new_constraint_fields = dict()
        new_limit_actions = []
        new_limit_fields = dict()

        count = 1
        for action in self.restraint_actions[1:]:
            action.setText(f"Linear Restraint {count}")
            new_restraint_actions.append(action)
            count += 1

        self.restraint_actions = [self.restraint_actions[0]] + new_restraint_actions if self.restraint_actions else []

        count = 1
        for val in self.restraint_fields.values():
            new_restraint_fields[f"Linear Restraint {count}"] = val
            count += 1

        self.restraint_fields = new_restraint_fields if self.restraint_fields else {}

        count = 1
        for action in self.constraint_actions[1:]:
            action.setText(f"Constraint {count}")
            new_constraint_actions.append(action)
            count += 1

        self.constraint_actions = [self.constraint_actions[0]] + new_constraint_actions if self.constraint_actions else []

        count = 1
        for val in self.constraint_fields.values():
            new_constraint_fields[f"Constraint {count}"] = val
            count += 1

        self.constraint_fields = new_constraint_fields if self.constraint_fields else {}

        count = 1
        for action in self.limit_actions[1:]:
            action.setText(f"Parameter Limit {count}")
            new_limit_actions.append(action)
            count += 1

        self.limit_actions = [self.limit_actions[0]] + new_limit_actions if self.limit_actions else []

        count = 1
        for val in self.limit_fields.values():
            new_limit_fields[f"Parameter Limit {count}"] = val
            count += 1

        self.limit_fields = new_limit_fields if self.limit_fields else {}

    def init_parameters(self, uniques: list, atoms: dict):
        self.parameter_tree = dict()
        self.parameter_tree['Sites'] = dict()

        for i in range(len(self._radiation)):
            self.parameter_tree[f"Pattern {i}"] = dict()

            if self._radiation[i] == 'xrd' or self._radiation[i] == 'cw':
                self.parameter_tree[f"Pattern {i}"][('Scale Factors', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Zero', qtc.Qt.Unchecked)] = None
                self.parameter_tree[f"Pattern {i}"][('SyCos', qtc.Qt.Unchecked)] = None
                self.parameter_tree[f"Pattern {i}"][('SySin', qtc.Qt.Unchecked)] = None
                self.parameter_tree[f"Pattern {i}"][('Background', qtc.Qt.Unchecked)] = None
                self.parameter_tree[f"Pattern {i}"][('Cell Parameters', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Ustrain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Xstrain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('V', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Ysize', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Gsize', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Boveralls', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Asym1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Asym2', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Asym3', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Asym4', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Phase Zero', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Phase SyCos', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Phase SySin', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)

            elif self._radiation[i] == 'tof':
                self.parameter_tree[f"Pattern {i}"][('Scale Factors', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Zero', qtc.Qt.Unchecked)] = None
                self.parameter_tree[f"Pattern {i}"][('Ddt1', qtc.Qt.Unchecked)] = None
                self.parameter_tree[f"Pattern {i}"][('Ddt2', qtc.Qt.Unchecked)] = None
                self.parameter_tree[f"Pattern {i}"][('Ddt1d', qtc.Qt.Unchecked)] = None
                self.parameter_tree[f"Pattern {i}"][('Background', qtc.Qt.Unchecked)] = None
                self.parameter_tree[f"Pattern {i}"][('Cell Parameters', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Sigma-2', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Sigma-1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Sigma-0', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Sigma-Q', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Iso-GStrain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Iso-GSize', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Gamma-2', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Gamma-1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Gamma-0', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Iso-LorStrain', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Iso-LorSize', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Alpha0', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Beta0', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Alpha1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Beta1', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('AlphaQ', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('BetaQ', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
                self.parameter_tree[f"Pattern {i}"][('Boveralls', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)

            self._init_strain_anisotropy(uniques, i)
            self._init_size_anisotropy(uniques, i)

        self.parameter_tree[f"Sites"][('X', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}
        self.parameter_tree[f"Sites"][('Y', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}
        self.parameter_tree[f"Sites"][('Z', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}
        self.parameter_tree[f"Sites"][('Biso', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}
        self.parameter_tree[f"Sites"][('Occupations', qtc.Qt.Unchecked)] = {k: atoms[k] for k in dict.fromkeys(uniques) if k in atoms}

    def _init_strain_anisotropy(self, uniques: list, ipatt: int):
        self.parameter_tree[f"Pattern {ipatt}"][('Strain Anisotropy', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
        for phase in uniques:
            laue = next(pcr.get_laue(phase[0], ipatt) for pcr in self.pattern_pcrs.values() if phase[0] in pcr.read_ids())
            if '-1' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'S_400 S_040 S_004 S_220 S_202 S_022 S_211 S_121 S_112 S_310 S_301 S_130 S_103 S_013 S_031'.split()]
            elif '12/m1' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'S_400 S_040 S_004 S_220 S_202 S_022 S_121 S_301 S_103'.split()]
            elif '112/m' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'S_400 S_040 S_004 S_220 S_202 S_022 S_112 S_310 S_130'.split()]
            elif 'mmm' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'S_400 S_040 S_004 S_220 S_202 S_022'.split()]
            elif '4/m' == laue or '4/mmm' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'S_400 S_004 S_220 S_202'.split()]
            elif '-3R' == laue or '-3mR' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'S_400 S_004 S_112 S_211'.split()]
            elif '-3' == laue or '-3m1' == laue or '-31m' == laue or '6/m' == laue or '6/mmm' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'S_400 S_004 S_112'.split()]
            elif 'm-3' == laue or 'm-3m' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'S_400 S_220'.split()]

            self.parameter_tree[f"Pattern {ipatt}"][('Strain Anisotropy', qtc.Qt.Unchecked)][phase] = params
            self.parameter_tree[f"Pattern {ipatt}"][('Strain Anisotropy', qtc.Qt.Unchecked)][phase].extend([('Lorentzian Strain', qtc.Qt.Unchecked)])
                
    def _init_size_anisotropy(self, uniques: list, ipatt: int):
        self.parameter_tree[f"Pattern {ipatt}"][('Size Anisotropy', qtc.Qt.Unchecked)] = dict.fromkeys(uniques)
        for phase in uniques:
            laue = next(pcr.get_laue(phase[0], ipatt) for pcr in self.pattern_pcrs.values() if phase[0] in pcr.read_ids())
            if '12/m1' == laue or '112/m' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'Y00 Y20 Y22+ Y22- Y40 Y42+ Y42- Y44+ Y44-'.split()]      
            elif '-3m1' == laue or '-31m' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'Y00 Y20 Y40 Y43- Y60 Y63- Y66+'.split()]
            elif 'm-3' == laue or 'm-3m' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'K00 K41 K61 K62 K81'.split()]
            elif 'mmm' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'Y00 Y20 Y22+ Y40 Y42+ Y44+'.split()]
            elif '6/m' == laue or '6/mmm' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'Y00 Y20 Y40 Y60 Y66+ Y66-'.split()]
            elif '-3' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'Y00 Y20 Y40 Y43- Y43+'.split()]
            elif '4/m' == laue or '4/mmm' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'Y00 Y20 Y40 Y44+ Y44- Y60 Y64+ Y64-'.split()]
            elif '-1' == laue:
                params = [(var, qtc.Qt.Unchecked) for var in 'Y00 Y20 Y21+ Y21- Y22+ Y22-'.split()]
            
            if params:
                self.parameter_tree[f"Pattern {ipatt}"][('Size Anisotropy', qtc.Qt.Unchecked)][phase] = params

    def load_pcr(self):
        combo_count = self.ui_autorietveld.pattern_combo.count()
        patterns = [self.ui_autorietveld.pattern_combo.itemText(i) for i in range(combo_count)]
        self.pcrparams = PcrDialog(patterns=patterns)
        self.pcrparams.accepted.connect(lambda: self.build_pcr_objects())

        self.pcrparams.exec_()

    def reset_all(self):
        self.pattern_pcrs = dict()
        self.pattern_phase = dict()
        self.parameter_tree = dict()

        self.workflow_commands = {}

        self.restraint_actions = list()
        self.restraint_fields = dict()
        self.restraint_codes = dict()

        self.constraint_actions = list()
        self.constraint_fields = dict()
        self.constraint_codes = dict()

        self.limit_actions = list()
        self.limit_fields = dict()
        self.limit_codes = dict()

        self.lebail_mode = dict()
        self.lebail_const = dict()

        self.set_model_patterns()
        self.set_model_parameters()
        self.set_model_workflow()
        self.enable_duplicate_button()

    def enable_duplicate_button(self):
        count = self.ui_autorietveld.pattern_combo.count()
        combo = self.ui_autorietveld.pattern_combo
        if self.pattern_pcrs and all(combo.itemText(i) in self.pattern_pcrs for i in range(count)):
            self.ui_autorietveld.button_duplicate.setEnabled(True)
            self.ui_autorietveld.button_select_step.setEnabled(True)
        else:
            self.ui_autorietveld.button_duplicate.setEnabled(False)
            self.ui_autorietveld.button_select_step.setEnabled(False)

    def build_pcr_objects(self):
        pcr_map = self.pcrparams.get_output()
        good = True
        for pattern, path in pcr_map.items():
            path = os.path.splitext(path)
            head = os.path.dirname(path[0])
            tail = os.path.basename(path[0])
            pcr = PcrIO(path=head, jobid=tail)
            pcr.read_custom_pcr()
            pcr.clear_fix_vary()

            res = self.check_pcr_radiation(pcr)
            res1 = self.check_names(pattern, pcr)
            if res:
                self.pattern_pcrs[pattern] = pcr
            else:
                self.warning.setIcon(qtw.QMessageBox.Critical)
                self.warning.setText("Loaded PCR files do not have the correct JOB type")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                self.warning.exec_()
                good = False
                break

            if not res1:
                self.warning.setIcon(qtw.QMessageBox.Warning)
                self.warning.setText("Names of the pattern files loaded in your multipattern PCR do not coincide with the ones loaded in FullProfAPP. The names will be replaced when you start the refinement run.\n\n BE CAREFUL! You might have loaded the wrong PCR.")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                self.warning.exec_()

        if good:
            self.update_parameters_phases()
            self.enable_run_button()
            self.ui_autorietveld.button_minus.setEnabled(True)
            self.ui_autorietveld.button_minusminus.setEnabled(True)

    def check_names(self, pattern: str, pcr: PcrIO):
        pattern = pattern.split('_and_')
        if pcr.multipattern:
            npatt = pcr.get_patt_num()
            if all(pattern[ipatt] in os.path.splitext(os.path.basename(pcr.get_names(ipatt)))[0] for ipatt in range(npatt)):
                return True
            else:
                return False
        else:
            return True

    def check_pcr_radiation(self, pcr: PcrIO):
        jobdict = {0: 'xrd', 1: 'cw', -1: 'tof'}
        npatt = pcr.get_patt_num()
        if npatt != len(self._radiation):
            return False

        for ipatt, rad in enumerate(self._radiation):
            job = pcr.get_flags_global('job', ipatt)
            val = jobdict.get(job, None)
            if val is None or val != rad:
                return False
        return True

    def get_phases_for_patterns(self):
        for keys, values in self.pattern_pcrs.items():
            self.pattern_phase[keys] = values.read_ids()

    def set_model_patterns(self):
        model = TreeModel('Pattern Phases', self.pattern_phase)
        self.ui_autorietveld.tree_patterns.setModel(model)

    def set_model_parameters(self):
        model = TreeModel('Refinement Parameters', self.parameter_tree)
        self.ui_autorietveld.tree_parameters.setModel(model)

    def set_model_workflow(self):
        model = TreeModel(
            ['Refinement Steps', 'Scope', 'Parameter', 'Phase', 'Site Label',
             'Command', 'Code', 'Coefficient'],
            self.workflow_commands,
            with_values=True)

        self.enable_run_button()
        self.ui_autorietveld.tree_workflow.setModel(model)

    @staticmethod
    def get_unique_phases(pattern_phase: dict):
        phases = []
        for vals in pattern_phase.values():
            for phase in vals:
                phases.append(phase)

        phases = [(i, qtc.Qt.Unchecked) for n, i in enumerate(phases) if i not in phases[:n]]

        return phases

    def get_atoms_for_phases_pcr(self):
        phase_atoms = dict()
        for values in self.pattern_pcrs.values():
            phases = values.read_ids()
            for phase in phases:
                atoms = values.read_site_labels(identifier=phase)
                atoms = atoms if atoms is not None else []
                atoms = {(atom, qtc.Qt.Unchecked): '' for atom in atoms}
                phase_atoms[(phase, qtc.Qt.Unchecked)] = atoms
 
        # I don't know why I did this leaving it like this for the moment
        non_duplicate_keys = set(phase_atoms)
        phase_atoms = {key: phase_atoms[key] for key in set(phase_atoms) & non_duplicate_keys}

        return phase_atoms

    def update_parameters_phases(self):
        self.get_phases_for_patterns()
        phases = self.get_unique_phases(self.pattern_phase)
        phase_atoms = self.get_atoms_for_phases_pcr()

        self.restraint_actions = list()
        self.restraint_fields = dict()
        self.restraint_codes = dict()

        self.constraint_actions = list()
        self.constraint_fields = dict()
        self.constraint_codes = dict()

        self.limit_actions = list()
        self.limit_fields = dict()
        self.limit_codes = dict()

        self.lebail_const = dict()
        self.lebail_mode = dict()

        self.workflow_commands = dict()
        self.backg_menu.seqpattaction.setChecked(False)

        self.init_parameters(uniques=phases, atoms=phase_atoms)

        self.set_model_patterns()
        self.set_model_parameters()
        self.set_model_workflow()
        self.enable_duplicate_button()

    def parameter_to_workflow(self):

        """ Init Commands """
        num = len(self.workflow_commands)
        self.workflow_commands[f"Step {num}"] = {'VARY': dict(), 'FIX': dict()}
        idx = self.ui_autorietveld.tree_parameters.rootIndex()
        vary = self.flatten(self.get_dict_checked(idx, d={}))
        fix = self.flatten(self.get_dict_unchecked(idx, d={}))
        res_const = self.generate_constraint_codes()
        res_restr = self.generate_restraint_codes()
        res_limit = self.generate_limit_codes()

        for i, key in enumerate(vary.keys()):
            if key[1] in ['Strain Anisotropy', 'Size Anisotropy']:
                key = (key[0], key[-1], key[-2]) 

            values = list(key)
            if len(values) == 2:
                values.extend(['', '', self._text_commands.get(key[1]), '', ''])
            elif len(values) == 3:
                values.extend(['', self._text_commands.get(key[1]), '', ''])
            else:
                values.extend([self._text_commands.get(key[1]), '', ''])

            if res_const:
                for key_const, constraint in self.constraint_codes.items():
                    for keys, val in constraint.items():
                        if val == 'PEQU':
                            if key == keys:
                                values[5] = val
                        else:
                            if key == keys:
                                values[5] = self.constraint_codes[key_const][keys][0]
                                values[6] = self.constraint_codes[key_const][keys][1]

            if res_restr:
                for key_restr, restraint in self.restraint_codes.items():
                    for keys, val in restraint.items():
                        if isinstance(keys, tuple):
                            if key == keys and not any(keys in val for val in self.constraint_codes.values()):
                                values[5] = self.restraint_codes[key_restr][keys][0]
                                values[6] = '1.0000'#self.restraint_codes[key_restr][keys][1]#'1.0000'

            if res_limit:
                for key_lim, limit in self.limit_codes.items():
                    for keys, val in limit.items():
                        cond1 = any(keys in val for val in self.constraint_codes.values())
                        cond2 = any(keys in val for val in self.restraint_codes.values() if isinstance(val, tuple))
                        if key == keys and not cond1 and not cond2:
                            values[5] = self.limit_codes[key_lim][keys][0]
                            values[6] = self.limit_codes[key_lim][keys][1]

            if not (values[0] == 'Sites' and not values[3]):
                self.workflow_commands[f"Step {num}"]['VARY'][f"No. {i}"] = values

        if res_restr:
            for key_restr, restraint in self.restraint_codes.items():
                restr = dict()
                for keys, val in restraint.items():
                    if not isinstance(keys, tuple):
                        restr[keys] = ['', '', '', '', '', '', val]
                    else:
                        restr[' > '.join(keys)] = ['', '', '', '', '', str(val[0]), val[1]]

                self.workflow_commands[f"Step {num}"][key_restr] = restr

        if res_limit:
            for key_lim, limit in self.limit_fields.items():
                limits = dict()
                for keys, val in limit.items():
                    limits[' > '.join(keys)] = ['', '', '', '', '', str(self.limit_codes[key_lim][keys][0]), '']
                    limits['Low Limit'] = ['', '', '', '', '', '', f"{float(val[0]):.4f}"]
                    limits['High Limit'] = ['', '', '', '', '', '', f"{float(val[1]):.4f}"]
                    limits['Boundary'] = ['', '', '', '', '', '', val[2]]

                self.workflow_commands[f"Step {num}"][key_lim] = limits

        for i, key in enumerate(fix.keys()):
            if key[1] in ['Strain Anisotropy', 'Size Anisotropy']:
                key = (key[0], key[-1], key[-2]) 

            values = list(key)
            if len(values) == 2:
                values.extend(['', '', self._text_commands.get(key[1])])
            elif len(values) == 3:
                values.extend(['', self._text_commands.get(key[1])])
            else:
                values.extend([self._text_commands.get(key[1])])

            if not (values[0] == 'Sites' and not values[3]):
                self.workflow_commands[f"Step {num}"]['FIX'][f"No. {i}"] = values

        self.set_model_workflow()

    def flatten(self, dictionary: collections.abc.MutableMapping, parent_key: list = None):
        """
        Turn a nested dictionary into a flattened dictionary
        :param dictionary: The dictionary to flatten
        :param parent_key: The string to prepend to dictionary's keys
        :return: A flattened dictionary
        """

        items = []
        for key, value in dictionary.items():
            new_key = parent_key + [key] if parent_key else [key]
            if isinstance(value, collections.abc.MutableMapping):
                items.extend(self.flatten(value, new_key).items())
            elif isinstance(value, list):
                for k, v in enumerate(value):
                    items.extend(self.flatten({str(k): v}, new_key).items())
            else:
                items.append((tuple(new_key), value))

        return dict(items)
    
    def flatten_keys(self, dictionary: collections.abc.MutableMapping, parent_key: list = None):
        """
        Turn a nested dictionary into a flattened dictionary
        :param dictionary: The dictionary to flatten
        :param parent_key: The string to prepend to dictionary's keys
        :return: A flattened dictionary
        """

        items = []
        for key, value in dictionary.items():
            new_key = parent_key + [key] if parent_key else [key]
            if isinstance(value, collections.abc.MutableMapping):
                items.extend(self.flatten_keys(value, new_key).items())
            else:
                items.append((tuple(new_key), value))

        return dict(items)

    def get_dict_checked(self, parent, d):
        model = self.ui_autorietveld.tree_parameters.model()
        for row in range(model.rowCount(parent)):
            child = model.index(row, 0, parent)
            if model.itemFromIndex(child).isCheckable():
                if model.itemFromIndex(child).checkState() == qtc.Qt.Checked:
                    if model.hasChildren(child):
                        d[child.data()] = dict()
                        self.get_dict_checked(child, d[child.data()])
                    else:
                        d[child.data()] = ''

                elif model.itemFromIndex(child).checkState() == qtc.Qt.PartiallyChecked:
                    d[child.data()] = dict()
                    self.get_dict_checked(child, d[child.data()])

            else:
                if model.hasChildren(child):
                    d[child.data()] = dict()
                    self.get_dict_checked(child, d[child.data()])

        return d

    def get_dict_unchecked(self, parent, d):
        model = self.ui_autorietveld.tree_parameters.model()
        for row in range(model.rowCount(parent)):
            child = model.index(row, 0, parent)

            if model.itemFromIndex(child).isCheckable():
                if model.itemFromIndex(child).checkState() == qtc.Qt.Unchecked:
                    if model.hasChildren(child):
                        d[child.data()] = dict()
                        self.get_dict_unchecked(child, d[child.data()])
                    else:
                        d[child.data()] = ''

                elif model.itemFromIndex(child).checkState() == qtc.Qt.PartiallyChecked:
                    d[child.data()] = dict()
                    self.get_dict_unchecked(child, d[child.data()])

            else:
                if model.hasChildren(child):
                    d[child.data()] = dict()
                    self.get_dict_unchecked(child, d[child.data()])

        return d

    def get_dict_all(self):
        if self.ui_autorietveld.tree_parameters.model() is not None:
            idx = self.ui_autorietveld.tree_parameters.rootIndex()

            return self.get_dict_all_params(idx, d={})

    def get_dict_all_params(self, parent, d):
        model = self.ui_autorietveld.tree_parameters.model()
        for row in range(model.rowCount(parent)):
            child = model.index(row, 0, parent)

            if model.itemFromIndex(child).isCheckable():
                state = model.itemFromIndex(child).checkState()
                if model.hasChildren(child):
                    d[(child.data(), state)] = dict()
                    self.get_dict_all_params(child, d[(child.data(), state)])
                else:
                    d[(child.data(), state)] = ''
            else:
                if model.hasChildren(child):
                    d[child.data()] = dict()
                    self.get_dict_all_params(child, d[child.data()])

        return d

    def get_pattern_top_names(self, parent):
        model = self.ui_autorietveld.tree_patterns.model()
        top = model.parent(parent)

        if model.itemFromIndex(top) is None:
            return parent.data()
        else:
            return self.get_pattern_top_names(top)

    def go_to_top(self, items: list, parent):
        model = self.ui_autorietveld.tree_parameters.model()
        top = model.parent(parent)

        items.append(parent.data())
        if model.itemFromIndex(top) is None:
            return items
        else:
            return self.go_to_top(items, top)

    def do_checks(self, parent):
        model = self.ui_autorietveld.tree_parameters.model()

        if model.hasChildren(parent) and model.itemFromIndex(parent).isCheckable():
            for row in range(model.rowCount(parent)):
                child = model.index(row, 0, parent)
                if model.itemFromIndex(parent).checkState() == qtc.Qt.Checked:
                    model.itemFromIndex(child).setCheckState(qtc.Qt.Checked)

                elif model.itemFromIndex(parent).checkState() == qtc.Qt.Unchecked:
                    model.itemFromIndex(child).setCheckState(qtc.Qt.Unchecked)

                self.do_checks(child)

    def clean_top_checks(self, parent):
        model = self.ui_autorietveld.tree_parameters.model()
        top = model.parent(parent)

        if model.itemFromIndex(top) is not None:
            checks = [0, 0, 0]
            for row in range(model.rowCount(top)):
                child = model.index(row, 0, top)
                if model.itemFromIndex(child).checkState() == qtc.Qt.Checked:
                    checks[0] += 1
                elif model.itemFromIndex(child).checkState() == qtc.Qt.Unchecked:
                    checks[1] += 1
                else:
                    checks[2] += 1

            if checks[0] == model.rowCount(top) and model.itemFromIndex(top).isCheckable():
                model.itemFromIndex(top).setCheckState(qtc.Qt.Checked)
            elif checks[1] == model.rowCount(top) and model.itemFromIndex(top).isCheckable():
                model.itemFromIndex(top).setCheckState(qtc.Qt.Unchecked)
            elif model.itemFromIndex(top).isCheckable():
                model.itemFromIndex(top).setCheckState(qtc.Qt.PartiallyChecked)

            self.clean_top_checks(top)

    def add_size_strain_zero_relations(self):
        idx = self.ui_autorietveld.tree_parameters.rootIndex()
        checked = self.flatten(self.get_dict_checked(idx, d={}))
        checked_tuples = set([(val[0], val[1], val[2]) for val in checked.keys() if val[1] in ['Strain Anisotropy', 'Size Anisotropy', 'Phase Zero', 'Phase SyCos', 'Phase SySin']])
        for parameters in checked_tuples:
            ipatt = int(parameters[0].replace('Pattern ', ''))
            for pcr in self.pattern_pcrs.values():
                if parameters[1] == 'Strain Anisotropy' and parameters[2] in pcr.read_ids():
                    strainmodel = pcr.get_profile_phase(parameters[2], 'straintag', ipatt)
                    if strainmodel == 0:
                        pcr.add_strain_model(parameters[2], ipatt)
                elif parameters[1] == 'Size Anisotropy' and parameters[2] in pcr.read_ids():
                    sizemodel = pcr.get_profile_phase(parameters[2], 'sizetag', ipatt)
                    if sizemodel == 0:
                        pcr.add_size_model(parameters[2], ipatt)
                elif parameters[1] in ['Phase Zero', 'Phase SyCos', 'Phase SySin'] and parameters[2] in pcr.read_ids():
                    more = pcr.get_flags_phase(parameters[2], 'more_phase')
                    if more == 0:
                        pcr.add_phase_more(parameters[2])
                    shift = pcr.get_flags_phase(parameters[2], 'phshift', ipatt)
                    if shift == 0:
                        pcr.add_phshifts(parameters[2], ipatt)

    def background_type_change(self, idx):

        if idx == 0:
            self.cheb_order.hide()
            self.cheb_label.hide()
            self.window_fourier.hide()
            self.window_label.hide()
            self.cycles_fourier.hide()
            self.cycles_label.hide()
        elif idx == 1:
            self.cheb_order.show()
            self.cheb_label.show()
            self.window_fourier.hide()
            self.window_label.hide()
            self.cycles_fourier.hide()
            self.cycles_label.hide()
        elif idx == 2:
            self.cheb_order.hide()
            self.cheb_label.hide()
            self.window_fourier.show()
            self.window_label.show()
            self.cycles_fourier.show()
            self.cycles_label.show()

    def enable_run_button(self):
        count = self.ui_autorietveld.pattern_combo.count()
        combo = self.ui_autorietveld.pattern_combo
        if self.workflow_commands and all(combo.itemText(i) in self.pattern_pcrs for i in range(count)):
            self.ui_autorietveld.button_run_fp.setEnabled(True)
        else:
            self.ui_autorietveld.button_run_fp.setEnabled(False)


class BackgroundMenu(qtw.QMenu):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.options = self.addMenu('Background Options')
        self.loadfrom = self.options.addMenu('Load on Selection')
        self.pattaction = self.loadfrom.addAction('From Patterns Window')
        self.fileaction = self.loadfrom.addAction('From Background File (.bgr)')

        self.sequential = self.options.addMenu('Load on Sequential')
        self.seqpattaction = self.sequential.addAction('Pattern Window')
        self.seqpattaction.setCheckable(True)

