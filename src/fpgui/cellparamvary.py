import sys

import numpy as np
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc

from fpgui.uifiles.uicellparamvary import Ui_Form

from crysfml_python import crysfml_forpy


class CellVary(qtw.QWidget):

    increments = qtc.pyqtSignal(object)

    def __init__(self, info, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """Info is just a one row pandas dataframe, that must be fed when calling the object"""

        self.ui_cellvary = Ui_Form()
        self.ui_cellvary.setupUi(self)

        """ Initialize numpy array wtih info """
        self.params = np.zeros(6, dtype=np.float32)
        self.params[0] = info.iloc[0]['_cell_length_a']
        self.params[1] = info.iloc[0]['_cell_length_b']
        self.params[2] = info.iloc[0]['_cell_length_c']
        self.params[3] = info.iloc[0]['_cell_angle_alpha']
        self.params[4] = info.iloc[0]['_cell_angle_beta']
        self.params[5] = info.iloc[0]['_cell_angle_gamma']

        self.crys_sys = info.iloc[0]['x_system_spacegroup']
        self.crys_fam = info.iloc[0]['x_system_cell']

        self.ui_cellvary.label_phase_info.setText(f"{info.iloc[0]['identifier']} ({info.iloc[0]['spacegroup']})"
                                                  f" Crystal System: {self.crys_sys}"
                                                  f" Lattice Type: {self.crys_fam}")

        if ("Trigonal" in self.crys_sys) and ("Trigonal" in self.crys_fam):
            self.ui_cellvary.label_phase_info.setText(f"{info.iloc[0]['identifier']} ({info.iloc[0]['spacegroup']})"
                                                      f" Crystal System: {self.crys_sys}"
                                                      f" Lattice Type: {self.crys_fam}"
                                                      f" (Cell parameters are transformed to an Hexagonal setting)")

            r = crysfml_forpy.transform_setting_R_to_H(info.iloc[0]['spacegroup'], self.params)
            self.params = np.append(r[0], r[1])

        self.perma = self.params.copy()

        self.ui_cellvary.label_a.setText("a: {res:10.4f}".format(res=self.params[0]))
        self.ui_cellvary.label_b.setText("b: {res:10.4f}".format(res=self.params[1]))
        self.ui_cellvary.label_c.setText("c: {res:10.4f}".format(res=self.params[2]))
        self.ui_cellvary.label_alpha.setText("alpha: {res:10.4f}".format(res=self.params[3]))
        self.ui_cellvary.label_beta.setText("beta: {res:10.4f}".format(res=self.params[4]))
        self.ui_cellvary.label_gamma.setText("gamma: {res:10.4f}".format(res=self.params[5]))

        self.sliderpos = np.array([50000, 50000, 50000, 50000, 50000, 50000], dtype=int)

        self.set_slider_pos()

        self.enable_sliders()

    def set_slider_pos(self):
        self.ui_cellvary.a_slider.setValue(self.sliderpos[0])
        self.ui_cellvary.b_slider.setValue(self.sliderpos[1])
        self.ui_cellvary.c_slider.setValue(self.sliderpos[2])
        self.ui_cellvary.alpha_slider.setValue(self.sliderpos[3])
        self.ui_cellvary.beta_slider.setValue(self.sliderpos[4])
        self.ui_cellvary.gamma_slider.setValue(self.sliderpos[5])

    def update_params(self):
        """ Set slider values and cell parameters according to the crystal system """
        if "Triclinic" in self.crys_sys:
            pass
        elif "Monoclinic" in self.crys_sys:
            pass
        elif "Orthorhombic" in self.crys_sys:
            pass
        elif "Tetragonal" in self.crys_sys:
            self.params[1] = self.params[0]
            self.sliderpos[1] = self.sliderpos[0]
            self.ui_cellvary.label_b.setText("b: {res:10.4f}".format(res=self.params[1]))
            self.ui_cellvary.b_slider.setValue(self.ui_cellvary.a_slider.value())
        elif "Trigonal" in self.crys_sys:
            self.params[1] = self.params[0]
            self.sliderpos[1] = self.sliderpos[0]
            self.ui_cellvary.label_b.setText("b: {res:10.4f}".format(res=self.params[1]))
            self.ui_cellvary.b_slider.setValue(self.ui_cellvary.a_slider.value())
        elif "Hexagonal" in self.crys_sys:
            self.params[1] = self.params[0]
            self.sliderpos[1] = self.sliderpos[0]
            self.ui_cellvary.label_b.setText("b: {res:10.4f}".format(res=self.params[1]))
            self.ui_cellvary.b_slider.setValue(self.ui_cellvary.a_slider.value())
        elif "Cubic" in self.crys_sys:
            self.params[1] = self.params[0]
            self.params[2] = self.params[0]
            self.sliderpos[1] = self.sliderpos[0]
            self.sliderpos[2] = self.sliderpos[0]
            self.ui_cellvary.label_b.setText("b: {res:10.4f}".format(res=self.params[1]))
            self.ui_cellvary.b_slider.setValue(self.ui_cellvary.a_slider.value())
            self.ui_cellvary.label_c.setText("c: {res:10.4f}".format(res=self.params[2]))
            self.ui_cellvary.c_slider.setValue(self.ui_cellvary.a_slider.value())

    def enable_sliders(self):
        """ Enable sliders and slots according to the crystal system """
        if "Triclinic" in self.crys_sys:
            self.ui_cellvary.a_slider.setEnabled(True)
            self.ui_cellvary.b_slider.setEnabled(True)
            self.ui_cellvary.c_slider.setEnabled(True)
            self.ui_cellvary.alpha_slider.setEnabled(True)
            self.ui_cellvary.beta_slider.setEnabled(True)
            self.ui_cellvary.gamma_slider.setEnabled(True)

            self.ui_cellvary.label_a.setEnabled(True)
            self.ui_cellvary.label_b.setEnabled(True)
            self.ui_cellvary.label_c.setEnabled(True)
            self.ui_cellvary.label_alpha.setEnabled(True)
            self.ui_cellvary.label_beta.setEnabled(True)
            self.ui_cellvary.label_gamma.setEnabled(True)

            self.ui_cellvary.a_slider.valueChanged.connect(lambda: self.record_a())
            self.ui_cellvary.b_slider.valueChanged.connect(lambda: self.record_b())
            self.ui_cellvary.c_slider.valueChanged.connect(lambda: self.record_c())
            self.ui_cellvary.alpha_slider.valueChanged.connect(lambda: self.record_alpha())
            self.ui_cellvary.beta_slider.valueChanged.connect(lambda: self.record_beta())
            self.ui_cellvary.gamma_slider.valueChanged.connect(lambda: self.record_gamma())

        elif "Monoclinic" in self.crys_sys:
            self.ui_cellvary.a_slider.setEnabled(True)
            self.ui_cellvary.b_slider.setEnabled(True)
            self.ui_cellvary.c_slider.setEnabled(True)
            self.ui_cellvary.alpha_slider.setEnabled(False)
            self.ui_cellvary.beta_slider.setEnabled(True)
            self.ui_cellvary.gamma_slider.setEnabled(False)

            self.ui_cellvary.label_a.setEnabled(True)
            self.ui_cellvary.label_b.setEnabled(True)
            self.ui_cellvary.label_c.setEnabled(True)
            self.ui_cellvary.label_alpha.setEnabled(False)
            self.ui_cellvary.label_beta.setEnabled(True)
            self.ui_cellvary.label_gamma.setEnabled(False)

            self.ui_cellvary.a_slider.valueChanged.connect(lambda: self.record_a())
            self.ui_cellvary.b_slider.valueChanged.connect(lambda: self.record_b())
            self.ui_cellvary.c_slider.valueChanged.connect(lambda: self.record_c())
            self.ui_cellvary.beta_slider.valueChanged.connect(lambda: self.record_beta())

        elif "Orthorhombic" in self.crys_sys:
            self.ui_cellvary.a_slider.setEnabled(True)
            self.ui_cellvary.b_slider.setEnabled(True)
            self.ui_cellvary.c_slider.setEnabled(True)
            self.ui_cellvary.alpha_slider.setEnabled(False)
            self.ui_cellvary.beta_slider.setEnabled(False)
            self.ui_cellvary.gamma_slider.setEnabled(False)

            self.ui_cellvary.label_a.setEnabled(True)
            self.ui_cellvary.label_b.setEnabled(True)
            self.ui_cellvary.label_c.setEnabled(True)
            self.ui_cellvary.label_alpha.setEnabled(False)
            self.ui_cellvary.label_beta.setEnabled(False)
            self.ui_cellvary.label_gamma.setEnabled(False)

            self.ui_cellvary.a_slider.valueChanged.connect(lambda: self.record_a())
            self.ui_cellvary.b_slider.valueChanged.connect(lambda: self.record_b())
            self.ui_cellvary.c_slider.valueChanged.connect(lambda: self.record_c())

        elif "Tetragonal" in self.crys_sys:
            self.ui_cellvary.a_slider.setEnabled(True)
            self.ui_cellvary.b_slider.setEnabled(False)
            self.ui_cellvary.c_slider.setEnabled(True)
            self.ui_cellvary.alpha_slider.setEnabled(False)
            self.ui_cellvary.beta_slider.setEnabled(False)
            self.ui_cellvary.gamma_slider.setEnabled(False)

            self.ui_cellvary.label_a.setEnabled(True)
            self.ui_cellvary.label_b.setEnabled(False)
            self.ui_cellvary.label_c.setEnabled(True)
            self.ui_cellvary.label_alpha.setEnabled(False)
            self.ui_cellvary.label_beta.setEnabled(False)
            self.ui_cellvary.label_gamma.setEnabled(False)

            self.ui_cellvary.a_slider.valueChanged.connect(lambda: self.record_a())
            self.ui_cellvary.c_slider.valueChanged.connect(lambda: self.record_c())

        elif "Trigonal" in self.crys_sys:
            self.ui_cellvary.a_slider.setEnabled(True)
            self.ui_cellvary.b_slider.setEnabled(False)
            self.ui_cellvary.c_slider.setEnabled(True)
            self.ui_cellvary.alpha_slider.setEnabled(False)
            self.ui_cellvary.beta_slider.setEnabled(False)
            self.ui_cellvary.gamma_slider.setEnabled(False)

            self.ui_cellvary.label_a.setEnabled(True)
            self.ui_cellvary.label_b.setEnabled(False)
            self.ui_cellvary.label_c.setEnabled(True)
            self.ui_cellvary.label_alpha.setEnabled(False)
            self.ui_cellvary.label_beta.setEnabled(False)
            self.ui_cellvary.label_gamma.setEnabled(False)

            self.ui_cellvary.a_slider.valueChanged.connect(lambda: self.record_a())
            self.ui_cellvary.c_slider.valueChanged.connect(lambda: self.record_c())

        elif "Hexagonal" in self.crys_sys:
            self.ui_cellvary.a_slider.setEnabled(True)
            self.ui_cellvary.b_slider.setEnabled(False)
            self.ui_cellvary.c_slider.setEnabled(True)
            self.ui_cellvary.alpha_slider.setEnabled(False)
            self.ui_cellvary.beta_slider.setEnabled(False)
            self.ui_cellvary.gamma_slider.setEnabled(False)

            self.ui_cellvary.label_a.setEnabled(True)
            self.ui_cellvary.label_b.setEnabled(False)
            self.ui_cellvary.label_c.setEnabled(True)
            self.ui_cellvary.label_alpha.setEnabled(False)
            self.ui_cellvary.label_beta.setEnabled(False)
            self.ui_cellvary.label_gamma.setEnabled(False)

            self.ui_cellvary.a_slider.valueChanged.connect(lambda: self.record_a())
            self.ui_cellvary.c_slider.valueChanged.connect(lambda: self.record_c())

        elif "Cubic" in self.crys_sys:
            self.ui_cellvary.a_slider.setEnabled(True)
            self.ui_cellvary.b_slider.setEnabled(False)
            self.ui_cellvary.c_slider.setEnabled(False)
            self.ui_cellvary.alpha_slider.setEnabled(False)
            self.ui_cellvary.beta_slider.setEnabled(False)
            self.ui_cellvary.gamma_slider.setEnabled(False)

            self.ui_cellvary.label_a.setEnabled(True)
            self.ui_cellvary.label_b.setEnabled(False)
            self.ui_cellvary.label_c.setEnabled(False)
            self.ui_cellvary.label_alpha.setEnabled(False)
            self.ui_cellvary.label_beta.setEnabled(False)
            self.ui_cellvary.label_gamma.setEnabled(False)

            self.ui_cellvary.a_slider.valueChanged.connect(lambda: self.record_a())

    def record_a(self):
        self.params[0] = (1.0 + self.value_handler(float(self.ui_cellvary.a_slider.value()))) * self.perma[0]
        self.sliderpos[0] = self.ui_cellvary.a_slider.value()
        self.ui_cellvary.label_a.setText("a: {res:10.4f}".format(res=self.params[0]))
        self.update_params()
        self.increments.emit(self.params)

    def record_b(self):
        self.params[1] = (1.0 + self.value_handler(float(self.ui_cellvary.b_slider.value()))) * self.perma[1]
        self.sliderpos[1] = self.ui_cellvary.b_slider.value()
        self.ui_cellvary.label_b.setText("b: {res:10.4f}".format(res=self.params[1]))
        self.update_params()
        self.increments.emit(self.params)

    def record_c(self):
        self.params[2] = (1.0 + self.value_handler(float(self.ui_cellvary.c_slider.value()))) * self.perma[2]
        self.sliderpos[2] = self.ui_cellvary.c_slider.value()
        self.ui_cellvary.label_c.setText("c: {res:10.4f}".format(res=self.params[2]))
        self.update_params()
        self.increments.emit(self.params)

    def record_alpha(self):
        self.params[3] = (1.0 + self.value_handler(float(self.ui_cellvary.alpha_slider.value()))) * self.perma[3]
        self.sliderpos[3] = self.ui_cellvary.alpha_slider.value()
        self.ui_cellvary.label_alpha.setText("alpha: {res:10.4f}".format(res=self.params[3]))
        self.update_params()
        self.increments.emit(self.params)

    def record_beta(self):
        self.params[4] = (1.0 + self.value_handler(float(self.ui_cellvary.beta_slider.value()))) * self.perma[4]
        self.sliderpos[4] = self.ui_cellvary.beta_slider.value()
        self.ui_cellvary.label_beta.setText("beta: {res:10.4f}".format(res=self.params[4]))
        self.update_params()
        self.increments.emit(self.params)

    def record_gamma(self):
        self.params[5] = (1.0 + self.value_handler(float(self.ui_cellvary.gamma_slider.value()))) * self.perma[5]
        self.sliderpos[5] = self.ui_cellvary.gamma_slider.value()
        self.ui_cellvary.label_gamma.setText("gamma: {res:10.4f}".format(res=self.params[5]))
        self.update_params()
        self.increments.emit(self.params)

    @staticmethod
    def value_handler(value):
        vmin = -0.1
        vmax = 0.1

        scaled_value = value*(vmax - vmin)/100000.0 + vmin

        return scaled_value
