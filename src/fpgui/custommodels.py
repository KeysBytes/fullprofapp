import re

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

from typing import Union, List, Tuple
from fastnumbers import fast_real

import numpy.typing as npt
import numpy as np
import pandas as pd


class PandasQSortFilterProxyModel(qtc.QSortFilterProxyModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.role = PandasTableModel.ValueRole
        self.regex = []

    def setFilterByColumn(self, regex):
        expression = regex.split()
        if expression and all(isinstance(reg, str) for reg in expression):
            self.regex = [re.compile(reg) for reg in expression]
        else:
            self.regex = []

        self.invalidateFilter()

    def filterAcceptsRow(self, source_row, source_parent):
        if not self.regex:
            return True

        all_results = []
        for regex in self.regex:
            results = []
            for icol in range(self.sourceModel().columnCount()):
                text = ''
                index = self.sourceModel().index(source_row, icol, source_parent)
                if index.isValid():
                    text = self.sourceModel().data(index, qtc.Qt.DisplayRole)
                    if text is None:
                        text = ''

                results.append(regex.search(text))

            all_results.append(results)

        return all(any(res) for res in all_results)

    def lessThan(self, left, right) -> bool:
        leftData = self.sourceModel().data(left, self.role)
        rightData = self.sourceModel().data(right, self.role)

        if leftData is None:
            return True
        elif rightData is None:
            return False
        elif type(leftData) != type(rightData):
            """ We know that the type of data is always consistent but in principle you should warn """
            return False

        """ We can do this because in Python you can compare any object as long as data types are equal """
        return leftData < rightData


class PandasTableModel(qtc.QAbstractTableModel):
    """ Class to populate TableView with a Pandas dataframe """
    DtypeRole = qtc.Qt.UserRole + 1000
    ValueRole = qtc.Qt.UserRole + 1001

    def __init__(self, data, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._data = data
        self.oldid = ''

    def remove_row(self, identifier):
        args = np.where(self._data["identifier"] == identifier)[0]
        if args.size > 0:
            row = args[0]
            self._data.drop([self._data.index[row]], inplace=True)
            self.removeRow(row)
            self.layoutChanged.emit()

    def change_data(self, oldentry, newentry):
        args = np.where(self._data["identifier"] == oldentry)[0]
        if args.size > 0:
            row = args[0]
            col = self._data.columns.get_loc("identifier")
            index = self.index(row, col)
            self.setData(index, newentry, qtc.Qt.EditRole)

    def data(self, index, role):
        if index.isValid() or (0 <= index.row() < self.rowCount() and 0 <= index.column() < self.columnCount()):
            dtype = self._data.iloc[:, index.column()].dtype
            val = self._data.iloc[index.row(), index.column()]

            if role == qtc.Qt.DisplayRole or role == qtc.Qt.EditRole:
                if isinstance(val, float):
                    return f"{val:.4f}"
                else:
                    return str(val)

            elif role == PandasTableModel.ValueRole:
                return val

            elif role == PandasTableModel.DtypeRole:
                return dtype

    def rowCount(self, parent=None):
        return self._data.shape[0]

    def columnCount(self, parent=None):
        return self._data.shape[1]

    def setData(self, index, value, role):
        if role == qtc.Qt.EditRole:
            self.oldid = self._data.iloc[index.row(), index.column()]
            self._data.iloc[index.row(), index.column()] = str(value)
            self.dataChanged.emit(index, index, [role])
            return True

        return False

    def headerData(self, section, orientation, role):
        if role == qtc.Qt.DisplayRole:
            if orientation == qtc.Qt.Horizontal:
                return str(self._data.columns[section])

            if orientation == qtc.Qt.Vertical:
                return str(self._data.index[section])

    def flags(self, index): 
        if self._data.columns[index.column()] == "identifier":
            return qtc.Qt.ItemIsEditable | qtc.Qt.ItemIsEnabled | qtc.Qt.ItemIsSelectable
        else:
            return qtc.Qt.ItemIsEnabled | qtc.Qt.ItemIsSelectable


class ListTableView(qtw.QTableView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def commitData(self, editor):
        super(ListTableView, self).commitData(editor)

        model = self.currentIndex().model()
        value = model.data(self.currentIndex(), qtc.Qt.EditRole)

        if len(self.selectionModel().selection().indexes()) > 1:
            for index in self.selectionModel().selection().indexes():
                if index.flags() & qtc.Qt.ItemIsEditable and index != self.currentIndex():
                    model.setData(index, value, qtc.Qt.EditRole)


class ListTableModel(qtc.QAbstractTableModel):
    def __init__(self, data: Union[npt.NDArray[np.float64], List[List[str]]], headers: List[str] = None, edit: List[Union[int, Tuple[int, int]]] = None,
                 checks: List[Union[int, Tuple[int, int]]] = None, offset=0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._data = data
        self._headers = headers
        self._edit = edit
        self._checks = checks
        self._check_state = {}
        self._name = None
        self._offset = offset

        if isinstance(data, list):
            self.rows = len(self._data)
            self.columns = max([len(i) for i in self._data])
        else:
            self.rows = self._data.shape[0]
            self.columns = self._data.shape[1]

        self._color = [qtg.QBrush(qtg.QColor(255, 255, 255))] * self.rows

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def check_state(self, value, index):
        if index in self._check_state:
            return self._check_state[index]
        else:
            if isinstance(value, int) and value == 0:
                return qtc.Qt.Unchecked
            if isinstance(value, str) and value != 'BVS':
                return qtc.Qt.Unchecked

            return qtc.Qt.Checked

    def change_color(self, color, rows):
        for row in rows:
            index1 = self.index(row, 0)
            index2 = self.index(row, 1)
            self._color[row] = color
            self.dataChanged.emit(index1, index1, [qtc.Qt.BackgroundRole])
            self.dataChanged.emit(index2, index2, [qtc.Qt.BackgroundRole])

    def data(self, index, role):
        if index.isValid() or (0 <= index.row() < self.rowCount() and 0 <= index.column() < self.columnCount()):
            if isinstance(self._data, list):
                rowlen = len(self._data[index.row()])
                roffset = self.columnCount() - self._offset
                loffset = self.columnCount() - rowlen - self._offset
                if rowlen < self.columnCount():
                    if loffset <= index.column() < roffset:
                        val = self._data[index.row()][index.column() - loffset]
                    else:
                        val = ''
                else:
                    val = self._data[index.row()][index.column()]
            else:
                val = self._data[index.row(), index.column()]

            val = fast_real(val, coerce=False)
            if (role == qtc.Qt.DisplayRole or role == qtc.Qt.EditRole) and not index.flags() & qtc.Qt.ItemIsUserCheckable:
                if isinstance(val, float):
                    if isinstance(self._name, tuple):
                        if 'table_phprof1' in self._name[1] and index.row() == 0 and index.column() == 0:
                            return f"{val:.4e}"
                        else:
                            return f"{val:.4f}"
                    else:
                        return f"{val:.4f}"
                return str(val)

            if role == qtc.Qt.CheckStateRole and index.flags() & qtc.Qt.ItemIsUserCheckable:
                return self.check_state(val, index)

            if role == qtc.Qt.BackgroundRole:
                return self._color[index.row()]

            return None
        return None

    def setData(self, index, value, role):
        if role == qtc.Qt.EditRole:
            val = fast_real(value, coerce=False)
            if isinstance(self._data, list):
                rowlen = len(self._data[index.row()])
                roffset = self.columnCount() - self._offset
                loffset = self.columnCount() - rowlen - self._offset
                if rowlen < self.columnCount():
                    if loffset <= index.column() < roffset:
                        self._data[index.row()][index.column() - loffset] = str(val)
                else:
                    self._data[index.row()][index.column()] = str(val)
            else:
                self._data[index.row(), index.column()] = val

            self.dataChanged.emit(index, index, [role])
            return True

        if role == qtc.Qt.CheckStateRole:
            self._check_state[index] = value
            self.dataChanged.emit(index, index, [role])
            return True

        return False

    def flags(self, index):
        if self._edit is not None and index.column() in self._edit:
            return qtc.Qt.ItemIsSelectable | qtc.Qt.ItemIsEnabled

        if self._edit is not None and (index.row(), index.column()) in self._edit:
            return qtc.Qt.ItemIsSelectable | qtc.Qt.ItemIsEnabled

        if self._checks is not None and index.column() in self._checks:
            return qtc.Qt.ItemIsSelectable | qtc.Qt.ItemIsEnabled | qtc.Qt.ItemIsUserCheckable

        if self._checks is not None and (index.row(), index.column()) in self._checks:
            return qtc.Qt.ItemIsSelectable | qtc.Qt.ItemIsEnabled | qtc.Qt.ItemIsUserCheckable

        return qtc.Qt.ItemIsSelectable | qtc.Qt.ItemIsEnabled | qtc.Qt.ItemIsEditable

    def rowCount(self, parent=None):
        return self.rows

    def columnCount(self, parent=None):
        return self.columns

    def headerData(self, section, orientation, role):
        if role == qtc.Qt.DisplayRole:
            if orientation == qtc.Qt.Horizontal and self._headers is not None:
                return str(self._headers[section])


class SystemDialog(qtw.QDialog):
    def __init__(self):
        super().__init__()
        self.button = qtw.QPushButton('Choose Directories')
        self.button.clicked.connect(self.handleChooseDirectories)
        self.listWidget = qtw.QListWidget()
        self.names = None

        self.dialog_button = qtw.QDialogButtonBox(qtw.QDialogButtonBox.Ok | qtw.QDialogButtonBox.Cancel)
        layout = qtw.QVBoxLayout(self)
        layout.addWidget(self.listWidget)
        layout.addWidget(self.button)
        layout.addWidget(self.dialog_button)

        self.dialog_button.accepted.connect(self.accept)
        self.dialog_button.rejected.connect(self.reject)

    def handleChooseDirectories(self):
        dialog = qtw.QFileDialog(self)
        dialog.setWindowTitle('Choose Directories')
        dialog.setOption(qtw.QFileDialog.DontUseNativeDialog, True)
        dialog.setFileMode(qtw.QFileDialog.DirectoryOnly)
        for view in dialog.findChildren(
                (qtw.QListView, qtw.QTreeView)):
            if isinstance(view.model(), qtw.QFileSystemModel):
                view.setSelectionMode(
                    qtw.QAbstractItemView.ExtendedSelection)
        if dialog.exec_() == qtw.QDialog.Accepted:
            self.listWidget.clear()
            self.listWidget.addItems(dialog.selectedFiles())
        dialog.deleteLater()

    def accept(self):
        self.names = []

        for i in range(self.listWidget.count()):
            self.names.append(self.listWidget.item(i).text())

        super(SystemDialog, self).accept()

    def get_output(self):
        return self.names


class TreeModel(qtg.QStandardItemModel):
    def __init__(self, text: Union[str, List[str]], data: dict, with_values: bool = False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        """ If checks is False, data structure is just stringed keys, if True its tuples containing the boolean
        value of the checked state """
        self._data = data
        self.values = with_values
        self.setHorizontalHeaderLabels([text] if isinstance(text, str) else text)

        if with_values:
            self.fill_model_with_values(self.invisibleRootItem(), self._data)
        else:
            self.fill_model_from_data(self.invisibleRootItem(), self._data)

    def fill_model_from_data(self, parent, d):
        if isinstance(d, dict):
            for key, val in d.items():
                if isinstance(key, tuple):
                    child = qtg.QStandardItem(str(key[0]))
                    child.setFlags(child.flags() ^ qtc.Qt.ItemIsDropEnabled | qtc.Qt.ItemIsUserCheckable
                                   ^ qtc.Qt.ItemIsEditable)
                    child.setCheckState(key[1])

                    parent.appendRow(child)
                    self.fill_model_from_data(child, val)

                else:
                    child = qtg.QStandardItem(str(key))
                    parent.appendRow(child)
                    self.fill_model_from_data(child, val)

        elif isinstance(d, list):
            for val in d:
                self.fill_model_from_data(parent, val)

        else:
            if isinstance(d, tuple):
                item = qtg.QStandardItem(str(d[0]))
                item.setFlags(item.flags() ^ qtc.Qt.ItemIsDropEnabled | qtc.Qt.ItemIsUserCheckable
                              ^ qtc.Qt.ItemIsEditable)
                item.setCheckState(d[1])
                parent.appendRow(item)

            else:
                if d:
                    item = qtg.QStandardItem(str(d))
                    parent.appendRow(item)

    def fill_model_with_values(self, parent, d):
        if isinstance(d, dict):
            for key, val in d.items():
                child = qtg.QStandardItem(str(key))
                if isinstance(val, dict):
                    parent.appendRow(child)
                    self.fill_model_with_values(child, val)
                elif isinstance(val, list):
                    child2 = [qtg.QStandardItem(str(v)) for v in val]
                    parent.appendRow([child] + child2)

