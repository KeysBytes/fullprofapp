from PyQt5 import QtWidgets as qtw

from fpgui.uifiles.uipcrparamdialog import Ui_Form
from fpgui.custommodels import PandasTableModel

from typing import List
import pandas as pd
import os


class PcrDialog(qtw.QDialog):

    def __init__(self, patterns: List[str], *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._pattern_pcr = dict()  # Maps pcr urls to pattern urls
        self.pcr_map = dict()
        self.patterns = patterns
        self._output = None
        self.warning = qtw.QMessageBox()

        # load form
        self.ui_pcrparams = Ui_Form()
        self.ui_pcrparams.setupUi(self)

        # Fill table with initial information about pattern files
        self.init_patterns()

        self.ui_pcrparams.table_files.setSelectionBehavior(qtw.QTableView.SelectRows)
        self.ui_pcrparams.table_files.setEditTriggers(qtw.QTableView.NoEditTriggers)
        self.ui_pcrparams.table_files.setAlternatingRowColors(True)
        self.ui_pcrparams.dialog_button.setEnabled(True)

        # Signals
        self.ui_pcrparams.push_browse_pcr.clicked.connect(self.select_pcr)
        self.ui_pcrparams.push_add_files.clicked.connect(self.fill_table)
        self.ui_pcrparams.push_clear_files.clicked.connect(self.clear_table)

        self.ui_pcrparams.dialog_button.accepted.connect(self.accept)
        self.ui_pcrparams.dialog_button.rejected.connect(self.reject)

    def init_patterns(self) -> None:
        self._pattern_pcr['patterns'] = self.patterns
        self._pattern_pcr['pcr'] = [None for _ in range(len(self.patterns))]
        self.populate_table()

    def select_pcr(self):
        self.ui_pcrparams.pcr_combo.clear()
        if self.ui_pcrparams.radio_from_file.isChecked():
            filename, _ = qtw.QFileDialog.getOpenFileName(self, 'Open File', os.getcwd(), 'PCR files (*.pcr)')
            self.ui_pcrparams.pcr_combo.addItem(filename)
        elif self.ui_pcrparams.radio_from_results.isChecked():
            dirname = qtw.QFileDialog.getExistingDirectory(self, 'Select Folder')

            for pattern in self._pattern_pcr['patterns']:
                for item in os.listdir(dirname):
                    item_path = os.path.join(dirname, item)
                    if os.path.isdir(item_path):
                        pcr_file = next((s for s in os.listdir(item_path) if '.pcr' in s), None)
                        if pcr_file is not None:
                            filename = os.path.join(item_path, pcr_file)
                            if pattern in filename:
                                self.ui_pcrparams.pcr_combo.addItem(filename)

    def clear_table(self):
        self._pattern_pcr['pcr'] = [None for _ in range(len(self.patterns))]
        self.populate_table()

    def fill_table(self):
        indexes = self.ui_pcrparams.table_files.selectionModel().selectedRows()
        selected_rows = [row.row() for row in indexes]

        """Different cases for filling tables"""

        """ We do not select any rows in the table, we fill all items in table by looping the same pcr's """
        if not selected_rows:
            lenpatt = len(self.patterns)
            lenpcr = self.ui_pcrparams.pcr_combo.count()
            result = None

            num_loops = int(lenpatt / lenpcr)
            num_res = lenpatt % lenpcr

            if num_loops != 1:
                self.warning.setIcon(qtw.QMessageBox.Warning)
                self.warning.setText("The amount of .pcr that will be read is different from the amount of loaded"
                                     "patterns. Continue with this job?")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok | qtw.QMessageBox.Cancel)

                result = self.warning.exec_()

            if result == qtw.QMessageBox.Cancel:
                pass
            else:
                count = 0
                for i in range(num_loops):
                    for j in range(lenpcr):
                        self._pattern_pcr['pcr'][count] = self.ui_pcrparams.pcr_combo.itemText(j)
                        count += 1

                for i in range(num_res):
                    self._pattern_pcr['pcr'][count] = self.ui_pcrparams.pcr_combo.itemText(i)
                    count += 1

                self.populate_table()

        else:
            if len(selected_rows) != self.ui_pcrparams.pcr_combo.count():
                self.warning.setIcon(qtw.QMessageBox.Critical)
                self.warning.setText("The amount of .pcr files that you want to load is different from the amount"
                                     "of patterns you selected. Please select the right amount of columns.")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                self.warning.exec_()

            else:
                count = 0
                for row in selected_rows:
                    self._pattern_pcr['pcr'][row] = self.ui_pcrparams.pcr_combo.itemText(count)
                    count += 1

                self.populate_table()

    def populate_table(self):

        summary = pd.DataFrame(self._pattern_pcr)

        model = PandasTableModel(summary)
        self.ui_pcrparams.table_files.setModel(model)

    def accept(self):

        for count, patterns in enumerate(self._pattern_pcr['patterns']):
            if self._pattern_pcr['pcr'][count] is not None:
                self.pcr_map[patterns] = self._pattern_pcr['pcr'][count]

        self._output = self.pcr_map

        super(PcrDialog, self).accept()

    def get_output(self):
        return self._output

