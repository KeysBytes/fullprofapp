# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'constraintdialog.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog_const(object):
    def setupUi(self, Dialog_const):
        Dialog_const.setObjectName("Dialog_const")
        Dialog_const.resize(329, 269)
        self.gridLayout = QtWidgets.QGridLayout(Dialog_const)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.layout_coefficients = QtWidgets.QVBoxLayout()
        self.layout_coefficients.setObjectName("layout_coefficients")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.layout_coefficients.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.layout_coefficients)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.button_dialog = QtWidgets.QDialogButtonBox(Dialog_const)
        self.button_dialog.setOrientation(QtCore.Qt.Horizontal)
        self.button_dialog.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.button_dialog.setObjectName("button_dialog")
        self.verticalLayout.addWidget(self.button_dialog)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_const)
        self.button_dialog.accepted.connect(Dialog_const.accept)
        self.button_dialog.rejected.connect(Dialog_const.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog_const)

    def retranslateUi(self, Dialog_const):
        _translate = QtCore.QCoreApplication.translate
        Dialog_const.setWindowTitle(_translate("Dialog_const", "Constraint Parameters"))
