from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

from fpgui.uifiles.uiphasesearchwindow import Ui_Form

from fpgui.custommodels import ListTableModel, ListTableView
from fpgui.diffgraph import FomGraph

import pyqtgraph as pg
import numpy as np


class PhaseSearchWindow(qtw.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # load form
        self.ui_phasesearch = Ui_Form()
        self.ui_phasesearch.setupUi(self)

        self.fomgraph = FomGraph()
        self.fomplot = self.fomgraph.ui_diffgraph.diff_graph.plot(pen=pg.mkPen(color=(0, 85, 255), width=2.0),
                                                                  symbolPen=pg.mkPen(color=(0, 85, 255), width=1.5),
                                                                  symbolSize=3.0, symbolBrush=(0, 85, 255),
                                                                  name='FoM', connect="finite")
        self.fomgraph.ui_diffgraph.diff_graph.setLabel('left', 'FoM')
        self.fomgraph.ui_diffgraph.diff_graph.setLabel('bottom', 'Scanned Phases')

        self.rwpgraph = FomGraph()
        self.rwpplot = self.rwpgraph.ui_diffgraph.diff_graph.plot(pen=pg.mkPen(color=(204, 0, 204), width=2.0),
                                                                  symbolPen=pg.mkPen(color=(204, 0, 204), width=1.5),
                                                                  symbolSize=3.0,
                                                                  symbolBrush=(204, 0, 204), name='Rwp', connect="finite")
        self.rwpgraph.ui_diffgraph.diff_graph.setLabel('left', 'Rwp')
        self.rwpgraph.ui_diffgraph.diff_graph.setLabel('bottom', 'Detected Phases')

    def set_pattern_text(self, pattern: str):
        label = qtw.QLabel(f"Searching phases in Pattern: '{pattern}'")
        label.setStyleSheet("font-weight: bold")
        self.ui_phasesearch.graphay.addWidget(label)

    def update_fom_graph(self, data: list):
        self.fomplot.setData(x=np.arange(len(data)), y=data)

    def update_rwp_graph(self, data:list):
        self.rwpplot.setData(x=np.arange(len(data)), y=data)

    def setup_window(self, s1: list = [], s2: list = [], first: bool = True):
        if s1:
            header = "Identifier Formula".split()
            edit = [0, 1]
            model = ListTableModel(s1, header, edit)
            table = ListTableView()
            table.setModel(model)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table = self.resizeVerticalTableViewToContents(table)
            self.ui_phasesearch.scanarea.setWidget(table)

        if s2:
            header = "Identifier Formula".split()
            edit = [0, 1]
            model = ListTableModel(s2, header, edit)
            table = ListTableView()
            table.setModel(model)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table = self.resizeVerticalTableViewToContents(table)
            self.ui_phasesearch.detectarea.setWidget(table)

        if first:
            self.ui_phasesearch.graphay.addWidget(self.fomgraph)
            self.ui_phasesearch.graphay.addWidget(self.rwpgraph)

    def set_scan_background(self, color, rows):
        self.ui_phasesearch.scanarea.widget().model().change_color(color, rows)

    def set_detect_background(self, color, rows):
        self.ui_phasesearch.detectarea.widget().model().change_color(color, rows)

    def resizeVerticalTableViewToContents(self, table: ListTableView):
        totalheight = 0

        for i in range(table.verticalHeader().count()):
            if not table.verticalHeader().isSectionHidden(i):
                totalheight += table.verticalHeader().sectionSize(i)

        if not table.horizontalHeader().isHidden():
            totalheight += table.horizontalHeader().height()

        table.setFixedHeight(totalheight)

        return table