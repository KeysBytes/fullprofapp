from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

from fpgui.diffgraph import DiffGraph

import qtawesome as qta
import numpy as np

import os


class Toolbar(qtw.QToolBar):

    checks = qtc.pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.checked_code = None
        self.checked_index = None
        self.checked_sum = None

        self.names = None
        self.menu = None
        self.checktable = None

        """ Combo Patterns """
        self.patterns_combo = qtw.QComboBox()
        self.patterns_combo.view().setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAsNeeded)
        self.patterns_combo.setSizePolicy(qtw.QSizePolicy.Preferred, qtw.QSizePolicy.Fixed)
        self.patterns_combo.clear()
        self.patterns_combo.currentIndexChanged.connect(self.enable_buttons)

        """ Button next <-> back """
        self.button_back = qtw.QToolButton()
        self.button_next = qtw.QToolButton()

        self.button_next.setIcon(qta.icon('fa5s.caret-right'))
        self.button_back.setIcon(qta.icon('fa5s.caret-left'))
        self.button_next.setAutoRepeat(True)
        self.button_back.setAutoRepeat(True)
        self.button_next.setAutoRepeatDelay(500)
        self.button_back.setAutoRepeatDelay(500)

        self.button_next.clicked.connect(self.change_next_item)
        self.button_back.clicked.connect(self.change_back_item)

        self.button_menu = qtw.QToolButton()
        self.button_menu.setCheckable(True)
        self.button_menu.setIcon(qta.icon('ei.chevron-right'))
        self.menu = qtw.QMenu()
        self.button_menu.setMenu(self.menu)
        self.button_menu.setPopupMode(qtw.QToolButton.InstantPopup)
        self.button_menu.menu().aboutToShow.connect(self.resize_menu)

        """ Plot button """
        self.contour_plot = qtw.QPushButton()
        self.contour_plot.setText("Contour Plot")

        """ NPATT section """
        self.patt_label = qtw.QLabel('PATT: ')
        self.patt_num = qtw.QLineEdit()
        self.patt_num.setReadOnly(True)
        self.patt_num.setFixedWidth(40)

        """ Button next <-> back patterns """
        self.button_back_patt = qtw.QToolButton()
        self.button_next_patt = qtw.QToolButton()

        self.button_next_patt.setIcon(qta.icon('fa5s.caret-right'))
        self.button_back_patt.setIcon(qta.icon('fa5s.caret-left'))

        self.button_next_patt.clicked.connect(self.change_next_item_patt)
        self.button_back_patt.clicked.connect(self.change_back_item_patt)

        self.label_check = qtw.QLabel('Pattern Checks')

        """ Build Toolbar """
        self.addWidget(self.button_back)
        self.addWidget(self.patterns_combo)
        self.addWidget(self.button_next)
        self.addSeparator()
        self.addWidget(self.button_back_patt)
        self.addWidget(self.patt_label)
        self.addWidget(self.patt_num)
        self.addWidget(self.button_next_patt)
        self.addSeparator()
        self.addWidget(self.label_check)
        self.addWidget(self.button_menu)
        self.addSeparator()
        self.addWidget(self.contour_plot)

        self.patterns_combo.setEnabled(False)
        self.button_back.setEnabled(False)
        self.button_next.setEnabled(False)
        self.label_check.setEnabled(False)
        self.contour_plot.setEnabled(False)
        self.button_next_patt.setEnabled(False)
        self.button_back_patt.setEnabled(False)
        self.patt_label.setEnabled(False)
        self.patt_num.setEnabled(False)
        self.button_menu.setEnabled(False)

    def init_toolbar(self, data: list):
        self.patterns_combo.setEnabled(True)
        self.button_back.setEnabled(False)
        self.button_next.setEnabled(True)
        self.label_check.setEnabled(True)
        self.contour_plot.setEnabled(True)
        self.button_next_patt.setEnabled(True)
        self.button_back_patt.setEnabled(False)
        self.patt_label.setEnabled(True)
        self.patt_num.setEnabled(True)
        self.button_menu.setEnabled(True)

        self.patterns_combo.blockSignals(True)
        self.patterns_combo.clear()
        self.names = []
        tmp = 0
        for patterns in data:
            tails = [os.path.split(os.path.splitext(patt.filename)[0])[1] for patt in patterns]
            self.names.append(tails)

        """ By default we start on the first pattern """
        for name in self.names:
            self.patterns_combo.addItem(name[0])
            if len(name) > tmp:
                tmp = len(name)
                stylesheet = f"QComboBox QAbstractItemView {{ min-width: {tmp};}}"
                self.patterns_combo.setStyleSheet(stylesheet)

        self.patt_num.setText('0')

        """Init menu and set it to the QToolButton"""
        if self.button_menu.menu().actions() is not None:
            self.button_menu.menu().removeAction(self.checktable)
            self.checktable = CheckTableWidget(len(self.names), len(self.names[0]), self.names, self.menu)
            self.checktable.init_table()
            self.checktable.defaultWidget().cellDoubleClicked.connect(self.change_combo_status)
            self.checktable.cellChecked.connect(lambda: self.set_checkboxes())
            self.button_menu.menu().addAction(self.checktable)
        else:
            self.checktable = CheckTableWidget(len(self.names), len(self.names[0]), self.names, self.menu)
            self.checktable.init_table()
            self.checktable.defaultWidget().cellDoubleClicked.connect(self.change_combo_status)
            self.checktable.cellChecked.connect(lambda: self.set_checkboxes())
            self.button_menu.menu().addAction(self.checktable)

        self.set_checkboxes()
        self.enable_buttons()

        self.patterns_combo.blockSignals(False)
        self.patterns_combo.setCurrentIndex(0)

    def resize_menu(self):
        self.menu.setFixedHeight(self.checktable.defaultWidget().height())
        self.menu.setFixedWidth(self.checktable.defaultWidget().width())

    def update_combo(self, index, index_patt):
        tmp = 0
        self.patterns_combo.blockSignals(True)
        self.patterns_combo.clear()
        for name in self.names:
            self.patterns_combo.addItem(name[index_patt])
            if len(name) > tmp:
                tmp = len(name)
                stylesheet = f"QComboBox QAbstractItemView {{ min-width: {tmp};}}"
                self.patterns_combo.setStyleSheet(stylesheet)

        self.patt_num.setText(str(index_patt))
        self.set_checkboxes()
        self.enable_buttons()

        if index == 0:
            self.patterns_combo.setCurrentIndex(index + 1)

        self.patterns_combo.blockSignals(False)
        self.patterns_combo.setCurrentIndex(index)

    def enable_buttons(self):

        size = self.patterns_combo.count()
        index = self.patterns_combo.currentIndex()

        if size == 1:
            self.button_next.setEnabled(False)
            self.button_back.setEnabled(False)
        else:
            if index + 1 >= size:
                self.button_next.setEnabled(False)
                self.button_back.setEnabled(True)
            elif index <= 0:
                self.button_back.setEnabled(False)
                self.button_next.setEnabled(True)
            else:
                self.button_next.setEnabled(True)
                self.button_back.setEnabled(True)

        size_patt = len(self.names[0])
        index_patt = int(self.patt_num.text())

        if size_patt == 1:
            self.button_next_patt.setEnabled(False)
            self.button_back_patt.setEnabled(False)
        else:
            if index_patt + 1 >= size_patt:
                self.button_next_patt.setEnabled(False)
                self.button_back_patt.setEnabled(True)
            elif index_patt <= 0:
                self.button_back_patt.setEnabled(False)
                self.button_next_patt.setEnabled(True)
            else:
                self.button_next_patt.setEnabled(True)
                self.button_back_patt.setEnabled(True)

    def change_next_item_patt(self):
        index = self.patterns_combo.currentIndex()
        index_patt = int(self.patt_num.text())
        newindex = index_patt + 1

        self.update_combo(index, newindex)

    def change_back_item_patt(self):
        index = self.patterns_combo.currentIndex()
        index_patt = int(self.patt_num.text())
        newindex = index_patt - 1

        self.update_combo(index, newindex)

    def change_next_item(self):
        index = self.patterns_combo.currentIndex()
        newindex = index + 1
        self.patterns_combo.setCurrentIndex(newindex)

    def change_back_item(self):
        index = self.patterns_combo.currentIndex()
        newindex = index - 1
        self.patterns_combo.setCurrentIndex(newindex)

    def change_combo_status(self, row, col):
        self.update_combo(row, col)

    def set_checkboxes(self):
        self.checked_code = self.checktable.return_checked_matrix()
        self.derive_checked_values()

    def derive_checked_values(self):
        self.checked_sum = np.sum(self.checked_code, axis=1)
        self.checked_index = np.where(self.checked_sum != 0)[0]
        self.checks.emit(self.checked_index)


class CheckTableWidget(qtw.QWidgetAction):
    cellChecked = qtc.pyqtSignal(bool)

    def __init__(self, rows: int, cols: int, names: list, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.rows = rows
        self.cols = cols
        self.names = names

        self.init_table()

    def init_table(self):
        table = qtw.QTableWidget(self.rows, self.cols)
        table.setAlternatingRowColors(True)

        """Horizontal Headers """
        table.setHorizontalHeaderLabels([f"PATT: {i}" for i in range(self.cols)])
        stylesheet = "::section{Background-color:rgb(220,220,220)}"
        table.horizontalHeader().setStyleSheet(stylesheet)

        for i in range(table.rowCount()):
            for j in range(table.columnCount()):
                it_widget = qtw.QWidget()
                it_layout = qtw.QHBoxLayout(it_widget)
                label = qtw.QLabel(f'{self.names[i][j]}')
                it_layout.addWidget(label)
                it = qtw.QCheckBox()
                if i == 0:
                    it.setChecked(True)
                else:
                    it.setChecked(False)
                it.clicked.connect(self.check_all_selected)
                it_layout.addWidget(it)
                it_layout.setAlignment(qtc.Qt.AlignCenter)
                it_layout.setContentsMargins(10, 0, 10, 0)

                table.setCellWidget(i, j, it_widget)

        table.resizeColumnsToContents()
        table.resizeRowsToContents()
        table = self.resizeVerticalTableWidgetToContents(table)
        table = self.resizeHorizontalTableWidgetToContents(table)
        self.setDefaultWidget(table)

    def resizeVerticalTableWidgetToContents(self, table: qtw.QTableWidget):
        totalheight = 0

        for i in range(table.verticalHeader().count()):
            if i <= 20:
                if not table.verticalHeader().isSectionHidden(i):
                    totalheight += table.verticalHeader().sectionSize(i)

        if not table.horizontalHeader().isHidden():
            totalheight += table.horizontalHeader().height()

        table.setFixedHeight(totalheight + 5)

        if table.horizontalScrollBar().isHidden():
            totalheight = table.height() + table.horizontalScrollBar().height()
            table.setFixedHeight(totalheight + 10)

        return table

    def resizeHorizontalTableWidgetToContents(self, table: qtw.QTableWidget):
        totalwidth = 0

        for i in range(table.horizontalHeader().count()):
            if i <= 3:
                if not table.horizontalHeader().isSectionHidden(i):
                    totalwidth += table.horizontalHeader().sectionSize(i)

        if not table.verticalHeader().isHidden():
            totalwidth += table.verticalHeader().width()

        table.setFixedWidth(totalwidth + 5)

        if table.verticalScrollBar().isHidden():
            totalwidth = table.width() + table.verticalScrollBar().width()
            table.setFixedWidth(totalwidth + 10)

        return table

    def check_all_selected(self):
        selection = self.defaultWidget().selectedIndexes()
        for item in selection:
            row = item.row()
            col = item.column()

            widget = self.defaultWidget().cellWidget(row, col)

            if isinstance(widget, qtw.QWidget):
                check = widget.layout().itemAt(1).widget()

                if check.isEnabled():
                    if self.sender().isChecked():
                        check.setCheckState(qtc.Qt.Checked)
                    else:
                        check.setCheckState(qtc.Qt.Unchecked)

        self.cellChecked.emit(True)

    def check_uncheck_all(self, checked: bool):
        for i in range(self.defaultWidget().rowCount()):
            for j in range(self.defaultWidget().columnCount()):
                widget = self.defaultWidget().cellWidget(i, j)

                if isinstance(widget, qtw.QWidget):
                    check = widget.layout().itemAt(1).widget()

                    if check.isEnabled():
                        if checked:
                            check.setCheckState(qtc.Qt.Checked)
                        else:
                            check.setCheckState(qtc.Qt.Unchecked)

        self.cellChecked.emit(True)

    def check_indexes(self, rows: list, cols: list):
        for i in rows:
            for j in cols:
                widget = self.defaultWidget().cellWidget(i, j)

                if isinstance(widget, qtw.QWidget):
                    check = widget.layout().itemAt(1).widget()

                    if check.isEnabled():
                        check.setCheckState(qtc.Qt.Checked)

        self.cellChecked.emit(True)

    def uncheck_indexes(self, index):
        for idx in index:
            widget = self.defaultWidget().cellWidget(idx[0], idx[1])

            if isinstance(widget, qtw.QWidget):
                check = widget.layout().itemAt(1).widget()

                if check.isEnabled():
                    check.setCheckState(qtc.Qt.Unchecked)

        self.cellChecked.emit(True)

    def return_checked_matrix(self):
        matrix = np.zeros((self.defaultWidget().rowCount(), self.defaultWidget().columnCount()), dtype=int)
        for i in range(self.defaultWidget().rowCount()):
            for j in range(self.defaultWidget().columnCount()):
                widget = self.defaultWidget().cellWidget(i, j)

                if isinstance(widget, qtw.QWidget):
                    check = widget.layout().itemAt(1).widget()

                    if check.isChecked():
                        matrix[i, j] = 1
                    else:
                        matrix[i, j] = 0

        return matrix


