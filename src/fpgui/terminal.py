import os
from PyQt5 import QtWidgets as qtw

from fpgui.uifiles.uiterminal import Ui_Form


class Terminal(qtw.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # load form
        self.ui_terminal = Ui_Form()
        self.ui_terminal.setupUi(self)

        # connect signal to slots
        self.ui_terminal.clear.clicked.connect(self.reset)
        self.ui_terminal.screen.verticalScrollBar().rangeChanged.connect(self.__scroll)

        self.reset()

    def append(self, text):
        """ Append text
        """
        cursor = self.ui_terminal.screen.textCursor()
        cursor.movePosition(cursor.End)
        cursor.insertText(text)

    def prompt(self):
        """ New line + prompt.
        """
        self.append("\n>>> ")

    def reset(self):
        """ Clean the terminal.
        """
        self.ui_terminal.screen.clear()
        self.append("### Terminal for displaying the standard output ###")
        self.prompt()

    def __scroll(self):
        """ Move to the end of the text
        """
        scrollBar = self.ui_terminal.screen.verticalScrollBar()
        scrollBar.setValue(scrollBar.maximum())
