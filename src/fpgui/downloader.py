from PyQt5 import QtCore as qtc
from PyQt5 import QtNetwork as qtn


class DownLoader(qtc.QObject):

    downloaded = qtc.pyqtSignal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._webcontrol = qtn.QNetworkAccessManager()
        self._data = None

        self._webcontrol.finished.connect(self._file_downloaded)

    def _file_downloaded(self, reply):
        self._data = reply.readAll()

        assert isinstance(self._data, qtc.QByteArray)
        reply.deleteLater()
        self.downloaded.emit()

    def do_download(self, url):
        assert isinstance(url, qtc.QUrl)

        request = qtn.QNetworkRequest(url)
        self._webcontrol.get(request)

    def get_data(self):
        return self._data
