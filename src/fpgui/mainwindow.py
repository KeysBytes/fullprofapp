import os
import shutil
import sys
import fnmatch
from datetime import datetime

import numpy as np
import pandas as pd
import pyqtgraph as pg

from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg

from fpfunctions.app_io import CifReaderWriter, PcrIO, PrfIO, SumIO, MicIO, BackgroundType
from fpfunctions.client import Client
from fpfunctions.signals import DiffractionSignal
import fpfunctions.symbols as symbols

from fpgui.bckgrparam import BckgrParam
from fpgui.textview import CifText, FileText
from fpgui.custommodels import PandasTableModel, SystemDialog, PandasQSortFilterProxyModel
from fpgui.diffparamdialog import DiffParamDialog
from fpgui.peaksparam import PeaksParam
from fpgui.periodictable import PeriodicTable
from fpgui.terminal import Terminal
from fpgui.diffgraph import DiffGraph, SimGraph, ResultsGraph, PopUpDiffGraph
from fpgui.toolbar import Toolbar
from fpgui.collapsable import CollapsableProtocol
from fpgui.fpsearchparams import SearchParams
from fpgui.fpmanualparams import ManualParams
from fpgui.fpautorietveld import AutoRietveld
from fpgui.imagegraph import ContourGraph
from fpgui.downloader import DownLoader
from fpgui.phasesearchwindow import PhaseSearchWindow
from fpgui.scheduler import FullProfProcessScheduler, FullProfProcess
from fpgui.customerrors import FullProfOutputReadError

from fpgui.uifiles.uimainwindow import Ui_MainWindow

from crysfml_python import crysfml08_forpy


class MainWindow(qtw.QMainWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """ Setup UI for the main window """
        self.ui_mainwindow = Ui_MainWindow()
        self.ui_mainwindow.setupUi(self)
        self.showMaximized()

        self.plt1 = None  #Diffraction line
        self.plt2 = None  #Background
        self.plt3 = None  #Anchor points
        self.plt4 = None  #Selected points
        self.plt5 = None  #Peaks

        self.peak_lines = None
        self.completed = None
        self.graph = None
        self.patterns = None
        self.workdir = None
        self.selected_points = None
        self.diffparam = None
        self.param = None
        self.peaksparam = None
        self.period = None
        self.data = None
        self.summary = None
        self.selection = None
        self.filetext = None
        self.currentpath = None
        self.cifdir = None
        self.filesysmodel = None
        self.action_show_view = None
        self.action_delete_file = None
        self.menu = None
        self.simgraph = None
        self.error = None
        self.warning = None
        self.rerunsim = None
        self.rerunparams = None
        self.result_dict = None
        self.generate_all = False
        self.settings = None
        self.s1 = None
        self.s2 = None
        self.lutefom = None
        self.v0 = None
        self.vf = None
        self.remove_from_s1 = None
        self.qerror = None
        self.luteres = None
        self.maxfom = None
        self.luterwp = None
        self.algo = None
        self._seqpataction = None

        self.plot_prf = {'current': None, 'popup': None}
        self.resultgraph = {'current': None, 'popup': None}

        """Executables"""
        self.find_executables()
        self.run_num = 0

        """ Desktop Service """
        self.desktop_service = qtg.QDesktopServices()

        """ Set Pens """
        self.pen1 = pg.mkPen(color=(0, 85, 255), width=1.5)
        self.pen2 = pg.mkPen(color=(0, 204, 0), width=1.5)
        self.pen3 = pg.mkPen(color=(0, 0, 0), width=1.5)
        self.pen4 = pg.mkPen(color=(255, 0, 0), width=1.5)
        self.pen5 = pg.mkPen(color=(255, 105, 180), width=1.5)
        self.pen6 = pg.mkPen(color=(255, 0, 0), width=1.5)
        self.pen7 = pg.mkPen(color=(0, 0, 0), width=1.5)
        self.pen8 = pg.mkPen(color=(0, 204, 0), width=1.5)

        """ Set custom graph events and properties """
        self.diffgraph = DiffGraph()
        self.ui_mainwindow.layout_raw_data.addWidget(self.diffgraph)
        self.diffgraph.reset_selection.connect(self.reset_selection)
        self.diffgraph.select_add.connect(self.select_add)
        self.diffgraph.add_point.connect(self.add_point)
        self.diffgraph.select_drag.connect(self.select_drag)
        self.diffgraph.delete.connect(self.delete)
        self.diffgraph.point_pos.connect(self.show_pos)

        self.indexRawData = qtw.QTabWidget.indexOf(self.ui_mainwindow.tabGraphs, self.ui_mainwindow.tab_raw_data)
        self.indexPrfData = qtw.QTabWidget.indexOf(self.ui_mainwindow.tabGraphs, self.ui_mainwindow.tab_prf_data)
        self.indexSummary = qtw.QTabWidget.indexOf(self.ui_mainwindow.tabGraphs, self.ui_mainwindow.tab_output_data)
        self.ui_mainwindow.tabGraphs.setTabVisible(self.indexRawData, True)
        self.ui_mainwindow.tabGraphs.setTabVisible(self.indexPrfData, False)
        self.ui_mainwindow.tabGraphs.setTabVisible(self.indexSummary, False)
        self.ui_mainwindow.tabGraphs.tabBar().setTabButton(0, qtw.QTabBar.RightSide, None)
        self.ui_mainwindow.tabGraphs.tabCloseRequested.connect(self.close_tabs)

        self.plot_prf['current'] = DiffGraph()
        self.plot_prf['current'].x_pos.connect(self.plot_prf['current'].highlight_reflections)
        self.ui_mainwindow.layout_prf_data.addWidget(self.plot_prf['current'])
        self.plot_prf['popup'] = PopUpDiffGraph()
        self.plot_prf['popup'].x_pos.connect(self.plot_prf['popup'].highlight_reflections)
        self.plot_prf['popup'].combo_paths.currentIndexChanged.connect(self.plot_selected_signal)
        self.plot_prf['popup'].button_back.clicked.connect(self.plot_selected_signal)
        self.plot_prf['popup'].button_next.clicked.connect(self.plot_selected_signal)

        self.resultgraph['current'] = ResultsGraph()
        self.ui_mainwindow.layout_output_data.addWidget(self.resultgraph['current'])
        self.resultgraph['popup'] = ResultsGraph()

        """ Customize ScrollArea events """
        self.ui_mainwindow.scrollArea.wheelEvent = self.custom_wheel_event

        """ Set ToolBar """
        self.graph_toolbar = Toolbar()
        self.ui_mainwindow.graph_layout_V.setMenuBar(self.graph_toolbar)
        self.graph_toolbar.patterns_combo.currentIndexChanged.connect(self.update_patterns_display)
        self.graph_toolbar.patterns_combo.currentIndexChanged.connect(self.update_plot_prf)
        self.graph_toolbar.patterns_combo.currentIndexChanged.connect(self.populate_tableresults)
        self.graph_toolbar.checks.connect(self.update_patterns_manual_combo)
        self.graph_toolbar.checks.connect(self.update_patterns_autorietveld_combo)
        self.graph_toolbar.contour_plot.clicked.connect(self.plot_contour)

        """ Widgets """
        self.search_params = SearchParams()
        self.search_params.ui_searchparams.button_fp_search.setEnabled(False)
        self.search_params.ui_searchparams.button_fp_search.clicked.connect(lambda: self.init_fp_search())

        self.manual_params = ManualParams()
        self.manual_params.ui_manualparams.pattern_combo.currentIndexChanged.connect(
            lambda ind: self.graph_toolbar.patterns_combo.setCurrentIndex(self.graph_toolbar.checked_index[ind]))
        self.manual_params.ui_manualparams.button_minus.clicked.connect(self.on_clicked_minus)
        self.manual_params.ui_manualparams.button_minusminus.clicked.connect(self.on_clicked_minusminus)
        self.manual_params.ui_manualparams.button_plus.clicked.connect(self.on_clicked_plus)
        self.manual_params.ui_manualparams.button_plusplus.clicked.connect(self.on_clicked_plusplus)
        self.manual_params.ui_manualparams.run_fp.clicked.connect(self.init_manual_rietveld)
        self.manual_params.ui_manualparams.run_fp_new.clicked.connect(self.init_manual_rietveld_new)

        self.auto_rietveld = AutoRietveld()
        self.auto_rietveld.ui_autorietveld.pattern_combo.currentIndexChanged.connect(
            lambda ind: self.graph_toolbar.patterns_combo.setCurrentIndex(self.graph_toolbar.checked_index[ind]))
        self.auto_rietveld.ui_autorietveld.button_minus.clicked.connect(self.on_clicked_minus)
        self.auto_rietveld.ui_autorietveld.button_minusminus.clicked.connect(self.on_clicked_minusminus)
        self.auto_rietveld.ui_autorietveld.button_plus.clicked.connect(self.on_clicked_plus)
        self.auto_rietveld.ui_autorietveld.button_plusplus.clicked.connect(self.on_clicked_plusplus)
        self.auto_rietveld.ui_autorietveld.button_run_fp.clicked.connect(self.auto_decider)
        self.auto_rietveld.backg_menu.pattaction.triggered.connect(self.current_background_to_selected_pattern)
        self.auto_rietveld.backg_menu.fileaction.triggered.connect(self.file_background_to_selected_pattern)
        self.auto_rietveld.ui_autorietveld.tree_patterns.customContextMenuRequested.connect(self.show_background_options)
        self.auto_rietveld.ui_autorietveld.tree_patterns.setContextMenuPolicy(qtc.Qt.CustomContextMenu)

        """ Protocol widgets """
        self.lute_protocol = CollapsableProtocol(
            title="Full-Profile Phase Search Protocol", widget=self.search_params)
        self.manual_protocol = CollapsableProtocol(
            title="Manual Refinement", widget=self.manual_params)
        self.auto_protocol = CollapsableProtocol(
            title="Automatic Refinement Protocol", widget=self.auto_rietveld)

        self.ui_mainwindow.protocolVLayout.addWidget(self.lute_protocol)
        self.ui_mainwindow.protocolVLayout.addWidget(self.auto_protocol)
        self.ui_mainwindow.protocolVLayout.addWidget(self.manual_protocol)

        self.ui_mainwindow.protocolVLayout.addStretch()

        self.lute_protocol.expandableButton.click()
        self.lute_protocol.expandableButton.click()
        self.manual_protocol.expandableButton.click()
        self.manual_protocol.expandableButton.click()
        self.auto_protocol.expandableButton.click()
        self.auto_protocol.expandableButton.click()

        """ 'File' Toolbar action functionalities """
        self.ui_mainwindow.actionSelect_Project_Folder.triggered.connect(self.select_folder)
        self.ui_mainwindow.actionClear_Work_Directory.triggered.connect(self.clear_folder)

        """ 'Pattern' Toolbar action functionalities """
        self.ui_mainwindow.actionImport_Pattern.triggered.connect(lambda: self.diffparam.exec_())
        self.ui_mainwindow.actionAutomatic_Background.triggered.connect(self.get_background_auto)
        self.ui_mainwindow.actionPeak_Detection.triggered.connect(self.get_peaks_auto)
        self.ui_mainwindow.actionEdit_Background.triggered.connect(lambda:
                                                                   self.ui_mainwindow.actionEdit_Peaks.setChecked(False))
        self.ui_mainwindow.actionEdit_Peaks.triggered.connect(lambda:
                                                              self.ui_mainwindow.actionEdit_Background.setChecked(False))

        self.ui_mainwindow.actionExport_Background.triggered.connect(self.export_background)

        """ Peak extraction formats """
        self.ui_mainwindow.actionFree.triggered.connect(self.export_free_peaks)

        """ 'Material' Toolbar action functionalities """
        self.ui_mainwindow.actionSearch_Remote.triggered.connect(self.search_remote)
        self.ui_mainwindow.actionSearch_Local.triggered.connect(self.search_local)

        """ Materials table functionalities """
        self.indexTerminal = qtw.QTabWidget.indexOf(self.ui_mainwindow.tabMaterials, self.ui_mainwindow.tabTerminal)
        self.indexSelection = qtw.QTabWidget.indexOf(self.ui_mainwindow.tabMaterials, self.ui_mainwindow.tabSelection)
        self.indexQuery = qtw.QTabWidget.indexOf(self.ui_mainwindow.tabMaterials, self.ui_mainwindow.tabQuery)
        self.indexResults = qtw.QTabWidget.indexOf(self.ui_mainwindow.tabMaterials, self.ui_mainwindow.tabResults)
        self.ui_mainwindow.tabMaterials.setTabVisible(self.indexSelection, False)
        self.ui_mainwindow.tabMaterials.setTabVisible(self.indexQuery, False)
        self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, False)
        self.ui_mainwindow.tableQuery.setSelectionBehavior(qtw.QTableView.SelectRows)
        self.ui_mainwindow.tableQuery.setAlternatingRowColors(True)
        self.proxyModel = PandasQSortFilterProxyModel()
        self.ui_mainwindow.search_filter.textChanged.connect(self.proxyModel.setFilterByColumn)
        self.ui_mainwindow.tableSelection.setSelectionBehavior(qtw.QTableView.SelectRows)
        self.ui_mainwindow.tableSelection.setAlternatingRowColors(True)
        self.ui_mainwindow.tableResults.setSelectionBehavior(qtw.QTableView.SelectRows)
        self.ui_mainwindow.tableResults.setEditTriggers(qtw.QTableView.NoEditTriggers)
        self.ui_mainwindow.tableResults.setAlternatingRowColors(True)

        """ Add Terminal """
        self.terminal = Terminal()
        self.ui_mainwindow.layoutTerminal.addWidget(self.terminal)

        self.ui_mainwindow.button_submit_selection.clicked.connect(self.update_selection)
        self.ui_mainwindow.button_simulate_selection.clicked.connect(self.simulate_from_table)

        self.ui_mainwindow.tableSelection.keyPressEvent = self.remove_selection_rows
        self.ui_mainwindow.tableQuery.keyPressEvent = self.remove_summary_rows

        """ 'View' Toolbar action fuctionalities """
        self.ui_mainwindow.actionCurrent_prf.triggered.connect(self.plot_current_prf)
        self.ui_mainwindow.actionSelect_prf.triggered.connect(self.plot_selected_prf)
        self.ui_mainwindow.actionCurrent_WinPLOTR.triggered.connect(self.run_winplotr)
        self.ui_mainwindow.actionSelect_WinPLOTR.triggered.connect(self.run_selected_winplotr)
        self.ui_mainwindow.actionCurrent_Result.triggered.connect(self.plot_current_results)
        self.ui_mainwindow.actionSelect_Result.triggered.connect(self.plot_selected_results)

        """ 'Help' Toolbar action functionalities """
        self.ui_mainwindow.actionManual.triggered.connect(self.show_web_docs)
        self.ui_mainwindow.actionAbout.triggered.connect(self.show_about)

        """ Tree View """
        self.ui_mainwindow.treeExplorer.setAlternatingRowColors(True)
        self.ui_mainwindow.treeExplorer.setSelectionMode(qtw.QAbstractItemView.ExtendedSelection)
        self.ui_mainwindow.treeExplorer.setContextMenuPolicy(qtc.Qt.CustomContextMenu)
        self.ui_mainwindow.treeExplorer.keyPressEvent = self.delete_filesystem_entry
        self.ui_mainwindow.treeExplorer.customContextMenuRequested.connect(self.show_context_menu)

        """ Scheduler """
        self.fpscheduler = FullProfProcessScheduler(process = FullProfProcess,
                                                    procargs = {'errors': 
                                                               ['Singular Matrix!!', 
                                                                ' NaN ', 
                                                                'Unrecoverable divergence!!', 
                                                                'Hole(s) in the Normal Matrix', 
                                                                'Strong DIVERGENCE', 
                                                                'NOT FOUND', 
                                                                'is illegal:',
                                                                'ERROR:',
                                                                'Error reading the PCR',
                                                                'invalid character',
                                                                'not exist',
                                                                'Too many reflections',
                                                                'Catastrophic error!',
                                                                'NaN detected',
                                                                'Error opening a BAC-file']})
        self.fpscheduler.process_started.connect(self.started_on_slot)
        self.fpscheduler.process_finished.connect(self.finished_on_slot)
        self.fpscheduler.process_error.connect(self.error_on_slot)
        self.fpscheduler.all_finished.connect(self.all_finished_slots)
        self.terminal.ui_terminal.kill_button.clicked.connect(self.kill_processes)
        
        """ Diffparamdialog """
        self.diffparam = DiffParamDialog()
        self.diffparam.accepted.connect(self.open_diffraction)

        """ Settings object for saving the current APP state """
        self.fileini = None
        self.ui_mainwindow.actionSave.setShortcut(qtg.QKeySequence("Ctrl+S"))
        self.ui_mainwindow.actionSave.triggered.connect(self.save_project)
        self.ui_mainwindow.actionSave_As.triggered.connect(self.save_project_as)
        self.ui_mainwindow.actionLoad.triggered.connect(self.load_project)

        """ Set Downloader, ProgressBar, and Slots """
        self.downloader = DownLoader()
        self.progressbar = qtw.QProgressBar()
        self.pathstatusbar = qtw.QStatusBar()
        self.ui_mainwindow.statusbar.addPermanentWidget(self.pathstatusbar)
        self.ui_mainwindow.statusbar.addPermanentWidget(self.progressbar)
        self.progressbar.hide()

        """ If .fpapp file is passed as argument we can catch it here and load it """
        for arg in sys.argv:
            if '.fpapp' in os.path.splitext(arg)[-1]:
                self.fileini = arg
                self.read_settings()
                break

    def show_web_docs(self):
        self.desktop_service.openUrl(qtc.QUrl("https://fullprofapp.readthedocs.io/en/stable/")) 

    def show_about(self):
        self.warning = qtw.QMessageBox()
        self.warning.setIcon(qtw.QMessageBox.Information)
        self.warning.setText("Version 1.2.11")
        self.warning.setStandardButtons(qtw.QMessageBox.Ok)

        self.warning.exec_()

    def close_windows(self):
        if self.simgraph is not None and self.simgraph.isVisible():
            self.simgraph.close()
        if self.param is not None and self.param.isVisible():
            self.param.close()
        if self.peaksparam is not None and self.peaksparam.isVisible():
            self.peaksparam.close()
        if self.resultgraph['popup'] is not None and self.resultgraph['popup'].isVisible():
            self.resultgraph['popup'].close()
        if self.plot_prf['popup'] is not None and self.plot_prf['popup'].isVisible():
            self.plot_prf['popup'].close()
        if self.graph is not None and self.graph.isVisible():
            self.graph.close()

    def find_executables(self):
        self.cif2pcr_path = shutil.which("CIFs_to_PCR")
        self.fp2k_path = shutil.which("fp2k")
        if sys.platform.startswith('linux'):
            self.winplotr_path = shutil.which("winplotr-2006")
            separator = ':'
        elif sys.platform.startswith('win32'):
            self.winplotr_path = shutil.which("WinPLOTR-2006")
            separator = ';'
        else:
            raise ValueError('Not implemented in MAC')
        
        if any(item is None for item in [self.cif2pcr_path, self.fp2k_path, self.winplotr_path]):

            try:
                with open(os.path.join(os.getcwd(), 'fullprof_path'), 'r') as f:
                    possible_paths = f.readlines()

                for path in possible_paths:
                    if path not in os.environ['PATH']:
                        os.environ['PATH'] += separator+os.path.normpath(path)

            except FileNotFoundError:
                self.warning = qtw.QMessageBox()
                self.warning.setIcon(qtw.QMessageBox.Warning)
                self.warning.setText("PATH Error: The FullProfSuite programs cannot be found in the PATH environment\n\n  - Ok: Add FullProfSuite path manually\n  - Cancel: Abort but you will not be able to refine or simulate")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok | qtw.QMessageBox.Cancel)
                result = self.warning.exec_()
                if result == qtw.QMessageBox.Ok:
                    fpsuite_path = qtw.QFileDialog.getExistingDirectory(self, 'Select FullProfSuite Folder', os.getcwd(), qtw.QFileDialog.ShowDirsOnly)
                    os.environ['PATH'] += separator+os.path.normpath(fpsuite_path)

                    with open(os.path.join(os.getcwd(), 'fullprof_path'), 'w') as f:
                        f.write(os.path.normpath(fpsuite_path))
                else:
                    return
            finally:
                self.cif2pcr_path = shutil.which("CIFs_to_PCR")
                self.fp2k_path = shutil.which("fp2k")
                if sys.platform.startswith('linux'):
                    self.winplotr_path = shutil.which("winplotr-2006")
                elif sys.platform.startswith('win32'):
                    self.winplotr_path = shutil.which("WinPLOTR-2006")
                else:
                    raise ValueError('Not implemented in MAC')
        
                if any(item is None for item in [self.cif2pcr_path, self.fp2k_path, self.winplotr_path]):
                    self.warning = qtw.QMessageBox()
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText("The FullProfSuite programs cannot be found in the provided paths")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    result = self.warning.exec_()
                
    def closeEvent(self, ev):
        question = qtw.QMessageBox()
        question.setIcon(qtw.QMessageBox.Question)
        question.setWindowTitle(f"Close")
        question.setText(f"Do you want to save the current configuration?")
        question.setStandardButtons(qtw.QMessageBox.Yes | qtw.QMessageBox.No | qtw.QMessageBox.Cancel)

        res = question.exec_()

        if res == qtw.QMessageBox.Yes:
            try:
                self.fileini = None
                self.save_project()
                question = qtw.QMessageBox()
                question.setWindowTitle(f"Success")
                question.setText(f"Configuration saved in \n\n  -{self.fileini}\n\n Closing FullProfAPP")
                question.setStandardButtons(qtw.QMessageBox.Ok) 
                res1 = None
            except:
                critical = qtw.QMessageBox()
                critical.setIcon(qtw.QMessageBox.Critical)
                critical.setWindowTitle(f"Critical")
                critical.setText(f"Failed at saving configuration. Close anyways?")
                critical.setStandardButtons(qtw.QMessageBox.Yes | qtw.QMessageBox.No)
                res1 = critical.exec_()
            finally:
                if res1 is not None:
                    if res1 == qtw.QMessageBox.Yes:
                        self.close_windows()
                        ev.accept()
                    elif res1 == qtw.QMessageBox.No:
                        ev.ignore()
                else:
                    self.close_windows()
                    ev.accept()
        
        elif res == qtw.QMessageBox.No:
            self.close_windows()
            ev.accept()

        else:
            ev.ignore()

    def custom_wheel_event(self, ev):
        if ev.modifiers() == qtc.Qt.ControlModifier:
            pass
        else:
            qtw.QScrollArea.wheelEvent(self.ui_mainwindow.scrollArea, ev)

    def set_filesystem(self, path: str):
        if path:
            os.chdir(path)
            self.workdir = path

            """ Update File System View """
            self.filesysmodel = qtw.QFileSystemModel()
            self.filesysmodel.setRootPath(os.getcwd())
            self.filesysmodel.setReadOnly(False)
            self.filesysmodel.fileRenamed.connect(self.handle_cif_rename_filesystem)
            """ Update Tree View """
            self.ui_mainwindow.treeExplorer.setModel(self.filesysmodel)
            self.ui_mainwindow.treeExplorer.setRootIndex(self.filesysmodel.index(os.getcwd()))

            """ Create in layout """
            self.ui_mainwindow.treeVLayout.addWidget(self.ui_mainwindow.treeExplorer)
            self.ui_mainwindow.tabExplorer.setLayout(self.ui_mainwindow.treeVLayout)

            """ Write to Terminal """
            self.terminal.append(f"Work directory is in {os.getcwd()}\n")
            msg = f"Current Workdir: {self.workdir}"
            self.pathstatusbar.showMessage(msg)
            self.terminal.prompt()

    def select_folder(self):
        """ Change directory to selected one """
        path = qtw.QFileDialog.getExistingDirectory(self, 'Select Project Folder')
        path = os.path.normpath(path)
        self.set_filesystem(path)

    def clear_folder(self):
        self.warning = qtw.QMessageBox()
        self.warning.setIcon(qtw.QMessageBox.Warning)
        self.warning.setText(f"Are you sure you want to delete all your previous jobs?")
        self.warning.setStandardButtons(qtw.QMessageBox.Ok | qtw.QMessageBox.Cancel)

        res = self.warning.exec_()

        if res == qtw.QMessageBox.Ok:
            for item in os.listdir(os.getcwd()):
                if "FPSEARCH" in item:
                    shutil.rmtree(os.path.join(os.getcwd(), item))

                if "SIMULATE" in item:
                    shutil.rmtree(os.path.join(os.getcwd(), item))

                if "MANUAL" in item:
                    shutil.rmtree(os.path.join(os.getcwd(), item))

                if "FPAUTO" in item:
                    shutil.rmtree(os.path.join(os.getcwd(), item))

                if 'FPSEQ' in item:
                    shutil.rmtree(os.path.join(os.getcwd(), item))

    def show_context_menu(self, position):
        self.action_show_view = qtw.QAction("Show File")
        self.action_show_view.triggered.connect(self.show_text_str)

        self.action_rename_item = qtw.QAction("Rename")
        self.action_rename_item.triggered.connect(self.rename_filesystem_entry)

        self.menu = qtw.QMenu(self.ui_mainwindow.treeExplorer)
        self.menu.addAction(self.action_show_view)
        self.menu.addAction(self.action_rename_item)
        self.menu.exec_(self.ui_mainwindow.treeExplorer.mapToGlobal(position))

    def rename_filesystem_entry(self):
        index = self.ui_mainwindow.treeExplorer.currentIndex()
        self.ui_mainwindow.treeExplorer.edit(index)

    def handle_cif_rename_filesystem(self, path, oldname, newname):
        oldpath = os.path.join(os.path.normpath(path), oldname)
        newpath = os.path.join(os.path.normpath(path), newname)

        if os.path.isfile(newpath) and ".cif" in os.path.splitext(oldpath)[-1]:
            if ".cif" not in os.path.splitext(newpath)[-1]:
                self.ui_mainwindow.treeExplorer.blockSignals(True)
                index = self.filesysmodel.index(newpath)
                self.filesysmodel.setData(index, oldname)
                self.ui_mainwindow.treeExplorer.blockSignals(False)

        if self.cifdir is not None:
            if self.cifdir == os.path.normpath(oldpath):
                self.cifdir = newpath
                self.enable_actions_buttons()    
                return

            if newname in os.listdir(self.cifdir):
                identifier = os.path.splitext(newname)[0]
                model = self.ui_mainwindow.tableQuery.model().sourceModel()
                if model is not None:
                    model.change_data(oldname, newname)

                model = self.ui_mainwindow.tableSelection.model()
                if model is not None:
                    model.change_data(oldname, newname)
            
                self.enable_actions_buttons()

    def handle_cif_delete_filesystem(self, path):
        if self.cifdir is None:
            return 

        if os.path.samefile(self.cifdir, path):
            self.cifdir = None
            self.selection = pd.DataFrame()
            self.summary = pd.DataFrame()
            self.populate_tablequery()
            self.populate_tableselection()
            self.enable_actions_buttons()    
            return 

        if ".cif" in os.path.splitext(path)[-1] and os.path.isfile(path) and os.path.basename(path) in os.listdir(self.cifdir):
            identifier = os.path.splitext(path)[0]
            identifier = os.path.basename(identifier)
            model = self.ui_mainwindow.tableQuery.model().sourceModel()
            if model is not None:
                model.remove_row(identifier)

            model = self.ui_mainwindow.tableSelection.model()
            if model is not None:
                model.remove_row(identifier)
           
            self.enable_actions_buttons()

    def delete_in_path(self, path):
        self.handle_cif_delete_filesystem(path)
        if os.path.isdir(path):
            shutil.rmtree(path)
        elif os.path.isfile(path):
            os.remove(path)

    def delete_filesystem_entry(self, ev):
        if ev.key() == qtc.Qt.Key_Delete:
            index = self.ui_mainwindow.treeExplorer.selectedIndexes()
            paths = [self.filesysmodel.filePath(idx) for idx in index]
            paths = set(paths)
            pathsmsg = '\n'.join(paths)
            msg = f"Are you sure you want to delete the following files/folders?\n\n{pathsmsg}"

            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Warning)
            self.warning.setText(msg)
            self.warning.setStandardButtons(qtw.QMessageBox.Ok | qtw.QMessageBox.Cancel)
            res = self.warning.exec_()
            if res == qtw.QMessageBox.Ok:
                for path in paths:
                    self.delete_in_path(path)

    def show_text_str(self):
        index = self.ui_mainwindow.treeExplorer.selectedIndexes()[0]

        path = self.filesysmodel.filePath(index)

        if not os.path.isdir(path):
            self.filetext = FileText()
            self.filetext.write_file_text(filename=path)
            self.filetext.ui_text.text.setReadOnly(True)
            self.filetext.show()

    def open_diffraction(self):
        data = self.diffparam.get_output()
        self.set_patterns(data)

    def set_patterns(self, data):
        self.patterns = []
        self.graph_toolbar.patterns_combo.blockSignals(True)
        self.graph_toolbar.patterns_combo.clear()
        self.graph_toolbar.patterns_combo.blockSignals(False)

        self.currentpath = None

        unique_radiation = [list(set(patt['radiation']))[0] for patt in data]
        self.auto_rietveld.set_radiation(unique_radiation)
        self.manual_params.set_radiation(unique_radiation)

        for patt in data:
            pattern_objects = []
            for files in zip(patt['patterns'], patt['irf'], patt['fmt'], patt['res'], patt['wavelength'], patt['radiation'], patt['geometry'], patt['option_ilo']):
                """ Initialize DiffractionData object and plot """
                diff = DiffractionSignal.from_file(filename=files[0], fmt=files[2])
                diff.irf = files[1]
                diff.irf_res = files[3]
                diff.wavelength = files[4]
                diff.radiation = files[5]
                diff.geometry = files[6]
                diff.option_ilo = files[7]
                pattern_objects.append(diff)
                
            self.patterns.append(pattern_objects)

        self.auto_rietveld.reset_all()
        self.manual_params.reset_all()
        self.patterns = list(map(list, zip(*self.patterns)))

        self.graph_toolbar.init_toolbar(data=self.patterns)
        self.update_patterns_display(first=True)
        self.enable_actions_buttons()
        self.search_params.excludedtab.max_patt = len(self.patterns[0])
        self.search_params.excludedtab.ui_excludetab.patt_num.setText('0')
        self.search_params.excludedtab.init_widget()
        self.auto_rietveld.excludedtab.ui_excludetab.patt_num.setText('0')
        self.auto_rietveld.excludedtab.max_patt = len(self.patterns[0])
        self.auto_rietveld.excludedtab.init_widget()
        self.terminal.append(f"{len(self.patterns)} sets of Patterns read from files \n")
        self.terminal.prompt()

    def update_patterns_manual_combo(self, ci):
        self.manual_params.ui_manualparams.pattern_combo.blockSignals(True)
        self.manual_params.ui_manualparams.pattern_combo.clear()
        self.manual_params.ui_manualparams.pattern_combo.setEnabled(True)
        self.manual_params.ui_manualparams.load_pcr.setEnabled(True)
        tmp = 0
        if np.size(ci):
            for index in ci:
                self.manual_params.ui_manualparams.pattern_combo.addItem('_and_'.join(self.graph_toolbar.names[index]))

                if len('_and_'.join(self.graph_toolbar.names[index])) > tmp:
                    tmp = len('_and_'.join(self.graph_toolbar.names[index]))
                    stylesheet = f"QComboBox QAbstractItemView {{ min-width: {tmp};}}"
                    self.manual_params.ui_manualparams.pattern_combo.setStyleSheet(stylesheet)

            items = [self.manual_params.ui_manualparams.pattern_combo.itemText(i)
                     for i in range(self.manual_params.ui_manualparams.pattern_combo.count())]

            keys = self.manual_params.pattern_pcrs.copy()

            for key in keys.keys():
                if key not in items:
                    self.manual_params.pattern_pcrs.pop(key)

            self.manual_params.change_pcr_view()
            self.enable_actions_buttons()
            self.manual_params.ui_manualparams.pattern_combo.blockSignals(False)

        else:
            self.manual_params.change_pcr_view()
            self.manual_params.ui_manualparams.pattern_combo.blockSignals(True)
            self.manual_params.ui_manualparams.pattern_combo.clear()
            self.manual_params.ui_manualparams.pattern_combo.setEnabled(False)
            self.manual_params.ui_manualparams.load_pcr.setEnabled(False)
            self.enable_actions_buttons()
            self.manual_params.ui_manualparams.pattern_combo.blockSignals(False)

    def update_patterns_autorietveld_combo(self, ci):
        self.auto_rietveld.ui_autorietveld.pattern_combo.blockSignals(True)
        self.auto_rietveld.ui_autorietveld.pattern_combo.clear()
        self.auto_rietveld.ui_autorietveld.pattern_combo.setEnabled(True)
        self.auto_rietveld.ui_autorietveld.load_pcr.setEnabled(True)
        tmp = 0
        if np.size(ci):
            for index in ci:
                self.auto_rietveld.ui_autorietveld.pattern_combo.addItem('_and_'.join(self.graph_toolbar.names[index]))

                if len('_and_'.join(self.graph_toolbar.names[index])) > tmp:
                    tmp = len('_and_'.join(self.graph_toolbar.names[index]))
                    stylesheet = f"QComboBox QAbstractItemView {{ min-width: {tmp};}}"
                    self.auto_rietveld.ui_autorietveld.pattern_combo.setStyleSheet(stylesheet)

            items = [self.auto_rietveld.ui_autorietveld.pattern_combo.itemText(i)
                     for i in range(self.auto_rietveld.ui_autorietveld.pattern_combo.count())]

            keys = self.auto_rietveld.pattern_pcrs.copy()

            for key in keys.keys():
                if key not in items:
                    self.auto_rietveld.pattern_pcrs.pop(key)
                    self.auto_rietveld.pattern_phase.pop(key)

            self.auto_rietveld.update_parameters_phases()
            self.enable_actions_buttons()
            self.auto_rietveld.ui_autorietveld.pattern_combo.blockSignals(False)

        else:
            self.auto_rietveld.reset_all()
            self.auto_rietveld.ui_autorietveld.pattern_combo.blockSignals(True)
            self.auto_rietveld.ui_autorietveld.pattern_combo.clear()
            self.auto_rietveld.ui_autorietveld.pattern_combo.setEnabled(False)
            self.auto_rietveld.ui_autorietveld.load_pcr.setEnabled(False)
            self.enable_actions_buttons()
            self.auto_rietveld.ui_autorietveld.pattern_combo.blockSignals(False)

    def enable_actions_buttons(self):

        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text())
        if any(val != 0 for val in self.graph_toolbar.checked_sum):
            self.ui_mainwindow.actionAutomatic_Background.setEnabled(True)
            self.ui_mainwindow.actionPeak_Detection.setEnabled(True)
            self.ui_mainwindow.button_submit_selection.setEnabled(True)
        else:
            self.ui_mainwindow.actionAutomatic_Background.setEnabled(False)
            self.ui_mainwindow.actionPeak_Detection.setEnabled(False)
            self.ui_mainwindow.button_submit_selection.setEnabled(False)

        if self.patterns[index][index_patt].anchor_points.shape[0] > 2:
            self.ui_mainwindow.actionEdit_Background.setEnabled(True)
            self.ui_mainwindow.actionExport_Background.setEnabled(True)
        else:
            self.ui_mainwindow.actionEdit_Background.setEnabled(False)
            self.ui_mainwindow.actionExport_Background.setEnabled(False)

        if self.patterns[index][index_patt].peaks is not None:
            self.ui_mainwindow.actionEdit_Peaks.setEnabled(True)
            self.ui_mainwindow.menuExport_Peaks.setEnabled(True)
        else:
            self.ui_mainwindow.actionEdit_Peaks.setEnabled(False)
            self.ui_mainwindow.menuExport_Peaks.setEnabled(False)

        if self.cifdir is not None and self.summary is not None:
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexQuery, True)
            self.ui_mainwindow.tabQuery.setEnabled(True)
            if self.summary.empty:
                self.ui_mainwindow.button_simulate_selection.setEnabled(False)
                self.ui_mainwindow.button_submit_selection.setEnabled(False)
            elif self.patterns[index][index_patt].background is None:
                self.ui_mainwindow.button_simulate_selection.setEnabled(False)
                self.ui_mainwindow.button_submit_selection.setEnabled(True)
            else:
                self.ui_mainwindow.button_simulate_selection.setEnabled(True)
                self.ui_mainwindow.button_submit_selection.setEnabled(True)
        else:
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexQuery, False)
            self.ui_mainwindow.tabQuery.setEnabled(False)
            self.ui_mainwindow.button_simulate_selection.setEnabled(False)
            self.ui_mainwindow.button_submit_selection.setEnabled(False)

        if self.selection is not None:
            self.ui_mainwindow.tabSelection.setEnabled(True)
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexSelection, True)
            if self.selection.empty:
                self.search_params.ui_searchparams.button_fp_search.setEnabled(False)
                self.auto_rietveld.ui_autorietveld.button_plus.setEnabled(False)
                self.auto_rietveld.ui_autorietveld.button_plusplus.setEnabled(False)
                self.manual_params.ui_manualparams.button_plus.setEnabled(False)
                self.manual_params.ui_manualparams.button_plusplus.setEnabled(False)
                if self.auto_rietveld.pattern_phase:
                    self.auto_rietveld.ui_autorietveld.button_minus.setEnabled(True)
                    self.auto_rietveld.ui_autorietveld.button_minusminus.setEnabled(True)
                else:
                    self.auto_rietveld.ui_autorietveld.button_minus.setEnabled(False)
                    self.auto_rietveld.ui_autorietveld.button_minusminus.setEnabled(False)
                if self.manual_params.pattern_pcrs:
                    self.manual_params.ui_manualparams.button_minus.setEnabled(True)
                    self.manual_params.ui_manualparams.button_minusminus.setEnabled(True)
                else:
                    self.manual_params.ui_manualparams.button_minus.setEnabled(False)
                    self.manual_params.ui_manualparams.button_minusminus.setEnabled(False)

            elif all(val == 0 for val in self.graph_toolbar.checked_sum):
                self.search_params.ui_searchparams.button_fp_search.setEnabled(False)
                self.auto_rietveld.ui_autorietveld.button_plus.setEnabled(False)
                self.auto_rietveld.ui_autorietveld.button_plusplus.setEnabled(False)
                self.manual_params.ui_manualparams.button_plus.setEnabled(False)
                self.manual_params.ui_manualparams.button_plusplus.setEnabled(False)
                self.manual_params.ui_manualparams.button_minus.setEnabled(False)
                self.manual_params.ui_manualparams.button_minusminus.setEnabled(False)
                self.auto_rietveld.ui_autorietveld.button_minus.setEnabled(False)
                self.auto_rietveld.ui_autorietveld.button_minusminus.setEnabled(False)
            else:
                self.auto_rietveld.ui_autorietveld.button_plus.setEnabled(True)
                self.auto_rietveld.ui_autorietveld.button_plusplus.setEnabled(True)
                self.manual_params.ui_manualparams.button_plus.setEnabled(True)
                self.manual_params.ui_manualparams.button_plusplus.setEnabled(True)
                if self.auto_rietveld.pattern_phase:
                    self.auto_rietveld.ui_autorietveld.button_minus.setEnabled(True)
                    self.auto_rietveld.ui_autorietveld.button_minusminus.setEnabled(True)
                else:
                    self.auto_rietveld.ui_autorietveld.button_minus.setEnabled(False)
                    self.auto_rietveld.ui_autorietveld.button_minusminus.setEnabled(False)

                if self.manual_params.pattern_pcrs:
                    self.manual_params.ui_manualparams.button_minus.setEnabled(True)
                    self.manual_params.ui_manualparams.button_minusminus.setEnabled(True)
                else:
                    self.manual_params.ui_manualparams.button_minus.setEnabled(False)
                    self.manual_params.ui_manualparams.button_minusminus.setEnabled(False)

                if all(all(patt.anchor_points.shape[0] > 2 for patt in self.patterns[index])
                       for index in self.graph_toolbar.checked_index):
                    self.search_params.ui_searchparams.button_fp_search.setEnabled(True)
                    self.auto_rietveld.ui_autorietveld.button_plus.setEnabled(True)
                    self.auto_rietveld.ui_autorietveld.button_plusplus.setEnabled(True)
                    self.manual_params.ui_manualparams.button_plus.setEnabled(True)
                    self.manual_params.ui_manualparams.button_plusplus.setEnabled(True)
                else:
                    self.search_params.ui_searchparams.button_fp_search.setEnabled(False)
                    self.auto_rietveld.ui_autorietveld.button_plus.setEnabled(False)
                    self.auto_rietveld.ui_autorietveld.button_plusplus.setEnabled(False)
                    self.manual_params.ui_manualparams.button_plus.setEnabled(False)
                    self.manual_params.ui_manualparams.button_plusplus.setEnabled(False)
        else:
            self.ui_mainwindow.tabSelection.setEnabled(False)
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexSelection, False)
            self.search_params.ui_searchparams.button_fp_search.setEnabled(False)
            self.auto_rietveld.ui_autorietveld.button_plus.setEnabled(False)
            self.auto_rietveld.ui_autorietveld.button_plusplus.setEnabled(False)
            self.manual_params.ui_manualparams.button_plus.setEnabled(False)
            self.manual_params.ui_manualparams.button_plusplus.setEnabled(False)
            if self.auto_rietveld.pattern_phase:
                self.auto_rietveld.ui_autorietveld.button_minus.setEnabled(True)
                self.auto_rietveld.ui_autorietveld.button_minusminus.setEnabled(True)
            else:
                self.auto_rietveld.ui_autorietveld.button_minus.setEnabled(False)
                self.auto_rietveld.ui_autorietveld.button_minusminus.setEnabled(False)

            if self.manual_params.pattern_pcrs:
                self.manual_params.ui_manualparams.button_minus.setEnabled(True)
                self.manual_params.ui_manualparams.button_minusminus.setEnabled(True)
            else:
                self.manual_params.ui_manualparams.button_minus.setEnabled(False)
                self.manual_params.ui_manualparams.button_minusminus.setEnabled(False)

        if self.currentpath is not None:
            self.ui_mainwindow.actionCurrent_Result.setEnabled(True)
            self.ui_mainwindow.actionCurrent_WinPLOTR.setEnabled(True)
            self.ui_mainwindow.actionCurrent_prf.setEnabled(True)
            msg = f"| Current Workdir: {self.workdir} | Current Results Path: {os.path.split(self.currentpath)[-1]} |"
            self.pathstatusbar.showMessage(msg)
        else:
            self.ui_mainwindow.actionCurrent_Result.setEnabled(False)
            self.ui_mainwindow.actionCurrent_WinPLOTR.setEnabled(False)
            self.ui_mainwindow.actionCurrent_prf.setEnabled(False)
            msg = f"| Current Workdir: {self.workdir} |"
            self.pathstatusbar.showMessage(msg)

    def plot_contour(self):
        index_patt = int(self.graph_toolbar.patt_num.text())
        try:
            if self.graph is None:
                self.graph = ContourGraph()
                self.graph.ui_imagegraph.dialog_box.accepted.connect(self.contour_accept)
                self.graph.ui_imagegraph.dialog_box.rejected.connect(self.contour_reject)
                self.graph.parse_data([patt[index_patt] for patt in self.patterns])
                self.graph.show()
            else:
                self.graph.parse_data([patt[index_patt] for patt in self.patterns])
                self.graph.show()
        except ValueError as e:
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"Can't load Contour Graphs\n\n {e}")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

    def contour_accept(self):
        if self.graph.ui_imagegraph.checkpoint.isChecked():
            self.define_checkpoins()
        self.graph.ui_imagegraph.checkpoint.setChecked(False)
        self.graph.remove_all_lines()
        self.graph.hide()

    def contour_reject(self):
        self.graph.ui_imagegraph.checkpoint.setChecked(False)
        self.graph.remove_all_lines()
        self.graph.hide()

    def define_checkpoins(self):
        checked_patterns = self.graph.get_outputs()
        checked_patterns = np.append([], checked_patterns)
        index_patt = int(self.graph_toolbar.patt_num.text())

        self.graph_toolbar.checktable.check_uncheck_all(False)
        self.graph_toolbar.checktable.check_indexes(rows=checked_patterns, cols=[index_patt])

    def update_patterns_display(self, first=False):

        if first is None:
            index = 0
            index_patt = 0
        else:
            index = self.graph_toolbar.patterns_combo.currentIndex()
            index_patt = int(self.graph_toolbar.patt_num.text())

        self.diffgraph.ui_diffgraph.diff_graph.clear()
        self.diffgraph.ui_diffgraph.peaks_graph.clear()
        self.diffgraph.setEnabled(True)
        self.plt1 = None
        self.plt2 = None
        self.plt3 = None
        self.plt4 = None
        self.plt5 = None

        """Set  ranges, legends and plot """
        self.plt1 = self.diffgraph.ui_diffgraph.diff_graph.plot(x=self.patterns[index][index_patt].diffraction[:, 0],
                                                                y=self.patterns[index][index_patt].diffraction[:, 1],
                                                                pen=self.pen1, name='Yobs')

        if self.patterns[index][index_patt].background is not None:
            self.plt2 = self.diffgraph.ui_diffgraph.diff_graph.plot(x=self.patterns[index][index_patt].diffraction[:, 0],
                                                                    y=self.patterns[index][index_patt].background,
                                                                    pen=self.pen2, name='Ybckgr')
        if self.patterns[index][index_patt].anchor_points is not None:
            self.plt3 = self.diffgraph.ui_diffgraph.diff_graph.plot(x=self.patterns[index][index_patt].anchor_points[:, 0],
                                                                    y=self.patterns[index][index_patt].anchor_points[:, 1],
                                                                    pen=None,
                                                                    symbolPen=self.pen3,
                                                                    symbolBrush=(0, 255, 0),
                                                                    symbolSize=10)
        if self.patterns[index][index_patt].peaks is not None:
            self.plt5 = self.diffgraph.ui_diffgraph.diff_graph.plot(x=self.patterns[index][index_patt].peaks[:, 0],
                                                                    y=self.patterns[index][index_patt].peaks[:, 1],
                                                                    pen=None,
                                                                    symbolPen=self.pen5,
                                                                    symbolBrush=(255, 182, 193),
                                                                    symbolSize=10)

            y = np.zeros(self.patterns[index][index_patt].peaks.shape[0])
            self.peak_lines = self.diffgraph.ui_diffgraph.peaks_graph.plot(x=self.patterns[index][index_patt].peaks[:, 0],
                                                                           y=y,
                                                                           pen=None,
                                                                           symbolPen=self.pen5,
                                                                           symbol=symbols.custom_symbol("|"))

        self.enable_actions_buttons()

    def export_background(self):

        try:
            index = self.graph_toolbar.patterns_combo.currentIndex()
            index_patt = int(self.graph_toolbar.patt_num.text())
            if self.patterns[index][index_patt].anchor_points is not None:
                head, tail = os.path.split(self.patterns[index][index_patt].filename)
                np.savetxt(tail + '.bgr', self.patterns[index][index_patt].anchor_points[1:-1, :])
                self.warning = qtw.QMessageBox()
                self.warning.setIcon(qtw.QMessageBox.Information)
                self.warning.setText(f"Background file {tail}.bgr saved successfully.")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                self.warning.exec_()
        except (ValueError, IndexError):
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"Something went wrong.")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

    def export_free_peaks(self):
        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text())
        if self.patterns[index][index_patt].peaks is not None:
            head, tail = os.path.split(self.patterns[index][index_patt].filename)

            backgr = self.patterns[index][index_patt].get_background_in_peak_pos()
            peaks = self.patterns[index][index_patt].peaks
            data = np.column_stack((peaks[:, 0], peaks[:, 1], backgr))
            fmt = self.patterns[index][index_patt].fmt
            ins_dict = {'xys': 10, 'free': 0, 'socabim': 9, 'xrdml': 13, 'd1ad2b': 6, 'd1bd20': 3, 'dmc': 8}

            header = f'! Data File: {self.patterns[index][index_patt].filename}\n! Format: {ins_dict[fmt]} - {fmt}\n' \
                     f'! peak_position     peak_intensity    peak_background'

            np.savetxt(tail + '.aps', data, fmt='%.6f', header=header, comments='')
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Information)
            self.warning.setText(f"Background file {tail}.aps saved succesifully.")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

    def get_background_auto(self):
        """ Initialize slider widgets """
        self.param = BckgrParam()
        self.param.show()

        """ Current comboBox index """
        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text())
        """ Get background value with outputs """
        for ind in self.graph_toolbar.checked_index:
            for patt in self.patterns[ind]: patt.get_background_asls(self.param.output_p, self.param.output_lambda)
            for patt in self.patterns[ind]: patt.get_anchor_points()

        self.update_patterns_display()

        self.param.values.connect(lambda p, lam: self.update_auto_background_plot(p, lam, index=index,
                                                                                  index_patt=index_patt))

        self.enable_actions_buttons()

    def update_auto_background_plot(self, p_out, lamba_out, index: int, index_patt: int):
        """ Get new background with outputs """
        for ind in self.graph_toolbar.checked_index:
            for patt in self.patterns[ind]: patt.get_background_asls(p_out, lamba_out)
            for patt in self.patterns[ind]: patt.get_anchor_points()

        """ Update plot """
        self.plt2.setData(x=self.patterns[index][index_patt].diffraction[:, 0],
                          y=self.patterns[index][index_patt].background)
        self.plt3.setData(x=self.patterns[index][index_patt].anchor_points[:, 0],
                          y=self.patterns[index][index_patt].anchor_points[:, 1])

    def update_manual_background_plot(self, index: int, index_patt: int):
        self.patterns[index][index_patt].get_background_manual()
        if self.patterns[index][index_patt].background is not None:
            """ Update plot """
            self.plt2.setData(x=self.patterns[index][index_patt].diffraction[:, 0],
                              y=self.patterns[index][index_patt].background)
            self.plt3.setData(x=self.patterns[index][index_patt].anchor_points[:, 0],
                              y=self.patterns[index][index_patt].anchor_points[:, 1])

    def get_peaks_auto(self):

        self.peaksparam = PeaksParam()
        self.peaksparam.show()

        """ Current comboBox index """
        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text())
        for ind in self.graph_toolbar.checked_index:
            for patt in self.patterns[ind]: patt.get_peaks_intens(alpha=self.peaksparam.output_alpha,
                                                                  width=self.peaksparam.output_width)

        self.update_patterns_display()

        self.peaksparam.values.connect(lambda alpha, width: self.update_auto_peaks_plot(alpha, width, index=index,
                                                                                        index_patt=index_patt))

        self.enable_actions_buttons()

    def update_auto_peaks_plot(self, alpha, width, index: int, index_patt: int):
        """ Get new peaks with outputs """
        for ind in self.graph_toolbar.checked_index:
            for patt in self.patterns[ind]: patt.get_peaks_intens(alpha=alpha, width=width)

        """ Update plot """
        self.plt5.setData(x=self.patterns[index][index_patt].peaks[:, 0],
                          y=self.patterns[index][index_patt].peaks[:, 1])

        self.diffgraph.ui_diffgraph.peaks_graph.clear()
        y = np.zeros(self.patterns[index][index_patt].peaks.shape[0])
        self.peak_lines = self.diffgraph.ui_diffgraph.peaks_graph.plot(x=self.patterns[index][index_patt].peaks[:, 0],
                                                                       y=y,
                                                                       pen=None,
                                                                       symbolPen=self.pen5,
                                                                       symbol=self.custom_symbol("|"))

    def update_manual_peaks_plot(self, index: int, index_patt: int):
        if self.patterns[index][index_patt].peaks is not None:
            self.plt5.setData(x=self.patterns[index][index_patt].peaks[:, 0],
                              y=self.patterns[index][index_patt].peaks[:, 1])
            y = np.zeros(self.patterns[index][index_patt].peaks.shape[0])
            self.peak_lines.setData(x=self.patterns[index][index_patt].peaks[:, 0], y=y)

    def reset_selection(self, val):
        if self.plt4 is not None and val:
            self.plt4.hide()
            self.plt4 = None
            self.selected_points = None

    def select_add(self, point):

        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text())

        if self.ui_mainwindow.actionEdit_Background.isChecked():
            self.patterns[index][index_patt].add_anchor_point(point=point)
            self.update_manual_background_plot(index, index_patt)
        elif self.ui_mainwindow.actionEdit_Peaks.isChecked():
            self.patterns[index][index_patt].add_peaks(point=point)
            self.update_manual_peaks_plot(index, index_patt)

    def add_point(self, point):

        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text())

        if self.ui_mainwindow.actionEdit_Background.isChecked():
            if self.selected_points is None:
                self.selected_points = self.patterns[index][index_patt].select_anchor_point(point=point)
                self.plt4 = self.diffgraph.ui_diffgraph.diff_graph.plot(x=self.selected_points[:, 0],
                                                                        y=self.selected_points[:, 1],
                                                                        pen=None,
                                                                        symbolPen=self.pen4,
                                                                        symbolBrush=(255, 127, 80))
            else:
                tmp = self.selected_points
                self.selected_points = self.patterns[index][index_patt].select_anchor_point(point=point)
                self.selected_points = np.append(tmp, self.selected_points, axis=0)
                self.selected_points = self.selected_points[self.selected_points[:, 0].argsort()]
                self.plt4.setData(x=self.selected_points[:, 0], y=self.selected_points[:, 1])

        elif self.ui_mainwindow.actionEdit_Peaks.isChecked():
            if self.selected_points is None:
                self.selected_points = self.patterns[index][index_patt].select_peak(point=point)
                self.plt4 = self.diffgraph.ui_diffgraph.diff_graph.plot(x=self.selected_points[:, 0],
                                                                        y=self.selected_points[:, 1],
                                                                        pen=None,
                                                                        symbolPen=self.pen4,
                                                                        symbolBrush=(255, 127, 80))
            else:
                tmp = self.selected_points
                self.selected_points = self.patterns[index][index_patt].select_peak(point=point)
                self.selected_points = np.append(tmp, self.selected_points, axis=0)
                self.selected_points = self.selected_points[self.selected_points[:, 0].argsort()]
                self.plt4.setData(x=self.selected_points[:, 0], y=self.selected_points[:, 1])

    def select_drag(self, xbeg, xfin, ybeg, yfin):

        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text())

        if self.ui_mainwindow.actionEdit_Background.isChecked():
            if self.selected_points is None:
                self.selected_points = self.patterns[index][index_patt].select_anchor_points_in_range(xbeg=xbeg,
                                                                                                      xfin=xfin,
                                                                                                      ybeg=ybeg,
                                                                                                      yfin=yfin)

                self.plt4 = self.diffgraph.ui_diffgraph.diff_graph.plot(x=self.selected_points[:, 0],
                                                                        y=self.selected_points[:, 1],
                                                                        pen=None,
                                                                        symbolPen=self.pen4,
                                                                        symbolBrush=(255, 127, 80))
            else:
                tmp = self.selected_points
                self.selected_points = self.patterns[index][index_patt].select_anchor_points_in_range(xbeg=xbeg,
                                                                                                      xfin=xfin,
                                                                                                      ybeg=ybeg,
                                                                                                      yfin=yfin)
                self.selected_points = np.append(tmp, self.selected_points, axis=0)
                self.selected_points = self.selected_points[self.selected_points[:, 0].argsort()]
                self.plt4.setData(x=self.selected_points[:, 0], y=self.selected_points[:, 1])

        elif self.ui_mainwindow.actionEdit_Peaks.isChecked():

            if self.selected_points is None:
                self.selected_points = self.patterns[index][index_patt].select_peaks_in_range(xbeg=xbeg,
                                                                                              xfin=xfin,
                                                                                              ybeg=ybeg,
                                                                                              yfin=yfin)
                self.plt4 = self.diffgraph.ui_diffgraph.diff_graph.plot(x=self.selected_points[:, 0],
                                                                        y=self.selected_points[:, 1],
                                                                        pen=None,
                                                                        symbolPen=self.pen4,
                                                                        symbolBrush=(255, 127, 80))
            else:
                tmp = self.selected_points
                self.selected_points = self.patterns[index][index_patt].select_peaks_in_range(xbeg=xbeg,
                                                                                              xfin=xfin,
                                                                                              ybeg=ybeg,
                                                                                              yfin=yfin)
                self.selected_points = np.append(tmp, self.selected_points, axis=0)
                self.selected_points = self.selected_points[self.selected_points[:, 0].argsort()]
                self.plt4.setData(x=self.selected_points[:, 0], y=self.selected_points[:, 1])

    def delete(self, val):

        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text())

        if val and self.selected_points.size != 0:

            if self.ui_mainwindow.actionEdit_Background.isChecked():
                self.patterns[index][index_patt].remove_selected_anchor_points(self.selected_points)
                self.update_manual_background_plot(index, index_patt)
                self.plt4.hide()
                self.plt4 = None
                self.selected_points = None

            elif self.ui_mainwindow.actionEdit_Peaks.isChecked():
                self.patterns[index][index_patt].remove_selected_peaks(self.selected_points)
                self.update_manual_peaks_plot(index, index_patt)
                self.plt4.hide()
                self.plt4 = None
                self.selected_points = None

    def show_pos(self, point):
        if point is not None:
            msg = f"x = {point[0]}     y = {point[1]}"
            self.ui_mainwindow.statusbar.showMessage(msg)
        else:
            self.ui_mainwindow.statusbar.clearMessage()

    def search_remote(self):
        self.period = PeriodicTable()

        if self.period.exec_() == 1:
            self.ui_mainwindow.statusbar.showMessage(
                f"Getting database entry ID's using OPTIMADE, this might take time...")
            provider = self.period.ui_periodictable.combo_db.currentText()
            self.cifdir = os.path.join(os.getcwd(), "Downloaded_CIFs/")
            if os.path.basename(os.path.normpath(self.cifdir)) not in os.listdir(os.getcwd()):
                os.mkdir(self.cifdir)
            try:
                self.progressbar.show()
                cl = Client(provider, self.period.selected, self.terminal)
                urls = cl.get_urls()
                self.progressbar.setRange(0, len(urls))
                self.completed = 0
                self.downloader.downloaded.connect(lambda: self.update_downloads(urls, cl.identifiers, cl.provider))
                self.download_urls(urls)
            except:
                raise RuntimeError(f"Something went wrong with the connection, please check the logger for more detail")

    def download_urls(self, urls):
        self.downloader.do_download(qtc.QUrl(urls[self.completed]))

    def update_downloads(self, urls, identifiers, provider):
        """Save CIF file """
        if provider == 'cod':
            name = os.path.join(self.cifdir, f"{provider}-{identifiers[self.completed]}.cif")
        elif provider == 'mp':
            name = os.path.join(self.cifdir, f"{identifiers[self.completed]}.cif")
        else:
            raise ValueError('Provider not supported')

        data = self.downloader.get_data()
        with open(name, 'wb') as cif:
            cif.write(data)

        if self.completed == len(urls) - 1:
            self.ui_mainwindow.statusbar.showMessage(f"Download completed")
            self.progressbar.hide()
            self.update_summary()
            self.downloader.downloaded.disconnect()
        else:
            self.completed += 1
            self.ui_mainwindow.statusbar.showMessage(f"Downloading CIF files ({self.completed}/{len(urls)})...")
            self.progressbar.setValue(self.completed)
            self.download_urls(urls)

    def search_local(self):

        tmpcifdir = self.cifdir
        self.cifdir = qtw.QFileDialog.getExistingDirectory(self, 'Select Local Database', os.getcwd())
        self.cifdir = os.path.normpath(self.cifdir)
        try:
            self.update_summary()
        except AttributeError:
            if tmpcifdir is not None:
                self.terminal.append(f"ERROR loading materials from {self.cifdir}, returning to previous folder")
                self.cifdir = tmpcifdir
                self.update_summary()
            else:
                self.cifdir = tmpcifdir

    def update_summary(self):
        self.terminal.append(f"Loading materials from local folder {self.cifdir}...\n")
        cr = CifReaderWriter()
        cr.read_from_dir(dirname=self.cifdir)

        if cr.error:
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Warning)
            list = ''.join([f"\n - {ids[0]}\n  {ids[1]}\n" for ids in cr.error])

            self.warning.setText(f"CIF READ ERROR: The following CIF files could not be loaded:\n{list}")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

        if cr.summary[1]:
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Warning)
            list = ''.join([f"\n - {ids}\n" for ids in cr.summary[1]])
    
            self.warning.setText(f"CIF READ ERROR: The following CIF files could not be loaded \n\n {list}\n\n"
                                 f" Some fields where missing or the program failed at reading them")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

        self.data = cr.data
        self.summary = cr.summary[0]
        self.populate_tablequery()
        self.ui_mainwindow.tabMaterials.setTabVisible(self.indexQuery, True)
        self.ui_mainwindow.tabMaterials.setCurrentIndex(1)
        self.terminal.append(f"Loaded {self.summary.shape[0]} files\n")
        self.terminal.prompt()

        ipatt = self.graph_toolbar.patt_num.text()
        if ipatt:
            self.enable_actions_buttons()

    def handle_cif_rename_table(self, index, _, role):
        oldname = self.sender().oldid
        if oldname:
            newname = self.sender().data(index, role[0])
        else:
            newname = oldname

        if self.sender().objectName() == "summary":
            model = self.ui_mainwindow.tableSelection.model()
            if model is not None:
                model.blockSignals(True)
                model.change_data(oldname, newname)
                model.blockSignals(False)
        elif self.sender().objectName() == "selection":
            model = self.ui_mainwindow.tableQuery.model().sourceModel()
            if model is not None:
                model.blockSignals(True)
                model.change_data(oldname, newname)
                model.blockSignals(False)

        os.rename(os.path.join(self.cifdir, f"{oldname}.cif"), os.path.join(self.cifdir, f"{newname}.cif"))

    def populate_tablequery(self):
        model = PandasTableModel(self.summary)
        model.dataChanged.connect(self.handle_cif_rename_table)
        model.setObjectName("summary")
        self.proxyModel.setSourceModel(model)
        self.ui_mainwindow.tableQuery.setModel(self.proxyModel)

    def populate_tableselection(self):
        model = PandasTableModel(self.selection)
        model.dataChanged.connect(self.handle_cif_rename_table)
        model.setObjectName("selection")
        self.ui_mainwindow.tableSelection.setModel(model)

    def update_selection(self):
        proxymodel = self.ui_mainwindow.tableQuery.model()
        iproxy = self.ui_mainwindow.tableQuery.selectionModel().selectedRows()
        index = [proxymodel.mapToSource(i) for i in iproxy]
        rows = self.summary.iloc[[row.row() for row in index]]
        """ First population of table """
        if not self.ui_mainwindow.tabMaterials.isTabVisible(self.indexSelection):
            self.selection = rows
            self.populate_tableselection()
            self.terminal.append(f"Selection consists of {self.selection.shape[0]} phases\n")
            self.terminal.prompt()
        else:
            try:
                self.selection = pd.concat([self.selection, rows]).drop_duplicates().reset_index(drop=True)
                self.populate_tableselection()
                self.terminal.append(f"New selection consists of {self.selection.shape[0]} phases\n")
                self.terminal.prompt()
            except ValueError:
                pass

        self.enable_actions_buttons()

    def remove_summary_rows(self, ev):
        if ev.key() == qtc.Qt.Key_Delete:
            proxymodel = self.ui_mainwindow.tableQuery.model()
            iproxy = self.ui_mainwindow.tableQuery.selectionModel().selectedRows()
            index = [proxymodel.mapToSource(i) for i in iproxy]
            self.summary = self.summary.drop([self.summary.index[row.row()] for row in index])
            self.populate_tablequery()
            self.enable_actions_buttons()
            self.terminal.append(f"Deleted {len(index)} phases from Queried Materials\n")
            self.terminal.prompt()

    def remove_selection_rows(self, ev):
        if ev.key() == qtc.Qt.Key_Delete:
            indexes = self.ui_mainwindow.tableSelection.selectionModel().selectedRows()
            self.selection = self.selection.drop([self.selection.index[row.row()] for row in indexes])
            self.populate_tableselection()
            self.enable_actions_buttons()
            self.terminal.append(f"Deleted {len(indexes)} phases from Selected Materials\n")
            self.terminal.prompt()

    def current_background_to_selected_pattern(self):
        names = self.auto_rietveld.get_selected_topnames()
        idx = [['_and_'.join(val) for val in self.graph_toolbar.names].index(name) for name in names]

        try:
            for i, index in enumerate(idx):
                for ipatt, patt in enumerate(self.patterns[index]):
                    anchor = patt.anchor_points[1:-1, :]
                    zeros = np.zeros(anchor.shape[0], dtype=float)
                    background = np.column_stack((anchor[:, 0], anchor[:, 1], zeros))
                    self.auto_rietveld.pattern_pcrs[names[i]].replace_background(background, ipatt, mode=BackgroundType.POINTS)

            nam = '\n - '.join(names)
            nam = ' - ' + nam

            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Information)
            self.warning.setText(f"Background points replaced into patterns loaded to AutoRietveld:\n\n{nam}")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

        except:
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"ERROR: Background points could not be replaced into patterns loaded to AutoRietveld")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

    def file_background_to_selected_pattern(self):
        names = self.auto_rietveld.get_selected_topnames()
        idx = [['_and_'.join(val) for val in self.graph_toolbar.names].index(name) for name in names]
        try:
            for i, index in enumerate(idx):
                for ipatt, patt in enumerate(self.patterns[index]):
                    filename, _ = qtw.QFileDialog.getOpenFileName(self, f'Background for Pattern file {patt.filename}',
                                                                  os.getcwd(), 'BGR files (*.bgr)')

                    if filename is not None:
                        background = np.loadtxt(filename, comments=['#', '!', '/', '@', '$'])
                        if background.shape[1] == 2:
                            zeros = np.zeros(background.shape[0], dtype=float)
                            background = np.column_stack((background[:, 0], background[:, 1], zeros))
                        elif background.shape[1] == 3:
                            background[:, 2] = 0.0
                        elif background.shape[1] > 3:
                            background = background[:, 0:2]
                            background[:, 2] = 0.0
                        else:
                            self.warning = qtw.QMessageBox()
                            self.warning.setIcon(qtw.QMessageBox.Critical)
                            self.warning.setText(f"ERROR: Bad format in background file\n - {filename}")
                            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                            self.warning.exec_()
                            continue

                        self.auto_rietveld.pattern_pcrs[names[i]].replace_background(background, ipatt, mode=BackgroundType.POINTS)
        except:
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"ERROR: Background points could not be replaced into patterns loaded to AutoRietveld")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

    def show_background_options(self, position):
        names = self.auto_rietveld.get_selected_topnames()
        if names:
            self.auto_rietveld.backg_menu.options.menuAction().setVisible(True)
            self.auto_rietveld.backg_menu.loadfrom.menuAction().setVisible(True)
            self.auto_rietveld.backg_menu.sequential.menuAction().setVisible(True)
        else:
            self.auto_rietveld.backg_menu.options.menuAction().setVisible(False)
            self.auto_rietveld.backg_menu.loadfrom.menuAction().setVisible(False)
            self.auto_rietveld.backg_menu.sequential.menuAction().setVisible(False)

        idx = [['_and_'.join(val) for val in self.graph_toolbar.names].index(name) for name in names]
        if all(all(patt.anchor_points.shape[0] > 2 for patt in self.patterns[index]) for index in idx):
            self.auto_rietveld.backg_menu.pattaction.setVisible(True)
        else:
            self.auto_rietveld.backg_menu.pattaction.setVisible(False)

        x = self.graph_toolbar.checked_index
        idx = lambda: range(x[0], x[-1] + 1) if x.shape[0] > 1 else range(x[0], len(self.graph_toolbar.names))
        if self.auto_rietveld.ui_autorietveld.sequential_check.isChecked() and\
                all(all(patt.anchor_points.shape[0] > 2 for patt in self.patterns[index]) for index in idx()):
            self.auto_rietveld.backg_menu.sequential.menuAction().setVisible(True)
            self.auto_rietveld.backg_menu.seqpattaction.setVisible(True)
        else:
            self.auto_rietveld.backg_menu.sequential.menuAction().setVisible(False)
            self.auto_rietveld.backg_menu.seqpattaction.setVisible(False)

        self.auto_rietveld.backg_menu.exec_(self.auto_rietveld.ui_autorietveld.tree_patterns.mapToGlobal(position))

    def on_clicked_minus(self):
        if self.sender().parent().objectName() == 'frame':
            pattern = self.auto_rietveld.ui_autorietveld.pattern_combo.currentText()

            if pattern in self.auto_rietveld.pattern_pcrs:
                self.auto_rietveld.pattern_pcrs.pop(pattern)
                self.auto_rietveld.pattern_phase.pop(pattern)

                self.auto_rietveld.update_parameters_phases()

            self.enable_actions_buttons()
            self.auto_rietveld.enable_run_button()

        elif self.sender().parent().objectName() == '':
            pattern = self.manual_params.ui_manualparams.pattern_combo.currentText()

            if pattern in self.manual_params.pattern_pcrs:
                self.manual_params.pattern_pcrs.pop(pattern)
                self.manual_params.change_pcr_view()

            self.enable_actions_buttons()
            self.manual_params.enable_run_button()

    def on_clicked_minusminus(self):
        if self.sender().parent().objectName() == 'frame':
            for idx in range(self.auto_rietveld.ui_autorietveld.pattern_combo.count()):
                pattern = self.auto_rietveld.ui_autorietveld.pattern_combo.itemText(idx)
                if pattern in self.auto_rietveld.pattern_pcrs:
                    self.auto_rietveld.pattern_pcrs.pop(pattern)
                    self.auto_rietveld.pattern_phase.pop(pattern)

                self.auto_rietveld.update_parameters_phases()

            self.enable_actions_buttons()
            self.auto_rietveld.enable_run_button()

        elif self.sender().parent().objectName() == '':
            for idx in range(self.manual_params.ui_manualparams.pattern_combo.count()):
                pattern = self.manual_params.ui_manualparams.pattern_combo.itemText(idx)
                if pattern in self.manual_params.pattern_pcrs:
                    self.manual_params.pattern_pcrs.pop(pattern)

            self.manual_params.change_pcr_view()
            self.enable_actions_buttons()
            self.manual_params.enable_run_button()

    def close_tabs(self, index):
        if index == self.indexPrfData:
            self.ui_mainwindow.tabGraphs.setTabVisible(index, False)
            self.plot_prf['current'].ui_diffgraph.diff_graph.clear()
            self.plot_prf['current'].ui_diffgraph.peaks_graph.clear()

        if index == self.indexSummary:
            self.ui_mainwindow.tabGraphs.setTabVisible(index, False)

    def on_clicked_plusplus(self):
        if self.sender().parent().objectName() == 'frame':
            self.generate_all = True
            self.init_generate_pcr_auto()

        elif self.sender().parent().objectName() == '':
            self.generate_all = True
            self.init_generate_pcr_manual()

    def on_clicked_plus(self):
        if self.sender().parent().objectName() == 'frame':
            self.generate_all = False
            self.init_generate_pcr_auto()

        elif self.sender().parent().objectName() == '':
            self.generate_all = False
            self.init_generate_pcr_manual()

    def init_current_workdir(self, counter, num):

        if self.algo == 'MANUAL':  # Manual Refine
            jobid = self.manual_params.ui_manualparams.pattern_combo.itemText(counter)
            jobtype = 'MANUAL'

        elif self.algo == 'AUTO':  # Auto Refine
            jobid = self.auto_rietveld.ui_autorietveld.pattern_combo.itemText(counter)
            jobtype = 'FPAUTO'

        elif self.algo == 'SEQ':  # Sequential Refine
            jobid = '_and_'.join(self.graph_toolbar.names[counter])
            jobtype = 'FPSEQ'

        elif self.algo == 'SEARCH':  # Luterotti Search Phase Refine
            idx = self.graph_toolbar.checked_index[counter]
            jobid = '_and_'.join(self.graph_toolbar.names[idx])
            jobtype = 'FPSEARCH'

        if num is not None:
            if not os.path.isdir(os.path.join(os.path.join(os.getcwd(), f"{jobtype}{str(num).zfill(3)}"), f"{jobid}")):
                os.mkdir(os.path.join(os.path.join(os.getcwd(), f"{jobtype}{str(num).zfill(3)}"), f"{jobid}"))

            jobpath = os.path.join(os.path.join(os.getcwd(), f"{jobtype}{str(num).zfill(3)}"), f"{jobid}")

            return jobpath, jobid
        else:
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"ERROR: self.algo variable is not correctly initialized")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

    def init_fpmanual(self):
        fp_manual_num = 0
        self.ui_mainwindow.tabMaterials.setCurrentIndex(0)

        local = []

        for item in os.listdir(os.getcwd()):
            if 'MANUAL' in item:
                local.append(item)
            else:
                pass

        local.sort()
        if len(local) == 0:
            self.currentpath = os.path.join(os.getcwd(), f"MANUAL{str(fp_manual_num).zfill(3)}")
            os.mkdir(self.currentpath)
        else:
            last = local[-1]
            fp_manual_num = int(last.replace('MANUAL', '')) + 1
            self.currentpath = os.path.join(os.getcwd(), f"MANUAL{str(fp_manual_num).zfill(3)}")
            os.mkdir(self.currentpath)
        
        return fp_manual_num

    def init_fpseq(self):
        fp_seq_num = 0
        self.ui_mainwindow.tabMaterials.setCurrentIndex(0)

        local = []

        for item in os.listdir(os.getcwd()):
            if 'FPSEQ' in item:
                local.append(item)
            else:
                pass

        local.sort()
        if len(local) == 0:
            self.currentpath = os.path.join(os.getcwd(), f"FPSEQ{str(fp_seq_num).zfill(3)}")
            os.mkdir(self.currentpath)
        else:
            last = local[-1]
            fp_seq_num = int(last.replace('FPSEQ', '')) + 1
            self.currentpath = os.path.join(os.getcwd(), f"FPSEQ{str(fp_seq_num).zfill(3)}")
            os.mkdir(self.currentpath)

        return fp_seq_num

    def init_fpauto(self):
        fp_auto_num = 0
        self.ui_mainwindow.tabMaterials.setCurrentIndex(0)

        local = []

        for item in os.listdir(os.getcwd()):
            if 'FPAUTO' in item:
                local.append(item)
            else:
                pass

        local.sort()
        if len(local) == 0:
            self.currentpath = os.path.join(os.getcwd(), f"FPAUTO{str(fp_auto_num).zfill(3)}")
            os.mkdir(self.currentpath)
        else:
            last = local[-1]
            fp_auto_num = int(last.replace('FPAUTO', '')) + 1
            self.currentpath = os.path.join(os.getcwd(), f"FPAUTO{str(fp_auto_num).zfill(3)}")
            os.mkdir(self.currentpath)
        
        return fp_auto_num

    def init_fplute(self):
        fp_lute_num = 0
        self.ui_mainwindow.tabMaterials.setCurrentIndex(0)

        local = []

        for item in os.listdir(os.getcwd()):
            if 'FPSEARCH' in item:
                local.append(item)
            else:
                pass

        local.sort()
        if len(local) == 0:
            self.currentpath = os.path.join(os.getcwd(), f"FPSEARCH{str(fp_lute_num).zfill(3)}")
            os.mkdir(self.currentpath)
        else:
            last = local[-1]
            fp_lute_num = int(last.replace('FPSEARCH', '')) + 1
            self.currentpath = os.path.join(os.getcwd(), f"FPSEARCH{str(fp_lute_num).zfill(3)}")
            os.mkdir(self.currentpath)
        
        return fp_lute_num

    def setup_hkl_file(self, identifier, pcr, jobpath, jobid):
        if pcr.multipattern:
            for i in range(pcr.get_patt_num()):
                filename, _ = qtw.QFileDialog.getOpenFileName(self, f'Select HKL file for Pattern {i}',
                                                              os.getcwd(), 'HKL files (*.hkl)')

                ids = pcr.read_ids()
                idx = ids.index(identifier) + 1

                shutil.copyfile(filename, os.path.join(jobpath, f"{jobid}{idx}_{i}.hkl"))
        else:
            filename, _ = qtw.QFileDialog.getOpenFileName(self, f'Select HKL file', os.getcwd(), 'HKL files (*.hkl)')

            ids = pcr.read_ids()
            idx = ids.index(identifier) + 1

            shutil.copyfile(filename, os.path.join(jobpath, f"{jobid}{idx}.hkl"))

    def copy_previous_hkl(self, identifier, pcr, prevjobpath, prevjobid, jobpath, jobid):
        prev_pcr = PcrIO(path=prevjobpath, jobid=prevjobid)
        prev_pcr.read_custom_pcr()
        prev_ids = prev_pcr.read_ids()
        prev_idx = prev_ids.index(identifier) + 1

        ids = pcr.read_ids()
        idx = ids.index(identifier) + 1

        if pcr.multipattern:
            for i in range(pcr.get_patt_num()):
                shutil.copyfile(os.path.join(prevjobpath, f"{prevjobid}{prev_idx}_{i}.hkl"),
                                os.path.join(jobpath, f"{jobid}{idx}_{i}.hkl"))
        else:
            shutil.copyfile(os.path.join(prevjobpath, f"{prevjobid}{prev_idx}.hkl"), os.path.join(jobpath, f"{jobid}{idx}.hkl"))

    def setup_bac_file(self, pcr, jobpath, jobid, ipatt, pattern):
        bac = pcr.get_flags_global('nba', ipatt)
        background = pcr.background.get(ipatt, None)
        if background is None:
            if bac == -2 and pcr.multipattern:
                filename = os.path.join(jobpath, f"{jobid}_{ipatt}.bac")
                np.savetxt(filename, pattern.background, header='! Background file', comments='')
            elif bac == -2 and not pcr.multipattern:
                filename = os.path.join(jobpath, f"{jobid}.bac")
                np.savetxt(filename, pattern.background, header='! Background file', comments='')
        else:
            if bac == -2 and pcr.multipattern:
                filename = os.path.join(jobpath, f"{jobid}_{ipatt}.bac")
                np.savetxt(filename, background, header='! Background file', comments='')
            elif bac == -2 and not pcr.multipattern:
                filename = os.path.join(jobpath, f"{jobid}.bac")
                np.savetxt(filename, background, header='! Background file', comments='')
    
    def copy_previous_bac(self, pcr, prevjobpath, prevjobid, jobpath, jobid):
        if pcr.multipattern:
            for i in range(pcr.get_patt_num()):
                shutil.copyfile(os.path.join(prevjobpath, f"{prevjobid}_{i}.bac"),
                                os.path.join(jobpath, f"{jobid}_{i}.bac"))
        else:
            shutil.copyfile(os.path.join(prevjobpath, f"{prevjobid}.bac"), os.path.join(jobpath, f"{jobid}.bac"))

    def get_custom_pcr(self, pcr: PcrIO, index: int, ipatt: int):

        if self.algo == 'SEARCH':
            excluded = self.search_params.excludedtab.regions
            idx = self.search_params.ui_searchparams.background_combo.currentIndex()
            if idx == 0:
                mode = BackgroundType.POINTS
            elif idx == 1:
                mode = BackgroundType.CHEBYSHEV
            elif idx == 2:
                mode = BackgroundType.FOURIER

            if mode == BackgroundType.CHEBYSHEV:
                order = int(self.search_params.cheb_order.text())
            elif mode == BackgroundType.FOURIER:
                fourin = (int(self.search_params.window_fourier.text()), int(self.search_params.cycles_fourier.text()))

        else:
            mode = BackgroundType.POINTS
            order = None
            fourin = None
            excluded = None

        if excluded is not None:
            excl = excluded[ipatt]
        else:
            excl = None

        if mode == BackgroundType.POINTS:
            pcr.edit_fromcif2pcr_to_custom(excluded=excl, ipatt=ipatt, patt=self.patterns[index][ipatt])
        elif mode == BackgroundType.CHEBYSHEV:
            if order <= 23:
                pcr.edit_fromcif2pcr_to_custom(mode=mode, order=order, excluded=excl, ipatt=ipatt, patt=self.patterns[index][ipatt])
            else:
                raise ValueError("Order of required chebyshev expansion exceed the maximum order (23)")
        elif mode == BackgroundType.FOURIER:
            pcr.edit_fromcif2pcr_to_custom(mode=mode, fourin=fourin, excluded=excl, ipatt=ipatt, patt=self.patterns[index][ipatt])  

        return pcr

    @staticmethod
    def create_batches(df):

        df = df.sample(frac=1)
        if df.shape[0] > 15:
            div = int(df.shape[0] / 15.0)
            if df.shape[0] % 15 == 0:
                return np.array_split(df, div)
            else:
                return np.array_split(df, (div + 1))
        else:
            return [df]

    def auto_decider(self):
        if self.auto_rietveld.ui_autorietveld.sequential_check.isChecked():
            self.init_seq_rietveld()
        else:
            self.init_auto_rietveld()

    def init_generate_pcr_auto(self):
        self.algo = 'GENERATE_AUTO'
        self.fpscheduler.set_print_errors(True)
        self.terminal.ui_terminal.kill_button.setEnabled(True)
        self.error = []

        if self.generate_all:
            for counter in range(self.auto_rietveld.ui_autorietveld.pattern_combo.count()):
                jobpath = os.path.join(os.getcwd(), f"GENERATE{counter}")
                try:
                    os.mkdir(jobpath)
                except FileExistsError:
                    shutil.rmtree(jobpath)
                    os.mkdir(jobpath)
                jobid = self.auto_rietveld.ui_autorietveld.pattern_combo.itemText(counter)
                self.generate_pcr(0, jobpath, jobid)
        else:
            jobpath = os.path.join(os.getcwd(), 'GENERATE')
            try:
                os.mkdir(jobpath)
            except FileExistsError:
                shutil.rmtree(jobpath)
                os.mkdir(jobpath)
            jobid = self.auto_rietveld.ui_autorietveld.pattern_combo.currentText()
            self.generate_pcr(0, jobpath, jobid)

        self.terminal.append('Generating PCR files using CIFs_to_PCR\n\n')
        self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
        self.fpscheduler.start_all_processes()

    def init_generate_pcr_manual(self):
        self.algo = 'GENERATE_MANUAL'
        self.fpscheduler.set_print_errors(True)
        self.terminal.ui_terminal.kill_button.setEnabled(True)
        self.error = []

        if self.generate_all:
            for counter in range(self.manual_params.ui_manualparams.pattern_combo.count()):
                jobpath = os.path.join(os.getcwd(), f'GENERATE{counter}')
                try:
                    os.mkdir(jobpath)
                except FileExistsError:
                    shutil.rmtree(jobpath)
                    os.mkdir(jobpath)
                jobid = self.manual_params.ui_manualparams.pattern_combo.itemText(counter)
                self.generate_pcr(0, jobpath, jobid)
        else:
            jobpath = os.path.join(os.getcwd(), 'GENERATE')
            try:
                os.mkdir(jobpath)
            except FileExistsError:
                shutil.rmtree(jobpath)
                os.mkdir(jobpath)
            jobid = self.manual_params.ui_manualparams.pattern_combo.currentText()
            self.generate_pcr(0, jobpath, jobid)
        
        self.terminal.append('Generating PCR files using CIFs_to_PCR\n\n')
        self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
        self.fpscheduler.start_all_processes()

    def generate_pcr(self, runstep, jobpath, jobid):
            
        if runstep == 0:
            index = ['_and_'.join(val) for val in self.graph_toolbar.names].index(jobid)

            indexes = self.ui_mainwindow.tableSelection.selectionModel().selectedRows()
            if len(indexes) != 0:
                rows = self.selection.iloc[[row.row() for row in indexes]]
                ids = rows['identifier'].tolist()
            else:
                ids = self.selection['identifier'].tolist()

            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.prepare_buf(identifiers=ids, dirname=self.cifdir)
            pcr.prepare_c2pcr(data=self.patterns[index])
            self.fpscheduler.append_command_queue(self.cif2pcr_path, [f"-file", f"{jobid}.c2pcr", f"-f_stop"], f'{runstep}', jobpath)
        
        elif runstep != 0 and self.algo == 'GENERATE_AUTO':
            index = ['_and_'.join(val) for val in self.graph_toolbar.names].index(jobid)
            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()
            for ipatt in range(len(self.patterns[index])):
                pcr.edit_fromcif2pcr_to_custom(ipatt=ipatt, patt=self.patterns[index][ipatt])
            ids = pcr.read_ids()
            self.clean_cifs(ids, jobpath)
            shutil.rmtree(jobpath)
            if jobid in self.auto_rietveld.pattern_pcrs:
                oldpcr = self.auto_rietveld.pattern_pcrs.get(jobid)
                self.auto_rietveld.pattern_pcrs.pop(jobid)
                for ids in pcr.read_ids():
                    if ids not in oldpcr.read_ids():
                       phase = pcr.get_phase(ids)
                       oldpcr.add_phase(phase)
                 
                self.auto_rietveld.pattern_pcrs[jobid] = oldpcr
            else:
                self.auto_rietveld.pattern_pcrs[jobid] = pcr

            self.auto_rietveld.pattern_phase[jobid] = ids
            self.auto_rietveld.update_parameters_phases()
            self.terminal.ui_terminal.kill_button.setEnabled(False)

        elif runstep != 0 and self.algo == 'GENERATE_MANUAL':
            index = ['_and_'.join(val) for val in self.graph_toolbar.names].index(jobid)

            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()
            for ipatt in range(len(self.patterns[index])):
                pcr.edit_fromcif2pcr_to_custom(ipatt=ipatt, patt=self.patterns[index][ipatt])

            ids = pcr.read_ids()
            self.clean_cifs(ids, jobpath)
            shutil.rmtree(jobpath)
            for ipatt in range(len(self.patterns[index])):
                pcr.edit_flags_global('ipr', 2, ipatt)
                pcr.edit_flags_global('ppl', 2, ipatt)
            pcr.edit_flags_global('rpa', -1)
            pcr.edit_flags_global('pcr', 1)

            if jobid in self.manual_params.pattern_pcrs:
                oldpcr = self.manual_params.pattern_pcrs.get(jobid)
                self.manual_params.pattern_pcrs.pop(jobid)
                for ids in pcr.read_ids():
                    if ids not in oldpcr.read_ids():
                       phase = pcr.get_phase(ids)
                       oldpcr.add_phase(phase)
                 
                self.manual_params.pattern_pcrs[jobid] = oldpcr
            else:
                self.manual_params.pattern_pcrs[jobid] = pcr

            self.terminal.ui_terminal.kill_button.setEnabled(False)
    
    def init_manual_rietveld(self):
        innew = True if (f'MANUAL' not in self.currentpath if self.currentpath is not None else True) or self.error else False
        if innew:
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, False)
            self.error = []
            manualnum = self.init_fpmanual()
        else:
            self.error = []
            manualnum = self.run_num

        self.algo = 'MANUAL'
        self.fpscheduler.set_print_errors(True)
        self.resultgraph['current'].initialize_tree_dict(self.manual_params.pattern_pcrs)
        self.result_dict = dict()
        self.run_num = manualnum
        for counter in range(self.manual_params.ui_manualparams.pattern_combo.count()):
            jobpath, jobid = self.init_current_workdir(counter, manualnum)
            idx = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
            pcr = self.manual_params.pattern_pcrs[jobid]
            for ipatt in range(len(self.patterns[idx])):
                if pcr.multipattern:
                    name = os.path.relpath(self.patterns[idx][ipatt].filename, jobpath)
                    pcr.replace_names(name, ipatt)

                irf = os.path.relpath(self.patterns[idx][ipatt].irf, jobpath)
                pcr.replace_irf(irf, ipatt)

            pcr.write_pcr_custom(path=os.path.join(jobpath, f"{jobid}.pcr"))

            if innew:
                for ipatt, patt in enumerate(self.patterns[idx]):
                    try:
                        self.setup_bac_file(pcr, jobpath, jobid, ipatt, patt) # It will internally check if bac flag is -2
                    except ValueError:
                        self.warning = qtw.QMessageBox()
                        self.warning.setIcon(qtw.QMessageBox.Critical)
                        self.warning.setText(f"Cannot setup .bac file for job {jobid}, make sure a background is generated using the 'Automatic Background' option")
                        self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                        self.warning.exec_()

                        return

            self.run_manual_rietveld(0, jobpath, jobid)

        self.terminal.ui_terminal.kill_button.setEnabled(True)
        
        self.terminal.append('Starting Manual Refinement\n\n')
        self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
        self.fpscheduler.start_all_processes()

    def init_manual_rietveld_new(self):
        self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, False)
        self.error = []
        self.algo = 'MANUAL'
        self.fpscheduler.set_print_errors(True)
        self.resultgraph['current'].initialize_tree_dict(self.manual_params.pattern_pcrs)
        self.result_dict = dict()

        manualnum = self.init_fpmanual()
        self.run_num = manualnum
        for counter in range(self.manual_params.ui_manualparams.pattern_combo.count()):
            jobpath, jobid = self.init_current_workdir(counter, manualnum)
            idx = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
            pcr = self.manual_params.pattern_pcrs[jobid]
            for ipatt in range(len(self.patterns[idx])):
                if pcr.multipattern:
                    name = os.path.relpath(self.patterns[idx][ipatt].filename, jobpath)
                    pcr.replace_names(name, ipatt)

                irf = os.path.relpath(self.patterns[idx][ipatt].irf, jobpath)
                pcr.replace_irf(irf, ipatt)
                
            pcr.write_pcr_custom(path=os.path.join(jobpath, f"{jobid}.pcr"))

            for ipatt, patt in enumerate(self.patterns[idx]):
                try:
                    self.setup_bac_file(pcr, jobpath, jobid, ipatt, patt) # It will internally check if bac flag is -2
                except ValueError:
                    self.warning = qtw.QMessageBox()
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText(f"Cannot setup .bac file for job {jobid}, make sure a background is generated using the 'Automatic Background' option")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    self.warning.exec_()

                    return

            self.run_manual_rietveld(0, jobpath, jobid)
        
        self.terminal.ui_terminal.kill_button.setEnabled(True)

        self.terminal.append('Starting Manual Refinement\n\n')
        self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
        self.fpscheduler.start_all_processes()
 
    def run_manual_rietveld(self, runstep, jobpath, jobid):

        if runstep == 0:
            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()
            idx = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
            if pcr.multipattern:
                self.fpscheduler.append_command_queue(self.fp2k_path, [f'{jobid}.pcr'], f'{runstep}', jobpath)
            else:
                self.fpscheduler.append_command_queue(self.fp2k_path,
                                                       [f'{jobid}.pcr', ' '.join([os.path.relpath(patt.filename, jobpath) for patt in self.patterns[idx]])], f'{runstep}', jobpath) 
        
        elif runstep != 0:
            try:
                self.obtain_results(jobpath, jobid)
                self.resultgraph['current'].fill_results_database(path=jobpath)
                self.ui_mainwindow.tabGraphs.setTabVisible(self.indexSummary, True)
                self.resultgraph['current'].update_plots()
                self.resultgraph['current'].export_all(filename=jobpath.replace(jobid, ""))
                self.ui_mainwindow.tabGraphs.setTabVisible(self.indexPrfData, True)
                self.save_manual_pcr(jobpath, jobid)
                self.terminal.ui_terminal.kill_button.setEnabled(False)
            except FullProfOutputReadError:
                self.ui_mainwindow.tabGraphs.setTabVisible(self.indexSummary, False)
                self.ui_mainwindow.tabGraphs.setTabVisible(self.indexPrfData, False)
                self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, False)

    def mix_pcrs(self, ipcr, jpcr):
        iids = ipcr.read_ids()
        jids = jpcr.read_ids()

        totids = set(iids + jids)
        count = 0
        for identifier in totids:
            if not ipcr.check_phase_exists(identifier=identifier):
                jphase = jpcr.get_phase(identifier=identifier)
                ipcr.custom_pcr[f'phase{len(iids) + count}'] = jphase
                count += 1

                ipcr.edit_flags_global('nph', len(iids) + count)

        return ipcr

    def init_seq_rietveld(self, jobpath = None, jobid = None):
        self.terminal.ui_terminal.kill_button.setEnabled(True)
        if jobpath is None and jobid is None:
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, False)
            self.error = []

            ncy = int(self.auto_rietveld.ui_autorietveld.line_ncy.text())
            ste = int(self.auto_rietveld.ui_autorietveld.line_ste.text())
            rat = float(self.auto_rietveld.ui_autorietveld.rat.text())
            ran = float(self.auto_rietveld.ui_autorietveld.ran.text())
            rpr = float(self.auto_rietveld.ui_autorietveld.rpr.text())
            rgl = float(self.auto_rietveld.ui_autorietveld.rgl.text())

            self.algo = 'SEQ'
            self._seqpataction = None
            self.fpscheduler.set_print_errors(True)
            self.resultgraph['current'].initialize_tree_dict(self.auto_rietveld.pattern_pcrs)
            self.result_dict = dict()

            removed_sites = []
            if self.auto_rietveld.lebail_const:
                for key, value in self.auto_rietveld.lebail_const.items():
                    if value.isChecked():
                        removed_sites.append(f"{key}\n")

            if removed_sites:
                self.warning = qtw.QMessageBox()
                self.warning.setIcon(qtw.QMessageBox.Warning)
                self.warning.setText(f"Profile Matching (Constant Relative Intensities) activated."
                                        f"\nThe sites of the following phases will be deleted:\n{' - '.join(removed_sites)}\n"
                                        f"The app will not save the newly generated PCR files in this case.")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                self.warning.exec_()

            seqnum = self.init_fpseq()       
            for i in range(self.auto_rietveld.ui_autorietveld.pattern_combo.count()):
                jobpath, jobid = self.init_current_workdir(self.graph_toolbar.checked_index[i], seqnum)
                idx = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
                try:
                    iid = self.auto_rietveld.ui_autorietveld.pattern_combo.itemText(i)
                    jid = self.auto_rietveld.ui_autorietveld.pattern_combo.itemText(i + 1)
                    ipcr = self.auto_rietveld.pattern_pcrs[iid]
                    jpcr = self.auto_rietveld.pattern_pcrs[jid]
                    pcr = self.mix_pcrs(ipcr, jpcr)
                except (IndexError, KeyError):
                    iid = self.auto_rietveld.ui_autorietveld.pattern_combo.itemText(i)
                    pcr = self.auto_rietveld.pattern_pcrs[iid]

                npatt = pcr.get_patt_num()
                regions = self.auto_rietveld.excludedtab.regions
                for ipatt in range(npatt):
                    exclude = np.array(regions[ipatt], dtype=float)
                    pcr.replace_excluded(exclude, ipatt)
                    pcr.edit_flags_global('ste', ste, ipatt)
                    pcr.edit_flags_global('ipr', 2, ipatt)
                    pcr.edit_flags_global('ppl', 2, ipatt)
                    pcr.edit_flags_global('prf', 3, ipatt)
                    pcr.replace_irf(os.path.relpath(self.patterns[idx][ipatt].irf, jobpath), ipatt)

                pcr.edit_flags_global('ncy', ncy)
                pcr.edit_flags_global('rat', rat)
                pcr.edit_flags_global('ran', ran)
                pcr.edit_flags_global('rpr', rpr)
                pcr.edit_flags_global('rgl', rgl)
                pcr.edit_flags_global('rpa', -1)
                pcr.edit_flags_global('pcr', 1)
                pcr.exclude_patterns(self.graph_toolbar.checked_code[self.graph_toolbar.checked_index[i], :])

                if pcr.multipattern:
                    for ipatt in range(npatt):
                        name = os.path.relpath(self.patterns[idx][ipatt].filename, jobpath)
                        pcr.replace_names(name, ipatt)

                ids = pcr.read_ids()
                for identifier in ids:
                    pcr.edit_flags_phase(identifier, 'jbt', 0)
                    for ipatt in range(npatt):
                        pcr.edit_flags_phase(identifier, 'irf', 0, ipatt)
                
                if self.auto_rietveld.lebail_mode:
                    for key, value in self.auto_rietveld.lebail_mode.items():
                        if value.isChecked():
                            pcr.edit_flags_phase(key, 'jbt', 2)
                            for ipatt in range(npatt):
                                pcr.edit_flags_phase(key, 'irf', 0, ipatt)
                
                if self.auto_rietveld.lebail_const:
                    for key, value in self.auto_rietveld.lebail_const.items():
                        if value.isChecked():
                            pcr.remove_sites(identifier=key)
                            self.setup_hkl_file(key, pcr, jobpath, jobid)
                            pcr.edit_flags_phase(key, 'jbt', 3)
                            for ipatt in range(npatt):
                                pcr.edit_flags_phase(key, 'irf', 2, ipatt)

                if self.auto_rietveld.backg_menu.seqpattaction.isChecked():
                    if self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() != 0 and self._seqpataction is None:
                        self.warning = qtw.QMessageBox()
                        self.warning.setIcon(qtw.QMessageBox.Warning)
                        self.warning.setText(f"You activated the 'Load on Sequential' option, but you are trying to use non-linearly interpolated backgrounds (chebyshev, fourier, ...), are you sure you want to continue?")
                        self.warning.setStandardButtons(qtw.QMessageBox.Yes | qtw.QMessageBox.No)
                        self._seqpataction = self.warning.exec_()

                        if self._seqpataction == qtw.QMessageBox.No:
                            self._seqpataction = None
                            return
                    
                    for ipatt, patt in enumerate(self.patterns[self.graph_toolbar.checked_index[i]]):
                        anchor = patt.anchor_points[1:-1, :]
                        zeros = np.zeros(anchor.shape[0], dtype=float)
                        background = np.column_stack((anchor[:, 0], anchor[:, 1], zeros))
                        pcr.replace_background(background, ipatt, mode=BackgroundType.POINTS)

                        if self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 1: #Chebyshev
                            order = int(self.auto_rietveld.cheb_order.text())
                            pcr.transform_anchor_to_cheb(order, ipatt)
                        elif self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 2: #Fourier
                            window = int(self.auto_rietveld.window_fourier.text())
                            cycle = int(self.auto_rietveld.cycles_fourier.text())
                            pcr.save_background(ipatt)
                            pcr.replace_background(np.array([[window, cycle]], dtype=int), ipatt, BackgroundType.FOURIER)

                            try:
                                self.setup_bac_file(pcr, jobpath, jobid, ipatt, patt)
                            except ValueError:
                                self.warning = qtw.QMessageBox()
                                self.warning.setIcon(qtw.QMessageBox.Critical)
                                self.warning.setText(f"Cannot setup .bac file for job {jobid}, make sure a background is generated using the 'Automatic Background' option")
                                self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                                self.warning.exec_()

                                return        
                        
                else:
                    for ipatt, patt in enumerate(self.patterns[self.graph_toolbar.checked_index[i]]):
                        nba = pcr.get_flags_global('nba', ipatt)
                        if nba == -2 and self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() != 2:
                            self.warning = qtw.QMessageBox()
                            self.warning.setIcon(qtw.QMessageBox.Critical)
                            self.warning.setText(f"The PCR file you loaded has the Fourier Filtering mode activated for the background (nba = -2), I cannot reconstruct a linear interpolation or a Chebyshev expansion of the background. Aborting...")
                            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                            self.warning.exec_()

                            return

                        if self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 0: #Points
                            pcr.transform_cheb_to_anchor(patt.diffraction[:, 0], ipatt)
                        elif self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 1: #Chebyshev
                            order = int(self.auto_rietveld.cheb_order.text())
                            pcr.transform_anchor_to_cheb(order, ipatt)
                        elif self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 2: #Fourier
                            window = int(self.auto_rietveld.window_fourier.text())
                            cycle = int(self.auto_rietveld.cycles_fourier.text())
                            pcr.save_background(ipatt)
                            pcr.replace_background(np.array([[window, cycle]], dtype=int), ipatt, BackgroundType.FOURIER)

                            try:
                                self.setup_bac_file(pcr, jobpath, jobid, ipatt, patt)
                            except ValueError:
                                self.warning = qtw.QMessageBox()
                                self.warning.setIcon(qtw.QMessageBox.Critical)
                                self.warning.setText(f"Cannot setup .bac file for job {jobid}, make sure a background is generated using the 'Automatic Background' option")
                                self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                                self.warning.exec_()

                                return                 

                pcr.write_pcr_custom(path=os.path.join(jobpath, f"{jobid}.pcr"))
                self.run_num = seqnum
                
                self.run_auto_rietveld(0, jobpath, jobid)
            
            self.terminal.append('Starting Sequential Rietveld Refinement Protocol\n\n')
            self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
            self.fpscheduler.start_all_processes()

        else:
            idx = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
            nextpath, nextid = self.init_current_workdir(idx + 1, self.run_num)
            
            prevpcr = PcrIO(path=jobpath, jobid=jobid)
            prevpcr.read_custom_pcr()
            ids = prevpcr.read_ids()
            npatt = prevpcr.get_patt_num()

            pcr = PcrIO(path=nextpath, jobid=nextid)
            pcr.custom_pcr = prevpcr.custom_pcr
            
            ids = pcr.read_ids()
            for identifier in ids:
                pcr.edit_flags_phase(identifier, 'jbt', 0)
                for ipatt in range(npatt):
                    pcr.edit_flags_phase(identifier, 'irf', 0, ipatt)

            if self.auto_rietveld.lebail_mode:
                for key, value in self.auto_rietveld.lebail_mode.items():
                    if value.isChecked():
                        pcr.edit_flags_phase(key, 'jbt', 2)
                        for ipatt in range(npatt):
                            pcr.edit_flags_phase(key, 'irf', 0, ipatt)
            
            if self.auto_rietveld.lebail_const:
                for key, value in self.auto_rietveld.lebail_const.items():
                    if value.isChecked():
                        pcr.remove_sites(identifier=key)
                        self.copy_previous_hkl(key, pcr, jobpath, jobid, nextpath, nextid)
                        pcr.edit_flags_phase(key, 'jbt', 3)
                        for ipatt in range(npatt):
                            pcr.edit_flags_phase(key, 'irf', 2, ipatt)
            
            if self.auto_rietveld.backg_menu.seqpattaction.isChecked():
                for ipatt, patt in enumerate(self.patterns[idx + 1]):
                    anchor = patt.anchor_points[1:-1, :]
                    zeros = np.zeros(anchor.shape[0], dtype=float)
                    background = np.column_stack((anchor[:, 0], anchor[:, 1], zeros))
                    pcr.replace_background(background, ipatt, mode=BackgroundType.POINTS)

                    if self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 1: #Chebyshev
                        order = int(self.auto_rietveld.cheb_order.text())
                        pcr.transform_anchor_to_cheb(order, ipatt)
                    elif self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 2: #Fourier
                        window = int(self.auto_rietveld.window_fourier.text())
                        cycle = int(self.auto_rietveld.cycles_fourier.text())
                        pcr.save_background(ipatt)
                        pcr.replace_background(np.array([[window, cycle]], dtype=int), ipatt, BackgroundType.FOURIER)   

                        try:
                            self.setup_bac_file(pcr, nextpath, nextid, ipatt, patt)
                        except ValueError:
                            self.warning = qtw.QMessageBox()
                            self.warning.setIcon(qtw.QMessageBox.Critical)
                            self.warning.setText(f"Cannot setup .bac file for job {jobid}, make sure a background is generated using the 'Automatic Background' option")
                            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                            self.warning.exec_()

                            return      
            else:
                for ipatt, patt in enumerate(self.patterns[idx + 1]):
                    if self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 0: #Points
                        pcr.transform_cheb_to_anchor(patt.diffraction[:, 0], ipatt)
                    elif self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 1: #Chebyshev
                        order = int(self.auto_rietveld.cheb_order.text())
                        pcr.transform_anchor_to_cheb(order, ipatt)
                    elif self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 2: #Fourier
                        window = int(self.auto_rietveld.window_fourier.text())
                        cycle = int(self.auto_rietveld.cycles_fourier.text())
                        self.copy_previous_bac(pcr, jobpath, jobid, nextpath, nextid)
                        pcr.replace_background(np.array([[window, cycle]], dtype=int), ipatt, BackgroundType.FOURIER)        

            pcr.write_pcr_custom(path=os.path.join(nextpath, f"{nextid}.pcr"))
            self.run_auto_rietveld(0, nextpath, nextid)

    def init_auto_rietveld(self):
        self.terminal.ui_terminal.kill_button.setEnabled(True)
        self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, False)
        self.error = []

        ncy = int(self.auto_rietveld.ui_autorietveld.line_ncy.text())
        ste = int(self.auto_rietveld.ui_autorietveld.line_ste.text())
        rat = float(self.auto_rietveld.ui_autorietveld.rat.text())
        ran = float(self.auto_rietveld.ui_autorietveld.ran.text())
        rpr = float(self.auto_rietveld.ui_autorietveld.rpr.text())
        rgl = float(self.auto_rietveld.ui_autorietveld.rgl.text())

        self.algo = 'AUTO'
        self.fpscheduler.set_print_errors(True)
        self.resultgraph['current'].initialize_tree_dict(self.auto_rietveld.pattern_pcrs)
        self.result_dict = dict()

        removed_sites = []
        if self.auto_rietveld.lebail_const:
            for key, value in self.auto_rietveld.lebail_const.items():
                if value.isChecked():
                    removed_sites.append(f"{key}\n")

        if removed_sites:
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Warning)
            self.warning.setText(f"Profile Matching (Constant Relative Intensities) activated."
                                 f"\nThe sites of the following phases will be deleted:\n{' - '.join(removed_sites)}\n"
                                 f"The app will not save the newly generated PCR files in this case.")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

        autonum = self.init_fpauto()
        for counter in range(self.auto_rietveld.ui_autorietveld.pattern_combo.count()):
            jobpath, jobid = self.init_current_workdir(counter, autonum)
            idx = self.graph_toolbar.checked_index[counter]

            pcr = self.auto_rietveld.pattern_pcrs[jobid]
            regions = self.auto_rietveld.excludedtab.regions
            for ipatt in range(len(self.patterns[idx])):
                exclude = np.array(regions[ipatt], dtype=float)
                pcr.replace_excluded(exclude, ipatt)
                pcr.edit_flags_global('ste', ste, ipatt)
                pcr.edit_flags_global('ipr', 2, ipatt)
                pcr.edit_flags_global('ppl', 2, ipatt)
                pcr.edit_flags_global('prf', 3, ipatt)
                pcr.replace_irf(os.path.relpath(self.patterns[idx][ipatt].irf, jobpath), ipatt)

            pcr.edit_flags_global('ncy', ncy)
            pcr.edit_flags_global('rat', rat)
            pcr.edit_flags_global('ran', ran)
            pcr.edit_flags_global('rpr', rpr)
            pcr.edit_flags_global('rgl', rgl)
            pcr.edit_flags_global('rpa', -1)
            pcr.edit_flags_global('pcr', 1)
            pcr.exclude_patterns(self.graph_toolbar.checked_code[idx, :])

            if pcr.multipattern:
                for ipatt in range(len(self.patterns[idx])):
                    name = os.path.relpath(self.patterns[idx][ipatt].filename, jobpath)
                    pcr.replace_names(name, ipatt)

            ids = pcr.read_ids()
            for identifier in ids:
                pcr.edit_flags_phase(identifier, 'jbt', 0)
                for ipatt in range(len(self.patterns[idx])):
                    pcr.edit_flags_phase(identifier, 'irf', 0, ipatt)

            if self.auto_rietveld.lebail_mode:
                for key, value in self.auto_rietveld.lebail_mode.items():
                    if value.isChecked():
                        pcr.edit_flags_phase(key, 'jbt', 2)
                        for ipatt in range(len(self.patterns[idx])):
                            pcr.edit_flags_phase(key, 'irf', 0, ipatt)

            if self.auto_rietveld.lebail_const:
                for key, value in self.auto_rietveld.lebail_const.items():
                    if value.isChecked():
                        pcr.remove_sites(identifier=key)
                        self.setup_hkl_file(key, pcr, jobpath, jobid)
                        pcr.edit_flags_phase(key, 'jbt', 3)
                        for ipatt in range(len(self.patterns[idx])):
                            pcr.edit_flags_phase(key, 'irf', 2, ipatt)

            for ipatt, patt in enumerate(self.patterns[idx]):
                nba = pcr.get_flags_global('nba', ipatt)
                if nba == -2 and self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() != 2:
                    self.warning = qtw.QMessageBox()
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText(f"The PCR file you loaded has the Fourier Filtering mode activated for the background (nba = -2), I cannot reconstruct a linear interpolation or a Chebyshev expansion of the background. Aborting...")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    self.warning.exec_()

                    return

                if self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 0: #Points
                    pcr.transform_cheb_to_anchor(patt.diffraction[:, 0], ipatt)
                elif self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 1: #Chebyshev
                    order = int(self.auto_rietveld.cheb_order.text())
                    pcr.transform_anchor_to_cheb(order, ipatt)
                elif self.auto_rietveld.ui_autorietveld.background_combo.currentIndex() == 2: #Fourier
                    window = int(self.auto_rietveld.window_fourier.text())
                    cycle = int(self.auto_rietveld.cycles_fourier.text())
                    pcr.save_background(ipatt)
                    pcr.replace_background(np.array([[window, cycle]], dtype=int), ipatt, BackgroundType.FOURIER)    

                    try:
                        self.setup_bac_file(pcr, jobpath, jobid, ipatt, patt)
                    except ValueError:
                        self.warning = qtw.QMessageBox()
                        self.warning.setIcon(qtw.QMessageBox.Critical)
                        self.warning.setText(f"Cannot setup .bac file for job {jobid}, make sure a background is generated using the 'Automatic Background' option")
                        self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                        self.warning.exec_()

                        return 

            pcr.write_pcr_custom(path=os.path.join(jobpath, f"{jobid}.pcr"))
            self.run_num = autonum

            self.run_auto_rietveld(0, jobpath, jobid)

        self.terminal.append('Starting Automatic Refinement Protocol\n\n')
        self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
        self.fpscheduler.start_all_processes()

    def run_auto_rietveld(self, runstep, jobpath, jobid):
        if runstep == len(self.auto_rietveld.workflow_commands):
            self.obtain_results(jobpath, jobid)
            self.terminal.ui_terminal.kill_button.setEnabled(False)
            self.resultgraph['current'].fill_results_database(path=jobpath)
            self.ui_mainwindow.tabGraphs.setTabVisible(self.indexSummary, True)
            self.resultgraph['current'].update_plots()
            self.resultgraph['current'].export_all(filename=jobpath.replace(jobid, ""))
            self.ui_mainwindow.tabGraphs.setTabVisible(self.indexPrfData, True)
            
            if self.auto_rietveld.ui_autorietveld.sequential_check.isChecked():
                idx = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
                if idx in self.graph_toolbar.checked_index:
                    self.save_auto_pcr(jobpath, jobid)

                if idx + 1 in self.graph_toolbar.checked_index:
                    return
                elif idx == self.graph_toolbar.checked_index[-1] and self.graph_toolbar.checked_index.shape[0] > 1:
                    return
                elif idx == self.graph_toolbar.patterns_combo.count() - 1:
                    return
                else:
                    self.init_seq_rietveld(jobpath, jobid)
                    return
            else:  
                self.save_auto_pcr(jobpath, jobid)
                return
    
        pcr = PcrIO(path=jobpath, jobid=jobid)
        pcr.read_custom_pcr()
        pcr.clear_fix_vary()
        pcr.remove_restraint()
        pcr.remove_limits()

        idx = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
        ids = pcr.read_ids()

        if runstep != 0:
            sumfile = SumIO(path=jobpath, jobid=jobid)
            vol_frac = sumfile.parse_volume_fractions(pcr, ids)
        else:
            vol_frac = None

        constrained_cells = {}
        
        self.auto_rietveld.raise_lebail_warning(
            self.auto_rietveld.workflow_commands[f"Step {runstep}"])

        """ --------------------------- PARAMETER VARY BLOCK --------------------------------------------"""
        for key, val in self.auto_rietveld.workflow_commands[f"Step {runstep}"]['VARY'].items():
            if 'No. ' in key:
                if 'Pattern' in val[0]:
                    ipatt = int(val[0].replace('Pattern', '').strip())
                else:
                    ipatt = None

                param = val[1]
                identifier = val[2]
                label = val[3]
                command = val[4]
                code = val[5]
                coeff = val[6]

                if (not code and not coeff) or code == 'PEQU':
                    codepcr = 1.0
                else:
                    if float(coeff) < 0.0:
                        codepcr = float(f"-{int(code)}{abs(float(coeff))}")
                    else:
                        codepcr = float(f"{int(code)}{abs(float(coeff))}")
                
                if identifier in ids:
                    if vol_frac is not None and all(vol[identifier] < 1.0 for vol in vol_frac.values()):
                        if command == 'scale':
                            pcr.edit_profile_phase(identifier, command, 0.00001, ipatt)

                if identifier in ids:
                    if vol_frac is not None and all(vol[identifier] < 2.0 for vol in vol_frac.values()):
                        d1 = self.auto_rietveld.lebail_mode
                        d2 = self.auto_rietveld.lebail_const
                        if d1 and d2:
                            if all(identifier in d for d in (d1, d2)):
                                if d1[identifier].isChecked():
                                    codepcr = 1.0
                                elif d2[identifier].isChecked():
                                    codepcr = 1.0
                                else:
                                    codepcr = 0.0

                            else:
                                codepcr = 0.0
                        else:
                            codepcr = 0.0

                if not identifier and not label:
                    if 'backgd' in command:
                        pcr.edit_background_code(True, ipatt)
                    else:
                        pcr.edit_shifts_global(command, codepcr, ipatt)

                elif identifier and not label:
                    if identifier in ids:
                        if 'cell' in command:
                            pcr.edit_profile_phase(identifier, 'acode', codepcr, ipatt)
                            pcr.edit_profile_phase(identifier, 'bcode', codepcr, ipatt)
                            pcr.edit_profile_phase(identifier, 'ccode', codepcr, ipatt)
                            pcr.edit_profile_phase(identifier, 'alphacode', codepcr, ipatt)
                            pcr.edit_profile_phase(identifier, 'betacode', codepcr, ipatt)
                            pcr.edit_profile_phase(identifier, 'gammacode', codepcr, ipatt)
                            if code:
                                constrained_cells[identifier] = code
                        else:
                            pcr.edit_profile_phase(identifier, command, codepcr, ipatt)

                elif identifier and label:
                    if identifier in ids:
                        atoms = pcr.read_site_labels(identifier)
                        atoms = atoms if atoms is not None else []
                        if label in atoms:
                            pcr.edit_site_phase(identifier, label, codepcr, command)

        for key in constrained_cells.keys():
            pcr.add_phase_pequpat(key, ['cell'])

        """ --------------------------- PARAMETER FIX BLOCK --------------------------------------------"""
        for key, val in self.auto_rietveld.workflow_commands[f"Step {runstep}"]['FIX'].items():
            if 'No. ' in key:
                if 'Pattern' in val[0]:
                    ipatt = int(val[0].replace('Pattern', '').strip())
                else:
                    ipatt = None

                param = val[1]
                identifier = val[2]
                label = val[3]
                command = val[4]

                if not identifier and not label:
                    if 'backgd' in command:
                        pcr.edit_background_code(False, ipatt)
                    else:
                        pcr.edit_shifts_global(command, 0.0, ipatt)
                elif identifier and not label:
                    if identifier in ids:
                        if 'cell' in command:
                            pcr.edit_profile_phase(identifier, 'acode', 0.0, ipatt)
                            pcr.edit_profile_phase(identifier, 'bcode', 0.0, ipatt)
                            pcr.edit_profile_phase(identifier, 'ccode', 0.0, ipatt)
                            pcr.edit_profile_phase(identifier, 'alphacode', 0.0, ipatt)
                            pcr.edit_profile_phase(identifier, 'betacode', 0.0, ipatt)
                            pcr.edit_profile_phase(identifier, 'gammacode', 0.0, ipatt)
                        else:
                            pcr.edit_profile_phase(identifier, command, 0.0, ipatt)
                elif identifier and label:
                    if identifier in ids:
                        atoms = pcr.read_site_labels(identifier)
                        atoms = atoms if atoms is not None else []
                        if label in atoms:
                            pcr.edit_site_phase(identifier, label, 0.0, command)

        """ ------------------------------ RESTRAINTS BLOCK --------------------------------------------"""
        count = 1
        for key, val in self.auto_rietveld.workflow_commands[f"Step {runstep}"].items():
            if 'Linear Restraint' in key:
                values = [f"Restraint{count}", f"{len(val) - 2}"]
                codes = []
                for names, coefficients in val.items():
                    if 'value' in names:
                        values.append(coefficients[-1])
                    elif 'sigma' in names:
                        values.append(coefficients[-1])
                    elif '>' in names:
                        codes.extend([coefficients[-1], coefficients[-2]])

                count += 1
                if values and codes:
                    pcr.add_restraint([values, codes])

        """ ------------------------------ LIMITS BLOCK --------------------------------------------"""
        count = 1
        for key, val in self.auto_rietveld.workflow_commands[f"Step {runstep}"].items():
            if 'Parameter Limit' in key:
                values = []
                for names, coefficients in val.items():
                    if '>' in names:
                        values.append(coefficients[-2])
                    elif 'Low' in names:
                        values.append(coefficients[-1])
                    elif 'High' in names:
                        values.append(coefficients[-1])
                        values.append('0.0000')
                    elif 'Boundary' in names:
                        values.append(coefficients[-1])

                count += 1
                if values:
                    pcr.add_limits(values)

        fixspc = self.auto_rietveld.ui_autorietveld.line_elements.text().replace(' ', '')
        if fixspc:
            for identifier in pcr.read_ids():
                pcr.add_phase_fixspc(identifier, fixspc.split())

        pcr.write_pcr_custom()
        if pcr.multipattern:
            self.fpscheduler.append_command_queue(self.fp2k_path, [f'{jobid}.pcr'], f'{runstep}', jobpath)
        else:
            self.fpscheduler.append_command_queue(self.fp2k_path,
                                                       [f'{jobid}.pcr', ' '.join([os.path.relpath(patt.filename, jobpath) for patt in self.patterns[idx]])], f'{runstep}', jobpath) 

    def init_fp_search(self, jobpath = None, jobid = None):
        self.terminal.ui_terminal.kill_button.setEnabled(True)
        if jobpath is None and jobid is None:
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, False)
            
            self.algo = 'SEARCH'
            self.result_dict = dict()
            self.fpscheduler.set_print_errors(False)

            searchnum = self.init_fplute()
            jobpath, jobid = self.init_current_workdir(0, searchnum)  

            self.s1 = self.selection['identifier'].tolist()
            self.s2 = []
            self.luterwp = []

            self.luteres = PhaseSearchWindow()
            self.luteres.show()
            self.luteres.set_pattern_text(jobid)
            self.run_num = searchnum

            self.start_search_cycle(jobpath)
            self.terminal.append('Starting Full-Profile Phase Search Protocol on Pattern {jobid} \n\n')
            self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
            self.fpscheduler.start_all_timers()
            self.fpscheduler.start_all_processes()

        else:
            idx = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
            nextpath, nextid = self.init_current_workdir(idx + 1, self.run_num)
            self.s1 = self.selection['identifier'].tolist()
            self.s2 = []
            self.luterwp = []

            self.luteres.set_pattern_text(nextid)

            self.start_search_cycle(nextpath)
            self.terminal.append('Starting Full-Profile Phase Search Protocol on Pattern {nextid} \n\n')
            self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
            self.fpscheduler.start_all_timers()
            self.fpscheduler.start_all_processes()
            
    def start_search_cycle(self, jobpath):
        self.lutefom = [np.nan] * len(self.s1)
        self.remove_from_s1 = []
        self.maxfom = [0.0, 0]
        self.v0 = None
        self.vf = None

        for candidate in reversed(self.s1):
            self.run_fp_search(0, jobpath, f"_phase_{candidate}")
        
        self.luteres.setup_window(
            self.selection.loc[self.selection['identifier'].isin(self.s1),
                                ['identifier', 'formula']].values.tolist(),
            self.selection.loc[self.selection['identifier'].isin(self.s2),
                                ['identifier', 'formula']].values.tolist())

    def run_fp_search(self, runstep, jobpath, jobid):
        if runstep == 0:
            candidate = jobid.replace("_phase_", "")
            phases = self.s2 + [candidate]

            pattern = os.path.split(jobpath)[1]
            idx = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == pattern)

            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.prepare_buf(identifiers=phases, dirname=self.cifdir)
            pcr.prepare_c2pcr(data=self.patterns[idx])

            runstep = f"_phase_{candidate}___{runstep}"
            self.fpscheduler.append_command_queue(self.cif2pcr_path, [f"-file", f"{jobid}.c2pcr", f"-f_stop"], runstep, jobpath)

        elif runstep == 1:
            ncy = int(self.search_params.ui_searchparams.scanncy_edit.text())
            ste = int(self.search_params.ui_searchparams.scanste_edit.text())
            rat = float(self.search_params.ui_searchparams.scanrat.text())
            ran = float(self.search_params.ui_searchparams.scanran.text())
            rpr = float(self.search_params.ui_searchparams.scanrpr.text())
            rgl = float(self.search_params.ui_searchparams.scanrgl.text())

            pattern = os.path.split(jobpath)[1]
            index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == pattern)

            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()
            for i in range(len(self.patterns[index])):
                pcr = self.get_custom_pcr(pcr, index=index, ipatt=i)
                if pcr is None:
                    return
                
                try:
                    self.setup_bac_file(pcr, jobpath, jobid, i, self.patterns[index][i])
                except ValueError:
                    self.warning = qtw.QMessageBox()
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText(f"Cannot setup .bac file for job {jobid}, make sure a background is generated using the 'Automatic Background' option")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    self.warning.exec_()

                    return
  
                pcr.edit_flags_global('ste', ste, i)
                pcr.edit_flags_global('ipr', 2, i)
                pcr.edit_flags_global('ppl', 2, i)

            pcr.edit_flags_global('ncy', ncy)
            pcr.edit_flags_global('rat', rat)
            pcr.edit_flags_global('ran', ran)
            pcr.edit_flags_global('rpr', rpr)
            pcr.edit_flags_global('rgl', rgl)
            pcr.edit_flags_global('rpa', -1)
            pcr.exclude_patterns(self.graph_toolbar.checked_code[index, :])
            for i in range(len(self.patterns[index])):
                for identifier in pcr.read_ids(): pcr.edit_profile_phase(identifier, 'scalecode', 1.0, i)
   
            vols = [pcr.get_phase_volume(pcr.read_ids()[-1], i) for i in range(len(self.patterns[index]))]
            self.v0 = sum(vols) / len(vols)

            pcr.write_pcr_custom()

            runstep = f"{jobid}___{runstep}"
            if pcr.multipattern:
                self.fpscheduler.append_command_queue(self.fp2k_path, [f'{jobid}.pcr'], runstep, jobpath)
            else:
                self.fpscheduler.append_command_queue(self.fp2k_path,
                                                       [f'{jobid}.pcr', ' '.join([os.path.relpath(patt.filename, jobpath) for patt in self.patterns[index]])], runstep, jobpath) 

        elif runstep == 2:
            r0 = float(self.search_params.ui_searchparams.scanr0_edit.text())
            r1 = float(self.search_params.ui_searchparams.scanr1_edit.text())
            
            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()

            pattern = os.path.split(jobpath)[1]
            index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == pattern)

            sumfile = SumIO(path=jobpath, jobid=jobid)
            vol_frac = sumfile.parse_volume_fractions(pcr, pcr.read_ids())

            if vol_frac is not None and vol_frac:
                for ids in pcr.read_ids():
                    for i in range(len(self.patterns[index])):
                        if vol_frac[i][ids] < r0:
                            pcr.edit_profile_phase(ids, 'scalecode', 0.0, i)
                        
                        if vol_frac[i][ids] > r1:
                            pcr.edit_profile_phase(ids, 'acode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'bcode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'ccode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'alphacode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'betacode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'gammacode', 1.0, i)

                for i in range(len(self.patterns[index])): pcr.edit_shifts_global('zerocode', 1.0, i)
                pcr.write_pcr_custom()

                runstep = f"{jobid}___{runstep}"
                if pcr.multipattern:
                    self.fpscheduler.append_command_queue(self.fp2k_path, [f'{jobid}.pcr'], runstep, jobpath)
                else:
                    self.fpscheduler.append_command_queue(self.fp2k_path,
                                                       [f'{jobid}.pcr', ' '.join([os.path.relpath(patt.filename, jobpath) for patt in self.patterns[index]])], runstep, jobpath) 

            else:
                candidate = jobid.replace("_phase_", "")
                icandidate = self.s1.index(candidate)
                self.remove_from_s1.append(candidate)
                self.lutefom[icandidate] = 0.0
                self.run_fp_search(5, jobpath, jobid)

        elif runstep == 3:
            r2 = float(self.search_params.ui_searchparams.scanr2_edit.text()) 

            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()

            pattern = os.path.split(jobpath)[1]
            index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == pattern)

            sumfile = SumIO(path=jobpath, jobid=jobid)
            vol_frac = sumfile.parse_volume_fractions(pcr, pcr.read_ids())

            if vol_frac is not None and vol_frac:
                for ids in pcr.read_ids():
                    for i in range(len(self.patterns[index])):
                        if vol_frac[i][ids] > r2:
                            pcr.edit_profile_phase(ids, 'ycode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'ucode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'bovcode', 1.0, i)

                pcr.write_pcr_custom()

                runstep = f"{jobid}___{runstep}"
                if pcr.multipattern:
                    self.fpscheduler.append_command_queue(self.fp2k_path, [f'{jobid}.pcr'], runstep, jobpath)
                else:
                    self.fpscheduler.append_command_queue(self.fp2k_path,
                                                       [f'{jobid}.pcr', ' '.join([os.path.relpath(patt.filename, jobpath) for patt in self.patterns[index]])], runstep, jobpath) 
            else:
                candidate = jobid.replace("_phase_", "")
                icandidate = self.s1.index(candidate)
                self.remove_from_s1.append(candidate)
                self.lutefom[icandidate] = 0.0
                self.run_fp_search(5, jobpath, jobid)
                
        elif runstep == 4:
            try:
                a = float(self.search_params.ui_searchparams.foma.text())
                b = float(self.search_params.ui_searchparams.fomb.text())
                c = float(self.search_params.ui_searchparams.fomc.text())
                d = float(self.search_params.ui_searchparams.fomd.text())
                s1thrsh = float(self.search_params.ui_searchparams.s1_edit.text())

                pattern = os.path.split(jobpath)[1]
                index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == pattern)
                pcr = PcrIO(path=jobpath, jobid=jobid)
                pcr.read_custom_pcr()
                ids = pcr.read_ids()
                candidate = jobid.replace("_phase_", "")
                icandidate = self.s1.index(candidate)
                lastindex = ids.index(candidate) + 1

                vols = [pcr.get_phase_volume(candidate, i) for i in range(len(self.patterns[index]))]
                self.vf = sum(vols) / len(vols)

                sumfile = SumIO(path=jobpath, jobid=jobid)
                rwp = sumfile.parse_rwp(pcr)
                vol_frac = sumfile.parse_volume_fractions(pcr, ids)
                micfile = MicIO(path=jobpath, jobid=jobid)

                if pcr.multipattern:
                    sizes = [micfile.read_fromfile(lastindex, ipatt + 1)[1] for ipatt in
                             range(len(self.patterns[index]))]
                    strains = [micfile.read_fromfile(lastindex, ipatt + 1)[2] for ipatt in
                               range(len(self.patterns[index]))]
                else:
                    sizes = [micfile.read_fromfile(lastindex)[1]]
                    strains = [micfile.read_fromfile(lastindex)[2]]

                if rwp is not None and vol_frac is not None:
                    avgrwp = sum([v for v in rwp.values()]) / len(rwp)
                    avgvolfrac = sum([v[candidate] for v in vol_frac.values()]) / len(vol_frac)

                    if all(size is not None for size in sizes):
                        avgsize = sum([s[0] for s in sizes]) / len(sizes)
                    else:
                        avgsize = 99999999.9

                    if all(strain is not None for strain in strains):
                        avgstrain = sum([s[0] for s in strains]) / len(strains)
                    else:
                        avgstrain = 0.0

                    if avgvolfrac < s1thrsh:
                        self.remove_from_s1.append(candidate)

                    self.lutefom[icandidate] = self.lute_fom(a, b, c, d, avgrwp, self.v0, self.vf, avgvolfrac, avgsize, avgstrain)

                else:
                    self.remove_from_s1.append(candidate)
                    self.lutefom[icandidate] = 0.0
                
                self.run_fp_search(5, jobpath, jobid)

            except Exception:
                candidate = jobid.replace("_phase_", "")
                icandidate = self.s1.index(candidate)
                self.remove_from_s1.append(candidate)
                self.lutefom[icandidate] = 0.0
                self.run_fp_search(5, jobpath, jobid)
            
        elif runstep == 5:
            candidate = jobid.replace("_phase_", "")
            icandidate = self.s1.index(candidate)
            if self.maxfom[0] < self.lutefom[icandidate]:
                self.luteres.set_scan_background(qtg.QBrush(qtg.QColor(255, 255, 153)), [self.maxfom[1]])
                self.luteres.set_scan_background(qtg.QBrush(qtg.QColor(0, 204, 0)), [icandidate])
                self.maxfom = [self.lutefom[icandidate], icandidate]
            elif self.s1[icandidate] in self.remove_from_s1:
                self.luteres.set_scan_background(qtg.QBrush(qtg.QColor(255, 51, 51)), [icandidate])
            else:
                self.luteres.set_scan_background(qtg.QBrush(qtg.QColor(255, 255, 153)), [icandidate])
            self.luteres.update_fom_graph(self.lutefom)
        
        elif runstep == 6: # <---- Only enter if all_finished 
            degelder = float(self.search_params.ui_searchparams.degelder_edit.text())
            dupl = True
            while dupl:
                idx = np.argmax(self.lutefom)
                newphase = self.s1[idx]
                newcif = os.path.join(self.cifdir, newphase)

                dupl = False
                for detected in self.s2:
                    detectcif = os.path.join(self.cifdir, detected)
                    r = crysfml08_forpy.simil_pow([newcif, detectcif], [2000.0, 0.5, 60.0])
                    if r[0] > degelder:
                        dupl = True
                        break
                
                if dupl:
                    self.s1.pop(idx)
                    self.lutefom.pop(idx)

                if not dupl:
                    self.s2.append(newphase)
                    self.s1.pop(idx)
                    self.lutefom.pop(idx)

            self.lutefom = [val for i, val in enumerate(self.lutefom) if self.s1[i] not in self.remove_from_s1]
            self.s1 = [val for val in self.s1 if val not in self.remove_from_s1]
            self.clean_candidates(jobpath)
            if len(self.s2) > 1:
                self.luteres.setup_window(
                    self.selection.loc[
                        self.selection['identifier'].isin(self.s1), ['identifier', 'formula']].values.tolist(),
                    self.selection.loc[
                        self.selection['identifier'].isin(self.s2), ['identifier', 'formula']].values.tolist(), False)
                
                self.run_fp_search(7, jobpath, jobid)
                self.fpscheduler.start_all_timers()
                self.fpscheduler.start_all_processes()
            else:
                self.start_search_cycle(jobpath)
                self.fpscheduler.start_all_timers()
                self.fpscheduler.start_all_processes()
                
        elif runstep == 7:
            pattern = os.path.split(jobpath)[1]
            index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == pattern)

            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.prepare_buf(identifiers=self.s2, dirname=self.cifdir)
            pcr.prepare_c2pcr(data=self.patterns[index])

            runstep = f"{jobid}___{runstep}"
            self.fpscheduler.append_command_queue(self.cif2pcr_path, [f"-file", f"{jobid}.c2pcr", f"-f_stop"], runstep, jobpath)

        elif runstep == 8:
            qncy = int(self.search_params.ui_searchparams.quanncy.text())
            qste = int(self.search_params.ui_searchparams.quanste.text())
            qrat = float(self.search_params.ui_searchparams.quanrat.text())
            qran = float(self.search_params.ui_searchparams.quanran.text())
            qrpr = float(self.search_params.ui_searchparams.quanrpr.text())
            qrgl = float(self.search_params.ui_searchparams.quanrgl.text())

            pattern = os.path.split(jobpath)[1]
            index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == pattern)

            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()
            for i in range(len(self.patterns[index])):
                pcr = self.get_custom_pcr(pcr, index=index, ipatt=i)
                if pcr is None:
                    return
                
                pcr.edit_flags_global('ste', qste, i)
                pcr.edit_flags_global('ipr', 2, i)
                pcr.edit_flags_global('ppl', 2, i)

            pcr.edit_flags_global('ncy', qncy)
            pcr.edit_flags_global('rat', qrat)
            pcr.edit_flags_global('ran', qran)
            pcr.edit_flags_global('rpr', qrpr)
            pcr.edit_flags_global('rgl', qrgl)
            pcr.edit_flags_global('rpa', -1)
            pcr.exclude_patterns(self.graph_toolbar.checked_code[index, :])
            for i in range(len(self.patterns[index])):
                for identifier in pcr.read_ids(): pcr.edit_profile_phase(identifier, 'scalecode', 1.0, i)

            vols = [pcr.get_phase_volume(pcr.read_ids()[-1], i) for i in range(len(self.patterns[index]))]
            self.v0 = sum(vols) / len(vols)
            pcr.write_pcr_custom()

            runstep = f"{jobid}___{runstep}"
            if pcr.multipattern:
                self.fpscheduler.append_command_queue(self.fp2k_path, [f'{jobid}.pcr'], runstep, jobpath)
            else:
                self.fpscheduler.append_command_queue(self.fp2k_path,
                                                       [f'{jobid}.pcr', ' '.join([os.path.relpath(patt.filename, jobpath) for patt in self.patterns[index]])], runstep, jobpath) 

        elif runstep == 9:
            qr0 = float(self.search_params.ui_searchparams.quanr0_edit.text())
            qr1 = float(self.search_params.ui_searchparams.quanr1_edit.text())
            
            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()

            pattern = os.path.split(jobpath)[1]
            index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == pattern)

            sumfile = SumIO(path=jobpath, jobid=jobid)
            vol_frac = sumfile.parse_volume_fractions(pcr, pcr.read_ids())

            if vol_frac is not None:
                for ids in pcr.read_ids():
                    for i in range(len(self.patterns[index])):
                        if vol_frac[i][ids] < qr0:
                            pcr.edit_profile_phase(ids, 'scalecode', 0.0, i)
                        
                        if vol_frac[i][ids] > qr1:
                            pcr.edit_profile_phase(ids, 'acode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'bcode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'ccode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'alphacode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'betacode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'gammacode', 1.0, i)

                for i in range(len(self.patterns[index])): pcr.edit_shifts_global('zerocode', 1.0, i)
                pcr.write_pcr_custom()

                runstep = f"{jobid}___{runstep}"
                if pcr.multipattern:
                    self.fpscheduler.append_command_queue(self.fp2k_path, [f'{jobid}.pcr'], runstep, jobpath)
                else:
                    self.fpscheduler.append_command_queue(self.fp2k_path,
                                                       [f'{jobid}.pcr', ' '.join([os.path.relpath(patt.filename, jobpath) for patt in self.patterns[index]])], runstep, jobpath) 
        elif runstep == 10:
            qr2 = float(self.search_params.ui_searchparams.quanr2_edit.text()) 

            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()

            pattern = os.path.split(jobpath)[1]
            index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == pattern)

            sumfile = SumIO(path=jobpath, jobid=jobid)
            vol_frac = sumfile.parse_volume_fractions(pcr, pcr.read_ids())

            if vol_frac is not None:
                for ids in pcr.read_ids():
                    for i in range(len(self.patterns[index])):
                        if vol_frac[i][ids] > qr2:
                            pcr.edit_profile_phase(ids, 'ycode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'ucode', 1.0, i)
                            pcr.edit_profile_phase(ids, 'bovcode', 1.0, i)

                pcr.write_pcr_custom()

                runstep = f"{jobid}___{runstep}"
                if pcr.multipattern:
                    self.fpscheduler.append_command_queue(self.fp2k_path, [f'{jobid}.pcr'], runstep, jobpath)
                else:
                    self.fpscheduler.append_command_queue(self.fp2k_path,
                                                       [f'{jobid}.pcr', ' '.join([os.path.relpath(patt.filename, jobpath) for patt in self.patterns[index]])], runstep, jobpath) 
        elif runstep == 11:
            pass

        elif runstep == 12: # <--- Only visited when allfinished
            s0thrsh = float(self.search_params.ui_searchparams.s0_edit.text())
            index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
            phases = self.s2.copy()

            if not self.qerror:
                pcr = PcrIO(path=jobpath, jobid='_quantification_phases')
                pcr.read_custom_pcr()
                sumfile = SumIO(path=jobpath, jobid='_quantification_phases')

                rwp = sumfile.parse_rwp(pcr)
                if rwp is not None:
                    avgrwp = sum([v for v in rwp.values()]) / len(rwp)
                    self.luterwp.append(avgrwp)
                
                vol_frac = sumfile.parse_volume_fractions(pcr, pcr.read_ids())
                if vol_frac is not None:
                    for ids in pcr.read_ids():
                        for i in range(len(self.patterns[index])):
                            if vol_frac[i][ids] < s0thrsh:
                                try:
                                    self.s2.remove(ids)
                                except ValueError:
                                    pass

                else:
                    self.warning = qtw.QMessageBox()
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText(f"Something went wrong in the Quantification Phase,"
                                        f" this is a severe error that may happen if there is not a single phase present"
                                        f" in 'Selected Materials' that fits your experimental pattern")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    self.warning.exec_()
                    return
            
            else:
                self.s2.pop(-1)
                phases.pop(-1)
            
            self.clean_quantification(jobpath)
            self.luteres.update_rwp_graph(self.luterwp)
            if (phases[-1] not in self.s2) or (len(self.s2) >= 7) or (len(self.s1) == 0):
                self.luteres.setup_window(
                    self.selection.loc[
                        self.selection['identifier'].isin(self.s1), ['identifier', 'formula']].values.tolist(),
                    self.selection.loc[
                        self.selection['identifier'].isin(self.s2), ['identifier', 'formula']].values.tolist(), False)
                
                self.qerror = False
                self.run_fp_search(7, jobpath, jobid)
                self.fpscheduler.start_all_timers()
                self.fpscheduler.start_all_processes()
            else:
                self.qerror = False
                self.start_search_cycle(jobpath)
                self.fpscheduler.start_all_timers()
                self.fpscheduler.start_all_processes()
        
        elif runstep == 13:
            try:
                self.luteres.set_detect_background(qtg.QBrush(qtg.QColor(0, 204, 0)), range(len(self.s2)))
                self.cleanup_search_run(jobpath, jobid)    
                self.obtain_results(jobpath, jobid)
                self.enable_actions_buttons()
                self.plot_current_prf()
                self.populate_tableresults()

                self.ui_mainwindow.tabGraphs.setTabVisible(self.indexPrfData, True)
                self.terminal.ui_terminal.kill_button.setEnabled(False)

                index = next(i for i, val in enumerate(self.graph_toolbar.names) if '_and_'.join(val) == jobid)
                if index != self.graph_toolbar.patterns_combo.count() - 1:
                    self.init_fp_search(jobpath, jobid)
            except FullProfOutputReadError:
                self.ui_mainwindow.tabGraphs.setTabVisible(self.indexSummary, False)
                self.ui_mainwindow.tabGraphs.setTabVisible(self.indexPrfData, False)
                self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, False)

    @staticmethod
    def lute_fom(a, b, c, d, rwp, v0, vf, fr, dv, eps):
        dt = 20.0
        emax = 0.02 * np.sqrt(np.pi * 0.5)

        if dv is not None and dv > dt:
            c = 0.0
        elif dv is None:
            c = 0.0
            dv = 1.0

        if eps is not None and eps * 0.0001 < emax:
            d = 0.0
        elif eps is None:
            d = 0.0
            eps = 1.0

        return (1.0 / (rwp + a * v0 * abs(1.0 / vf - 1.0 / v0)) + b * (100.0 - fr)) * (1.0 / (1.0 + c / dv + d * eps))

    def clean_cifs(self, identifiers, jobpath):
        if not self.workdir == jobpath:
            for identifier in identifiers:
                try:
                    os.remove(os.path.join(jobpath, f"{identifier}.cif"))
                except FileNotFoundError:
                    pass

    def clean_candidates(self, jobpath):
        for file in os.listdir(jobpath):
            if os.path.isfile(os.path.join(jobpath, file)) and "_phase_" in file:
                try:
                    os.remove(os.path.join(jobpath, file))
                except FileNotFoundError:
                    pass

    def clean_quantification(self, jobpath):
        for file in os.listdir(jobpath):
            if os.path.isfile(os.path.join(jobpath, file)) and "_quantification_phases" in file:
                try:
                    os.remove(os.path.join(jobpath, file))
                except FileNotFoundError:
                    pass

    def cleanup_search_run(self, jobpath, jobid):

        pcr = PcrIO(path=jobpath)
        pcr.read_custom_pcr()
        ids = pcr.read_ids()

        for item in os.listdir(f"{jobpath}"):
            substr = item.replace(jobid, '')

            if '_' in substr:
                substr = substr.split('_')[0]

            number = ''.join([n for n in substr if n.isdigit()])

            if number and '.c2pcr' not in substr:
                if int(number) > len(ids):
                    os.remove(os.path.join(jobpath, item))

    def build_results_dataframe(self, jobpath: str, jobid: str):
        pcr = PcrIO(path=jobpath, jobid=jobid)
        pcr.read_custom_pcr()
        ids = pcr.read_ids()
        npatt = pcr.get_patt_num()

        sumfile = SumIO(path=jobpath, jobid=jobid)
        vol_frac = sumfile.parse_volume_fractions(pcr, ids)
        r_bragg = sumfile.parse_r_bragg(pcr, ids)
        cell_params = sumfile.parse_cell_params(pcr, ids)

        result_final = dict()
        for i in range(npatt):
            result = dict()
            result['identifier'] = []
            result['spacegroup'] = []
            if self.selection is not None and not self.selection.empty:
                result['formula'] = []

            result['weight_fraction'] = []
            result['r_bragg'] = []
            result['cell_a'] = []
            result['cell_b'] = []
            result['cell_c'] = []
            result['cell_alpha'] = []
            result['cell_beta'] = []
            result['cell_gamma'] = []

            for identifier in ids:
                result['identifier'].append(identifier)
                result['spacegroup'].append(pcr.read_hm_symbol(identifier=identifier))

                if self.selection is not None and not self.selection.empty:
                    formula = self.selection[self.selection['identifier'] == identifier].iloc[0]['formula'] if 'formula' in self.selection else None
                    result['formula'].append(formula)

                result['weight_fraction'].append(vol_frac[i][identifier])
                result['r_bragg'].append(r_bragg[i][identifier])
                result['cell_a'].append(cell_params[i][identifier]['cell_a'])
                result['cell_b'].append(cell_params[i][identifier]['cell_b'])
                result['cell_c'].append(cell_params[i][identifier]['cell_c'])
                result['cell_alpha'].append(cell_params[i][identifier]['cell_alpha'])
                result['cell_beta'].append(cell_params[i][identifier]['cell_beta'])
                result['cell_gamma'].append(cell_params[i][identifier]['cell_gamma'])

            result_final[i] = pd.DataFrame(result)

        return result_final

    def obtain_all_results(self):
        self.result_dict = dict()
        for pattern in os.listdir(self.currentpath):
            if os.path.isdir(os.path.join(self.currentpath, pattern)):
                self.obtain_results(jobpath=os.path.join(self.currentpath, pattern), jobid=pattern)

    def obtain_results(self, jobpath, jobid):
        #try:
        result = self.build_results_dataframe(jobpath=jobpath, jobid=jobid)
        idx = ['_and_'.join(val) for val in self.graph_toolbar.names].index(jobid)
        self.result_dict[idx] = result
        # except (ValueError, IndexError, TypeError, KeyError) as e:
        #     self.warning = qtw.QMessageBox()
        #     self.warning.setIcon(qtw.QMessageBox.Critical)
        #     self.warning.setText(f"Failed at reading .sum file in {jobpath}.\n\n"
        #                          f"Check .sum file, this is usually a consequence of a bad refinement")
        #     self.warning.setStandardButtons(qtw.QMessageBox.Ok)
        #     self.warning.exec_()

        #     raise FullProfOutputReadError

    def save_auto_pcr(self, jobpath: str, jobid: str):
        pcr = PcrIO(jobpath, jobid)
        pcr.read_custom_pcr()
        self.auto_rietveld.pattern_pcrs[jobid] = pcr

    def save_manual_pcr(self, jobpath: str, jobid: str):
        pcr = PcrIO(jobpath, jobid)
        pcr.read_custom_pcr()
        self.manual_params.pattern_pcrs[jobid] = pcr

    def populate_tableresults(self):
        if self.result_dict is not None:
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, True)
            self.ui_mainwindow.tabMaterials.setEnabled(True)

            index = self.graph_toolbar.patterns_combo.currentIndex()
            index_patt = int(self.graph_toolbar.patt_num.text())

            result = self.result_dict.get(index, None)

            if result is not None:
                model = PandasTableModel(result[index_patt])
                self.ui_mainwindow.tableResults.setModel(model)

    def simulate_from_table(self):
        self.rerunsim = False
        self.terminal.append('Starting Simulation of selected entries \n\n')
        self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
        self.init_simulate_selection()

    def init_simulate_selection(self):
        self.terminal.ui_terminal.kill_button.setEnabled(True)
        proxymodel = self.ui_mainwindow.tableQuery.model()
        iproxy = self.ui_mainwindow.tableQuery.selectionModel().selectedRows()
        indexes = [proxymodel.mapToSource(i) for i in iproxy]
        rows = self.summary.iloc[[row.row() for row in indexes]]
        self.error = []

        if rows.shape[0] <= 16:
            simulation_num = 0
            self.ui_mainwindow.tabMaterials.setCurrentIndex(0)

            local = []
            
            for item in os.listdir(os.getcwd()):
                if 'SIMULATE' in item:
                    local.append(item)
                else:
                    pass

            local.sort()
            if len(local) == 0:
                self.currentpath = os.path.join(os.getcwd(), f"SIMULATE{str(simulation_num).zfill(3)}")
                os.mkdir(self.currentpath)
            else:
                last = local[-1]
                simulation_num = int(last.replace('SIMULATE', '')) + 1
                self.currentpath = os.path.join(os.getcwd(), f"SIMULATE{str(simulation_num).zfill(3)}")
                os.mkdir(self.currentpath)

            self.run_num = simulation_num
            index = self.graph_toolbar.patterns_combo.currentIndex()
            index_patt = int(self.graph_toolbar.patt_num.text())
            jobid = self.graph_toolbar.names[index][index_patt]
            jobpath = os.path.join(self.currentpath, jobid)

            os.mkdir(jobpath)

            self.algo = 'SIMULATE'
            self.fpscheduler.set_print_errors(True)
            self.run_simulate_selection(0, jobpath, jobid)
            self.fpscheduler.start_all_processes()
        else:
            self.error = qtw.QErrorMessage()
            self.error.showMessage("ERROR: Choose 16 or less phases for a single simulation (FullProf maximum)")

    def run_simulate_selection(self, runstep, jobpath, jobid):
        if runstep == 0:
            proxymodel = self.ui_mainwindow.tableQuery.model()
            iproxy = self.ui_mainwindow.tableQuery.selectionModel().selectedRows()
            indexes = [proxymodel.mapToSource(i) for i in iproxy]
            rows = self.summary.iloc[[row.row() for row in indexes]]
            index = self.graph_toolbar.patterns_combo.currentIndex()
            index_patt = int(self.graph_toolbar.patt_num.text())
            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.prepare_buf(identifiers=rows['identifier'].tolist(), dirname=self.cifdir)
            pcr.prepare_c2pcr(data=[self.patterns[index][index_patt]], simulation=True)

            self.fpscheduler.append_command_queue(self.cif2pcr_path, [f"-file", f"{jobid}.c2pcr", f"-f_stop"], f'{runstep}', jobpath)

        elif runstep == 1:
            index = self.graph_toolbar.patterns_combo.currentIndex()
            index_patt = int(self.graph_toolbar.patt_num.text())

            pcr = PcrIO(path=jobpath, jobid=jobid)
            pcr.read_custom_pcr()
            pcr = self.get_custom_pcr(pcr, index=index, ipatt=index_patt)
            if pcr is None:
                return

            if self.rerunsim:
                for key, value in self.rerunparams.items():
                    if 'zero' in key:
                        pcr.edit_shifts_global('zero', value)
                    elif 'scale' in key:
                        identifier = key.replace("-scale", "")
                        pcr.edit_profile_phase(identifier, 'scale', value)
                    elif 'params' in key:
                        identifier = key.replace("-params", "")
                        pcr.edit_profile_phase(identifier, 'a', value[0])
                        pcr.edit_profile_phase(identifier, 'b', value[1])
                        pcr.edit_profile_phase(identifier, 'c', value[2])
                        pcr.edit_profile_phase(identifier, 'alpha', value[3])
                        pcr.edit_profile_phase(identifier, 'beta', value[4])
                        pcr.edit_profile_phase(identifier, 'gamma', value[5])

            pcr.edit_flags_global('rpa', -1)
            pcr.edit_flags_global('pcr', 1)
            pcr.edit_flags_global('ipr', 2, index_patt)
            pcr.edit_flags_global('ppl', 2, index_patt)

            pcr.write_pcr_custom()
            self.fpscheduler.append_command_queue(self.fp2k_path, [f'{jobid}.pcr', os.path.relpath(self.patterns[index][index_patt].filename, jobpath)], f'{runstep}', jobpath) 
        else:
            self.plot_current_prf()
            self.plot_simulation(jobpath, jobid)
            self.enable_actions_buttons()
            self.terminal.ui_terminal.kill_button.setEnabled(False)
            self.algo = None
    
    def plot_simulation(self, jobpath, jobid):
        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text())

        proxymodel = self.ui_mainwindow.tableQuery.model()
        iproxy = self.ui_mainwindow.tableQuery.selectionModel().selectedRows()
        indexes = [proxymodel.mapToSource(i) for i in iproxy]
        rows = self.summary.iloc[[row.row() for row in indexes]]

        self.simgraph = SimGraph(jobpath, jobid, self.patterns[index][index_patt], rows,
                                 self.cifdir, self.data, self.rerunparams, self.rerunsim)

        self.simgraph.rerun_params.connect(self.rerun_simulation)
        self.simgraph.save_cif.connect(lambda: self.update_summary())
        self.simgraph.show()
    
    def rerun_simulation(self, params):
        self.rerunsim = True
        self.rerunparams = params
        self.terminal.append('Rerunning Simulation with previous parameters \n\n')
        self.terminal.append(f"Start Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n\n")
        self.simgraph.close()
        self.simgraph = None

        self.init_simulate_selection()

    def run_winplotr(self):
        index = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text()) + 1

        jobid = '_and_'.join(self.graph_toolbar.names[index])
        jobpath = os.path.join(self.currentpath, jobid)

        if len(self.graph_toolbar.names[index]) == 1:
            filename = f"{jobid}.prf"
        else:
            filename = f"{jobid}_{index_patt}.prf"

        self.terminal.append(f'Running WinPLOTR on file {os.path.join(jobpath, filename)} \n\n')
        qtc.QProcess.startDetached(self.winplotr_path, [os.path.join(jobpath, filename)])

    def run_selected_winplotr(self):
        path = qtw.QFileDialog.getOpenFileName(self, 'Open a file', '', 'All Files (*.*)')
        qtc.QProcess.startDetached(self.winplotr_path, [path[0]])

    def _plot_prf(self, path: str, ipatt: int = None, location: str = 'current'):

        self.plot_prf[location].ui_diffgraph.diff_graph.clear()
        self.plot_prf[location].ui_diffgraph.peaks_graph.clear()

        for file in os.listdir(path):
            if ".pcr" in file:
                file = file.replace(".pcr", "")
                file_pcr = file
                custom_pcr = PcrIO(path=path, jobid=file_pcr)
                custom_pcr.read_custom_pcr()
                identifier = custom_pcr.read_ids()

                res_prf = any('.prf' in string for string in os.listdir(path))
                res_sub = any('.sub' in string for string in os.listdir(path))

                if res_prf:
                    prf = PrfIO(path=path, jobid=file_pcr)
                    prf.read_data(ipatt)

                    temp_pattern = prf.patterns
                    if prf.excluded is not None:
                        for region in prf.excluded:
                            mask1 = temp_pattern[:, 0] < region[0]
                            mask2 = temp_pattern[:, 0] < region[1]
                            mask = ~(mask1 ^ mask2)
                            temp_pattern = temp_pattern[mask]

                    self.plot_prf[location].ui_diffgraph.diff_graph.plot(x=temp_pattern[:, 0], y=temp_pattern[:, 1],
                                                                         pen=self.pen1, name='Yobs',
                                                                         symbolPen=self.pen1,
                                                                         symbolSize=4, symbolBrush=(0, 85, 255))
                    self.plot_prf[location].ui_diffgraph.diff_graph.plot(x=temp_pattern[:, 0], y=temp_pattern[:, 2],
                                                                         pen=self.pen6, name='Ycalc')
                    self.plot_prf[location].ui_diffgraph.diff_graph.plot(x=temp_pattern[:, 0], y=temp_pattern[:, 3],
                                                                         pen=self.pen7, name='Yobs-Ycalc')
                    self.plot_prf[location].ui_diffgraph.diff_graph.plot(x=temp_pattern[:, 0], y=temp_pattern[:, 4],
                                                                         pen=self.pen8, name='Ybckgr')

                    """ Plot reflections as lines """
                    temp_values = prf.reflections
                    if prf.excluded is not None:
                        for region in prf.excluded:
                            mask1 = temp_values['x'] < region[0]
                            mask2 = temp_values['x'] < region[1]
                            mask = ~(mask1 ^ mask2)
                            temp_values = temp_values[mask]

                    self.plot_prf[location].set_reflections(temp_values)

                    self.plot_prf[location].ui_diffgraph.peaks_graph.plot(x=temp_values['x'].to_numpy(),
                                                                          y=-1.0 * temp_values['ph'].to_numpy(),
                                                                          pen=None,
                                                                          symbolPen=self.pen8,
                                                                          symbol=symbols.custom_symbol("|"))

                    if res_sub:
                        if 'SIMULATE' in path:
                            fmt = 'free'
                        elif 'FPSEARCH' in path:
                            fmt = 'xys'
                        elif 'MANUAL' in path:
                            fmt = 'xys'
                        elif 'FPAUTO' in path:
                            fmt = 'xys'
                        else:
                            fmt = 'xys'

                        """ Load .sub files (free format) """
                        for i in range(len(identifier)):
                            if ipatt is not None:
                                filename = os.path.join(path, f"{file_pcr}{i + 1}_{ipatt}.sub")
                            else:
                                filename = os.path.join(path, f"{file_pcr}{i + 1}.sub")

                            sims = DiffractionSignal.from_file(filename=filename, fmt=fmt)
                            if prf.excluded is not None:
                                for region in prf.excluded:
                                    mask1 = sims.diffraction[:, 0] < region[0]
                                    mask2 = sims.diffraction[:, 0] < region[1]
                                    mask = ~(mask1 ^ mask2)
                                    sims.diffraction = sims.diffraction[mask]

                            color = tuple(np.random.choice(range(256), size=3))
                            pen = pg.mkPen(color=color, width=1.5)

                            self.plot_prf[location].ui_diffgraph.diff_graph.plot(x=sims.diffraction[:, 0] + prf.zerovalue,
                                                                                 y=sims.diffraction[:, 1],
                                                                                 name=identifier[i],
                                                                                 pen=pen)

    def plot_current_results(self):
        list_pcrs = dict()
        names = [os.path.join(self.currentpath, directory) for directory in os.listdir(self.currentpath) if os.path.isdir(os.path.join(self.currentpath, directory))]
        for path in names:
            for file in os.listdir(path):
                if ".pcr" in file:
                    file = file.replace(".pcr", "")
                    pcr = PcrIO(path, file)
                    pcr.read_custom_pcr()
                    list_pcrs[os.path.split(path)[1]] = pcr

        self.resultgraph['current'].initialize_tree_dict(pcr_dict=list_pcrs)
        self.resultgraph['current'].fill_results_database(path=names)
        self.resultgraph['current'].plot_checked_results()
        self.resultgraph['current'].show()

        if not self.ui_mainwindow.tabGraphs.isTabVisible(self.indexSummary):
            self.ui_mainwindow.tabGraphs.setTabVisible(self.indexSummary, True)
            self.ui_mainwindow.tabGraphs.setCurrentIndex(self.indexSummary)

    def plot_selected_results(self):
        listfolders = SystemDialog()
        res = listfolders.exec_()

        list_pcrs = dict()
        if res == qtw.QDialog.Accepted:
            names = listfolders.get_output()
            for path in names:
                for file in os.listdir(path):
                    if ".pcr" in file:
                        file = file.replace(".pcr", "")
                        pcr = PcrIO(path, file)
                        pcr.read_custom_pcr()
                        list_pcrs[os.path.split(path)[1]] = pcr

            self.resultgraph['popup'].initialize_tree_dict(pcr_dict=list_pcrs)
            self.resultgraph['popup'].fill_results_database(path=names)
            self.resultgraph['popup'].show()

    def plot_selected_prf(self):
        listfolders = SystemDialog()
        res = listfolders.exec_()

        if res == qtw.QDialog.Accepted:
            names = listfolders.get_output()
            self.plot_prf['popup'].combo_paths.blockSignals(True)
            self.plot_prf['popup'].combo_paths.clear()
            self.plot_prf['popup'].combo_paths.addItems(names)
            self.plot_prf['popup'].lineedit.setText('0')
            self.plot_prf['popup'].combo_paths.blockSignals(False)
            path = self.plot_prf['popup'].combo_paths.currentText()
            self.get_npatt(path)

            if self.plot_prf['popup'].npatt == 1:
                self._plot_prf(path=path, location='popup')
            else:
                ipatt = int(self.plot_prf['popup'].lineedit.text()) + 1
                self._plot_prf(path=path, ipatt=ipatt, location='popup')

            self.plot_prf['popup'].show()

    def get_npatt(self, path: str):
        for file in os.listdir(path):
            if ".pcr" in file:
                file = file.replace(".pcr", "")
                file_pcr = file
                custom_pcr = PcrIO(path=path, jobid=file_pcr)
                custom_pcr.read_custom_pcr()
                npatt = custom_pcr.get_patt_num()
                self.plot_prf['popup'].npatt = npatt
                self.plot_prf['popup'].enable_buttons()

                ipatt = int(self.plot_prf['popup'].lineedit.text())
                if ipatt + 1 > self.plot_prf['popup'].npatt:
                    self.plot_prf['popup'].lineedit.setText(str(self.plot_prf['popup'].npatt - 1))

                break

    def plot_selected_signal(self):
        path = self.plot_prf['popup'].combo_paths.currentText()
        if os.path.isdir(path):
            self.get_npatt(path)

            if self.plot_prf['popup'].npatt == 1:
                self._plot_prf(path=path, location='popup')
            else:
                ipatt = int(self.plot_prf['popup'].lineedit.text()) + 1
                self._plot_prf(path=path, ipatt=ipatt, location='popup')

    def plot_current_prf(self):
        idx = self.graph_toolbar.patterns_combo.currentIndex()
        index_patt = int(self.graph_toolbar.patt_num.text()) + 1
        jobid = '_and_'.join(self.graph_toolbar.names[idx])
    
        self.ui_mainwindow.tabGraphs.setTabVisible(self.indexPrfData, True)

        if os.path.isdir(os.path.join(self.currentpath, jobid)):
            if len(self.graph_toolbar.names[idx]) == 1:
                self._plot_prf(os.path.join(self.currentpath, jobid))
            else:
                self._plot_prf(os.path.join(self.currentpath, jobid), index_patt)

    def update_plot_prf(self, index):
        jobid = '_and_'.join(self.graph_toolbar.names[index])
        index_patt = int(self.graph_toolbar.patt_num.text()) + 1

        if self.currentpath is not None:
            if os.path.isdir(os.path.join(self.currentpath, jobid)):
                if len(self.graph_toolbar.names[index]) == 1:
                    self._plot_prf(os.path.join(self.currentpath, jobid))
                else:
                    self._plot_prf(os.path.join(self.currentpath, jobid), index_patt)

    def started_on_slot(self, slot, runstep, jobpath):
        if self.algo == 'SEARCH':
            runstep = int(runstep.split('___')[1])
            self.terminal.append(f"RUNNING  ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
        else:
            self.terminal.append(f"RUNNING  ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")

    def finished_on_slot(self, slot, runstep, jobpath, status):  
        if self.algo == 'AUTO' or self.algo == 'SEQ':
            if status == qtc.QProcess.CrashExit:
                self.terminal.append(f"FP ERROR ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
                self.error.append(jobpath)
                return
            
            self.terminal.append(f"FINISHED ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
            jobid = next(file.replace('.pcr', '') for file in os.listdir(jobpath) if '.pcr' in file)
            self.run_auto_rietveld(int(runstep) + 1, jobpath, jobid)

        elif self.algo == 'MANUAL':
            if status == qtc.QProcess.CrashExit:
                self.terminal.append(f"FP ERROR ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
                self.error.append(jobpath)
                return
        
            self.terminal.append(f"FINISHED ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
            jobid = next(file.replace('.pcr', '') for file in os.listdir(jobpath) if '.pcr' in file)
            self.run_manual_rietveld(int(runstep) + 1, jobpath, jobid)

        elif self.algo == 'GENERATE_AUTO' or self.algo == 'GENERATE_MANUAL':
            if status == qtc.QProcess.CrashExit:
                self.terminal.append(f"FP ERROR ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
                self.error.append(jobpath)
                return

            self.terminal.append(f"FINISHED ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
            jobid = next(file.replace('.pcr', '') for file in os.listdir(jobpath) if '.pcr' in file)
            self.generate_pcr(int(runstep) + 1, jobpath, jobid)

        elif self.algo == 'SEARCH':
            jobid = runstep.split('___')[0]
            runstep = int(runstep.split('___')[1])
            if status == qtc.QProcess.CrashExit:
                self.terminal.append(f"FP ERROR ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}    File: {jobid}\n")
                if 7 <= runstep <= 10:
                    self.qerror = True
                    self.run_fp_search(11, jobpath, jobid)
                
                elif 0 <= runstep <= 3:
                    candidate = jobid.replace("_phase_", "")
                    icandidate = self.s1.index(candidate)
                    self.remove_from_s1.append(candidate)
                    self.lutefom[icandidate] = 0.0
                    self.run_fp_search(5, jobpath, jobid)
                
                return

            self.terminal.append(f"FINISHED ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
            self.run_fp_search(runstep + 1, jobpath, jobid)  

        elif self.algo == 'SIMULATE':
            if status == qtc.QProcess.CrashExit:
                self.terminal.append(f"FP ERROR ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
                self.error.append(jobpath)
                return

            self.terminal.append(f"FINISHED ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
            jobid = next(file.replace('.pcr', '') for file in os.listdir(jobpath) if '.pcr' in file)
            self.run_simulate_selection(int(runstep) + 1, jobpath, jobid)

    def error_on_slot(self, slot, runstep, jobpath):
        self.terminal.append(f"PROC ERR ----> Process# {slot}:    Step#: {runstep}    Directory: {jobpath}\n")
            
    def all_finished_slots(self):
        if self.algo != 'SEARCH':
            self.terminal.append(f"\n------------------------- RUN FINISHED -------------------------\n")
            self.terminal.append(f"\n")
            if self.error:
                msg = '\n'.join(['   -' + error for error in self.error])
                self.warning = qtw.QMessageBox()
                self.warning.setIcon(qtw.QMessageBox.Critical)
                self.warning.setText(f"The following jobs failed:\n"
                                    f"{msg}\n\n Read the generated ERROR.txt files")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                self.warning.exec_()
                for error in self.error:
                    self.terminal.append(f"ERROR    ----> Directory: {error}\n")
            self.terminal.append(f"\nEnd Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n")
            self.terminal.prompt()
        else:
            jobfolders = [folder for folder in os.listdir(self.currentpath) if os.path.isdir(os.path.join(self.currentpath, folder))]
            jobid = next('_and_'.join(self.graph_toolbar.names[i]) for i in reversed(self.graph_toolbar.checked_index) if '_and_'.join(self.graph_toolbar.names[i]) in jobfolders)
            jobpath = os.path.join(self.currentpath, jobid)

            if any('_phase_' in file for file in os.listdir(jobpath)):
                self.terminal.append(f"\n------------------------- SCAN CYCLE FINISHED -------------------------\n")
                self.terminal.append(f"\n")
                self.run_fp_search(6, jobpath, '_quantification_phases')
            elif any('_quantification_phases' in file for file in os.listdir(jobpath)):
                self.terminal.append(f"\n-------------------- QUANTIFICATION CYCLE FINISHED --------------------\n")
                self.terminal.append(f"\n")
                self.run_fp_search(12, jobpath, jobid)
            else:
                self.terminal.append(f"\n----------------------------- RUN FINISHED ----------------------------\n")
                self.terminal.append(f"\n")
                self.fpscheduler.stop_all_timers()
                self.run_fp_search(13, jobpath, jobid)
                self.terminal.append(f"\nEnd Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n")
                self.terminal.prompt()

        if self.algo == 'AUTO' or self.algo == 'SEQ':
            self.algo = None
            self.populate_tableresults()
            self.plot_current_prf()
            self.enable_actions_buttons()
            self.resultgraph['current'].sort_results(['_and_'.join(val) for val in self.graph_toolbar.names])
            self.ui_mainwindow.tabMaterials.setCurrentIndex(self.indexResults)
            self.ui_mainwindow.tabGraphs.setCurrentIndex(self.indexPrfData)

        elif self.algo == 'MANUAL':
            self.algo = None
            self.manual_params.change_pcr_view()
            self.populate_tableresults()
            self.plot_current_prf()
            self.enable_actions_buttons()
            self.resultgraph['current'].sort_results(['_and_'.join(val) for val in self.graph_toolbar.names])
            self.ui_mainwindow.tabMaterials.setCurrentIndex(self.indexResults)
            self.ui_mainwindow.tabGraphs.setCurrentIndex(self.indexPrfData)

        elif self.algo == 'GENERATE_AUTO':
            self.algo = None
            self.enable_actions_buttons()
            self.auto_rietveld.enable_run_button()

        elif self.algo == 'GENERATE_MANUAL':
            self.algo = None
            self.enable_actions_buttons()
            self.manual_params.change_pcr_view()
            self.manual_params.enable_run_button()

    def kill_processes(self):
        self.algo = None
        self.fpscheduler.kill_all_processes()
        self.terminal.ui_terminal.kill_button.setEnabled(False)

    def save_project(self):
        if self.fileini is None:
            self.save_project_as()
        else:
            self.record_settings()

    def save_project_as(self):
        self.fileini = qtw.QFileDialog.getSaveFileName(self, 'Save FPAPP file', '', "FPAPP files (*.fpapp)")[0]
        if self.fileini:
            self.record_settings()

    def load_project(self):
        self.fileini = qtw.QFileDialog.getOpenFileName(self, 'Open FPAPP File', self.workdir, 'FPAPP files (*.fpapp)')[0]
        if self.fileini:
            self.read_settings()

    def record_settings(self):
        head, tail = os.path.split(self.fileini)
        qtc.QSettings.setPath(qtc.QSettings.IniFormat, qtc.QSettings.UserScope, head)
        self.settings = qtc.QSettings(self.fileini, qtc.QSettings.IniFormat)
        self.settings.setValue('mainwindow/workdir', self.workdir)

        if self.patterns is not None:
            data = []
            rows = len(self.patterns)
            cols = max([len(i) for i in self.patterns])
            for icol in range(cols):
                patterns, irfs, fmts, ress, wavelengths, radiations, geometrys, option_ilos = ([] for _ in range(8))
                for irow in range(rows):
                    patterns.append(self.patterns[irow][icol].filename)
                    irfs.append(self.patterns[irow][icol].irf)
                    fmts.append(self.patterns[irow][icol].fmt)
                    ress.append(self.patterns[irow][icol].irf_res)
                    wavelengths.append(self.patterns[irow][icol].wavelength)
                    radiations.append(self.patterns[irow][icol].radiation)
                    geometrys.append(self.patterns[irow][icol].geometry)
                    option_ilos.append(self.patterns[irow][icol].option_ilo)
                
                data.append({'patterns': patterns, 'irf': irfs, 'fmt': fmts, 'res': ress, 'wavelength': wavelengths, 'radiation': radiations, 'geometry': geometrys, 'option_ilo': option_ilos})
        else:
            data = None

        self.settings.setValue('toolbar/_output', data)
        self.settings.setValue('toolbar/checks', self.graph_toolbar.checked_code)

        self.settings.setValue('toolbar/npatt', self.graph_toolbar.patt_num.text())
        self.settings.setValue('toolbar/pattern', self.graph_toolbar.patterns_combo.currentIndex())

        if self.patterns is not None:
            self.settings.setValue('signals/anchors', [[data.anchor_points for data in patt] for patt in self.patterns])
            self.settings.setValue('signals/peaks', [[data.peaks for data in patt] for patt in self.patterns])

        self.settings.setValue('mainwindow/querymaterials', self.cifdir)
        self.settings.setValue('mainwindow/queryfilter', self.ui_mainwindow.search_filter.text())

        self.settings.setValue('mainwindow/selectedmaterials', self.selection)

        self.settings.setValue('fpsearch/backgroundmode', self.search_params.ui_searchparams.background_combo.currentIndex())
        self.settings.setValue('fpsearch/cheborder', self.search_params.cheb_order.text())
        self.settings.setValue('fpsearch/windowfouri', self.search_params.window_fourier.text())
        self.settings.setValue('fpsearch/cyclefouri', self.search_params.cycles_fourier.text())
        self.settings.setValue('fpsearch/scanr0', self.search_params.ui_searchparams.scanr0_edit.text())
        self.settings.setValue('fpsearch/scanr1', self.search_params.ui_searchparams.scanr1_edit.text())
        self.settings.setValue('fpsearch/scanr2', self.search_params.ui_searchparams.scanr2_edit.text())
        self.settings.setValue('fpsearch/scanncy', self.search_params.ui_searchparams.scanncy_edit.text())
        self.settings.setValue('fpsearch/scanste', self.search_params.ui_searchparams.scanste_edit.text())
        self.settings.setValue('fpsearch/scanrat', self.search_params.ui_searchparams.scanrat.text())
        self.settings.setValue('fpsearch/scanran', self.search_params.ui_searchparams.scanran.text())
        self.settings.setValue('fpsearch/scanrpr', self.search_params.ui_searchparams.scanrpr.text())
        self.settings.setValue('fpsearch/scanrgl', self.search_params.ui_searchparams.scanrgl.text())
        self.settings.setValue('fpsearch/foma', self.search_params.ui_searchparams.foma.text())
        self.settings.setValue('fpsearch/fomb', self.search_params.ui_searchparams.fomb.text())
        self.settings.setValue('fpsearch/fomc', self.search_params.ui_searchparams.fomc.text())
        self.settings.setValue('fpsearch/fomd', self.search_params.ui_searchparams.fomd.text())
        self.settings.setValue('fpsearch/quanr0', self.search_params.ui_searchparams.quanr0_edit.text())
        self.settings.setValue('fpsearch/quanr1', self.search_params.ui_searchparams.quanr1_edit.text())
        self.settings.setValue('fpsearch/quanr2', self.search_params.ui_searchparams.quanr2_edit.text())
        self.settings.setValue('fpsearch/quanncy', self.search_params.ui_searchparams.quanncy.text())
        self.settings.setValue('fpsearch/quanste', self.search_params.ui_searchparams.quanste.text())
        self.settings.setValue('fpsearch/quanrat', self.search_params.ui_searchparams.quanrat.text())
        self.settings.setValue('fpsearch/quanran', self.search_params.ui_searchparams.quanran.text())
        self.settings.setValue('fpsearch/quanrpr', self.search_params.ui_searchparams.quanrpr.text())
        self.settings.setValue('fpsearch/quanrgl', self.search_params.ui_searchparams.quanrgl.text())
        self.settings.setValue('fpsearch/s0', self.search_params.ui_searchparams.s0_edit.text())
        self.settings.setValue('fpsearch/s1', self.search_params.ui_searchparams.s1_edit.text())
        self.settings.setValue('fpsearch/degelder', self.search_params.ui_searchparams.degelder_edit.text())
        self.settings.setValue('fpsearch/excluded', self.search_params.excludedtab.regions)
        self.settings.setValue('fpsearch/npatt', self.search_params.excludedtab.ui_excludetab.patt_num.text())

        self.settings.setValue('fpmanual/pcrpatt', self.manual_params.pattern_pcrs)
        self.settings.setValue('fpmanual/currentcombo', self.manual_params.ui_manualparams.pattern_combo.currentIndex())

        self.settings.setValue('fpauto/pattphase', self.auto_rietveld.pattern_phase)
        self.settings.setValue('fpauto/pcrpatt', self.auto_rietveld.pattern_pcrs)
        self.settings.setValue('fpauto/currentcombo', self.auto_rietveld.ui_autorietveld.pattern_combo.currentIndex())
        self.settings.setValue('fpauto/backgroundmode', self.auto_rietveld.ui_autorietveld.background_combo.currentIndex())
        self.settings.setValue('fpauto/cheborder', self.auto_rietveld.cheb_order.text())
        self.settings.setValue('fpauto/windowfouri', self.auto_rietveld.window_fourier.text())
        self.settings.setValue('fpauto/cyclefouri', self.auto_rietveld.cycles_fourier.text())
        self.settings.setValue('fpauto/ncy', self.auto_rietveld.ui_autorietveld.line_ncy.text())
        self.settings.setValue('fpauto/ste', self.auto_rietveld.ui_autorietveld.line_ste.text())
        self.settings.setValue('fpauto/rat', self.auto_rietveld.ui_autorietveld.rat.text())
        self.settings.setValue('fpauto/ran', self.auto_rietveld.ui_autorietveld.ran.text())
        self.settings.setValue('fpauto/rpr', self.auto_rietveld.ui_autorietveld.rpr.text())
        self.settings.setValue('fpauto/rgl', self.auto_rietveld.ui_autorietveld.rgl.text())
        self.settings.setValue('fpauto/excluded', self.auto_rietveld.excludedtab.regions)
        self.settings.setValue('fpauto/sequential', self.auto_rietveld.ui_autorietveld.sequential_check.checkState())
        self.settings.setValue('fpauto/npatt', self.auto_rietveld.excludedtab.ui_excludetab.patt_num.text())
        self.settings.setValue('fpauto/fixspc', self.auto_rietveld.ui_autorietveld.line_elements.text())
        self.settings.setValue('fpauto/paramtree', self.auto_rietveld.get_dict_all())
        self.settings.setValue('fpauto/workflow', self.auto_rietveld.workflow_commands)
        self.settings.setValue('fpauto/restraintactions', [item.text() for item in self.auto_rietveld.restraint_actions])
        self.settings.setValue('fpauto/restraintfields', self.auto_rietveld.restraint_fields)
        self.settings.setValue('fpauto/restraintcodes', self.auto_rietveld.restraint_codes)
        self.settings.setValue('fpauto/constraintactions', [item.text() for item in self.auto_rietveld.constraint_actions])
        self.settings.setValue('fpauto/constraintfields', self.auto_rietveld.constraint_fields)
        self.settings.setValue('fpauto/constraintcodes', self.auto_rietveld.constraint_codes)
        self.settings.setValue('fpauto/limitactions', [item.text() for item in self.auto_rietveld.limit_actions])
        self.settings.setValue('fpauto/limitfields', self.auto_rietveld.limit_fields)
        self.settings.setValue('fpauto/limitcodes', self.auto_rietveld.limit_codes)
        self.settings.setValue('fpauto/lebailconst', {k: v.isChecked() for k, v in self.auto_rietveld.lebail_const.items()})
        self.settings.setValue('fpauto/lebailmode', {k: v.isChecked() for k, v in self.auto_rietveld.lebail_mode.items()})

        self.settings.setValue('mainwindow/currentpath', self.currentpath)
        self.settings.setValue('mainwindow/runnum', self.run_num)
        self.settings.setValue('mainwindow/prftab', self.ui_mainwindow.tabGraphs.isTabVisible(self.indexPrfData))
        self.settings.setValue('mainwindow/summarytab', self.ui_mainwindow.tabGraphs.isTabVisible(self.indexSummary))
        self.settings.setValue('mainwindow/resulttab', self.ui_mainwindow.tabMaterials.isTabVisible(self.indexResults))
        self.settings.setValue('mainwindow/resulttree', self.resultgraph['current'].get_dict_all())
        self.settings.setValue('mainwindow/materfocus', self.ui_mainwindow.tabMaterials.currentIndex())
        self.settings.setValue('mainwindow/graphfocus', self.ui_mainwindow.tabGraphs.currentIndex())

        self.ui_mainwindow.statusbar.showMessage(f"Project saved in {self.fileini}")

    def read_settings(self):
        self.settings = qtc.QSettings(self.fileini, qtc.QSettings.IniFormat)

        oldpath = os.path.normpath(self.settings.value('mainwindow/workdir', type=str))
        if oldpath is not None:
            if os.path.exists(oldpath):
                self.set_filesystem(oldpath)
            else:
                pathname = os.path.split(self.fileini)[0]
                self.set_filesystem(pathname)
        currentpath = os.getcwd()

        pattern_data = self.settings.value('toolbar/_output')
        if pattern_data is not None:
            """ Check if files exist in original location """
            if all(os.path.exists(path) for patt in pattern_data for path in patt['patterns']) and\
                    all(os.path.exists(path) for patt in pattern_data for path in patt['irf']):
                self.set_patterns(pattern_data)
            else:
                """ If not check if files exist in .fpapp file location """
                if oldpath != currentpath:
                    tmp = pattern_data
                    for i, patt in enumerate(tmp):
                        for j, ospath in enumerate(zip(patt['patterns'], patt['irf'])):
                            newpatt = os.path.join(currentpath, os.path.relpath(ospath[0], oldpath))
                            newirf = os.path.join(currentpath, os.path.relpath(ospath[1], oldpath))

                            if os.path.exists(newpatt) and os.path.exists(newirf):
                                pattern_data[i]['patterns'][j] = newpatt
                                pattern_data[i]['irf'][j] = newirf
                            else:
                                self.warning = qtw.QMessageBox()
                                self.warning.setIcon(qtw.QMessageBox.Critical)
                                self.warning.setText(
                                    f"Error reading pattern or IRF files, files expected in:\n\n"
                                    f"Patterns: {ospath[0]}\n"
                                    f"IRFs: {ospath[1]}\n\n"
                                    f"Not found, changing search to .fpapp file location:\n\n"
                                    f"Patterns: {newpatt}\n"
                                    f"IRFs: {newirf}\n\n"
                                    f"Could not find the targeted files in new location, ABORTING settings load.")
                                self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                                self.warning.exec_()
                                return
                    self.set_patterns(pattern_data)
                else:
                    """ Workdir is same but files can't be found """
                    patterns = [path for patt in pattern_data for path in patt['patterns']]
                    irfs = [path for patt in pattern_data for path in patt['irf']]
                    folders = list(set(os.path.split(path)[0] + '\n' for path in patterns))
                    foldirf = list(set(path + '\n' for path in irfs))
                    str_folder = '\n'.join(['      - ' + folder for folder in folders])
                    str_irf = '\n'.join(['      - ' + folder for folder in foldirf])
                    self.warning = qtw.QMessageBox()
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText(f"Error reading pattern OR .irf files, I was expecting the following locations:\n\n"
                                         f"{str_folder}\n"
                                         f"{str_irf}")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    self.warning.exec_()
                    return

        checkmat = self.settings.value('toolbar/checks')
        if checkmat is not None:
            self.graph_toolbar.checktable.check_indexes(np.argwhere(checkmat == 1)[:, 0], np.argwhere(checkmat == 1)[:, 1])

        ipatt = self.settings.value('toolbar/npatt')
        index = self.settings.value('toolbar/pattern')
        if ipatt and index:
            self.graph_toolbar.update_combo(int(index), int(ipatt))

        anchors = self.settings.value('signals/anchors')
        if anchors is not None and self.patterns is not None:
            for i, anchor_row in enumerate(anchors):
                for j, anchor in enumerate(anchor_row):
                    self.patterns[i][j].anchor_points = anchor
                    self.patterns[i][j].get_background_manual()
            self.update_patterns_display()
            self.update_manual_background_plot(int(index), int(ipatt))

        peaks = self.settings.value('signals/peaks')
        if peaks is not None and self.patterns is not None:
            for i, peak_row in enumerate(peaks):
                for j, peak in enumerate(peak_row):
                    self.patterns[i][j].peaks = peak
            self.update_patterns_display()
            self.update_manual_peaks_plot(int(index), int(ipatt))

        self.cifdir = self.settings.value('mainwindow/querymaterials')
        if self.cifdir is not None:
            if os.path.exists(self.cifdir):
                self.update_summary()
            else:
                if oldpath != currentpath:
                    newcif = os.path.join(currentpath, os.path.relpath(self.cifdir, oldpath))
                    if os.path.exists(newcif):
                        self.cifdir = newcif
                        self.update_summary()
                    else:
                        self.warning = qtw.QMessageBox()
                        self.warning.setIcon(qtw.QMessageBox.Critical)
                        self.warning.setText(
                            f"Error reading folder containing CIF files, I was expecting:\n  {self.cifdir}\n\n"
                            f"Not found, changing search to .fpapp file location:\n  {newcif}\n\n"
                            f"Could not find the targeted files in new location, ABORTING settings load.")
                        self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                        self.warning.exec_()
                        return
                else:
                    self.warning = qtw.QMessageBox()
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText(f"Error reading folder containing CIF files, I was expecting:\n  {self.cifdir}")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    self.warning.exec_()
                    return

        self.ui_mainwindow.search_filter.setText(self.settings.value('mainwindow/queryfilter'))

        self.selection = self.settings.value('mainwindow/selectedmaterials')
        if self.selection is not None:
            self.populate_tableselection()
            self.enable_actions_buttons()

        self.settings.setValue('fpsearch/backgroundmode', self.search_params.ui_searchparams.background_combo.currentIndex())
        self.settings.setValue('fpsearch/cheborder', self.search_params.cheb_order.text())
        self.settings.setValue('fpsearch/windowfouri', self.search_params.window_fourier.text())
        self.settings.setValue('fpsearch/cyclefouri', self.search_params.cycles_fourier.text())

        lutebackground = self.settings.value('fpsearch/backgroundmode')
        self.search_params.ui_searchparams.background_combo.setCurrentIndex(int(lutebackground))
        luteorder = self.settings.value('fpsearch/cheborder')
        self.search_params.cheb_order.setText(luteorder)
        lutewindow = self.settings.value('fpsearch/windowfouri')
        self.search_params.window_fourier.setText(lutewindow)
        lutecycle = self.settings.value('fpsearch/cyclefouri')
        self.search_params.cycles_fourier.setText(lutecycle)
        scanr0 = self.settings.value('fpsearch/scanr0')
        self.search_params.ui_searchparams.scanr0_edit.setText(scanr0)
        scanr1 = self.settings.value('fpsearch/scanr1')
        self.search_params.ui_searchparams.scanr1_edit.setText(scanr1)
        scanr2 = self.settings.value('fpsearch/scanr2')
        self.search_params.ui_searchparams.scanr2_edit.setText(scanr2)
        scanncy = self.settings.value('fpsearch/scanncy')
        self.search_params.ui_searchparams.scanncy_edit.setText(scanncy)
        scanste = self.settings.value('fpsearch/scanste')
        self.search_params.ui_searchparams.scanste_edit.setText(scanste)
        scanrat = self.settings.value('fpsearch/scanrat')
        self.search_params.ui_searchparams.scanrat.setText(scanrat)
        scanran = self.settings.value('fpsearch/scanran')
        self.search_params.ui_searchparams.scanran.setText(scanran)
        scanrpr = self.settings.value('fpsearch/scanrpr')
        self.search_params.ui_searchparams.scanrpr.setText(scanrpr)
        scanrgl = self.settings.value('fpsearch/scanrgl')
        self.search_params.ui_searchparams.scanrgl.setText(scanrgl)
        foma = self.settings.value('fpsearch/foma')
        self.search_params.ui_searchparams.foma.setText(foma)
        fomb = self.settings.value('fpsearch/fomb')
        self.search_params.ui_searchparams.fomb.setText(fomb)
        fomc = self.settings.value('fpsearch/fomc')
        self.search_params.ui_searchparams.fomc.setText(fomc)
        fomd = self.settings.value('fpsearch/fomd')
        self.search_params.ui_searchparams.fomd.setText(fomd)
        quanr0 = self.settings.value('fpsearch/quanr0')
        self.search_params.ui_searchparams.quanr0_edit.setText(quanr0)
        quanr1 = self.settings.value('fpsearch/quanr1')
        self.search_params.ui_searchparams.quanr1_edit.setText(quanr1)
        quanr2 = self.settings.value('fpsearch/quanr2')
        self.search_params.ui_searchparams.quanr2_edit.setText(quanr2)
        quanncy = self.settings.value('fpsearch/quanncy')
        self.search_params.ui_searchparams.quanncy.setText(quanncy)
        quanste = self.settings.value('fpsearch/quanste')
        self.search_params.ui_searchparams.quanste.setText(quanste)
        quanrat = self.settings.value('fpsearch/quanrat')
        self.search_params.ui_searchparams.quanrat.setText(quanrat)
        quanran = self.settings.value('fpsearch/quanran')
        self.search_params.ui_searchparams.quanran.setText(quanran)
        quanrpr = self.settings.value('fpsearch/quanrpr')
        self.search_params.ui_searchparams.quanrpr.setText(quanrpr)
        quanrgl = self.settings.value('fpsearch/quanrgl')
        self.search_params.ui_searchparams.quanrgl.setText(quanrgl)
        s0 = self.settings.value('fpsearch/s0')
        self.search_params.ui_searchparams.s0_edit.setText(s0)
        s1 = self.settings.value('fpsearch/s1')
        self.search_params.ui_searchparams.s1_edit.setText(s1)
        degelder = self.settings.value('fpsearch/degelder')
        self.search_params.ui_searchparams.degelder_edit.setText(degelder)

        regions = self.settings.value('fpsearch/excluded')
        self.search_params.excludedtab.ui_excludetab.ranges_view.clear()
        self.search_params.excludedtab.load_regions(regions)
        ipatt = self.settings.value('fpsearch/npatt')
        if ipatt:
            self.search_params.excludedtab.ui_excludetab.patt_num.setText(ipatt)
            self.search_params.excludedtab.enable_buttons()

        self.manual_params.reset_all()
        pcrpatt = self.settings.value('fpmanual/pcrpatt')
        idxcombo = self.settings.value('fpmanual/currentcombo')
        if pcrpatt is None:
            pcrpatt = {}
        self.manual_params.pattern_pcrs = pcrpatt
        self.manual_params.ui_manualparams.pattern_combo.setCurrentIndex(int(idxcombo))
        self.manual_params.change_pcr_view()
        ipatt = self.settings.value('toolbar/npatt')
        if ipatt and index:
            self.enable_actions_buttons()
            self.manual_params.enable_run_button()

        self.auto_rietveld.reset_all()
        pattphase = self.settings.value('fpauto/pattphase')
        if pattphase is None:
            pattphase = {}
        self.auto_rietveld.pattern_phase = pattphase
        pcrpatt = self.settings.value('fpauto/pcrpatt')
        if pcrpatt is None:
            pcrpatt = {}
        self.auto_rietveld.pattern_pcrs = pcrpatt
        idxcombo = self.settings.value('fpauto/currentcombo')
        self.auto_rietveld.ui_autorietveld.pattern_combo.setCurrentIndex(int(idxcombo))
        autobackground = self.settings.value('fpauto/backgroundmode')
        self.auto_rietveld.ui_autorietveld.background_combo.setCurrentIndex(int(autobackground))
        autoorder = self.settings.value('fpauto/cheborder')
        self.auto_rietveld.cheb_order.setText(autoorder)
        autowindow = self.settings.value('fpauto/windowfouri')
        self.auto_rietveld.window_fourier.setText(autowindow)
        autocycle = self.settings.value('fpauto/cyclefouri')
        self.auto_rietveld.cycles_fourier.setText(autocycle)
        ncy = self.settings.value('fpauto/ncy')
        self.auto_rietveld.ui_autorietveld.line_ncy.setText(ncy)
        ste = self.settings.value('fpauto/ste')
        self.auto_rietveld.ui_autorietveld.line_ste.setText(ste)
        rat = self.settings.value('fpauto/rat')
        self.auto_rietveld.ui_autorietveld.rat.setText(rat)
        ran = self.settings.value('fpauto/ran')
        self.auto_rietveld.ui_autorietveld.ran.setText(ran)
        rpr = self.settings.value('fpauto/rpr')
        self.auto_rietveld.ui_autorietveld.rpr.setText(rpr)
        rgl = self.settings.value('fpauto/rgl')
        self.auto_rietveld.ui_autorietveld.rgl.setText(rgl)
        regions = self.settings.value('fpauto/excluded')
        self.auto_rietveld.excludedtab.ui_excludetab.ranges_view.clear()
        self.auto_rietveld.excludedtab.load_regions(regions)
        ipatt = self.settings.value('fpauto/npatt')
        if ipatt:
            self.auto_rietveld.excludedtab.ui_excludetab.patt_num.setText(ipatt)
            self.auto_rietveld.excludedtab.enable_buttons()
        seqcheck = self.settings.value('fpauto/sequential')
        self.auto_rietveld.ui_autorietveld.sequential_check.setCheckState(int(seqcheck))
        fixspc = self.settings.value('fpauto/fixspc')
        self.auto_rietveld.ui_autorietveld.line_elements.setText(fixspc)
        paramtree = self.settings.value('fpauto/paramtree')
        self.auto_rietveld.parameter_tree = paramtree
        workflow = self.settings.value('fpauto/workflow')
        self.auto_rietveld.workflow_commands = workflow
        restact = self.settings.value('fpauto/restraintactions')
        if restact:
            for item in restact:
                self.auto_rietveld.restraint_actions.append(qtw.QAction(item))
            self.auto_rietveld.restraint_actions[0].triggered.connect(self.auto_rietveld.new_restraint)
        restfield = self.settings.value('fpauto/restraintfields')
        self.auto_rietveld.restraint_fields = restfield
        restcod = self.settings.value('fpauto/restraintcodes')
        self.auto_rietveld.restraint_codes = restcod
        constact = self.settings.value('fpauto/constraintactions')
        if constact:
            for item in constact:
                self.auto_rietveld.constraint_actions.append(qtw.QAction(item))
            self.auto_rietveld.constraint_actions[0].triggered.connect(self.auto_rietveld.new_constraint)
        constfield = self.settings.value('fpauto/constraintfields')
        self.auto_rietveld.constraint_fields = constfield
        constcode = self.settings.value('fpauto/constraintcodes')
        self.auto_rietveld.constraint_codes = constcode
        limitact = self.settings.value('fpauto/limitactions')
        if limitact:
            for item in limitact:
                self.auto_rietveld.limit_actions.append(qtw.QAction(item))
            self.auto_rietveld.limit_actions[0].triggered.connect(self.auto_rietveld.new_limit)
        limitfield = self.settings.values('fpauto/limitfields')
        self.auto_rietveld.limit_fields = limitfield
        limitcode = self.settings.values('fpauto/limitcodes')
        self.auto_rietveld.limit_codes = limitcode
        lebconst = self.settings.value('fpauto/lebailconst')
        if lebconst:
            for key, val in lebconst.items():
                self.auto_rietveld.lebail_const[key] = qtw.QAction('Profile Matching (Constant Relative Intensities)')
                self.auto_rietveld.lebail_const[key].setObjectName(f'const____{key}')
                self.auto_rietveld.lebail_const[key].setCheckable(True)
                self.auto_rietveld.lebail_const[key].triggered.connect(self.auto_rietveld.check_lebail_modes)
                self.auto_rietveld.lebail_const[key].setChecked(val)

        lebmod = self.settings.value('fpauto/lebailmode')
        if lebmod:
            for key, val in lebmod.items():
                self.auto_rietveld.lebail_mode[key] = qtw.QAction('Profile Matching (Constant Scale Factor)')
                self.auto_rietveld.lebail_mode[key].setObjectName(f'lebail____{key}')
                self.auto_rietveld.lebail_mode[key].setCheckable(True)
                self.auto_rietveld.lebail_mode[key].triggered.connect(self.auto_rietveld.check_lebail_modes)
                self.auto_rietveld.lebail_mode[key].setChecked(val)

        self.auto_rietveld.set_model_patterns()
        self.auto_rietveld.set_model_parameters()
        self.auto_rietveld.set_model_workflow()
        self.auto_rietveld.enable_duplicate_button()
        ipatt = self.settings.value('toolbar/npatt')
        if ipatt and index:
            self.enable_actions_buttons()

        self.currentpath = self.settings.value('mainwindow/currentpath')
        self.run_num = self.settings.value('mainwindow/runnum')

        if self.currentpath is not None:
            if os.path.exists(self.currentpath):
                self.read_results()
            else:
                if oldpath != currentpath:
                    newjobpath = os.path.join(currentpath, os.path.relpath(self.currentpath, oldpath))
                    if os.path.exists(newjobpath):
                        self.currentpath = newjobpath
                        self.read_results()
                    else:
                        self.warning = qtw.QMessageBox()
                        self.warning.setIcon(qtw.QMessageBox.Critical)
                        self.warning.setText(
                            f"Error reading folder containing results files, I was expecting:\n  {self.currentpath}\n\n"
                            f"Not found, changing search to .fpapp file location:\n  {newjobpath}\n\n"
                            f"Could not find the targeted files in new location, ABORTING settings load.")
                        self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                        self.warning.exec_()
                        return
                else:
                    self.warning = qtw.QMessageBox()
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText(f"Error reading Results folder I was expecting the following:\n{self.currentpath}")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    self.warning.exec_()
                    return

        ipatt = self.settings.value('toolbar/npatt')
        if ipatt and index:
            self.enable_actions_buttons()

        self.ui_mainwindow.statusbar.showMessage(f"Project loaded from {self.fileini}")

    def read_results(self):
        try:
            self.plot_current_prf()
            self.obtain_all_results()
            self.populate_tableresults()

            prftab = self.settings.value('mainwindow/prftab')
            self.ui_mainwindow.tabGraphs.setTabVisible(self.indexPrfData, prftab == 'true')
            summarytab = self.settings.value('mainwindow/summarytab')
            self.ui_mainwindow.tabGraphs.setTabVisible(self.indexSummary, summarytab == 'true')
            resulttab = self.settings.value('mainwindow/resulttab')
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, resulttab == 'true')
            resulttree = self.settings.value('mainwindow/resulttree')
            materfocus = self.settings.value('mainwindow/materfocus')
            self.ui_mainwindow.tabMaterials.setCurrentIndex(int(materfocus))
            graphfocus = self.settings.value('mainwindow/graphfocus')
            self.ui_mainwindow.tabGraphs.setCurrentIndex(int(graphfocus))

            if resulttree is not None:
                self.resultgraph['current'].load_tree_dict(resulttree)
                names = []
                for pattern in os.listdir(self.currentpath):
                    if os.path.isdir(os.path.join(self.currentpath, pattern)):
                        names.append(os.path.join(self.currentpath, pattern))
                self.resultgraph['current'].fill_results_database(names)

        except FullProfOutputReadError:
            self.ui_mainwindow.tabGraphs.setTabVisible(self.indexPrfData, False)
            self.ui_mainwindow.tabGraphs.setTabVisible(self.indexSummary, False)
            self.ui_mainwindow.tabMaterials.setTabVisible(self.indexResults, False)
