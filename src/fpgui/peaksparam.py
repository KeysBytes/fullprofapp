from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qtw

from fpgui.uifiles.uipeaksparam import Ui_Form


class PeaksParam(qtw.QWidget):

    values = qtc.pyqtSignal(float, float)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """ Setup UI for the main window """
        self.ui_peaksparam = Ui_Form()
        self.ui_peaksparam.setupUi(self)
        self.output_width = 3
        self.output_alpha = 1.0

        """ Attributes """
        self.setAttribute(qtc.Qt.WA_DeleteOnClose)
        """ Behavior of sliders """
        self.ui_peaksparam.width_slider.valueChanged.connect(lambda: self.update_width_value())
        self.ui_peaksparam.alpha_slider.valueChanged.connect(lambda: self.update_alpha_value())

    def update_width_value(self):
        new_text = f"width: {self.ui_peaksparam.width_slider.value()}"

        self.ui_peaksparam.label_width.setText(new_text)
        self.output_width = float(self.ui_peaksparam.width_slider.value())

        self.values.emit(self.output_alpha, self.output_width)

    def update_alpha_value(self):
        svalue = self.value_handler(self.ui_peaksparam.alpha_slider.value())
        new_text = f"alpha: {svalue}"
        self.output_alpha = svalue

        self.ui_peaksparam.label_alpha.setText(new_text)
        self.values.emit(self.output_alpha, self.output_width)

    def closeEvent(self, event):
        event.accept()

    @staticmethod
    def value_handler(value):
        scaled_value = float(value)/50

        return scaled_value
