from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qtw

from fpgui.uifiles.uibckgrparam import Ui_Form


class BckgrParam(qtw.QWidget):

    values = qtc.pyqtSignal(float, float)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """ Setup UI for the main window """
        self.ui_bckgrparam = Ui_Form()
        self.ui_bckgrparam.setupUi(self)
        self.output_lambda = 7
        self.output_p = -1.5

        """ Attributes """
        self.setAttribute(qtc.Qt.WA_DeleteOnClose)
        """ Behavior of sliders """
        self.ui_bckgrparam.lambda_slider.valueChanged.connect(lambda: self.update_lambda_value())
        self.ui_bckgrparam.p_slider.valueChanged.connect(lambda: self.update_p_value())

    def update_lambda_value(self):
        svalue = self.value_handler(self.ui_bckgrparam.lambda_slider.value())
        new_text = f"log_lambda: {svalue}"
        self.output_lambda = svalue

        self.ui_bckgrparam.label_lambda.setText(new_text)
        self.values.emit(self.output_p, self.output_lambda)

    def update_p_value(self):
        svalue = self.value_handler(self.ui_bckgrparam.p_slider.value())
        new_text = f"log_p: {svalue}"
        self.output_p = svalue

        self.ui_bckgrparam.label_p.setText(new_text)
        self.values.emit(self.output_p, self.output_lambda)

    def closeEvent(self, event):
        event.accept()

    @staticmethod
    def value_handler(value):
        scaled_value = float(value)/10.0

        return scaled_value
