from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

from fpgui.uifiles.uifpsearchparams import Ui_Form
from fpgui.excludetabwidget import ExcludeTabWidget


class SearchParams(qtw.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # load form
        self.ui_searchparams = Ui_Form()
        self.ui_searchparams.setupUi(self)

        self.excludedtab = ExcludeTabWidget()
        self.ui_searchparams.verticalLayout.insertWidget(2, self.excludedtab)

        self.ui_searchparams.scanr0_edit.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.scanr1_edit.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.scanr2_edit.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.scanncy_edit.setValidator(qtg.QIntValidator(1, 100))
        self.ui_searchparams.scanste_edit.setValidator(qtg.QIntValidator(1, 100))
        self.ui_searchparams.scanrat.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.scanran.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.scanrpr.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.scanrgl.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.foma.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.fomb.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.fomc.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.fomd.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.quanr0_edit.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.quanr1_edit.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.quanr2_edit.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.quanncy.setValidator(qtg.QIntValidator(1, 100))
        self.ui_searchparams.quanste.setValidator(qtg.QIntValidator(1, 100))
        self.ui_searchparams.quanrat.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.quanran.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.quanrpr.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.quanrgl.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.s0_edit.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.s1_edit.setValidator(qtg.QDoubleValidator())
        self.ui_searchparams.degelder_edit.setValidator(qtg.QDoubleValidator(0.0, 100.0, 4))

        self.ui_searchparams.lsr0.setToolTip(
            "Weight fraction threshold, if lower than given value phases are eliminated from the refinement")
        self.ui_searchparams.scanr0_edit.setToolTip(
            "Weight fraction threshold, if lower than given value phases are eliminated from the refinement")
        self.ui_searchparams.lsr1.setToolTip(
            "Weight fraction threshold, if lower than given value the refinement of unit cell parameters is ignored")
        self.ui_searchparams.scanr1_edit.setToolTip(
            "Weight fraction threshold, if lower than given value the refinement of unit cell parameters is ignored")
        self.ui_searchparams.lsr2.setToolTip(
            "Weight fraction threshold, if lower than given value crystallite size and microstrain profile parameters"
            " are ignored from refinement")
        self.ui_searchparams.scanr2_edit.setToolTip(
            "Weight fraction threshold, if lower than given value crystallite size and microstrain profile parameters"
            " are ignored from refinement")
        self.ui_searchparams.lsncy.setToolTip("Number of cycles")
        self.ui_searchparams.scanncy_edit.setToolTip("Number of cycles")
        self.ui_searchparams.lsste.setToolTip("Take one datapoint every 'Ste' points")
        self.ui_searchparams.scanste_edit.setToolTip("Take one datapoint every 'Ste' points")
        self.ui_searchparams.lsrat.setToolTip("Relaxation factor of atomic parameters")
        self.ui_searchparams.scanrat.setToolTip("Relaxation factor of atomic parameters")
        self.ui_searchparams.lsran.setToolTip("Relaxation factor of anisotropic displacements")
        self.ui_searchparams.scanran.setToolTip("Relaxation factor of anisotropic displacements")
        self.ui_searchparams.lsrpr.setToolTip("Relaxation factor of profile parameters")
        self.ui_searchparams.scanrpr.setToolTip("Relaxation factor of profile parameters")
        self.ui_searchparams.lsrgl.setToolTip("Relaxation factor of global parameters")
        self.ui_searchparams.scanrgl.setToolTip("Relaxation factor of global parameters")
        self.ui_searchparams.lfoma.setToolTip("Modified Figure of Merit parameter 'a'."
                                               " Increase its value to favor those phases"
                                               "whose initial cell parameters are closer to the observed peak positions")
        self.ui_searchparams.foma.setToolTip("Modified Figure of Merit parameter 'a'."
                                           " Increase its value to favor those phases"
                                           "whose initial cell parameters are closer to the observed peak positions")
        self.ui_searchparams.lfomb.setToolTip("Modified Figure of Merit parameter 'b'."
                                               " Increase its value to favor low scattering phases")
        self.ui_searchparams.fomb.setToolTip("Modified Figure of Merit parameter 'b'."
                                           " Increase its value to favor low scattering phases")
        self.ui_searchparams.lfomc.setToolTip("Modified Figure of Merit parameter 'c'."
                                               " Increase its value to penalize phases whose average apparent"
                                               " crystallite size is very low")
        self.ui_searchparams.fomc.setToolTip("Modified Figure of Merit parameter 'c'."
                                           " Increase its value to penalize phases whose average apparent"
                                           " crystallite size is very low")
        self.ui_searchparams.lfomd.setToolTip("Modified Figure of Merit parameter 'd'."
                                               " Increase its value to penalize phases whose average maximum strain"
                                               " is high")
        self.ui_searchparams.fomd.setToolTip("Modified Figure of Merit parameter 'd'."
                                           " Increase its value to penalize phases whose average maximum strain"
                                           " is high")
        self.ui_searchparams.lqr0.setToolTip(
            "Weight fraction threshold, if lower than given value phases are eliminated from the refinement")
        self.ui_searchparams.quanr0_edit.setToolTip(
            "Weight fraction threshold, if lower than given value phases are eliminated from the refinement")
        self.ui_searchparams.lqr1.setToolTip(
            "Weight fraction threshold, if lower than given value the refinement of unit cell parameters is ignored")
        self.ui_searchparams.quanr1_edit.setToolTip(
            "Weight fraction threshold, if lower than given value the refinement of unit cell parameters is ignored")
        self.ui_searchparams.lqr2.setToolTip(
            "Weight fraction threshold, if lower than given value crystallite size and microstrain profile parameters"
            " are ignored from refinement")
        self.ui_searchparams.quanr2_edit.setToolTip(
            "Weight fraction threshold, if lower than given value crystallite size and microstrain profile parameters"
            " are ignored from refinement")
        self.ui_searchparams.lqncy.setToolTip("Number of cycles")
        self.ui_searchparams.quanncy.setToolTip("Number of cycles")
        self.ui_searchparams.lqste.setToolTip("Take one datapoint every 'Ste' points")
        self.ui_searchparams.quanste.setToolTip("Take one datapoint every 'Ste' points")
        self.ui_searchparams.lqrat.setToolTip("Relaxation factor of atomic parameters")
        self.ui_searchparams.quanrat.setToolTip("Relaxation factor of atomic parameters")
        self.ui_searchparams.lqran.setToolTip("Relaxation factor of anisotropic displacements")
        self.ui_searchparams.quanran.setToolTip("Relaxation factor of anisotropic displacements")
        self.ui_searchparams.lqrpr.setToolTip("Relaxation factor of profile parameters")
        self.ui_searchparams.quanrpr.setToolTip("Relaxation factor of profile parameters")
        self.ui_searchparams.lqrgl.setToolTip("Relaxation factor of global parameters")
        self.ui_searchparams.quanrgl.setToolTip("Relaxation factor of global parameters")
        self.ui_searchparams.ls0.setToolTip(
            "Weight fraction threshold,"
            " if lower than given value phases are eliminated from the 'found' phases in the scan")
        self.ui_searchparams.s0_edit.setToolTip(
            "Weight fraction threshold,"
            " if lower than given value phases are eliminated from the 'found' phases in the scan")
        self.ui_searchparams.ls1.setToolTip(
            "Weight fraction threshold, if lower than given value phases are eliminated from the scan")
        self.ui_searchparams.s1_edit.setToolTip(
            "Weight fraction threshold, if lower than given value phases are eliminated from the scan")
        self.ui_searchparams.ldegelder.setToolTip("DeGelder similarity index, two phases are considered equal if the"
                                               " calculated value has a DeGelder index "
                                               "larger than the given value (0 - 100), normally > 90.0")
        self.ui_searchparams.degelder_edit.setToolTip("DeGelder similarity index, two phases are considered equal if the"
                                                    " calculated value has a DeGelder index "
                                                    "larger than the given value between 0 and 100")
        
        # Background values
        self.cheb_order = qtw.QLineEdit()
        self.cheb_label = qtw.QLabel('Order')
        self.cheb_order.setValidator(qtg.QIntValidator(1, 23))
        self.cheb_order.setText('5')
        self.cycles_fourier = qtw.QLineEdit()
        self.cycles_label = qtw.QLabel('Cycles')
        self.cycles_fourier.setValidator(qtg.QIntValidator())
        self.cycles_fourier.setText('1')
        self.window_fourier = qtw.QLineEdit()
        self.window_label = qtw.QLabel('Window')
        self.window_fourier.setValidator(qtg.QIntValidator())
        self.window_fourier.setText('2000')

        self.cheb_order.setToolTip("Set the order of the Chebyshev polynomial expansion of the background")
        self.cheb_label.setToolTip("Set the order of the Chebyshev polynomial expansion of the background")
        self.window_fourier.setToolTip("Set the window (in number of data points) to perform the filtering (min Npoints/6)")
        self.window_label.setToolTip("Set the window (in number of data points) to perform the filtering (min Npoints/6)")
        self.cycles_fourier.setToolTip("Apply fourier filtering once every input number of cycles")
        self.cycles_label.setToolTip("Apply fourier filtering once every input number of cycles")

        self.cheb_order.hide()
        self.cheb_label.hide()
        self.window_fourier.hide()
        self.window_label.hide()
        self.cycles_fourier.hide()
        self.cycles_label.hide()

        size = len(self.ui_searchparams.background_layout)
        self.ui_searchparams.background_layout.insertWidget(size, self.cheb_order)
        self.ui_searchparams.background_layout.insertWidget(size + 1, self.cheb_label)
        self.ui_searchparams.background_layout.insertWidget(size + 2, self.window_fourier)
        self.ui_searchparams.background_layout.insertWidget(size + 3, self.window_label)
        self.ui_searchparams.background_layout.insertWidget(size + 4, self.cycles_fourier)
        self.ui_searchparams.background_layout.insertWidget(size + 5, self.cycles_label)

        self.ui_searchparams.background_combo.currentIndexChanged.connect(self.background_type_change)

    def background_type_change(self, idx):

        if idx == 0:
            self.cheb_order.hide()
            self.cheb_label.hide()
            self.window_fourier.hide()
            self.window_label.hide()
            self.cycles_fourier.hide()
            self.cycles_label.hide()
        elif idx == 1:
            self.cheb_order.show()
            self.cheb_label.show()
            self.window_fourier.hide()
            self.window_label.hide()
            self.cycles_fourier.hide()
            self.cycles_label.hide()
        elif idx == 2:
            self.cheb_order.hide()
            self.cheb_label.hide()
            self.window_fourier.show()
            self.window_label.show()
            self.cycles_fourier.show()
            self.cycles_label.show()
            