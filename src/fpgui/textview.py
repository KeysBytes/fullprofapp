import os

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg

from fpgui.uifiles.uitextview import Ui_Form


class FileText(qtw.QWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """ Setup UI for the main window """
        self.ui_text = Ui_Form()
        self.ui_text.setupUi(self)
        self.ui_text.text.setReadOnly(False)

        font = qtg.QFont('Courier')
        self.ui_text.text.setFont(font)

    def write_file_text(self, filename: str):

        if os.path.isfile(filename):
            with open(filename, 'r') as file:
                content = file.read()
                self.ui_text.text.setText(content)


class CifText(qtw.QWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """ Setup UI for the main window """
        self.ui_ciftext = Ui_Form()
        self.ui_ciftext.setupUi(self)

    def write_cif_text(self, filename: str):
        """ Filename is a path to a .cif file """
        if os.path.isfile(filename) and ('.cif' in filename):
            with open(filename, 'r') as cif:
                content = cif.read()
                self.ui_ciftext.text.setText(content)
