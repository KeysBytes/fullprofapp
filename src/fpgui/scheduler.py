import os
from collections import deque
from typing import List, Union, Type, TypeVar, Mapping, Optional

from PyQt5 import QtCore as qtc


P = TypeVar('P', bound=qtc.QProcess)


class ProcessScheduler(qtc.QObject):
    process_started = qtc.pyqtSignal(int, str, str)
    process_error = qtc.pyqtSignal(int, str, str)
    all_finished = qtc.pyqtSignal()
    process_finished = qtc.pyqtSignal(int, str, str, int)

    """ This class returns signals in this order: 
        1st - (int) Slot possition of the process
        2nd - (str) Custom information (runstep of the process, file names, etc.)
        3rd - (str) Working directory for the process 
        4th - (int) ExitStatus for finished processes """

    def __init__(self, process: Type[P], procargs: Optional[Mapping] = None, nmax: int = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._command_queue = deque()

        if nmax is None:
            self._nmax = os.cpu_count()
        else:
            self._nmax = nmax

        self._process_pool = [process(**procargs) if procargs is not None else process() for _ in range(self._nmax)]

        for process in self._process_pool:
            process.started.connect(self.send_start_message)
            process.errorOccurred.connect(self.handle_error)

            if not process.receivers(process.finished):
                process.finished.connect(self.handle_finished) #  Else, the finished connection is handled in a custom subclass of QProcess and execute self.hangle_finished anyways
            
    def append_command_queue(self, cmd: str, arg: List[str], runstep: int, location: Union[str, os.PathLike]):
        self._command_queue.append((cmd, arg, runstep, location))

    def start_next_process(self, procid: int):
        try:
            cmds = self._command_queue.pop()
        except IndexError:
            has_items = False
        else:
            has_items = True
            self._process_pool[procid].setWorkingDirectory(cmds[3])
            self._process_pool[procid].setObjectName(cmds[2])
            self._process_pool[procid].start(cmds[0], cmds[1])
        finally:
            return has_items

    def start_all_processes(self):
        for i in range(self._nmax):
            if not self.start_next_process(i):
                break

    def kill_all_processes(self):
        self._command_queue = deque()
        for p in self._process_pool:
            p.errorOccurred.emit(qtc.QProcess.Crashed)
            p.blockSignals(True)
            p.kill()
            p.blockSignals(False)

    def send_start_message(self):
        procid = next(i for i, p in enumerate(self._process_pool) if p.processId() == self.sender().processId())
        self.process_started.emit(procid, self.sender().objectName(), self.sender().workingDirectory())
        
    def handle_finished(self, _, status):
        procid = next(i for i, p in enumerate(self._process_pool) if p.processId() == self.sender().processId())
        self.process_finished.emit(procid, self.sender().objectName(), self.sender().workingDirectory(), status)

        if not self.start_next_process(procid):
            self.check_all_finished()

    def check_all_finished(self):
        if all(p.state() == qtc.QProcess.NotRunning for p in self._process_pool):
            self.all_finished.emit()

    def handle_error(self):
        procid = next(i for i, p in enumerate(self._process_pool) if p.processId() == self.sender().processId())
        self.process_error.emit(procid, self.sender().objectName(), self.sender().workingDirectory())


class FullProfProcessScheduler(ProcessScheduler):
     
    def __init__(self, *args, **kwargs): #  When using a QProcess subclass the ProcessScheduler must be subclassed to capture the additional custom signals 
        super().__init__(*args, **kwargs)

        for process in self._process_pool:
            process.fperror.connect(self.handle_finished)
            process.fpsuccess.connect(self.handle_finished)

    def kill_all_processes(self):
        self._command_queue = deque()
        for p in self._process_pool:
            p.errorOccurred.emit(qtc.QProcess.Crashed)
            p.blockSignals(True)
            p.stop_timer()
            p.kill()
            p.blockSignals(False)

    def set_print_errors(self, val: bool):
        for p in self._process_pool:
            p.set_print_errors(val)

    def start_all_timers(self):
        for p in self._process_pool:
            p.start_timer()

    def stop_all_timers(self):
        for p in self._process_pool:
            p.stop_timer()


class FullProfProcess(qtc.QProcess):
    fperror = qtc.pyqtSignal(int, int)
    fpsuccess = qtc.pyqtSignal(int, int)

    def __init__(self, errors: List[str], print_errors: bool = True, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._errors = errors
        self._print = print_errors
        self._time = 0
        self._timer = qtc.QTimer(self)
        
        self.finished.connect(self._check_stdout_errors) #  This connection need to be there to correctly work with the scheduler (which also subclasses from parent scheduler)
        self._timer.timeout.connect(self._check_timeout_error)

    def _check_stdout_errors(self, code, status):
        self._time = 0
        if status == qtc.QProcess.CrashExit or code != 0:
            self.fperror.emit(code, status)
        else:
            text = bytes(self.readAllStandardOutput()).decode('utf8', errors='ignore')
            if any(error in text for error in self._errors):
                if self._print:
                    with open(os.path.join(self.workingDirectory(), 'ERROR.txt'), 'w') as f:
                        f.write(text)
                self.fperror.emit(0, qtc.QProcess.CrashExit)
                self.kill()
            else:
                self.fpsuccess.emit(1, qtc.QProcess.NormalExit)

    def _check_timeout_error(self):
        text = bytes(self.readAllStandardOutput()).decode('utf8')
        self._time += 15000
        if any(error in text for error in self._errors) or self._time > 29999:
            self.kill()

    def set_print_errors(self, val: bool):
        self._print = val
    
    def start_timer(self):
        self._time = 0
        self._timer.start(15000)

    def stop_timer(self):
        self._timer.stop()
