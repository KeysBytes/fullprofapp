import pandas as pd

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

from fpgui.uifiles.uipatternlist import Ui_Form
from fpgui.custommodels import PandasTableModel


class PatternList(qtw.QWidget):
    def __init__(self, number: int, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.data_dict = dict()

        self.ui_patternlist = Ui_Form()
        self.ui_patternlist.setupUi(self)

        self.ui_patternlist.check_box.setObjectName(str(number))

        self.ui_patternlist.check_box.setText(f'Pattern Group #{number}')

        self.ui_patternlist.table_pattern.setSelectionBehavior(qtw.QTableView.SelectRows)
        self.ui_patternlist.table_pattern.setEditTriggers(qtw.QTableView.NoEditTriggers)
        self.ui_patternlist.table_pattern.setAlternatingRowColors(True)

    def populate_table(self):

        summary = pd.DataFrame(self.data_dict)

        model = PandasTableModel(summary)
        self.ui_patternlist.table_pattern.setModel(model)
