from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

from fpgui.uifiles.uiexcludetabwidget import Ui_Exclude
import qtawesome as qta


class ExcludeTabWidget(qtw.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.regions = {}
        self.max_patt = 0

        # load form
        self.ui_excludetab = Ui_Exclude()
        self.ui_excludetab.setupUi(self)

        self.ui_excludetab.range_max.setValidator(qtg.QDoubleValidator())
        self.ui_excludetab.range_min.setValidator(qtg.QDoubleValidator())

        self.ui_excludetab.button_minus.setIcon(qta.icon('fa5s.minus'))
        self.ui_excludetab.button_minus.pressed.connect(lambda: self.on_pressed_minus())

        self.ui_excludetab.button_plus.setIcon(qta.icon('fa5s.plus'))
        self.ui_excludetab.button_plus.pressed.connect(lambda: self.on_pressed_plus())

        self.ui_excludetab.button_back_patt.setIcon(qta.icon('fa5s.caret-left'))
        self.ui_excludetab.button_next_patt.setIcon(qta.icon('fa5s.caret-right'))
        self.ui_excludetab.button_next_patt.clicked.connect(lambda: self.change_next_item_patt())
        self.ui_excludetab.button_back_patt.clicked.connect(lambda: self.change_back_item_patt())

        self.ui_excludetab.ranges_view.keyPressEvent = self.delete_regions

    def delete_regions(self, ev):
        if ev.key() == qtc.Qt.Key_Delete and len(self.ui_excludetab.ranges_view.selectedItems()) != 0:
            for item in self.ui_excludetab.ranges_view.selectedItems():
                key = int(item.text().split()[1])
                idx = self.ui_excludetab.ranges_view.indexFromItem(item)

                self.ui_excludetab.ranges_view.takeItem(idx.row())
                self.regions[key].pop()

    def init_widget(self):
        index_patt = int(self.ui_excludetab.patt_num.text())
        self.ui_excludetab.patt_num.setEnabled(True)
        self.ui_excludetab.label.setEnabled(True)
        if not self.regions:
            self.regions = {i: [] for i in range(self.max_patt)}

        if self.max_patt == 1:
            self.ui_excludetab.button_next_patt.setEnabled(False)
            self.ui_excludetab.button_back_patt.setEnabled(False)
        else:
            if index_patt + 1 >= self.max_patt:
                self.ui_excludetab.button_next_patt.setEnabled(False)
                self.ui_excludetab.button_back_patt.setEnabled(True)
            elif index_patt <= 0:
                self.ui_excludetab.button_back_patt.setEnabled(False)
                self.ui_excludetab.button_next_patt.setEnabled(True)
            else:
                self.ui_excludetab.button_next_patt.setEnabled(True)
                self.ui_excludetab.button_back_patt.setEnabled(True)

    def enable_buttons(self):
        index_patt = int(self.ui_excludetab.patt_num.text())

        if self.max_patt == 1 or self.max_patt == 0:
            self.ui_excludetab.button_next_patt.setEnabled(False)
            self.ui_excludetab.button_back_patt.setEnabled(False)
        else:
            if index_patt + 1 >= self.max_patt:
                self.ui_excludetab.button_next_patt.setEnabled(False)
                self.ui_excludetab.button_back_patt.setEnabled(True)
            elif index_patt <= 0:
                self.ui_excludetab.button_back_patt.setEnabled(False)
                self.ui_excludetab.button_next_patt.setEnabled(True)
            else:
                self.ui_excludetab.button_next_patt.setEnabled(True)
                self.ui_excludetab.button_back_patt.setEnabled(True)

    def change_next_item_patt(self):
        index_patt = int(self.ui_excludetab.patt_num.text())
        self.ui_excludetab.patt_num.setText(str(index_patt + 1))
        self.enable_buttons()

    def change_back_item_patt(self):
        index_patt = int(self.ui_excludetab.patt_num.text())
        self.ui_excludetab.patt_num.setText(str(index_patt - 1))
        self.enable_buttons()

    def on_pressed_minus(self):
        length = sum([len(self.regions[key]) for key in self.regions.keys()])
        if length > 0:
            item = self.ui_excludetab.ranges_view.item(length - 1)
            key = int(item.text().split()[1])
            self.ui_excludetab.ranges_view.takeItem(length - 1)
            self.regions[key].pop()

    def on_pressed_plus(self):
        text_min = self.ui_excludetab.range_min.text()
        text_min = text_min.replace(',', '.')
        text_max = self.ui_excludetab.range_max.text()
        text_max = text_max.replace(',', '.')
        self.regions[int(self.ui_excludetab.patt_num.text())].append([float(text_min), float(text_max)])
        line_str = f"PATT: {int(self.ui_excludetab.patt_num.text())}" \
                   f" Excluded Region #{len(self.regions[int(self.ui_excludetab.patt_num.text())])}" \
                   f" ==> Min: {text_min} --- Max: {text_max}"
        self.ui_excludetab.ranges_view.addItem(line_str)

    def load_regions(self, regions):
        self.regions = regions

        for ipatt, region in self.regions.items():
            for count, reg in enumerate(region):
                line_str = f"PATT: {ipatt} Excluded Region #{count + 1} ==> Min: {reg[0]} --- Max: {reg[1]}"
                self.ui_excludetab.ranges_view.addItem(line_str)
