from PyQt5 import QtWidgets as qtw

from fpgui.uifiles.uiperiodictable import Ui_PeriodicTable


class PeriodicTable(qtw.QDialog):
    """ This is based on https://github.com/BrendanSweeny/clusterid"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """ Setup UI for the main window """
        self.ui_periodictable = Ui_PeriodicTable()
        self.ui_periodictable.setupUi(self)

        """ Exclude MaterialsProject until further notice (wait MarkReply) """
        self.ui_periodictable.combo_db.removeItem(1)

        self.selected = dict()

        for name in dir(self.ui_periodictable):
            if 'ebtn' in name:
                btn = getattr(self.ui_periodictable, name)
                self.selected[btn.text()] = 0
                btn.clicked.connect(lambda: self.on_click())

        self.ui_periodictable.dialog_button.accepted.connect(self.accept)
        self.ui_periodictable.dialog_button.rejected.connect(self.reject)

    def on_click(self):
        """ Generate element name with status: 0: NOT PRESENT (Grey), 1: CAN BE PRESENT (Yellow),
        2: HAS TO BE PRESENT (Green) """
        if self.selected[self.sender().text()] == 0:
            self.selected[self.sender().text()] = 1
            self.sender().setStyleSheet('background-color: rgb(214, 214, 0);')
        elif self.selected[self.sender().text()] == 1:
            self.selected[self.sender().text()] = 2
            self.sender().setStyleSheet('background-color: rgb(70, 212, 104);')
        elif self.selected[self.sender().text()] == 2:
            self.selected[self.sender().text()] = 0
            self.sender().setStyleSheet('')

