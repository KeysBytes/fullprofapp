from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

from fpgui.uifiles.uirestraintdialog import Ui_Dialog
from fpgui.uifiles.uiconstraintdialog import Ui_Dialog_const
from fpgui.uifiles.uiduplicatedialog import Ui_Dialog_dupli
from fpgui.uifiles.uilimitsdialog import Ui_Dialog_limits


class DuplicateDialog(qtw.QDialog):
    def __init__(self, names, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._names = names
        self._fields = dict()

        self.ui_duplicatedialog = Ui_Dialog_dupli()
        self.ui_duplicatedialog.setupUi(self)

        self.warning = qtw.QMessageBox()

        self.ui_duplicatedialog.button_dialog.accepted.connect(self.accept)
        self.ui_duplicatedialog.button_dialog.rejected.connect(self.reject)

        font = qtg.QFont()
        font.setBold(True)
        font.setWeight(75)

        label = qtw.QLabel(f'Site(s) to be duplicated:')
        label.setFont(font)
        self.ui_duplicatedialog.layout_duplis.addWidget(label, 0, 0)

        label = qtw.QLabel(f'New Site Label')
        label.setFont(font)
        self.ui_duplicatedialog.layout_duplis.addWidget(label, 0, 1)

        label = qtw.QLabel(f'New Site Element')
        label.setFont(font)
        self.ui_duplicatedialog.layout_duplis.addWidget(label, 0, 2)

        count = 1
        for name in self._names:
            label = qtw.QLabel(f'{name[0]} > {name[1]}')
            self.ui_duplicatedialog.layout_duplis.addWidget(label, count, 0)

            line1 = qtw.QLineEdit()
            line2 = qtw.QLineEdit()

            self.ui_duplicatedialog.layout_duplis.addWidget(line1, count, 1)
            self.ui_duplicatedialog.layout_duplis.addWidget(line2, count, 2)

            count += 1

        self.adjustSize()

    def read_fields(self):
        for count, i in enumerate(range(1, self.ui_duplicatedialog.layout_duplis.rowCount())):
            vals = []
            for count1, j in enumerate(range(1, self.ui_duplicatedialog.layout_duplis.columnCount())):
                vals.append(self.ui_duplicatedialog.layout_duplis.itemAtPosition(i, j).widget().text())

            self._fields[self._names[count]] = vals

    def sanity_check_fields(self):
        for count, i in enumerate(range(1, self.ui_duplicatedialog.layout_duplis.rowCount())):
            for count1, j in enumerate(range(1, self.ui_duplicatedialog.layout_duplis.columnCount())):
                if not self.ui_duplicatedialog.layout_duplis.itemAtPosition(i, j).widget().text():
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText(f"Empty fields, input a value")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                    self.warning.exec_()

                    return False

        return True

    def accept(self):
        valid = self.sanity_check_fields()
        if valid:
            self.read_fields()
            super(DuplicateDialog, self).accept()

    def get_outputs(self):
        return self._fields


class LimitsDialog(qtw.QDialog):
    def __init__(self, model, index, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._model = model
        self._index = index
        self._fields = dict()
        self._paths = [[] for _ in range(len(index))]

        self.warning = qtw.QMessageBox()

        self.ui_limitsdialog = Ui_Dialog_const()
        self.ui_limitsdialog.setupUi(self)

        self.ui_limitsdialog.button_dialog.accepted.connect(self.accept)
        self.ui_limitsdialog.button_dialog.rejected.connect(self.reject)
      
        count = 0
        for ix in index:
            self._paths[count] = list(reversed(self.get_path_to_top(ix, self._paths[count])))
            count += 1

        count = 0
        for path in self._paths:
            path.reverse()
            font = qtg.QFont()
            font.setBold(True)
            font.setWeight(75)
            label = qtw.QLabel(' > '.join(path))
            label.setFont(font)
            self.ui_limitsdialog.layout_coefficients.insertWidget(count, label)
            count += 1
            hlay = qtw.QHBoxLayout()
            hlay.addWidget(qtw.QLabel('Low:'))
            line_edit = qtw.QLineEdit()
            line_edit.setValidator(qtg.QDoubleValidator())
            hlay.addWidget(line_edit)
            hlay.addWidget(qtw.QLabel('High:'))
            line_edit1 = qtw.QLineEdit()
            line_edit1.setValidator(qtg.QDoubleValidator())
            hlay.addWidget(line_edit1)
            hlay.addWidget(qtw.QLabel('Boundary (0 or 1):'))
            line_edit2 = qtw.QLineEdit()
            line_edit2.setValidator(qtg.QIntValidator(0, 1))
            hlay.addWidget(line_edit2)
            hlay.addWidget(qtw.QLabel('Coefficient:'))
            line_edit3 = qtw.QLineEdit()
            line_edit3.setValidator(qtg.QDoubleValidator())
            hlay.addWidget(line_edit3)
            self.ui_limitsdialog.layout_coefficients.insertLayout(count, hlay)
            count += 1

        self.adjustSize()

    def sanity_check_fields(self):
        for count, i in enumerate(range(1, self.ui_limitsdialog.layout_coefficients.count(), 2)):
            for j in range(1, 8, 2):
                if not self.ui_limitsdialog.layout_coefficients.itemAt(i).itemAt(j).widget().text():
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText(f"Input a coefficient for parameter '{' > '.join(reversed(self._paths[count]))}'")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                    self.warning.exec_()

                    return False

        return True
    
    def read_fields(self):
        for count, i in enumerate(range(1, self.ui_limitsdialog.layout_coefficients.count(), 2)):
            path = tuple(reversed(self._paths[count]))
            if path[1] in ['Strain Anisotropy', 'Size Anisotropy']:
                key = (path[0], path[-1], path[-2])
            else:
                key = path

            self._fields[key] = []
            for j in range(1, 8, 2):
                self._fields[key].append(self.ui_limitsdialog.layout_coefficients.itemAt(i).itemAt(j).widget().text())

    def accept(self):
        valid = self.sanity_check_fields()
        if valid:
            self.read_fields()
            super(LimitsDialog, self).accept()

    def get_outputs(self):
        return self._fields

    def get_path_to_top(self, parent, path):
        top = self._model.parent(parent)

        if self._model.itemFromIndex(top) is not None:
            path.append(parent.data())
            return self.get_path_to_top(top, path)
        else:
            path.append(parent.data())
            return path

class ConstraintDialog(qtw.QDialog):
    def __init__(self, model, index, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._model = model
        self._index = index
        self._fields = dict()
        self._paths = [[] for _ in range(len(index))]

        self.warning = qtw.QMessageBox()

        self.ui_constraintdialog = Ui_Dialog_const()
        self.ui_constraintdialog.setupUi(self)

        self.ui_constraintdialog.button_dialog.accepted.connect(self.accept)
        self.ui_constraintdialog.button_dialog.rejected.connect(self.reject)

        count = 0
        for ix in index:
            self._paths[count] = list(reversed(self.get_path_to_top(ix, self._paths[count])))
            count += 1

        count = 0
        for path in self._paths:
            path.reverse()
            font = qtg.QFont()
            font.setBold(True)
            font.setWeight(75)
            label = qtw.QLabel(' > '.join(path))
            label.setFont(font)
            self.ui_constraintdialog.layout_coefficients.insertWidget(count, label)
            count += 1
            hlay = qtw.QHBoxLayout()
            hlay.addWidget(qtw.QLabel('Coefficient'))
            line_edit = qtw.QLineEdit()
            line_edit.setValidator(qtg.QDoubleValidator())
            hlay.addWidget(line_edit)
            self.ui_constraintdialog.layout_coefficients.insertLayout(count, hlay)
            count += 1

        self.adjustSize()

    def read_fields(self):
        for count, i in enumerate(range(1, self.ui_constraintdialog.layout_coefficients.count(), 2)):
            path = tuple(reversed(self._paths[count]))
            if path[1] in ['Strain Anisotropy', 'Size Anisotropy']:
                self._fields[(path[0], path[-1], path[-2])] = self.ui_constraintdialog.layout_coefficients.itemAt(i).itemAt(1).widget().text()
            else:
                self._fields[path] = self.ui_constraintdialog.layout_coefficients.itemAt(i).itemAt(1).widget().text()

    def sanity_check_fields(self):
        for count, i in enumerate(range(1, self.ui_constraintdialog.layout_coefficients.count(), 2)):
            if not self.ui_constraintdialog.layout_coefficients.itemAt(i).itemAt(1).widget().text():
                self.warning.setIcon(qtw.QMessageBox.Critical)
                self.warning.setText(f"Input a coefficient for parameter '{' > '.join(reversed(self._paths[count]))}'")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                self.warning.exec_()

                return False

        return True

    def get_path_to_top(self, parent, path):
        top = self._model.parent(parent)

        if self._model.itemFromIndex(top) is not None:
            path.append(parent.data())
            return self.get_path_to_top(top, path)
        else:
            path.append(parent.data())
            return path

    def accept(self):
        valid = self.sanity_check_fields()
        if valid:
            self.read_fields()
            super(ConstraintDialog, self).accept()

    def get_outputs(self):
        return self._fields


class RestraintDialog(qtw.QDialog):

    def __init__(self, model, index, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._model = model
        self._index = index
        self._fields = dict()
        self._paths = [[] for _ in range(len(index))]

        self.warning = qtw.QMessageBox()

        self.ui_restraintdialog = Ui_Dialog()
        self.ui_restraintdialog.setupUi(self)

        self.ui_restraintdialog.value_edit.setValidator(qtg.QDoubleValidator())
        self.ui_restraintdialog.sigma_edit.setValidator(qtg.QDoubleValidator())

        self.ui_restraintdialog.button_dialog.accepted.connect(self.accept)
        self.ui_restraintdialog.button_dialog.rejected.connect(self.reject)

        count = 0
        for ix in index:
            self._paths[count] = list(reversed(self.get_path_to_top(ix, self._paths[count])))
            count += 1

        count = 0
        for path in self._paths:
            path.reverse()
            font = qtg.QFont()
            font.setBold(True)
            font.setWeight(75)
            label = qtw.QLabel(' > '.join(path))
            label.setFont(font)
            self.ui_restraintdialog.layout_coefficients.insertWidget(count, label)
            count += 1
            hlay = qtw.QHBoxLayout()
            hlay.addWidget(qtw.QLabel('Coefficient'))
            line_edit = qtw.QLineEdit()
            line_edit.setValidator(qtg.QDoubleValidator())
            hlay.addWidget(line_edit)
            self.ui_restraintdialog.layout_coefficients.insertLayout(count, hlay)
            count += 1

        self.adjustSize()

    def read_fields(self):
        self._fields['value'] = self.ui_restraintdialog.value_edit.text()
        self._fields['sigma'] = self.ui_restraintdialog.sigma_edit.text()

        for count, i in enumerate(range(1, self.ui_restraintdialog.layout_coefficients.count(), 2)):
            path = tuple(reversed(self._paths[count]))
            if path[1] in ['Strain Anisotropy', 'Size Anisotropy']:
                self._fields[(path[0], path[-1], path[-2])] = self.ui_restraintdialog.layout_coefficients.itemAt(i).itemAt(1).widget().text()
            else:
                self._fields[path] = self.ui_restraintdialog.layout_coefficients.itemAt(i).itemAt(1).widget().text()

    def sanity_check_fields(self):
        if not self.ui_restraintdialog.value_edit.text():
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText("Input a value for the result of the linear combination of restraints.")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)

            self.warning.exec_()

            return False

        if not self.ui_restraintdialog.sigma_edit.text():
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText("Input a sigma for the tolerance of the linear combination of restraints.")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)

            self.warning.exec_()

            return False

        for count, i in enumerate(range(1, self.ui_restraintdialog.layout_coefficients.count(), 2)):
            if not self.ui_restraintdialog.layout_coefficients.itemAt(i).itemAt(1).widget().text():
                self.warning.setIcon(qtw.QMessageBox.Critical)
                self.warning.setText(f"Input a coefficient for parameter '{' > '.join(reversed(self._paths[count]))}'")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                self.warning.exec_()

                return False

        return True

    def accept(self):
        valid = self.sanity_check_fields()
        if valid:
            self.read_fields()
            super(RestraintDialog, self).accept()

    def get_outputs(self):
        return self._fields

    def get_path_to_top(self, parent, path):
        top = self._model.parent(parent)

        if self._model.itemFromIndex(top) is not None:
            path.append(parent.data())
            return self.get_path_to_top(top, path)
        else:
            path.append(parent.data())
            return path
