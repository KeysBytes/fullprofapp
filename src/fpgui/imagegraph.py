from typing import List

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

import pyqtgraph as pg
#import pyqtgraph.opengl as pgl 5.13.0 pyinstaller
import numpy as np

from matplotlib import cm

from fpgui.uifiles.uiimagegraph import Ui_imageGraph

from fpfunctions.signals import DiffractionSignal


class ContourGraph(qtw.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.ui_imagegraph = Ui_imageGraph()
        self.ui_imagegraph.setupUi(self)

        self._data = None
        self._image = None
        self._scale = None
        self._x = None
        self._y = None

        self._ilines = dict()
        self._last_pos = None
        self._levels = None
        
        """ Definition of the PlotItem """
        axis_pen = pg.mkPen(color=(0, 0, 0), width=2)

        self.ui_imagegraph.image_graph.getAxis('left').setPen(axis_pen)
        self.ui_imagegraph.image_graph.getAxis('left').setTextPen(axis_pen)

        self.ui_imagegraph.image_graph.setLabel('bottom', u'2θ (º)')
        self.ui_imagegraph.image_graph.getAxis('bottom').setPen(axis_pen)
        self.ui_imagegraph.image_graph.getAxis('bottom').setTextPen(axis_pen)

        self.ui_imagegraph.image_graph.setLabel('right', ' ')
        self.ui_imagegraph.image_graph.showAxis('right')
        self.ui_imagegraph.image_graph.getAxis('right').setPen(axis_pen)
        self.ui_imagegraph.image_graph.getAxis('right').setStyle(showValues=False)

        self.ui_imagegraph.image_graph.setLabel('top', ' ')
        self.ui_imagegraph.image_graph.showAxis('top')
        self.ui_imagegraph.image_graph.getAxis('top').setPen(axis_pen)
        self.ui_imagegraph.image_graph.getAxis('top').setStyle(showValues=False)

        self.ui_imagegraph.image_graph.setMouseEnabled(x=False, y=False)

        self._cmap = cm.get_cmap("rainbow")
        self._cmap._init()
        self._lut = (self._cmap._lut * 255).view(np.ndarray)

        self._img = pg.ImageItem()
        # self._surface = pgl.GLSurfacePlotItem(smooth=True, shader='pointSprite')

        # self.ui_imagegraph.surface_widget.addItem(self._surface) 
        # self.ui_imagegraph.surface_widget.setBackgroundColor(qtg.QColor(190, 190, 190))

        # gx = pgl.GLGridItem()
        # gx.rotate(90, 0, 1, 0)
        # gx.translate(0.0, 0.25, 0.5)
        # gx.setSize(1.0, 0.5, 1.0)
        # gx.setSpacing(0.1, 0.1, 0.1)
        # self.ui_imagegraph.surface_widget.addItem(gx)
        # gy = pgl.GLGridItem()
        # gy.rotate(90, 1, 0, 0)
        # gy.translate(0.5, 0.5, 0.5)
        # gy.setSize(1.0, 1.0, 1.0)
        # gy.setSpacing(0.1, 0.1, 0.1)
        # self.ui_imagegraph.surface_widget.addItem(gy)
        # gz = pgl.GLGridItem()
        # gz.translate(0.5, 0.25, 0.0)
        # gz.setSize(1.0, 0.5, 1.0)
        # gz.setSpacing(0.1, 0.1, 0.1)
        # self.ui_imagegraph.surface_widget.addItem(gz)  

        """ PlotItem's ViewBox events """
        self.ui_imagegraph.image_graph.plotItem.getViewBox().mouseClickEvent = self.click_graph
        self.ui_imagegraph.image_graph.plotItem.getViewBox().hoverEvent = self.record_mouse_pos
        self.ui_imagegraph.image_graph.plotItem.getViewBox().keyPressEvent = self.delete_lines
        self.ui_imagegraph.image_graph.plotItem.getViewBox().mouseDragEvent = self.drag_graph
        self.ui_imagegraph.image_graph.plotItem.getViewBox().wheelEvent = self.change_levels 

    def parse_data(self, data: List[DiffractionSignal]):
        self._data = data
        self._image, self._scale, self._x, self._y = self._convert_data_to_image()
        self._set_plots()

    def _set_plots(self):
        self._set_contour_plot()
        #self._set_surface_plot()

    def _set_contour_plot(self):
        x = self._data[0].diffraction[0, 0]
        y = 0.0
        w = self._data[0].diffraction[-1, 0] - self._data[0].diffraction[0, 0]
        h = len(self._data)

        self._img.setImage(self._image)
        self._img.setRect(x, y, w, h)
        self._img.setLookupTable(lut=self._lut)

        self._levels = self._img.getLevels()
        self._plot = self.ui_imagegraph.image_graph.getPlotItem()
        self._plot.addItem(self._img)
        self._plot.getViewBox().autoRange(padding=0)
        self._plot.getViewBox().setMouseMode(pg.ViewBox.RectMode)

    # def _set_surface_plot(self):
    #     maximum = np.amax(self._image)
    #     minimum = np.amin(self._image)
    #     image = (self._image - minimum) / (maximum - minimum)

    #     colors = self._cmap(image)   
    #     self._surface.setData(self._x, self._y, image, colors)

    #     vertex = self._surface._meshdata.vertexes()
    #     xc = 0.5 * (np.amax(vertex[:, 0]) - np.amin(vertex[:, 0]))
    #     yc = 0.5 * (np.amax(vertex[:, 1]) - np.amin(vertex[:, 1]))
    #     zc = 0.5 * (np.amax(vertex[:, 2]) - np.amin(vertex[:, 2]))

    #     camera_params = {'rotation': qtg.QQuaternion(1.0, 0.0, 0.0, 0.0),
    #                     'distance': 10.0,
    #                     'fov': 60,
    #                     'center': qtg.QVector3D(xc, yc, zc)}
        
    #     self.ui_imagegraph.surface_widget.setCameraParams(**camera_params)

    def _convert_data_to_image(self):
        image = np.zeros((len(self._data), self._data[0].diffraction.shape[0]), dtype=np.float64)
        for count, objects in enumerate(self._data):
            image[count, :] = objects.diffraction[:, 1]

        scale = (self._data[0].diffraction[-1, 0] - self._data[0].diffraction[0, 0]) / float(self._data[0].diffraction.shape[0])

        x = self._data[0].diffraction[:, 0]
        y = np.arange(len(self._data), dtype=np.float64)
        x = (x - x[0]) / (x[-1] - x[0])
        y = 0.5 * (y - y[0]) / (y[-1] - y[0])

        return [image.T, scale, x, y]

    def record_mouse_pos(self, ev):
        self.ui_imagegraph.image_graph.scene().items(ev.scenePos())
        vb = self.ui_imagegraph.image_graph.plotItem.getViewBox()
        mousepoint = vb.mapSceneToView(ev._scenePos)
        self._last_pos = int(mousepoint.y())

        ev.accept()

    def click_graph(self, ev):
        if ev.button() == qtc.Qt.MouseButton.RightButton and not ev.modifiers() & qtc.Qt.ControlModifier:
            self.ui_imagegraph.image_graph.autoRange(padding=0)
            ev.accept()
            self.rescale_image_in_box()

        elif ev.button() == qtc.Qt.MouseButton.LeftButton and self.ui_imagegraph.checkpoint.isChecked():
            self.ui_imagegraph.image_graph.scene().items(ev.scenePos())
            vb = self.ui_imagegraph.image_graph.plotItem.getViewBox()
            mousepoint = vb.mapSceneToView(ev._scenePos)

            line_pen = pg.mkPen(color=(51, 153, 255), width=2)
            iline = pg.InfiniteLine(mousepoint.y(), 0, movable=False, pen=line_pen)
            self._ilines[int(mousepoint.y())] = iline
            self._plot.addItem(self._ilines[int(mousepoint.y())])
            ev.accept()

        else:
            pg.ViewBox.mouseClickEvent(self.ui_imagegraph.image_graph.plotItem.getViewBox(), ev)

    def delete_lines(self, ev):
        if ev.key() == qtc.Qt.Key_Delete:
            keys = np.array(list(self._ilines.keys()), dtype=int)

            dist = np.abs(keys - self._last_pos)
            index = np.argmin(dist)

            self._plot.removeItem(self._ilines[keys[index]])
            self._ilines.pop(keys[index])

    def remove_all_lines(self):
        for lines in self._ilines.values():
            self._plot.removeItem(lines)

        self._ilines = dict()

    def drag_graph(self, ev):
        """ Redefine some graph drag functions
        Ctrl + LeftClickDrag : Select Points/s
        """

        if ev.button() == qtc.Qt.MouseButton.LeftButton:
            vb = self.ui_imagegraph.image_graph.plotItem.getViewBox()
            if ev.isFinish():
                vb.rbScaleBox.hide()
                pg.ViewBox.mouseDragEvent(self.ui_imagegraph.image_graph.plotItem.getViewBox(), ev)
                ev.accept()

                self.rescale_image_in_box(self._plot.getViewBox().state)
            else:
                vb.updateScaleBox(ev.buttonDownPos(), ev.pos())
                ev.accept()

        else:
            pg.ViewBox.mouseDragEvent(self.ui_imagegraph.image_graph.plotItem.getViewBox(), ev)

    def rescale_image_in_box(self, state: dict = None):

        if state is not None:
            x0 = state['targetRange'][0][0]
            x1 = state['targetRange'][0][1]
            y0 = state['targetRange'][1][0]
            y1 = state['targetRange'][1][1]

            ibeg = int(x0 / self._scale + 0.5)
            ifin = int(x1 / self._scale + 0.5)
            jbeg = int(y0 + 0.5)
            jfin = int(y1 + 0.5)

            image = self._image[ibeg:ifin, jbeg:jfin]

            maximum = np.amax(image)
            minimum = np.amin(image)

            self._img.setLevels(levels=[minimum, maximum])

        else:
            self._img.setLevels(levels=self._levels)

    def change_levels(self, ev):
        if ev.modifiers() == qtc.Qt.ControlModifier:
            delta = ev.delta()
            step = (delta and delta // abs(delta))
            botlvl, toplvl = self._img.getLevels()
            newlvl = toplvl + (toplvl - botlvl) * step / 100.0
            self._img.setLevels(levels=[botlvl, newlvl])

        elif ev.modifiers() == qtc.Qt.ShiftModifier:
            delta = ev.delta()
            step = (delta and delta // abs(delta))
            botlvl, toplvl = self._img.getLevels()
            newlvl = botlvl + (toplvl - botlvl) * step / 100.0
            self._img.setLevels(levels=[newlvl, toplvl])

    def get_outputs(self):
        keys = np.array(list(self._ilines.keys()), dtype=int)
        return keys
