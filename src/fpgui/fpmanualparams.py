import os
from typing import List
import numpy as np

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

from fpgui.uifiles.uifpmanualparams import Ui_Form

from fpgui.pcrparamdialog import PcrDialog
from fpgui.custommodels import ListTableModel, ListTableView

from fpfunctions.app_io import PcrIO, BackgroundType

import qtawesome as qta


class ManualParams(qtw.QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # load form
        self.ui_manualparams = Ui_Form()
        self.ui_manualparams.setupUi(self)

        self.pattern_pcrs = dict()

        " List of widgets to include, some maybe LineEdit, TableViews, ListViews..., all contained" \
        "within list of dictionaries"
        self.line_comm = None
        self.table_npatt = None
        self.table_wpatt = None
        self.table_params1 = None
        self.line_names = None
        self.line_irf = None
        self.table_params2 = None
        self.table_params3 = None
        self.line_vary_global = None
        self.line_fix_global = None
        self.table_params4 = None
        self.table_background = None
        self.table_fourier = None
        self.table_chebyshev = None
        self.table_excluded = None
        self.line_number = None
        self.table_params5 = None
        self.table_params5more = None
        self.line_phidentifiers = None # All objects starting from ph, are dicts with identifiers as keys
        self.lines_phcommands = None
        self.table_phparams1global = None
        self.table_phparams1more = None
        self.table_phcontributions = None
        self.table_phparams1_1 = None
        self.table_phparams1_2 = None
        self.table_phbvs = None
        self.table_phbvsions = None
        self.line_phspacegroup = None
        self.table_phatoms = None
        self.table_phprof1 = None
        self.table_phprof2 = None
        self.table_phprof3 = None
        self.table_phprof4 = None
        self.table_phprof5 = None
        self.table_phabscor = None
        self.table_phshifts = None
        self.table_phstrainmodel = None
        self.table_phsizemodel = None
        self.lines_limits = None
        self.lines_restraints = None

        self.pcrparams = None
        self._radiation = None
        self.warning = qtw.QMessageBox()

        self.ui_manualparams.button_minus.setIcon(qta.icon('fa5s.minus'))
        self.ui_manualparams.button_minus.setToolTip('Eliminate current entry')
        self.ui_manualparams.button_plus.setIcon(qta.icon('fa5s.plus'))
        self.ui_manualparams.button_plus.setToolTip('Add selection phases to current entry')
        self.ui_manualparams.button_minusminus.setIcon(qta.icon('fa5s.minus-square'))
        self.ui_manualparams.button_minusminus.setToolTip('Eliminate ALL entries')
        self.ui_manualparams.button_plusplus.setIcon(qta.icon('fa5s.plus-square'))
        self.ui_manualparams.button_plusplus.setToolTip('Add selection phases to ALL entries')

        self.ui_manualparams.pattern_combo.currentIndexChanged.connect(self.change_pcr_view)

        self.ui_manualparams.run_fp.setEnabled(False)
        self.ui_manualparams.run_fp_new.setEnabled(False)

        self.ui_manualparams.load_pcr.setEnabled(False)
        self.ui_manualparams.load_pcr.clicked.connect(self.load_pcr)

    def check_pcr_radiation(self, pcr: PcrIO):
        jobdict = {0: 'xrd', 1: 'cw', -1: 'tof'}
        npatt = pcr.get_patt_num()
        if npatt != len(self._radiation):
            return False

        for ipatt, rad in enumerate(self._radiation):
            job = pcr.get_flags_global('job', ipatt)
            val = jobdict.get(job, None)
            if val is None or val != rad:
                return False
        return True

    def check_names(self, pattern: str, pcr: PcrIO):
        pattern = pattern.split('_and_')
        if pcr.multipattern:
            npatt = pcr.get_patt_num()
            if all(pattern[ipatt] in os.path.splitext(os.path.basename(pcr.get_names(ipatt)))[0] for ipatt in range(npatt)):
                return True
            else:
                return False
        else:
            return True

    def set_radiation(self, parameter_info: List[str]):
        self._radiation = parameter_info

    def load_pcr(self):
        combo_count = self.ui_manualparams.pattern_combo.count()
        patterns = [self.ui_manualparams.pattern_combo.itemText(i) for i in range(combo_count)]
        self.pcrparams = PcrDialog(patterns=patterns)
        self.pcrparams.accepted.connect(self.build_pcr_objects)

        self.pcrparams.exec_()

    def build_pcr_objects(self):
        pcr_map = self.pcrparams.get_output()
        good = True
        for pattern, path in pcr_map.items():
            head, tail = os.path.split(path)
            jobid = tail.replace(".pcr", "")
            pcr = PcrIO(path=head, jobid=jobid)
            pcr.read_custom_pcr()
            npatt = pcr.get_patt_num()
            ids = pcr.read_ids()

            pcr.edit_flags_global('rpa', -1)
            pcr.edit_flags_global('pcr', 1)
            for ipatt in range(npatt):
                pcr.edit_flags_global('ipr', 2, ipatt)
                pcr.edit_flags_global('ppl', 2, ipatt)
                pcr.edit_flags_global('prf', 3, ipatt)
                
                for identifier in ids:
                    pcr.edit_flags_phase(identifier, 'irf', 0, ipatt)

            res = self.check_pcr_radiation(pcr)
            res1 = self.check_names(pattern, pcr)
            if res:
                self.pattern_pcrs[pattern] = pcr
            else:
                self.warning.setIcon(qtw.QMessageBox.Critical)
                self.warning.setText("Loaded PCR files do not have the correct JOB type")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                self.warning.exec_()
                good = False
                break

            if not res1:
                self.warning.setIcon(qtw.QMessageBox.Warning)
                self.warning.setText("Names of the pattern files loaded in your multipattern PCR do not coincide with the ones loaded in FullProfAPP. The names will be replaced when you start the refinement run.\n\n BE CAREFUL! You might have loaded the wrong PCR.")
                self.warning.setStandardButtons(qtw.QMessageBox.Ok)

                self.warning.exec_()

        if good:
            " Here function that controls every update based on changes to self.pattern_pcr"
            self.change_pcr_view()
            self.enable_run_button()
            self.ui_manualparams.button_minus.setEnabled(True)
            self.ui_manualparams.button_minusminus.setEnabled(True)

    def enable_run_button(self):
        count = self.ui_manualparams.pattern_combo.count()
        combo = self.ui_manualparams.pattern_combo
        if all(combo.itemText(i) in self.pattern_pcrs for i in range(count)):
            self.ui_manualparams.run_fp.setEnabled(True)
            self.ui_manualparams.run_fp_new.setEnabled(True)
        else:
            self.ui_manualparams.run_fp.setEnabled(False)
            self.ui_manualparams.run_fp_new.setEnabled(False)

    def update_text(self, text):
        patt = self.ui_manualparams.pattern_combo.currentText()
        name = self.sender().objectName()
        name = name.split('_')
        if self.pattern_pcrs[patt].multipattern:
            if 'irfpatt' in name[0]:
                ipatt = int(name[0].replace('irfpatt', ''))
                self.pattern_pcrs[patt].replace_irf(text, ipatt)
            if 'namepatt' in name[0]:
                ipatt = int(name[0].replace('namepatt', ''))
                self.pattern_pcrs[patt].replace_names(text, ipatt)
            elif 'varyglobalpatt' in name[0]:
                ipatt = int(name[0].replace('varyglobalpatt', ''))
                self.pattern_pcrs[patt].replace_global_vary(text, ipatt)
            elif 'fixglobalpatt' in name[0]:
                ipatt = int(name[0].replace('fixglobalpatt', ''))
                self.pattern_pcrs[patt].replace_global_fix(text, ipatt)

        else:
            if 'irf' in name:
                self.pattern_pcrs[patt].replace_irf(text)
            elif 'varyglobal' in name:
                self.pattern_pcrs[patt].replace_global_vary(text)
            elif 'fixglobal' in name:
                self.pattern_pcrs[patt].replace_global_fix(text)

        if 'varyphase' in name[0]:
            num_vary = int(name[1])
            iphas = int(name[0].replace('varyphase', ''))
            identifier = self.pattern_pcrs[patt].read_ids()[iphas]
            self.pattern_pcrs[patt].replace_phase_vary(identifier, text, num_vary)
        elif 'fixphase' in name[0]:
            num_fix = int(name[1])
            iphas = int(name[0].replace('fixphase', ''))
            identifier = self.pattern_pcrs[patt].read_ids()[iphas]
            self.pattern_pcrs[patt].replace_phase_vary(identifier, text, num_fix)
        elif 'fixspc' in name[0]:
            num_fixspc = int(name[1])
            iphas = int(name[0].replace('fixspc', ''))
            identifier = self.pattern_pcrs[patt].read_ids()[iphas]
            self.pattern_pcrs[patt].replace_phase_vary(identifier, text, num_fixspc)
        elif 'pequpat' in name[0]:
            num_pequpat = int(name[1])
            iphas = int(name[0].replace('pequpat', ''))
            identifier = self.pattern_pcrs[patt].read_ids()[iphas]
            self.pattern_pcrs[patt].replace_phase_vary(identifier, text, num_pequpat)
        elif 'cations' in name[0]:
            iphas = int(name[0].replace('cations', ''))
            identifier = self.pattern_pcrs[patt].read_ids()[iphas]
            self.table_phatoms[identifier].model().beginResetModel()
            self.table_phbvsions[identifier][0].model().beginResetModel()
            self.pattern_pcrs[patt].replace_bvs_cations(identifier, text)
            self.pattern_pcrs[patt].set_correct_spc(identifier)
            self.table_phatoms[identifier].model().endResetModel()
            self.table_phbvsions[identifier][0].model().endResetModel()

        elif 'anions' in name[0]:
            iphas = int(name[0].replace('anions', ''))
            identifier = self.pattern_pcrs[patt].read_ids()[iphas]
            self.table_phatoms[identifier].model().beginResetModel()
            self.table_phbvsions[identifier][0].model().beginResetModel()
            self.pattern_pcrs[patt].replace_bvs_anions(identifier, text)
            self.pattern_pcrs[patt].set_correct_spc(identifier)
            self.table_phatoms[identifier].model().endResetModel()
            self.table_phbvsions[identifier][0].model().endResetModel()

        elif 'limval' in name[0]:
            ilim = int(name[0].replace('limval', ''))
            self.pattern_pcrs[patt].replace_limits(ilim, text)

        elif 'resval' in name[0]:
            ires = int(name[0].replace('resval', ''))
            self.pattern_pcrs[patt].replace_restraint_vals(ires, text)
        
        elif 'rescoeff' in name[0]:
            ires = int(name[0].replace('rescoeff', ''))
            self.pattern_pcrs[patt].replace_restraint_coeff(ires, text)

    def update_data(self, index, _, role):
        """ Here we capture dataChanged signals to dynamically respond
        to changes in PCR values and include extra tables"""

        patt = self.ui_manualparams.pattern_combo.currentText()
        if isinstance(self.sender(), ListTableModel):
            name = self.sender().get_name()
            data = self.sender().data(index, role[0])

            if 'table_npatt' in name:
                self.pattern_pcrs[patt].custom_pcr['npatt'][index.column() - 1] = data
            if 'table_wpatt' in name:
                self.pattern_pcrs[patt].custom_pcr['wpatt'][index.column() - 1] = data
            if 'table_params1' in name:
                """ DYNAMICALLY CHANGE BACKGROUND INFORMATION """
                if self.pattern_pcrs[patt].multipattern and index.column() == 2:
                    background = self.pattern_pcrs[patt].custom_pcr.get(f'backgroundpatt{index.row()}', None)
                
                    if isinstance(background, np.ndarray):
                        if background.shape[1] == 3 :
                            mode = BackgroundType.POINTS
                        elif background.shape[1] == 6 and background.shape[0] == 8:
                            mode = BackgroundType.CHEBYSHEV
                        elif background.shape[1] == 2 and background.shape[0] == 1:
                            mode = BackgroundType.FOURIER
                    else:
                        raise TypeError('background variable must be a ndarray')

                    if mode is BackgroundType.POINTS:
                        if int(data) > 2:
                            if int(data) > background.shape[0]:
                                zero = np.zeros((int(data) - background.shape[0], 3), dtype=float)
                                background = np.append(background, zero, axis=0)
                                self.pattern_pcrs[patt].replace_background(background, index.row(), BackgroundType.POINTS)
                            elif int(data) < background.shape[0]:
                                background = background[:int(data), :]
                                self.pattern_pcrs[patt].replace_background(background, index.row(), BackgroundType.POINTS)

                        elif int(data) == -5:
                            self.pattern_pcrs[patt].transform_anchor_to_cheb(6, index.row())

                        elif int(data) == -2:
                            thmin = self.pattern_pcrs[patt].get_flags_global('thmin', index.row())
                            step = self.pattern_pcrs[patt].get_flags_global('step', index.row())
                            thmax = self.pattern_pcrs[patt].get_flags_global('thmax', index.row())

                            N = int((thmax - thmin)/(step * 5))
                            self.pattern_pcrs[patt].save_background(index.row())
                            self.pattern_pcrs[patt].replace_background(np.array([[N, 1]], dtype=int), index.row(), BackgroundType.FOURIER)
                            
                        else:
                            self.warning.setIcon(qtw.QMessageBox.Critical)
                            self.warning.setText("Value of Nba parameter not implemented")
                            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                            self.warning.exec_()
                            self.pattern_pcrs[patt].edit_flags_global('nba', background.shape[0], index.row())

                    elif mode is BackgroundType.CHEBYSHEV:
                        if int(data) > 2:
                            thmin = self.pattern_pcrs[patt].get_flags_global('thmin', index.row())
                            step = self.pattern_pcrs[patt].get_flags_global('step', index.row())
                            thmax = self.pattern_pcrs[patt].get_flags_global('thmax', index.row())

                            x = np.arange(thmin, thmax, step)
                            self.pattern_pcrs[patt].transform_cheb_to_anchor(x, index.row())
      
                        elif int(data) == -5:
                            pass

                        elif int(data) == -2:
                            thmin = self.pattern_pcrs[patt].get_flags_global('thmin', index.row())
                            step = self.pattern_pcrs[patt].get_flags_global('step', index.row())
                            thmax = self.pattern_pcrs[patt].get_flags_global('thmax', index.row())

                            N = int((thmax - thmin)/(step * 5))
                            self.pattern_pcrs[patt].save_background(index.row())
                            self.pattern_pcrs[patt].replace_background(np.array([[N, 1]], dtype=int), index.row(), BackgroundType.FOURIER)
        
                        else:
                            self.warning.setIcon(qtw.QMessageBox.Critical)
                            self.warning.setText("Value of Nba parameter not implemented")
                            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                            self.warning.exec_()
                            self.pattern_pcrs[patt].edit_flags_global('nba', -5, index.row())
                    
                    elif mode is BackgroundType.FOURIER:
                        if int(data) == -2:
                            pass
                        else:
                            self.warning.setIcon(qtw.QMessageBox.Critical)
                            self.warning.setText("PCR is set with fourier filtered background, can't transform to any other mode")
                            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                            self.warning.exec_()
                            self.pattern_pcrs[patt].edit_flags_global('nba', -2, index.row())

                elif not self.pattern_pcrs[patt].multipattern and index.column() == 3:
                    background = self.pattern_pcrs[patt].custom_pcr.get(f'background', None)

                    if isinstance(background, np.ndarray):
                        if background.shape[1] == 3 :
                            mode = BackgroundType.POINTS
                        elif background.shape[1] == 6 and background.shape[0] == 8:
                            mode = BackgroundType.CHEBYSHEV
                        elif background.shape[1] == 2 and background.shape[0] == 1:
                            mode = BackgroundType.FOURIER
                    else:
                        raise TypeError('background variable must be a ndarray')

                    if mode is BackgroundType.POINTS:
                        if int(data) > 2:
                            if int(data) > background.shape[0]:
                                zero = np.zeros((int(data) - background.shape[0], 3), dtype=float)
                                background = np.append(background, zero, axis=0)
                                self.pattern_pcrs[patt].replace_background(background, mode = BackgroundType.POINTS)
                            elif int(data) < background.shape[0]:
                                background = background[:int(data), :]
                                self.pattern_pcrs[patt].replace_background(background, mode = BackgroundType.POINTS)

                        elif int(data) == -5:
                            self.pattern_pcrs[patt].transform_anchor_to_cheb(6, index.row())
               
                        elif int(data) == -2:
                            thmin = self.pattern_pcrs[patt].get_flags_global('thmin', index.row())
                            step = self.pattern_pcrs[patt].get_flags_global('step', index.row())
                            thmax = self.pattern_pcrs[patt].get_flags_global('thmax', index.row())

                            N = int((thmax - thmin)/(step * 5))
                            self.pattern_pcrs[patt].save_background(index.row())
                            self.pattern_pcrs[patt].replace_background(np.array([[N, 1]], dtype=int), index.row(), BackgroundType.FOURIER)
                    
                        else:
                            self.warning.setIcon(qtw.QMessageBox.Critical)
                            self.warning.setText("Value of Nba parameter not implemented")
                            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                            self.warning.exec_()
                            self.pattern_pcrs[patt].edit_flags_global('nba', background.shape[0])

                    elif mode is BackgroundType.CHEBYSHEV:
                        if int(data) > 2:
                            thmin = self.pattern_pcrs[patt].get_flags_global('thmin', index.row())
                            step = self.pattern_pcrs[patt].get_flags_global('step', index.row())
                            thmax = self.pattern_pcrs[patt].get_flags_global('thmax', index.row())

                            x = np.arange(thmin, thmax, step)
                            self.pattern_pcrs[patt].transform_cheb_to_anchor(x, index.row())
           
                        elif int(data) == -5:
                            pass

                        elif int(data) == -2:
                            thmin = self.pattern_pcrs[patt].get_flags_global('thmin', index.row())
                            step = self.pattern_pcrs[patt].get_flags_global('step', index.row())
                            thmax = self.pattern_pcrs[patt].get_flags_global('thmax', index.row())

                            N = int((thmax - thmin)/(step * 5))
                            self.pattern_pcrs[patt].save_background(index.row())
                            self.pattern_pcrs[patt].replace_background(np.array([[N, 1]], dtype=int), index.row(), BackgroundType.FOURIER)
                            
                        else:
                            self.warning.setIcon(qtw.QMessageBox.Critical)
                            self.warning.setText("Value of Nba parameter not implemented")
                            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                            self.warning.exec_()
                            self.pattern_pcrs[patt].edit_flags_global('nba', -5)
                    
                    elif mode is BackgroundType.FOURIER:
                        if int(data) == -2:
                            pass
                        else:
                            self.warning.setIcon(qtw.QMessageBox.Critical)
                            self.warning.setText("PCR is set with fourier filtered background, can't transform to any other mode")
                            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                            self.warning.exec_()
                            self.pattern_pcrs[patt].edit_flags_global('nba', -2)

                """ DYNAMICALLY CHANGE EXCLUDED REGIONS INFORMATION """
                if self.pattern_pcrs[patt].multipattern and index.column() == 3:
                    exclude = self.pattern_pcrs[patt].custom_pcr.get(f'excludepatt{index.row()}', None)
                    if int(data) < 0:
                        self.warning.setIcon(qtw.QMessageBox.Critical)
                        self.warning.setText("Can't have negative Nex value")
                        self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                        self.warning.exec_()
                        if exclude is not None:
                            self.pattern_pcrs[patt].edit_flags_global('nex', exclude.shape[0], index.row())
                        else:
                            self.pattern_pcrs[patt].edit_flags_global('nex', 0, index.row())
                    elif int(data) == 0:
                        if exclude is not None:
                            self.pattern_pcrs[patt].remove_excluded(index.row())
                            
                    else:
                        if exclude is not None:
                            if int(data) > exclude.shape[0]:
                                zero = np.zeros((int(data) - exclude.shape[0], 2), dtype=float)
                                exclude = np.append(exclude, zero, axis=0)
                                self.pattern_pcrs[patt].replace_excluded(exclude, index.row())
                            elif int(data) < exclude.shape[0]:
                                exclude = exclude[:int(data), :]
                                self.pattern_pcrs[patt].replace_excluded(exclude, index.row())
                        else:
                            exclude = np.zeros((int(data), 2), dtype=float)
                            self.pattern_pcrs[patt].replace_excluded(exclude, index.row())

                elif not self.pattern_pcrs[patt].multipattern and index.column() == 4:
                    exclude = self.pattern_pcrs[patt].custom_pcr.get(f'exclude', None)
                    if int(data) < 0:
                        self.warning.setIcon(qtw.QMessageBox.Critical)
                        self.warning.setText("Can't have negative Nex value")
                        self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                        self.warning.exec_()
                        if exclude is not None:
                            self.pattern_pcrs[patt].edit_flags_global('nex', exclude.shape[0])
                        else:
                            self.pattern_pcrs[patt].edit_flags_global('nex', 0)
                    elif int(data) == 0:
                        if exclude is not None:
                            self.pattern_pcrs[patt].remove_excluded()
                            
                    else:
                        if exclude is not None:
                            if int(data) > exclude.shape[0]:
                                zero = np.zeros((int(data) - exclude.shape[0], 2), dtype=float)
                                exclude = np.append(exclude, zero, axis=0)
                                self.pattern_pcrs[patt].replace_excluded(exclude)
                            elif int(data) < exclude.shape[0]:
                                exclude = exclude[:int(data), :]
                                self.pattern_pcrs[patt].replace_excluded(exclude)
                        else:
                            exclude = np.zeros((int(data), 2), dtype=float)
                            self.pattern_pcrs[patt].replace_excluded(exclude)

            if 'table_params1' in name and role[0] == qtc.Qt.EditRole:
                if ('global' in name and 'pattern' not in name and index.column() == 3) or\
                        ('global' not in name and 'pattern' not in name and index.column() == 13):
                    if int(data) < 0:
                        self.warning.setIcon(qtw.QMessageBox.Critical)
                        self.warning.setText("Can't have negative Nre value")
                        self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                        self.warning.exec_()
                        nre = self.pattern_pcrs[patt].count_limits()
                        self.pattern_pcrs[patt].edit_flags_global('nre', nre)
                    elif int(data) == 0:
                        self.pattern_pcrs[patt].remove_limits()
                    elif int(data) > 0:
                        nre = self.pattern_pcrs[patt].count_limits()
                        if int(data) > nre:
                            for i in range(nre, int(data)):
                                vals = ['299', '0.0000', '1.0000', '0.0000', '0']
                                self.pattern_pcrs[patt].add_limits(vals)
                        elif int(data) < nre:
                            for i in range(nre - int(data)):
                                self.pattern_pcrs[patt].remove_limits_one()

            if 'table_params2' in name and role[0] == qtc.Qt.EditRole:
                if ('global' in name and 'pattern' not in name and index.column() == 2) or\
                        ('global' not in name and 'pattern' not in name and index.column() == 8):
                    if int(data) < 0:
                        self.warning.setIcon(qtw.QMessageBox.Critical)
                        self.warning.setText("Can't have negative NLI value")
                        self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                        self.warning.exec_()
                        nli = self.pattern_pcrs[patt].count_restraint()
                        self.pattern_pcrs[patt].edit_flags_global('nli', nli)
                    elif int(data) == 0:
                        self.pattern_pcrs[patt].remove_restraint()
                    elif int(data) > 0:
                        nli = self.pattern_pcrs[patt].count_restraint()
                        if int(data) > nli:
                            for i in range(nli, int(data)):
                                vals = [f'Restraint{i}', '2', '0.0000', '0.0000']
                                coeffs = ['1.0000', '299', '1.0000', '298']
                                self.pattern_pcrs[patt].add_restraint([vals, coeffs])

                        elif int(data) < nli:
                            for i in range(nli - int(data)):
                                self.pattern_pcrs[patt].remove_restraint_one()

            if 'table_params5' in name and role[0] == qtc.Qt.CheckStateRole:
                if self.pattern_pcrs[patt].multipattern:
                    ipatt = int(name.replace('table_params5_patt', ''))
                    if data == qtc.Qt.Checked:
                        self.pattern_pcrs[patt].replace_zero_more(ipatt)
                    else:
                        self.pattern_pcrs[patt].remove_zero_more(ipatt)

                else:
                    if data == qtc.Qt.Checked:
                        self.pattern_pcrs[patt].replace_zero_more()
                    else:
                        self.pattern_pcrs[patt].remove_zero_more()

            if isinstance(name, tuple) and 'table_phparams1global' in name[1] and role[0] == qtc.Qt.CheckStateRole:
                identifier = self.pattern_pcrs[patt].read_ids()[name[0]]
                if data == qtc.Qt.Checked:
                    self.pattern_pcrs[patt].add_phase_more(identifier)
                else:
                    self.pattern_pcrs[patt].remove_phase_more(identifier)

            if isinstance(name, tuple) and 'table_phparams1global' in name[1] and role[0] == qtc.Qt.EditRole and index.column() == 0:

                identifier = self.pattern_pcrs[patt].read_ids()[name[0]]
                if int(data) < 0:
                    self.warning.setIcon(qtw.QMessageBox.Critical)
                    self.warning.setText("Can't have negative Nat value")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    self.warning.exec_()
                    nat = self.pattern_pcrs[patt].count_site_phase(identifier)
                    self.pattern_pcrs[patt].edit_flags_phase(identifier, 'nat', nat)
                elif int(data) == 0:
                    self.pattern_pcrs[patt].remove_sites(identifier)
                else:
                    nat = self.pattern_pcrs[patt].count_site_phase(identifier)
                    if int(data) < nat:
                        labels = self.pattern_pcrs[patt].read_site_labels(identifier)
                        labels = labels[int(data):]
                        for atom in labels:
                            self.pattern_pcrs[patt].remove_site_phase(identifier, atom)
                            self.pattern_pcrs[patt].reformat_atom_keys(identifier)
                    elif int(data) > nat:
                        labels = self.pattern_pcrs[patt].read_site_labels(identifier)
                        if labels is not None:
                            xnum = sum('X' in item for item in labels)
                        else:
                            xnum = 0

                        for i in range(int(data) - nat):
                            xnum += 1
                            self.pattern_pcrs[patt].add_site_phase(identifier, f"X{xnum}")

            if isinstance(name, tuple) and 'table_phparams1more' in name[1] and role[0] == qtc.Qt.CheckStateRole and index.column() == 1:
                identifier = self.pattern_pcrs[patt].read_ids()[name[0]]
                atoms = self.pattern_pcrs[patt].read_site_labels(identifier)
                if atoms is not None:
                    if data == qtc.Qt.Checked:
                        self.pattern_pcrs[patt].add_bvs(identifier)
                    else:
                        self.pattern_pcrs[patt].remove_bvs(identifier)

            if isinstance(name, tuple) and 'table_phparams1more' in name[1] and role[0] == qtc.Qt.CheckStateRole and index.column() == 12:
                identifier = self.pattern_pcrs[patt].read_ids()[name[0]]
                if data == qtc.Qt.Checked:
                    self.pattern_pcrs[patt].add_phshifts(identifier)
                else:
                    self.pattern_pcrs[patt].remove_phshifts(identifier)

            if isinstance(name, tuple) and 'table_phbvs' in name[1] and role[0] == qtc.Qt.CheckStateRole:
                identifier = self.pattern_pcrs[patt].read_ids()[name[0]]
                atoms = self.pattern_pcrs[patt].read_site_labels(identifier)
                if atoms is not None:
                    if data == qtc.Qt.Checked:
                        self.pattern_pcrs[patt].add_bvs_ions(identifier)
                    else:
                        self.pattern_pcrs[patt].remove_bvs_ions(identifier)

            if isinstance(name, tuple) and 'table_phparams1_1_patt' in name[1] and role[0] == qtc.Qt.CheckStateRole:
                identifier = self.pattern_pcrs[patt].read_ids()[name[0]]
                ipatt = int(name[1].replace('table_phparams1_1_patt', ''))
                if data == qtc.Qt.Checked:
                    self.pattern_pcrs[patt].add_phshifts(identifier, ipatt)
                else:
                    self.pattern_pcrs[patt].remove_phshifts(identifier, ipatt)

            if isinstance(name, tuple) and 'table_phatoms' in name[1] and role[0] == qtc.Qt.CheckStateRole:
                identifier = self.pattern_pcrs[patt].read_ids()[name[0]]
                ida = index.sibling(index.row(), 0)
                atom = self.sender().data(ida, qtc.Qt.DisplayRole)
                if data == qtc.Qt.Checked:
                    self.pattern_pcrs[patt].add_anisotropic_site(identifier, atom)
                else:
                    self.pattern_pcrs[patt].remove_anisotropic_site(identifier, atom)

            if isinstance(name, tuple) and 'table_phprof1' in name[1] and role[0] == qtc.Qt.CheckStateRole:
                identifier = self.pattern_pcrs[patt].read_ids()[name[0]]
                if self.pattern_pcrs[patt].multipattern:
                    ipatt = int(name[1].replace('table_phprof1_patt', ''))
                    if data == qtc.Qt.Checked:
                        self.pattern_pcrs[patt].add_strain_model(identifier, ipatt)
                    else:
                        self.pattern_pcrs[patt].remove_strain_model(identifier, ipatt)
                        
                else:
                    if data == qtc.Qt.Checked:
                        self.pattern_pcrs[patt].add_strain_model(identifier)
                    else:
                        self.pattern_pcrs[patt].remove_strain_model(identifier)
                        
            if isinstance(name, tuple) and 'table_phprof2' in name[1] and role[0] == qtc.Qt.CheckStateRole:
                identifier = self.pattern_pcrs[patt].read_ids()[name[0]]
                if self.pattern_pcrs[patt].multipattern:
                    ipatt = int(name[1].replace('table_phprof2_patt', ''))
                    if data == qtc.Qt.Checked:
                        self.pattern_pcrs[patt].add_size_model(identifier, ipatt)
                    else:
                        self.pattern_pcrs[patt].remove_size_model(identifier, ipatt)
                        
                else:
                    if data == qtc.Qt.Checked:
                        self.pattern_pcrs[patt].add_size_model(identifier)
                    else:
                        self.pattern_pcrs[patt].remove_size_model(identifier)
        
        self.change_pcr_view()

    def change_pcr_view(self):
        self.clear_widgets()
        self.reset_widgets()
        patt = self.ui_manualparams.pattern_combo.currentText()
        if patt in self.pattern_pcrs:
            self.set_widgets()

    def reset_all(self):
        self.pattern_pcrs = dict()
        self.reset_widgets()

    def clear_widgets(self):
        if self.ui_manualparams.widget_layout.count() != 0:
            for i in reversed(range(self.ui_manualparams.widget_layout.count() - 1)):
                widget = self.ui_manualparams.widget_layout.itemAt(i).widget()
                if widget is not None:
                    widget.deleteLater()
                
    def add_widgets(self):
        self.ui_manualparams.widget_layout.addWidget(self.line_comm)
        if self.table_npatt is not None:
            self.ui_manualparams.widget_layout.addWidget(self.table_npatt)
        if self.table_wpatt is not None:
            self.ui_manualparams.widget_layout.addWidget(self.table_wpatt)
        if isinstance(self.table_params1, list):
            self.ui_manualparams.widget_layout.addWidget(self.table_params1[0])
            self.ui_manualparams.widget_layout.addWidget(self.table_params1[1])
        else:
            self.ui_manualparams.widget_layout.addWidget(self.table_params1)
        if self.line_names is not None:
            for line in self.line_names:
                self.ui_manualparams.widget_layout.addWidget(line)
        if self.line_irf is not None:
            for line in self.line_irf:
                self.ui_manualparams.widget_layout.addWidget(line)
        if isinstance(self.table_params2, list):
            self.ui_manualparams.widget_layout.addWidget(self.table_params2[0])
            self.ui_manualparams.widget_layout.addWidget(self.table_params2[1])
        else:
            self.ui_manualparams.widget_layout.addWidget(self.table_params2)
        if self.table_params3 is not None:
            for i, table in enumerate(self.table_params3):
                self.ui_manualparams.widget_layout.addWidget(table)
                if self.line_vary_global[i] is not None:
                    self.ui_manualparams.widget_layout.addWidget(self.line_vary_global[i])
                if self.line_fix_global[i] is not None:
                    self.ui_manualparams.widget_layout.addWidget(self.line_fix_global[i])
        if self.table_params4 is not None:
            for table in self.table_params4:
                self.ui_manualparams.widget_layout.addWidget(table)
        if self.table_background is not None:
            for i, table in enumerate(self.table_background):
                if table is not None:
                    self.ui_manualparams.widget_layout.addWidget(table)
                if self.table_excluded[i] is not None:
                    self.ui_manualparams.widget_layout.addWidget(self.table_excluded[i])
        if self.line_number is not None:
            self.ui_manualparams.widget_layout.addWidget(self.line_number)
        if self.table_params5 is not None:
            for i, table in enumerate(self.table_params5):
                self.ui_manualparams.widget_layout.addWidget(table)
                if self.table_params5more[i] is not None:
                    self.ui_manualparams.widget_layout.addWidget(self.table_params5more[i])
                if self.table_chebyshev[i] is not None:
                    self.ui_manualparams.widget_layout.addWidget(qtw.QLabel('!   Background coefficients/codes'))
                    self.ui_manualparams.widget_layout.addWidget(self.table_chebyshev[i])
                if self.table_fourier[i] is not None:
                    self.ui_manualparams.widget_layout.addWidget(self.table_fourier[i])

        for i, keys in enumerate(self.line_phidentifiers.keys()):
            self.ui_manualparams.widget_layout.addWidget(qtw.QLabel('!------------------------------------------------------------------------------'))
            self.ui_manualparams.widget_layout.addWidget(qtw.QLabel(f'!                                 PHASE: {i + 1}'))
            self.ui_manualparams.widget_layout.addWidget(qtw.QLabel('!------------------------------------------------------------------------------'))
            self.ui_manualparams.widget_layout.addWidget(self.line_phidentifiers[keys])
            self.ui_manualparams.widget_layout.addWidget(self.table_phparams1global[keys])
            if self.table_phparams1more[keys] is not None:
                self.ui_manualparams.widget_layout.addWidget(self.table_phparams1more[keys])
            if self.table_phcontributions[keys] is None:
                self.ui_manualparams.widget_layout.addWidget(self.table_phcontributions[keys])
            if self.table_phparams1_1[keys] is not None and self.table_phparams1_2[keys] is not None:
                for tables in zip(self.table_phparams1_1[keys], self.table_phparams1_2[keys]):
                    if tables[0] is not None:
                        self.ui_manualparams.widget_layout.addWidget(tables[0])
                    if tables[1] is not None:
                        self.ui_manualparams.widget_layout.addWidget(tables[1])
            if self.table_phbvs[keys] is not None:
                self.ui_manualparams.widget_layout.addWidget(self.table_phbvs[keys])
            if self.table_phbvsions[keys] is not None:
                for item in self.table_phbvsions[keys]:
                    self.ui_manualparams.widget_layout.addWidget(item)
            self.ui_manualparams.widget_layout.addWidget(self.line_phspacegroup[keys])
            if self.table_phatoms[keys] is not None:
                self.ui_manualparams.widget_layout.addWidget(self.table_phatoms[keys])

            tables = [self.table_phprof1[keys], self.table_phprof2[keys], self.table_phprof3[keys],
                      self.table_phprof4[keys], self.table_phprof5[keys], self.table_phabscor[keys],
                      self.table_phshifts[keys], self.table_phsizemodel[keys], self.table_phstrainmodel[keys]]
            if all(item is not None for item in tables):
                for profile in zip(*tables):
                    if profile[0] is not None:
                        self.ui_manualparams.widget_layout.addWidget(profile[0])
                    if profile[1] is not None:
                        self.ui_manualparams.widget_layout.addWidget(profile[1])
                    if profile[2] is not None:
                        self.ui_manualparams.widget_layout.addWidget(profile[2])
                    if profile[3] is not None:
                        self.ui_manualparams.widget_layout.addWidget(profile[3])
                    if profile[4] is not None:
                        self.ui_manualparams.widget_layout.addWidget(profile[4])
                    if profile[5] is not None:
                        self.ui_manualparams.widget_layout.addWidget(profile[5])
                    if profile[6] is not None:
                        self.ui_manualparams.widget_layout.addWidget(profile[6])
                    if profile[7] is not None:
                        for table in profile[7]:
                            if table is not None:
                                self.ui_manualparams.widget_layout.addWidget(table)
                    if profile[8] is not None:
                        for table in profile[8]:
                            if table is not None:
                                self.ui_manualparams.widget_layout.addWidget(table)

        if self.lines_limits:
            self.ui_manualparams.widget_layout.addWidget(qtw.QLabel(f'! Limits for selected parameters:'))
            for limit in self.lines_limits:
                self.ui_manualparams.widget_layout.addWidget(limit)
        if self.lines_restraints:
            self.ui_manualparams.widget_layout.addWidget(
                qtw.QLabel(f'! Set of {len(self.lines_restraints)} linear restraints'))
            for restr in self.lines_restraints:
                self.ui_manualparams.widget_layout.addWidget(restr)

        self.ui_manualparams.widget_layout.addStretch()

    def reset_widgets(self):
        self.line_comm = None
        self.table_npatt = None
        self.table_wpatt = None
        self.table_params1 = None
        self.line_names = None
        self.line_irf = None
        self.table_params2 = None
        self.table_params3 = None
        self.line_vary_global = None
        self.line_fix_global = None
        self.table_params4 = None
        self.table_background = None
        self.table_fourier = None
        self.table_excluded = None
        self.line_number = None
        self.table_params5 = None
        self.table_params5more = None
        self.line_phidentifiers = None  # All objects starting from ph, are dicts with identifiers as keys
        self.lines_phcommands = None
        self.table_phparams1global = None
        self.table_phparams1more = None
        self.table_phcontributions = None
        self.table_phparams1_1 = None
        self.table_phparams1_2 = None
        self.table_phbvs = None
        self.table_phbvsions = None
        self.line_phspacegroup = None
        self.table_phatoms = None
        self.table_phprof1 = None
        self.table_phprof2 = None
        self.table_phprof3 = None
        self.table_phprof4 = None
        self.table_phprof5 = None
        self.table_phabscor = None
        self.table_phshifts = None
        self.table_phstrainmodel = None
        self.table_phsizemodel = None
        self.lines_limits = None
        self.lines_restraints = None

    def set_widgets(self):
        patt = self.ui_manualparams.pattern_combo.currentText()
        self.set_line_comm(patt)
        self.set_table_npatt(patt)
        self.set_table_wpatt(patt)
        self.set_table_params1(patt)
        self.set_line_names(patt)
        self.set_line_irf(patt)
        self.set_table_params2(patt)
        self.set_table_params3(patt)
        self.set_line_vary_global(patt)
        self.set_line_fix_global(patt)
        self.set_table_params4(patt)
        self.set_table_background(patt)
        self.set_table_fourier(patt)
        self.set_table_chebyshev(patt)
        self.set_table_excluded(patt)
        self.set_line_number(patt)
        self.set_table_params5(patt)
        self.set_table_params5more(patt)
        self.set_line_phidentifiers(patt)
        self.set_lines_phcommands(patt)
        self.set_table_phparams1global(patt)
        self.set_table_phparams1more(patt)
        self.set_table_phcontributions(patt)
        self.set_table_phparams1_1(patt)
        self.set_table_phparams1_2(patt)
        self.set_table_phbvs(patt)
        self.set_table_phbvsions(patt)
        self.set_line_phspacegroup(patt)
        self.set_table_phatoms(patt)
        self.set_table_phprof1(patt)
        self.set_table_phprof1(patt)
        self.set_table_phprof2(patt)
        self.set_table_phprof3(patt)
        self.set_table_phprof4(patt)
        self.set_table_phprof5(patt)
        self.set_table_phabscor(patt)
        self.set_table_phshifts(patt)
        self.set_table_phstrainmodel(patt)
        self.set_table_phsizemodel(patt)
        self.set_lines_limits(patt)
        self.set_lines_restraints(patt)

        self.add_widgets()

    def set_line_comm(self, patt: str):
        self.line_comm = qtw.QLabel()
        self.line_comm.setText(self.pattern_pcrs[patt].custom_pcr['comment'])

    def set_table_npatt(self, patt: str):
        if self.pattern_pcrs[patt].multipattern:
            model = ListTableModel([['NPATT'] + self.pattern_pcrs[patt].custom_pcr['npatt']], edit=[0])
            model.set_name('table_npatt')
            model.dataChanged.connect(self.update_data)
            self.table_npatt = ListTableView()
            self.table_npatt.setModel(model)
            self.table_npatt.resizeRowsToContents()
            self.table_npatt.resizeColumnsToContents()
            self.table_npatt.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            self.table_npatt = self.resizeVerticalTableViewToContents(self.table_npatt)

    def set_table_wpatt(self, patt: str):
        if self.pattern_pcrs[patt].multipattern:
            model = ListTableModel([['W_PAT'] + self.pattern_pcrs[patt].custom_pcr['wpatt']], edit=[0])
            model.set_name('table_wpatt')
            model.dataChanged.connect(self.update_data)
            self.table_wpatt = ListTableView()
            self.table_wpatt.setModel(model)
            self.table_wpatt.resizeRowsToContents()
            self.table_wpatt.resizeColumnsToContents()
            self.table_wpatt.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            self.table_wpatt = self.resizeVerticalTableViewToContents(self.table_wpatt)

    def set_table_params1(self, patt: str):
        if self.pattern_pcrs[patt].multipattern:
            header1 = 'Nph Dum Ias Nre Cry Opt Aut'.split()
            header2 = 'Job Npr Nba Nex Nsc Nor Iwg Ilo Res Ste Uni Cor Anm Int'.split()

            edit = [0, 1, 2, 4, 5, 6]
            model1 = ListTableModel([self.pattern_pcrs[patt].custom_pcr['params1global']], header1, edit)
            model1.set_name('table_params1_global')
            model1.dataChanged.connect(self.update_data)
            table1 = ListTableView()
            table1.setModel(model1)
            table1.resizeColumnsToContents()
            table1.resizeRowsToContents()
            table1.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table1 = self.resizeVerticalTableViewToContents(table1)

            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            edit = [0, 1, 4, 5, 6, 10, 11, 12, 13]
            model2 = ListTableModel(
                [self.pattern_pcrs[patt].custom_pcr[f'params1patt{ipatt}'] for ipatt in range(npatt)], header2, edit)
            model2.set_name('table_params1_pattern')
            model2.dataChanged.connect(self.update_data)
            table2 = ListTableView()
            table2.setModel(model2)
            table2.resizeColumnsToContents()
            table2.resizeRowsToContents()
            table2.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table2.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table2 = self.resizeVerticalTableViewToContents(table2)

            self.table_params1 = [table1, table2]

        else:
            header = 'Job Npr Nph Nba Nex Nsc Nor Dum Iwg Ilo Ias Res Ste Nre Cry Uni Cor Opt Aut'.split()
            edit = [0, 1, 2, 5, 6, 7, 8, 10, 14, 15, 16, 17, 18]
            model = ListTableModel([self.pattern_pcrs[patt].custom_pcr['params1']], header, edit)
            model.set_name('table_params1')
            model.dataChanged.connect(self.update_data)
            self.table_params1 = ListTableView()
            self.table_params1.setModel(model)
            self.table_params1.resizeRowsToContents()
            self.table_params1.resizeColumnsToContents()
            self.table_params1.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            self.table_params1 = self.resizeVerticalTableViewToContents(self.table_params1)

    def set_line_names(self, patt: str):
        if self.pattern_pcrs[patt].multipattern:
            self.line_names = []
            self.line_names.append(qtw.QLabel('!File names of data(patterns) files'))
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                name = qtw.QLineEdit()
                name.setObjectName(f'namepatt{ipatt}')
                name.textEdited.connect(self.update_text)
                name.setText(self.pattern_pcrs[patt].custom_pcr[f'namepatt{ipatt}'])

                self.line_names.append(name)

    def set_line_irf(self, patt: str):
        self.line_irf = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                res = self.pattern_pcrs[patt].get_flags_global('res', ipatt)
                if res != 0:
                    self.line_irf.append(qtw.QLabel(f"!  Resolution file for Pattern#   {ipatt}"))
                    irf = qtw.QLineEdit()
                    irf.setObjectName(f'irfpatt{ipatt}')
                    irf.textEdited.connect(self.update_text)
                    irf.setText(self.pattern_pcrs[patt].custom_pcr[f'irfpatt{ipatt}'])
                    self.line_irf.append(irf)

            if len(self.line_irf) == 0:
                self.line_irf = None

        else:
            res = self.pattern_pcrs[patt].get_flags_global('res')
            if res != 0:
                self.line_irf.append(qtw.QLabel(f"!  Resolution file for Pattern#   1"))
                irf = qtw.QLineEdit()
                irf.setObjectName(f'irf')
                irf.textEdited.connect(self.update_text)
                irf.setText(self.pattern_pcrs[patt].custom_pcr[f'irf'])
                self.line_irf.append(irf)

            if len(self.line_irf) == 0:
                self.line_irf = None

    def set_table_params2(self, patt: str):
        if self.pattern_pcrs[patt].multipattern:
            header1 = 'Mat Pcr NLI Rpa Sym Sho'.split()
            header2 = 'Ipr Ppl Ioc Ls1 Ls2 Ls3 Prf Ins Hkl Fou Ana'.split()

            edit = [0, 1, 3, 4, 5]
            model1 = ListTableModel([self.pattern_pcrs[patt].custom_pcr['params2global']], header1, edit)
            model1.set_name('table_params2_global')
            model1.dataChanged.connect(self.update_data)
            table1 = ListTableView()
            table1.setModel(model1)
            table1.resizeColumnsToContents()
            table1.resizeRowsToContents()
            table1.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table1 = self.resizeVerticalTableViewToContents(table1)

            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            edit = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            model2 = ListTableModel(
                [self.pattern_pcrs[patt].custom_pcr[f'params2patt{ipatt}'] for ipatt in range(npatt)], header2, edit)
            model2.set_name('table_params2_pattern')
            model2.dataChanged.connect(self.update_data)
            table2 = ListTableView()
            table2.setModel(model2)
            table2.resizeColumnsToContents()
            table2.resizeRowsToContents()
            table2.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table2.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table2 = self.resizeVerticalTableViewToContents(table2)

            self.table_params2 = [table1, table2]

        else:
            header = 'Ipr Ppl Ioc Mat Pcr Ls1 Ls2 Ls3 NLI Prf Ins Rpa Sym Hkl Fou Sho Ana'.split()
            edit = [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16]
            model = ListTableModel([self.pattern_pcrs[patt].custom_pcr['params2']], header, edit)
            model.set_name('table_params2')
            model.dataChanged.connect(self.update_data)
            self.table_params2 = ListTableView()
            self.table_params2.setModel(model)
            self.table_params2.resizeRowsToContents()
            self.table_params2.resizeColumnsToContents()
            self.table_params2.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            self.table_params2 = self.resizeVerticalTableViewToContents(self.table_params2)

    def set_table_params3(self, patt: str):
        self.table_params3 = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                if job < 0:
                    header = 'Bkpos        Wdt    Iabscor '.split()
                else:
                    header = 'Lambda1 Lambda2 Ratio Bkpos Wdt Cthm muR AsyLim Rpolarz 2nd-muR'.split()
                model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'params3patt{ipatt}']], header)
                model.set_name(f'table_params3_patt{ipatt}')
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)

                self.table_params3.append(table)

        else:
            job = self.pattern_pcrs[patt].get_flags_global('job')
            if job < 0:
                header = 'Bkpos        Wdt    Iabscor '.split()
            else:
                header = 'Lambda1 Lambda2 Ratio Bkpos Wdt Cthm muR AsyLim Rpolarz 2nd-muR'.split()
            model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'params3']], header)
            model.set_name(f'table_params3')
            model.dataChanged.connect(self.update_data)
            table = ListTableView()
            table.setModel(model)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table = self.resizeVerticalTableViewToContents(table)

            self.table_params3.append(table)

    def set_line_vary_global(self, patt: str):
        self.line_vary_global = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                if f'varyglobalpatt{ipatt}' in self.pattern_pcrs[patt].custom_pcr:
                    vary = qtw.QLineEdit()
                    vary.setObjectName(f'varyglobalpatt{ipatt}')
                    vary.textEdited.connect(self.update_text)
                    vary.setText(self.pattern_pcrs[patt].custom_pcr[f'varyglobalpatt{ipatt}'])
                    self.line_vary_global.append(vary)
                else:
                    self.line_vary_global.append(None)
        else:
            if f'varyglobal' in self.pattern_pcrs[patt].custom_pcr:
                vary = qtw.QLineEdit()
                vary.setObjectName(f'varyglobal')
                vary.textEdited.connect(self.update_text)
                vary.setText(self.pattern_pcrs[patt].custom_pcr[f'varyglobal'])
                self.line_vary_global.append(vary)
            else:
                self.line_vary_global.append(None)

    def set_line_fix_global(self, patt: str):
        self.line_fix_global = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                if f'fixglobalpatt{ipatt}' in self.pattern_pcrs[patt].custom_pcr:
                    fix = qtw.QLineEdit()
                    fix.setObjectName(f'fixglobalpatt{ipatt}')
                    fix.textEdited.connect(self.update_text)
                    fix.setText(self.pattern_pcrs[patt].custom_pcr[f'fixglobalpatt{ipatt}'])
                    self.line_fix_global.append(fix)
                else:
                    self.line_fix_global.append(None)
        else:
            if f'fixglobal' in self.pattern_pcrs[patt].custom_pcr:
                fix = qtw.QLineEdit()
                fix.setObjectName(f'fixglobal')
                fix.textEdited.connect(self.update_text)
                fix.setText(self.pattern_pcrs[patt].custom_pcr[f'fixglobal'])
                self.line_fix_global.append(fix)
            else:
                self.line_fix_global.append(None)

    def set_table_params4(self, patt: str):
        self.table_params4 = []
        if self.pattern_pcrs[patt].multipattern:
            header = 'NCY  Eps  R_at  R_an  R_pr  R_gl'.split()

            model = ListTableModel([self.pattern_pcrs[patt].custom_pcr['params4global']], header)
            model.set_name(f'table_params4_global')
            model.dataChanged.connect(self.update_data)
            table = ListTableView()
            table.setModel(model)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table = self.resizeVerticalTableViewToContents(table)
            self.table_params4.append(table)

            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                if job < 0:
                    header = 'TOF-min      <Step>       TOF-max'.split()
                else:
                    header = 'Thmin       Step       Thmax    PSD    Sent0'.split()

                model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'params4patt{ipatt}']], header)
                model.set_name(f'table_params4_patt{ipatt}')
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)
                self.table_params4.append(table)

        else:
            job = self.pattern_pcrs[patt].get_flags_global('job')
            if job < 0:
                header = 'NCY  Eps  R_at  R_an  R_pr  R_gl  TOF-min   <Step>   TOF-max'.split()
            else:
                header = 'NCY  Eps  R_at  R_an  R_pr  R_gl  Thmin   Step  Thmax PSD Sent0'.split()

            model = ListTableModel([self.pattern_pcrs[patt].custom_pcr['params4']], header)
            model.set_name(f'table_params4')
            model.dataChanged.connect(self.update_data)
            table = ListTableView()
            table.setModel(model)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table = self.resizeVerticalTableViewToContents(table)
            self.table_params4.append(table)

    def set_table_background(self, patt: str):
        self.table_background = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                nba = self.pattern_pcrs[patt].get_flags_global('nba', ipatt)
                if nba > 2:
                    header = f'!2Theta/TOF/E(Kev)   Background  Code'.split()
                    model = ListTableModel(self.pattern_pcrs[patt].custom_pcr[f'backgroundpatt{ipatt}'], header)
                    model.set_name(f'table_background_patt{ipatt}')
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)

                    self.table_background.append(table)
                else:
                    self.table_background.append(None)

        else:
            nba = self.pattern_pcrs[patt].get_flags_global('nba')
            if nba > 2:
                header = f'!2Theta/TOF/E(Kev)   Background  Code'.split()
                model = ListTableModel(self.pattern_pcrs[patt].custom_pcr[f'background'], header)
                model.set_name(f'table_background')
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)

                self.table_background.append(table)
            else:
                self.table_background.append(None)

    def set_table_fourier(self, patt: str):
        self.table_fourier = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                nba = self.pattern_pcrs[patt].get_flags_global('nba', ipatt)
                if nba == -2:
                    header = ['Window', 'Cycles']
                    model = ListTableModel(self.pattern_pcrs[patt].custom_pcr[f'backgroundpatt{ipatt}'], header)
                    model.set_name(f'table_fourier_patt{ipatt}')
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)

                    self.table_fourier.append(table)
                else:
                    self.table_fourier.append(None)

        else:
            nba = self.pattern_pcrs[patt].get_flags_global('nba')
            if nba == -2:
                header = ['Window', 'Cycles']
                model = ListTableModel(self.pattern_pcrs[patt].custom_pcr[f'background'], header)
                model.set_name(f'table_fourier')
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)

                self.table_fourier.append(table)
            else:
                self.table_fourier.append(None)

    def set_table_chebyshev(self, patt: str):
        self.table_chebyshev = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                nba = self.pattern_pcrs[patt].get_flags_global('nba', ipatt)
                if nba == -5:
                    model = ListTableModel(self.pattern_pcrs[patt].custom_pcr[f'backgroundpatt{ipatt}'])
                    model.set_name(f'table_chebyshev_patt{ipatt}')
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)

                    self.table_chebyshev.append(table)
                else:
                    self.table_chebyshev.append(None)

        else:
            nba = self.pattern_pcrs[patt].get_flags_global('nba')
            if nba == -5:
                model = ListTableModel(self.pattern_pcrs[patt].custom_pcr[f'background'])
                model.set_name(f'table_chebyshev')
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)

                self.table_chebyshev.append(table)
            else:
                self.table_chebyshev.append(None)

    def set_table_excluded(self, patt: str):
        self.table_excluded = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                nex = self.pattern_pcrs[patt].get_flags_global('nex', ipatt)
                if nex > 0:
                    header = ['Excluded Regions (Low)', 'Excluded Regions (High)']
                    model = ListTableModel(self.pattern_pcrs[patt].custom_pcr[f'excludepatt{ipatt}'], header)
                    model.set_name(f'table_excluded_patt{ipatt}')
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)

                    self.table_excluded.append(table)

                else:
                    self.table_excluded.append(None)

        else:
            nex = self.pattern_pcrs[patt].get_flags_global('nex')
            if nex > 0:
                header = ['Excluded Regions (Low)', 'Excluded Regions (High)']
                model = ListTableModel(self.pattern_pcrs[patt].custom_pcr[f'exclude'], header)
                model.set_name(f'table_excluded')
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)

                self.table_excluded.append(table)

            else:
                self.table_excluded.append(None)

    def set_line_number(self, patt: str):
        self.line_number = qtw.QLabel()
        self.line_number.setText(f"    {self.pattern_pcrs[patt].custom_pcr['number']}  !Number of refined parameters")

    def set_table_params5(self, patt: str):
        self.table_params5 = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                if job < 0:
                    header = 'Zero  Code  Dtt1  Code   Dtt2 Code Dtt_1overd  Code 2ThetaBank'.split()
                    check = None
                else:
                    header = 'Zero Code  SyCos  Code  SySin   Code Lambda  Code MORE'.split()
                    check = [8]

                model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'params5patt{ipatt}']], header, checks=check)
                model.set_name(f"table_params5_patt{ipatt}")
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)

                self.table_params5.append(table)

        else:
            job = self.pattern_pcrs[patt].get_flags_global('job')
            if job < 0:
                header = 'Zero  Code  Dtt1  Code   Dtt2 Code Dtt_1overd  Code 2ThetaBank'.split()
                check = None
            else:
                header = 'Zero Code  SyCos  Code  SySin   Code Lambda  Code MORE'.split()
                check = [8]
 
            model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'params5']], header, checks=check)
            model.set_name(f"table_params5")
            model.dataChanged.connect(self.update_data)
            table = ListTableView()
            table.setModel(model)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
            table = self.resizeVerticalTableViewToContents(table)
            self.table_params5.append(table)

    def set_table_params5more(self, patt: str):
        self.table_params5more = []
        if self.pattern_pcrs[patt].multipattern:
            npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
            for ipatt in range(npatt):
                job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                if job >= 0:
                    more = self.pattern_pcrs[patt].get_shifts_global('more_zero', ipatt)
                    if more > 0:
                        header = 'P0 Cod_P0 Cp Cod_Cp Tau Cod_Tau'.split()
                        model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'params5morepatt{ipatt}']], header)
                        model.set_name(f"table_params5more_patt{ipatt}")
                        model.dataChanged.connect(self.update_data)
                        table = ListTableView()
                        table.setModel(model)
                        table.resizeColumnsToContents()
                        table.resizeRowsToContents()
                        table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table = self.resizeVerticalTableViewToContents(table)

                        self.table_params5more.append(table)

                    else:
                        self.table_params5more.append(None)

                else:
                    self.table_params5more.append(None)

        else:
            job = self.pattern_pcrs[patt].get_flags_global('job')
            if job >= 0:
                more = self.pattern_pcrs[patt].get_shifts_global('more_zero')
                if more > 0:
                    header = 'P0    Cod_P0    Cp   Cod_Cp     Tau  Cod_Tau'.split()
                    model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'params5more']], header)
                    model.set_name(f"table_params5more")
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)

                    self.table_params5more.append(table)

                else:
                    self.table_params5more.append(None)

            else:
                self.table_params5more.append(None)

    def set_line_phidentifiers(self, patt: str):
        self.line_phidentifiers = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for i in ids:
            self.line_phidentifiers[i] = qtw.QLabel(i)

    def set_lines_phcommands(self, patt: str):
        self.lines_phcommands = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            commands = []
            num_vary = 1
            num_fix = 1
            num_fixspc = 1
            num_pequpat = 1
            for key, val in self.pattern_pcrs[patt].custom_pcr[f'phase{count}'].items():
                if 'varyphase' in key:
                    vary = qtw.QLineEdit()
                    vary.setText(val)
                    vary.setObjectName(f'varyphase{count}_{num_vary}')
                    num_vary += 1
                    vary.textEdited.connect(self.update_text)
                    commands.append(vary)
                if 'fixphase' in key:
                    fix = qtw.QLineEdit()
                    fix.setText(val)
                    fix.setObjectName(f'fixphase{count}_{num_fix}')
                    num_fix += 1
                    fix.textEdited.connect(self.update_text)
                    commands.append(fix)
                if 'fixspc' in key:
                    fixsp = qtw.QLineEdit()
                    fixsp.setText(val)
                    fixsp.setObjectName(f'fixspc{count}_{num_fixspc}')
                    num_fixspc += 1
                    fixsp.textEdited.connect(self.update_text)
                    commands.append(fixsp)
                if 'pequpat' in key:
                    pequ = qtw.QLineEdit()
                    pequ.setText(val)
                    pequ.setObjectName(f'pequpat{count}_{num_pequpat}')
                    num_pequpat += 1
                    pequ.textEdited.connect(self.update_text)
                    commands.append(pequ)
            if commands:
                self.lines_phcommands[i] = commands
            else:
                self.lines_phcommands[i] = None

    def set_table_phparams1global(self, patt: str):
        self.table_phparams1global = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                header = 'Nat Dis Ang Jbt Isy Str Furth   ATZ Nvk More'.split()
                edit = [1, 2, 4, 5, 6, 7, 8]
                check = [9]
                model = ListTableModel(
                    [self.pattern_pcrs[patt].custom_pcr[f'phase{count}']['params1global']], header, edit, check)
                model.set_name((count, "table_phparams1global_multi"))
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)
                self.table_phparams1global[i] = table
            else:
                header = 'Nat Dis Ang Pr1 Pr2 Pr3 Jbt Irf Isy Str Furth  ATZ Nvk Npr More'.split()
                edit = [1, 2, 8, 9, 10, 11, 12, 13]
                check = [14]
                model = ListTableModel(
                    [self.pattern_pcrs[patt].custom_pcr[f'phase{count}']['params1']], header, edit, check)
                model.set_name((count, "table_phparams1global_single"))
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)
                self.table_phparams1global[i] = table

    def set_table_phparams1more(self, patt: str):
        self.table_phparams1more = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            more = self.pattern_pcrs[patt].get_flags_phase(i, 'more_phase')
            if more != 0:
                if self.pattern_pcrs[patt].multipattern:
                    header = 'Jvi Jdi Hel Sol Mom Ter N_Domains'.split()
                    edit = [0, 2, 3, 4, 5, 6, 7]
                    check = [1]
                else:
                    edit = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13]
                    check = [12, 1]
                    header = 'Jvi Jdi Hel Sol Mom Ter Brind RMua RMub RMuc Jtyp Nsp_Ref Ph_Shift N_Domains'.split()

                model = ListTableModel(
                    [self.pattern_pcrs[patt].custom_pcr[f'phase{count}']['params1more']], header, edit, check)
                model.set_name((count, "table_phparams1more"))
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)
                self.table_phparams1more[i] = table
            else:
                self.table_phparams1more[i] = None

    def set_table_phcontributions(self, patt: str):
        self.table_phcontributions = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'phase{count}']['contributions']])
                model.set_name((count, "table_phcontributions"))
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)
                self.table_phcontributions[i] = table
            else:
                self.table_phcontributions[i] = None

    def set_table_phparams1_1(self, patt: str):
        self.table_phparams1_1 = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                data = []
                header = 'Irf Npr Jtyp  Nsp_Ref Ph_Shift'.split()
                edit = [1, 2, 3]
                check = [4]
                for ipatt in range(npatt):
                    model = ListTableModel(
                        [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'params1-1patt{ipatt}']], header, edit, check)
                    model.set_name((count, f"table_phparams1_1_patt{ipatt}"))
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)
                    data.append(table)

                self.table_phparams1_1[i] = data
            else:
                self.table_phparams1_1[i] = None

    def set_table_phparams1_2(self, patt: str):
        self.table_phparams1_2 = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                data = []
                header = 'Pr1  Pr2 Pr3 Brind. Rmua Rmub Rmuc'.split()
                edit = [3, 4, 5, 6]
                for ipatt in range(npatt):
                    model = ListTableModel(
                        [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'params1-2patt{ipatt}']], header, edit)
                    model.set_name((count, f"table_phparams1_2_patt{ipatt}"))
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)
                    data.append(table)

                self.table_phparams1_2[i] = data
            else:
                self.table_phparams1_2[i] = None

    def set_table_phbvs(self, patt: str):
        self.table_phbvs = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            more = self.pattern_pcrs[patt].get_flags_phase(i, 'more_phase')
            if more != 0:
                jdi = self.pattern_pcrs[patt].get_flags_phase(i, 'jdi')
                if jdi == 3 or jdi == 4:
                    header = "Max_dst Max_angle  Bond-Valence-Calc.".split()
                    check = [2]
                    model = ListTableModel(
                        [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'bvs']], header, checks=check)
                    model.set_name((count, f"table_phbvs"))
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)
                    self.table_phbvs[i] = table
                else:
                    self.table_phbvs[i] = None
            else:
                self.table_phbvs[i] = None

    def set_table_phbvsions(self, patt: str):
        self.table_phbvsions = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            more = self.pattern_pcrs[patt].get_flags_phase(i, 'more_phase')
            atoms = self.pattern_pcrs[patt].read_site_labels(i)
            if more != 0 and atoms is not None:
                jdi = self.pattern_pcrs[patt].get_flags_phase(i, 'jdi')
                if jdi == 3 or jdi == 4:
                    bvs = self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'bvs'][-1]
                    if bvs == 'BVS':
                        header = "N_cations   N_anions    Tolerance(%)".split()
                        edit = [0, 1]
                        model = ListTableModel(
                            [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'bvs_ions']['nions']], header, edit)
                        model.set_name((count, f"table_phbvsions"))
                        model.dataChanged.connect(self.update_data)
                        table = ListTableView()
                        table.setModel(model)
                        table.resizeColumnsToContents()
                        table.resizeRowsToContents()
                        table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table = self.resizeVerticalTableViewToContents(table)
                        cations = qtw.QLineEdit()
                        cations.setText(self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'bvs_ions']['cations'])
                        cations.setObjectName(f'cations{count}')
                        cations.textEdited.connect(self.update_text)
                        anions = qtw.QLineEdit()
                        anions.setText(self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'bvs_ions']['anions'])
                        anions.setObjectName(f'anions{count}')
                        anions.textEdited.connect(self.update_text)
                        self.table_phbvsions[i] = [table, cations, anions]
                    else:
                        self.table_phbvsions[i] = None
                else:
                    self.table_phbvsions[i] = None
            else:
                self.table_phbvsions[i] = None

    def set_line_phspacegroup(self, patt: str):
        self.line_phspacegroup = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            self.line_phspacegroup[i] = qtw.QLabel(self.pattern_pcrs[patt].custom_pcr[f'phase{count}']['spacegroup'])

    def set_table_phatoms(self, patt: str):
        self.table_phatoms = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            atoms = []
            nat = self.pattern_pcrs[patt].get_flags_phase(i, 'nat')

            if nat != 0:
                edit = [7, 8, 10]
                check = []
                isec = 0
                for iat in range(nat):
                    atoms.append(self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'atom{iat}']['params'])
                    check.extend([(isec, 9)])
                    isec += 1
                    edit.extend([(isec, 0), (isec, 1), (isec, 9)])
                    atoms.append(self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'atom{iat}']['code'])
                    isec += 1
                    n_t = int(self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'atom{iat}']['params'][-2])
                    if n_t == 2:
                        edit.extend([(isec, 0), (isec, 9)])
                        atoms.append(self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'atom{iat}']['anisotropy'])
                        isec += 1
                        edit.extend([(isec, 0), (isec, 9)])
                        atoms.append(self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'atom{iat}']['codeaniso'])
                        isec += 1

                header = "Atom   Typ  X  Y  Z  Biso  Occ In Fin N_t Spc".split()
                model = ListTableModel(atoms, header, edit, check, offset=4)
                model.set_name((count, f"table_phatoms"))
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)

                self.table_phatoms[i] = table

            else:
                self.table_phatoms[i] = None

    def set_table_phprof1(self, patt: str):
        self.table_phprof1 = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                tables = []
                for ipatt in range(npatt):
                    job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                    if job >= 0:
                        header = 'Scale  Shape1  Bov  Str1 Str2  Str3  Strain-Model'.split()
                    else:
                        header = ' Scale  Extinc Bov Str1 Str2 Str3 Strain-Mode'.split()

                    profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile1patt{ipatt}']['params'],
                               self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile1patt{ipatt}']['code']]

                    edit = [(1, 6)]
                    check = [(0, 6)]
                    model = ListTableModel(profile, header, edit, check, offset=1)
                    model.set_name((count, f"table_phprof1_patt{ipatt}"))
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)
                    tables.append(table)

                self.table_phprof1[i] = tables

            else:
                tables = []
                job = self.pattern_pcrs[patt].get_flags_global('job')
                if job >= 0:
                    header = 'Scale  Shape1  Bov  Str1 Str2  Str3  Strain-Model'.split()
                else:
                    header = 'Scale  Extinc Bov Str1 Str2 Str3 Strain-Mode'.split()

                profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile1']['params'],
                           self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile1']['code']]

                edit = [(1, 6)]
                check = [(0, 6)]
                model = ListTableModel(profile, header, edit, check, offset=1)
                model.set_name((count, f"table_phprof1"))
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)
                tables.append(table)

                self.table_phprof1[i] = tables

    def set_table_phprof2(self, patt: str):
        self.table_phprof2 = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                tables = []
                for ipatt in range(npatt):
                    job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                    if job >= 0:
                        header = 'U V W X Y GauSiz LorSiz Size-Model'.split()
                    else:
                        header = 'Sigma-2 Sigma-1  Sigma-0 Sigma-Q Iso-GStrain Iso-GSize Ani-LSize Size-Model'.split()

                    profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile2patt{ipatt}']['params'],
                               self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile2patt{ipatt}']['code']]

                    edit = [(1, 7)]
                    check = [(0, 7)]
                    model = ListTableModel(profile, header, edit, check, offset=1)
                    model.set_name((count, f"table_phprof2_patt{ipatt}"))
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)
                    tables.append(table)

                self.table_phprof2[i] = tables

            else:
                tables = []
                job = self.pattern_pcrs[patt].get_flags_global('job')
                if job >= 0:
                    header = 'U V W X Y GauSiz LorSiz Size-Model'.split()
                else:
                    header = 'Sigma-2 Sigma-1  Sigma-0 Sigma-Q Iso-GStrain Iso-GSize Ani-LSize Size-Model'.split()

                profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile2']['params'],
                           self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile2']['code']]

                edit = [(1, 7)]
                check = [(0, 7)]
                model = ListTableModel(profile, header, edit, check, offset=1)
                model.set_name((count, f"table_phprof2"))
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)
                tables.append(table)

                self.table_phprof2[i] = tables

    def set_table_phprof3(self, patt: str):
        self.table_phprof3 = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                tables = []
                for ipatt in range(npatt):
                    job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                    if job >= 0:
                        header = 'a          b         c        alpha      beta       gamma'.split()
                    else:
                        header = 'Gamma-2       Gamma-1       Gamma-0   Iso-LorStrain   Iso-LorSize'.split()

                    profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile3patt{ipatt}']['params'],
                               self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile3patt{ipatt}']['code']]

                    model = ListTableModel(profile, header)
                    model.set_name((count, f"table_phprof3_patt{ipatt}"))
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)
                    tables.append(table)

                self.table_phprof3[i] = tables

            else:
                tables = []
                job = self.pattern_pcrs[patt].get_flags_global('job')
                if job >= 0:
                    header = 'a          b         c        alpha      beta       gamma'.split()
                else:
                    header = 'Gamma-2       Gamma-1       Gamma-0   Iso-LorStrain   Iso-LorSize'.split()

                profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile3']['params'],
                           self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile3']['code']]

                model = ListTableModel(profile, header)
                model.set_name((count, f"table_phprof3"))
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)
                tables.append(table)

                self.table_phprof3[i] = tables

    def set_table_phprof4(self, patt: str):
        self.table_phprof4 = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                tables = []
                for ipatt in range(npatt):
                    job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                    if job >= 0:
                        header = 'Pref1    Pref2      Asy1     Asy2     Asy3     Asy4      S_L      D_L'.split()
                    else:
                        header = 'a          b         c        alpha      beta       gamma'.split()

                    profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile4patt{ipatt}']['params'],
                               self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile4patt{ipatt}']['code']]

                    model = ListTableModel(profile, header)
                    model.set_name((count, f"table_phprof4_patt{ipatt}"))
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)
                    tables.append(table)

                self.table_phprof4[i] = tables

            else:
                tables = []
                job = self.pattern_pcrs[patt].get_flags_global('job')
                if job >= 0:
                    header = 'Pref1    Pref2      Asy1     Asy2     Asy3     Asy4      S_L      D_L'.split()
                else:
                    header = 'a          b         c        alpha      beta       gamma'.split()

                profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile4']['params'],
                           self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile4']['code']]

                model = ListTableModel(profile, header)
                model.set_name((count, f"table_phprof4"))
                model.dataChanged.connect(self.update_data)
                table = ListTableView()
                table.setModel(model)
                table.resizeColumnsToContents()
                table.resizeRowsToContents()
                table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                table = self.resizeVerticalTableViewToContents(table)
                tables.append(table)

                self.table_phprof4[i] = tables

    def set_table_phprof5(self, patt: str):
        self.table_phprof5 = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                tables = []
                for ipatt in range(npatt):
                    job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                    if job < 0:
                        header = 'Pref1 Pref2   alph0   beta0   alph1   beta1   alphQ  betaQ'.split()
                        profile = [
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile5patt{ipatt}']['params'],
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile5patt{ipatt}']['code']]

                        model = ListTableModel(profile, header)
                        model.set_name((count, f"table_phprof5_patt{ipatt}"))
                        model.dataChanged.connect(self.update_data)
                        table = ListTableView()
                        table.setModel(model)
                        table.resizeColumnsToContents()
                        table.resizeRowsToContents()
                        table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table = self.resizeVerticalTableViewToContents(table)
                        tables.append(table)

                    else:
                        tables.append(None)

                self.table_phprof5[i] = tables

            else:
                tables = []
                job = self.pattern_pcrs[patt].get_flags_global('job')
                if job < 0:
                    header = 'Pref1 Pref2   alph0   beta0   alph1   beta1   alphQ  betaQ'.split()
                    profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile5']['params'],
                               self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'profile5']['code']]

                    model = ListTableModel(profile, header)
                    model.set_name((count, f"table_phprof5"))
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)
                    tables.append(table)

                else:
                    tables.append(None)

                self.table_phprof5[i] = tables

    def set_table_phabscor(self, patt: str):
        self.table_phabscor = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                tables = []
                for ipatt in range(npatt):
                    job = self.pattern_pcrs[patt].get_flags_global('job', ipatt)
                    if job < 0:
                        header = ['ABSCOR1', 'Code', 'ABSCOR2', 'Code']
                        model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'abscorrpatt{ipatt}']], header)
                        model.set_name((count, f"table_phabscor_multi"))
                        model.dataChanged.connect(self.update_data)
                        table = ListTableView()
                        table.setModel(model)
                        table.resizeColumnsToContents()
                        table.resizeRowsToContents()
                        table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table = self.resizeVerticalTableViewToContents(table)
                        tables.append(table)
                    else:
                        tables.append(None)

                self.table_phabscor[i] = tables

            else:
                job = self.pattern_pcrs[patt].get_flags_global('job')
                tables = []
                if job < 0:
                    header = ['ABSCOR1', 'Code', 'ABSCOR2', 'Code']
                    model = ListTableModel([self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'abscorr']], header)
                    model.set_name((count, f"table_phabscor_single"))
                    model.dataChanged.connect(self.update_data)
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)
                    tables.append(table)
                else:
                    tables.append(None)

                self.table_phabscor[i] = tables

    def set_table_phshifts(self, patt: str):
        self.table_phshifts = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                tables = []
                for ipatt in range(npatt):
                    phshift = self.pattern_pcrs[patt].get_flags_phase(i, 'phshift', ipatt)
                    if phshift == 1:
                        header = 'Zero_ph      SyCos_ph      SySin_ph'.split()
                        profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'shiftspatt{ipatt}']['params'],
                                   self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'shiftspatt{ipatt}']['code']]

                        model = ListTableModel(profile, header)
                        model.set_name((count, f"table_phshifts_multi"))
                        model.dataChanged.connect(self.update_data)
                        table = ListTableView()
                        table.setModel(model)
                        table.resizeColumnsToContents()
                        table.resizeRowsToContents()
                        table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table = self.resizeVerticalTableViewToContents(table)
                        tables.append(table)

                    else:
                        tables.append(None)

                self.table_phshifts[i] = tables

            else:
                more = self.pattern_pcrs[patt].get_flags_phase(i, 'more_phase')
                tables = []
                if more != 0:
                    phshift = self.pattern_pcrs[patt].get_flags_phase(i, 'phshift')
                    if phshift == 1:
                        header = 'Zero_ph      SyCos_ph      SySin_ph'.split()
                        profile = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'shifts']['params'],
                                   self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'shifts']['code']]

                        model = ListTableModel(profile, header)
                        model.set_name((count, f"table_phshifts_single"))
                        model.dataChanged.connect(self.update_data)
                        table = ListTableView()
                        table.setModel(model)
                        table.resizeColumnsToContents()
                        table.resizeRowsToContents()
                        table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table = self.resizeVerticalTableViewToContents(table)
                        tables.append(table)

                    else:
                        tables.append(None)

                else:
                    tables.append(None)

                self.table_phshifts[i] = tables

    def set_table_phstrainmodel(self, patt: str):
        self.table_phstrainmodel = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                totables = []
                strf = self.pattern_pcrs[patt].get_flags_phase(i, 'str')
                for ipatt in range(npatt):
                    strainmodel = self.pattern_pcrs[patt].get_profile_phase(i, 'straintag', ipatt)
                    if strf == 1:
                        header = None
                        header1 = None
                        header2 = None
                        table = None
                        table1 = None
                        table2 = None
                        table3 = None
                        if strainmodel == 1:
                            header = 's_400 s_040 s_004 s_220 s_202'.split()
                            header1 = 's_022 s_211 s_121 s_112 s_310'.split()
                            header2 = 's_301 s_130 s_103 s_013 s_031'.split()
                        elif strainmodel == 2:
                            header = 's_400 s_040 s_004 s_220 s_202'.split()
                            header1 = 's_022 s_121 s_301 s_103'.split()
                        elif strainmodel == -2:
                            header = 's_400 s_040 s_004 s_220 s_202'.split()
                            header1 = 's_022 s_112 s_310 s_130'.split()
                        elif strainmodel == 3:
                            header = 's_400 s_040 s_004 s_220 s_202 s_022'.split()
                        elif strainmodel == 4 or strainmodel == 5:
                            header = 's_400 s_004 s_220 s_202'.split()
                        elif strainmodel == 6 or strainmodel == 7:
                            header = 's_400 s_004 s_112 s_211'.split()
                        elif strainmodel == 8 or strainmodel == 9 or strainmodel == 10 or strainmodel == 11 or strainmodel == 12:
                            header = 's_400 s_004 s_112'.split()
                        elif strainmodel == 13 or strainmodel == 14:
                            header = 's_400 s_220'.split()

                        if isinstance(header, list):
                            profile = [
                                self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso0patt{ipatt}'][
                                    'params'],
                                self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso0patt{ipatt}'][
                                    'code']]

                            model = ListTableModel(profile, header)
                            model.set_name((count, f"table_strain_0_patt{ipatt}"))
                            table = ListTableView()
                            table.setModel(model)
                            table.resizeColumnsToContents()
                            table.resizeRowsToContents()
                            table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                            table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                            table = self.resizeVerticalTableViewToContents(table)

                        if isinstance(header1, list):
                            profile1 = [
                                self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso1patt{ipatt}'][
                                    'params'],
                                self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso1patt{ipatt}'][
                                    'code']]

                            model1 = ListTableModel(profile1, header1)
                            model1.set_name((count, f"table_strain_1_patt{ipatt}"))
                            table1 = ListTableView()
                            table1.setModel(model1)
                            table1.resizeColumnsToContents()
                            table1.resizeRowsToContents()
                            table1.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                            table1.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                            table1 = self.resizeVerticalTableViewToContents(table1)

                        if isinstance(header2, list):
                            profile2 = [
                                self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso2patt{ipatt}'][
                                    'params'],
                                self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso2patt{ipatt}'][
                                    'code']]

                            model2 = ListTableModel(profile2, header2)
                            model2.set_name((count, f"table_strain_2_patt{ipatt}"))
                            table2 = ListTableView()
                            table2.setModel(model2)
                            table2.resizeColumnsToContents()
                            table2.resizeRowsToContents()
                            table2.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                            table2.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                            table2 = self.resizeVerticalTableViewToContents(table2)

                        if any(item is not None for item in [table, table1, table2]):
                            header3 = 'Lorentz-Strain(Coeff.) Lorentz-Strain(Code)'.split()
                            profile3 = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'lorentzstrainpatt{ipatt}']]
                            model3 = ListTableModel(profile3, header3)
                            model3.set_name((count, f"table_lorentzstrain_patt{ipatt}"))
                            table3 = ListTableView()
                            table3.setModel(model3)
                            table3.resizeColumnsToContents()
                            table3.resizeRowsToContents()
                            table3.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                            table3.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                            table3 = self.resizeVerticalTableViewToContents(table3)

                        tables = [table, table1, table2, table3]
                    else:
                        tables = [None]

                    totables.append(tables)

                self.table_phstrainmodel[i] = totables

            else:
                strf = self.pattern_pcrs[patt].get_flags_phase(i, 'str')
                strainmodel = self.pattern_pcrs[patt].get_profile_phase(i, 'straintag')
                if strf == 1:
                    header = None
                    header1 = None
                    header2 = None
                    table = None
                    table1 = None
                    table2 = None
                    table3 = None
                    if strainmodel == 1:
                        header = 's_400 s_040 s_004 s_220 s_202'.split()
                        header1 = 's_022 s_211 s_121 s_112 s_310'.split()
                        header2 = 's_301 s_130 s_103 s_013 s_031'.split()
                    elif strainmodel == 2:
                        header = 's_400 s_040 s_004 s_220 s_202'.split()
                        header1 = 's_022 s_121 s_301 s_103'.split()
                    elif strainmodel == -2:
                        header = 's_400 s_040 s_004 s_220 s_202'.split()
                        header1 = 's_022 s_112 s_310 s_130'.split()
                    elif strainmodel == 3:
                        header = 's_400 s_040 s_004 s_220 s_202 s_022'.split()
                    elif strainmodel == 4 or strainmodel == 5:
                        header = 's_400 s_004 s_220 s_202'.split()
                    elif strainmodel == 6 or strainmodel == 7:
                        header = 's_400 s_004 s_112 s_211'.split()
                    elif strainmodel == 8 or strainmodel == 9 or strainmodel == 10 or strainmodel == 11 or strainmodel == 12:
                        header = 's_400 s_004 s_112'.split()
                    elif strainmodel == 13 or strainmodel == 14:
                        header = 's_400 s_220'.split()

                    if isinstance(header, list):
                        profile = [
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso0']['params'],
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso0']['code']]

                        model = ListTableModel(profile, header)
                        model.set_name((count, f"table_strain_0"))
                        table = ListTableView()
                        table.setModel(model)
                        table.resizeColumnsToContents()
                        table.resizeRowsToContents()
                        table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table = self.resizeVerticalTableViewToContents(table)

                    if isinstance(header1, list):
                        profile1 = [
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso1']['params'],
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso1']['code']]

                        model1 = ListTableModel(profile1, header1)
                        model1.set_name((count, f"table_strain_1"))
                        table1 = ListTableView()
                        table1.setModel(model1)
                        table1.resizeColumnsToContents()
                        table1.resizeRowsToContents()
                        table1.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table1.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table1 = self.resizeVerticalTableViewToContents(table1)

                    if isinstance(header2, list):
                        profile2 = [
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso2']['params'],
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'strainaniso2']['code']]

                        model2 = ListTableModel(profile2, header2)
                        model2.set_name((count, f"table_strain_2"))
                        table2 = ListTableView()
                        table2.setModel(model2)
                        table2.resizeColumnsToContents()
                        table2.resizeRowsToContents()
                        table2.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table2.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table2 = self.resizeVerticalTableViewToContents(table2)

                    if any(item is not None for item in [table, table1, table2]):
                        header3 = 'Lorentz-Strain(Coeff.) Lorentz-Strain(Code)'.split()
                        profile3 = [self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'lorentzstrain']]
                        model3 = ListTableModel(profile3, header3)
                        model3.set_name((count, f"table_lorentzstrain"))
                        table3 = ListTableView()
                        table3.setModel(model3)
                        table3.resizeColumnsToContents()
                        table3.resizeRowsToContents()
                        table3.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table3.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table3 = self.resizeVerticalTableViewToContents(table3)

                    tables = [table, table1, table2, table3]
                else:
                    tables = [None]

                self.table_phstrainmodel[i] = [tables]

    def set_table_phsizemodel(self, patt: str):
        self.table_phsizemodel = {}
        ids = self.pattern_pcrs[patt].read_ids()
        for count, i in enumerate(ids):
            if self.pattern_pcrs[patt].multipattern:
                npatt = int(self.pattern_pcrs[patt].custom_pcr['npatt'][0])
                totables = []
                for ipatt in range(npatt):
                    sizemodel = self.pattern_pcrs[patt].get_profile_phase(i, 'sizetag', ipatt)
                    header = None
                    header1 = None
                    table = None
                    table1 = None
                    if sizemodel == 15:
                        header = 'Y00 Y22+ Y22- Y20 Y44+ Y44-'.split()
                        header1 = 'Y42+ Y42- Y40'.split()
                    elif sizemodel == 16:
                        header = 'Y00 Y20 Y40 Y43- Y60 Y63-'.split()
                        header1 = ['Y66+']
                    elif sizemodel == 17:
                        header = 'K00 K41 K61 K62 K81'.split()
                    elif sizemodel == 18:
                        header = 'Y00 Y20 Y22+ Y40 Y42+ Y44+'.split()
                    elif sizemodel == 19:
                        header = 'Y00 Y20 Y40 Y60 Y66+ Y66-'.split()
                    elif sizemodel == 20:
                        header = 'Y00 Y20 Y40 Y43- Y43+'.split()
                    elif sizemodel == 21:
                        header = 'Y00 Y20 Y40 Y44+ Y44- Y60'.split()
                        header1 = 'Y64+ Y64-'.split()
                    elif sizemodel == 22:
                        header = 'Y00 Y20 Y21+ Y21- Y22+ Y22-'.split()

                    if isinstance(header, list):
                        profile = [
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'sizeharmonics0patt{ipatt}'][
                                'params'],
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'sizeharmonics0patt{ipatt}'][
                                'code']]

                        model = ListTableModel(profile, header)
                        model.set_name((count, f"table_size_0_patt{ipatt}"))
                        table = ListTableView()
                        table.setModel(model)
                        table.resizeColumnsToContents()
                        table.resizeRowsToContents()
                        table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table = self.resizeVerticalTableViewToContents(table)

                    if isinstance(header1, list):
                        profile1 = [
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'sizeharmonics1patt{ipatt}'][
                                'params'],
                            self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'sizeharmonics1patt{ipatt}'][
                                'code']]

                        model1 = ListTableModel(profile1, header1)
                        model1.set_name((count, f"table_size_1_patt{ipatt}"))
                        table1 = ListTableView()
                        table1.setModel(model1)
                        table1.resizeColumnsToContents()
                        table1.resizeRowsToContents()
                        table1.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table1.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                        table1 = self.resizeVerticalTableViewToContents(table1)

                    tables = [table, table1]

                    totables.append(tables)

                self.table_phsizemodel[i] = totables

            else:
                sizemodel = self.pattern_pcrs[patt].get_profile_phase(i, 'sizetag')
                header = None
                header1 = None
                table = None
                table1 = None
                if sizemodel == 15:
                    header = 'Y00 Y22+ Y22- Y20 Y44+ Y44-'.split()
                    header1 = 'Y42+ Y42- Y40'.split()
                elif sizemodel == 16:
                    header = 'Y00 Y20 Y40 Y43- Y60 Y63-'.split()
                    header1 = ['Y66+']
                elif sizemodel == 17:
                    header = 'K00 K41 K61 K62 K81'.split()
                elif sizemodel == 18:
                    header = 'Y00 Y20 Y22+ Y40 Y42+ Y44+'.split()
                elif sizemodel == 19:
                    header = 'Y00 Y20 Y40 Y60 Y66+ Y66-'.split()
                elif sizemodel == 20:
                    header = 'Y00 Y20 Y40 Y43- Y43+'.split()
                elif sizemodel == 21:
                    header = 'Y00 Y20 Y40 Y44+ Y44- Y60'.split()
                    header1 = 'Y64+ Y64-'.split()
                elif sizemodel == 22:
                    header = 'Y00 Y20 Y21+ Y21- Y22+ Y22-'.split()

                if isinstance(header, list):
                    profile = [
                        self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'sizeharmonics0']['params'],
                        self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'sizeharmonics0']['code']]

                    model = ListTableModel(profile, header)
                    model.set_name((count, f"table_size_0"))
                    table = ListTableView()
                    table.setModel(model)
                    table.resizeColumnsToContents()
                    table.resizeRowsToContents()
                    table.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table = self.resizeVerticalTableViewToContents(table)

                if isinstance(header1, list):
                    profile1 = [
                        self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'sizeharmonics1']['params'],
                        self.pattern_pcrs[patt].custom_pcr[f'phase{count}'][f'sizeharmonics1']['code']]

                    model1 = ListTableModel(profile1, header1)
                    model1.set_name((count, f"table_size_1"))
                    table1 = ListTableView()
                    table1.setModel(model1)
                    table1.resizeColumnsToContents()
                    table1.resizeRowsToContents()
                    table1.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table1.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOff)
                    table1 = self.resizeVerticalTableViewToContents(table1)

                tables = [table, table1]

                self.table_phsizemodel[i] = [tables]

    def set_lines_restraints(self, patt: str):
        self.lines_restraints = []
        nli = self.pattern_pcrs[patt].get_flags_global('nli')
        for i in range(nli):
            resval = qtw.QLineEdit()
            resval.setText(' '.join(self.pattern_pcrs[patt].custom_pcr[f'restraint{i}']['values']))
            resval.setObjectName(f'resval{i}')
            resval.textEdited.connect(self.update_text)
            self.lines_restraints.append(resval)
            rescoeff = qtw.QLineEdit()
            rescoeff.setText(' '.join(self.pattern_pcrs[patt].custom_pcr[f'restraint{i}']['coeff']))
            rescoeff.setObjectName(f'rescoeff{i}')
            rescoeff.textEdited.connect(self.update_text)
            self.lines_restraints.append(rescoeff)

    def set_lines_limits(self, patt: str):
        self.lines_limits = []
        nre = self.pattern_pcrs[patt].get_flags_global('nre')
        for i in range(nre):
            limval = qtw.QLineEdit()
            limval.setText(' '.join(self.pattern_pcrs[patt].custom_pcr[f'limits{i}']))
            limval.setObjectName(f'limval{i}')
            limval.textEdited.connect(self.update_text)
            self.lines_limits.append(limval)

    def resizeListWidgetToContents(self, listwidget: qtw.QListWidget):
        totalheight = 0
        for i in range(listwidget.count()):
            totalheight += listwidget.sizeHintForRow(i)

        listwidget.setFixedHeight(totalheight)

        return listwidget

    def resizeVerticalTableViewToContents(self, table: ListTableView):
        totalheight = 0

        for i in range(table.verticalHeader().count()):
            if not table.verticalHeader().isSectionHidden(i):
                totalheight += table.verticalHeader().sectionSize(i)

        if not table.horizontalHeader().isHidden():
            totalheight += table.horizontalHeader().height()

        table.setFixedHeight(totalheight)

        return table
