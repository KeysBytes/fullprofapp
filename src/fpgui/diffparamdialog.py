from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg

from fpgui.uifiles.uidiffparamdialog import Ui_Form

from fpgui.patternlist import PatternList
from fpgui.customerrors import IrfReadError
from fpfunctions.app_io import IrfIO
import qtawesome as qta
import numpy as np
import os


class DiffParamDialog(qtw.QDialog):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """ Setup UI for the main window """
        self.ui_diffparamdialog = Ui_Form()
        self.ui_diffparamdialog.setupUi(self)

        self._output = None
        self.healthy = None
        self.irf = None

        self._formats = {0: 'xys', 1: 'free', 2: 'socabim', 3: 'xrdml', 4: 'd1ad2b', 5: 'd1bd20', 6: 'dmc'}

        self.pattlist = [PatternList(number=0)]
        self.pattlist[0].ui_patternlist.check_box.setChecked(True)
        self.pattlist[0].ui_patternlist.check_box.clicked.connect(self.do_checks)
        self.ui_diffparamdialog.pattern_group_layout.addWidget(self.pattlist[0])

        self.ui_diffparamdialog.line_wav1.setValidator(qtg.QDoubleValidator())
        self.ui_diffparamdialog.line_wav2.setValidator(qtg.QDoubleValidator())
        self.ui_diffparamdialog.line_ratio.setValidator(qtg.QDoubleValidator())

        self.ui_diffparamdialog.minus_button.setIcon(qta.icon('fa5s.minus'))
        self.ui_diffparamdialog.minus_button.pressed.connect(lambda: self.on_pressed_minus())

        self.ui_diffparamdialog.plus_button.setIcon(qta.icon('fa5s.plus'))
        self.ui_diffparamdialog.plus_button.pressed.connect(lambda: self.on_pressed_plus())

        """ Behavior of widget """
        self.ui_diffparamdialog.push_browse_irf.clicked.connect(lambda: self.browse_irf())
        self.ui_diffparamdialog.push_browse_pattern.clicked.connect(lambda: self.browse_pattern())

        self.ui_diffparamdialog.dialog_button.accepted.connect(self.accept)
        self.ui_diffparamdialog.dialog_button.rejected.connect(self.reject)

        self.ui_diffparamdialog.push_add_files.clicked.connect(self.add_to_dict)
        self.ui_diffparamdialog.push_clear_files.clicked.connect(self.clear_files)

        self.ui_diffparamdialog.dialog_button.button(qtw.QDialogButtonBox.Ok).setEnabled(False)

        self.ui_diffparamdialog.wav_combo.currentIndexChanged.connect(self.fill_wavelength)
        self.ui_diffparamdialog.wav_combo.setCurrentIndex(1)

        self.rad = 'xrd'
        self.ui_diffparamdialog.button_xrd.clicked.connect(lambda _, x='xrd': self.radiation_type(x))
        self.ui_diffparamdialog.button_cw.clicked.connect(lambda _, x='cw': self.radiation_type(x))
        self.ui_diffparamdialog.button_tof.clicked.connect(lambda _, x='tof': self.radiation_type(x))
        self.ui_diffparamdialog.button_tof.hide()

        self.geom = 'deby'
        self.ui_diffparamdialog.button_deby.setToolTip('Debye-Scherrer geometry (Ilo = -2 in the PCR file)')
        self.ui_diffparamdialog.button_bragg.setToolTip('Bragg-Brentano geometry (Ilo = 0 in the PCR file)')
        self.ui_diffparamdialog.button_sync.setToolTip('Synchrotron Debye-Scherrer (Ilo = 3 in the PCR file). Loading the patterns in this geometry requires that you provide a value of the polarization')
        self.ui_diffparamdialog.button_psd.setToolTip('Flat plate PSD geometry with incident fixed angle (Ilo = 1 in the PCR file). Loading the patterns in this geometry requires that you provide a value of the angle in degrees')
        self.ui_diffparamdialog.button_trmb.setToolTip('Transmission bisecting geometry (Ilo = 2 in the PCR file)')
        self.ui_diffparamdialog.button_trmf.setToolTip('Transmission fixed geometry (Ilo = 4 in the PCR file). Loading the patterns in this geometry requires that you provide a value of the incident angle in degrees with respect to the sample normal')

        self.ui_diffparamdialog.button_deby.clicked.connect(lambda _, x='deby': self.geometry_type(x))
        self.ui_diffparamdialog.button_bragg.clicked.connect(lambda _, x='brag': self.geometry_type(x))
        self.ui_diffparamdialog.button_sync.clicked.connect(lambda _, x='sync': self.geometry_type(x))
        self.ui_diffparamdialog.button_psd.clicked.connect(lambda _, x='psd': self.geometry_type(x))
        self.ui_diffparamdialog.button_trmb.clicked.connect(lambda _, x='trmb': self.geometry_type(x))
        self.ui_diffparamdialog.button_trmf.clicked.connect(lambda _, x='trmf': self.geometry_type(x))

    def radiation_type(self, x):
        self.rad = x

    def geometry_type(self, x):
        self.geom = x

    def do_checks(self):
        for widgets in self.pattlist:
            if widgets.ui_patternlist.check_box.text() == self.sender().text():
                widgets.ui_patternlist.check_box.setChecked(True)
            else:
                widgets.ui_patternlist.check_box.setChecked(False)

    def clear_files(self):
        for widget in self.pattlist:
            widget.data_dict = dict()
            widget.populate_table()

        self.health_check()
        self.enable_buttons()

    def on_pressed_plus(self):
        num = self.ui_diffparamdialog.pattern_group_layout.count()
        self.ui_diffparamdialog.npatt_label.setText(f'NPATT: {num + 1}  ')

        self.pattlist.append(PatternList(number=num))
        self.pattlist[num].ui_patternlist.check_box.clicked.connect(self.do_checks)
        self.ui_diffparamdialog.pattern_group_layout.addWidget(self.pattlist[num])

        self.health_check()
        self.enable_buttons()

    def on_pressed_minus(self):
        num = self.ui_diffparamdialog.pattern_group_layout.count()

        if num > 1:
            self.ui_diffparamdialog.npatt_label.setText(f'NPATT: {num - 1}  ')
            self.ui_diffparamdialog.pattern_group_layout.itemAt(num - 1).widget().setParent(None)
            self.pattlist.pop(-1)

        self.health_check()
        self.enable_buttons()

    def optional_ilo(self):
        if self.irf.optional_ilo is None:
            if self.geom == 'sync':
                option = OptionalIloDialog(self.geom)
                option.setWindowTitle('SYNC Polarization')
                res = option.exec_()
                if res == qtw.QDialog.Accepted:
                    output = option.get_output()
                    return output
                elif res == qtw.QDialog.Rejected:
                    return res
            elif self.geom == 'psd':
                option = OptionalIloDialog(self.geom)
                option.setWindowTitle('PSD Angle')
                res = option.exec_()
                if res == qtw.QDialog.Accepted:
                    output = option.get_output()
                    return output
                elif res == qtw.QDialog.Rejected:
                    return res
            elif self.geom == 'trmf':
                option = OptionalIloDialog(self.geom)
                option.setWindowTitle('TRMF Angle')
                res = option.exec_()
                if res == qtw.QDialog.Accepted:
                    output = option.get_output()
                    return output    
                elif res == qtw.QDialog.Rejected:
                    return res
        else:
            return self.irf.optional_ilo

    def add_to_dict(self):
        option_ilo = self.optional_ilo()
        if option_ilo == qtw.QDialog.Rejected and isinstance(option_ilo, int):
            return
        
        """ Select index """
        for i in range(self.ui_diffparamdialog.pattern_group_layout.count()):
            if self.pattlist[i].ui_patternlist.check_box.isChecked():
                index = i
                break

        if 'patterns' not in self.pattlist[index].data_dict:
            self.pattlist[index].data_dict['patterns'] = [self.ui_diffparamdialog.patterns_combo.itemText(i)
                                                          for i in range(self.ui_diffparamdialog.patterns_combo.count())]
        else:
            for i in range(self.ui_diffparamdialog.patterns_combo.count()):
                self.pattlist[index].data_dict['patterns'].append(self.ui_diffparamdialog.patterns_combo.itemText(i))

        if 'irf' not in self.pattlist[index].data_dict:
            self.pattlist[index].data_dict['irf'] = [self.ui_diffparamdialog.irf_url.text()
                                                     for _ in range(self.ui_diffparamdialog.patterns_combo.count())]
        else:
            for i in range(self.ui_diffparamdialog.patterns_combo.count()):
                self.pattlist[index].data_dict['irf'].append(self.ui_diffparamdialog.irf_url.text())

        if 'fmt' not in self.pattlist[index].data_dict:
            self.pattlist[index].data_dict['fmt'] = [self._formats[self.ui_diffparamdialog.combo_fileformat.currentIndex()]
                                                     for _ in range(self.ui_diffparamdialog.patterns_combo.count())]
        else:
            for i in range(self.ui_diffparamdialog.patterns_combo.count()):
                self.pattlist[index].data_dict['fmt'].append(self._formats[self.ui_diffparamdialog.combo_fileformat.currentIndex()])
        
        if 'res' not in self.pattlist[index].data_dict:
            self.pattlist[index].data_dict['res'] = [self.ui_diffparamdialog.res_combo.currentText()
                                                     for _ in range(self.ui_diffparamdialog.patterns_combo.count())]
        else:
            for i in range(self.ui_diffparamdialog.patterns_combo.count()):
                self.pattlist[index].data_dict['res'].append(self.ui_diffparamdialog.res_combo.currentText())

        if 'wavelength' not in self.pattlist[index].data_dict:
            wav1 = float(self.ui_diffparamdialog.line_wav1.text())
            wav2 = float(self.ui_diffparamdialog.line_wav2.text())
            ratio = float(self.ui_diffparamdialog.line_ratio.text())
            self.pattlist[index].data_dict['wavelength'] = [(wav1, wav2, ratio)
                                                           for _ in range(self.ui_diffparamdialog.patterns_combo.count())]
        else:
            wav1 = float(self.ui_diffparamdialog.line_wav1.text())
            wav2 = float(self.ui_diffparamdialog.line_wav2.text())
            ratio = float(self.ui_diffparamdialog.line_ratio.text())
            for i in range(self.ui_diffparamdialog.patterns_combo.count()):
                self.pattlist[index].data_dict['wavelength'].append((wav1, wav2, ratio))

        if 'radiation' not in self.pattlist[index].data_dict:
            self.pattlist[index].data_dict['radiation'] = [self.rad
                                                           for _ in range(self.ui_diffparamdialog.patterns_combo.count())]
        else:
            for i in range(self.ui_diffparamdialog.patterns_combo.count()):
                self.pattlist[index].data_dict['radiation'].append(self.rad)

        if 'geometry' not in self.pattlist[index].data_dict:
            self.pattlist[index].data_dict['geometry'] = [self.geom
                                                          for _ in range(self.ui_diffparamdialog.patterns_combo.count())]
        else:
            for i in range(self.ui_diffparamdialog.patterns_combo.count()):
                self.pattlist[index].data_dict['geometry'].append(self.geom)

        if 'option_ilo' not in self.pattlist[index].data_dict:
            self.pattlist[index].data_dict['option_ilo'] = [option_ilo
                                                          for _ in range(self.ui_diffparamdialog.patterns_combo.count())]
        else:
            for i in range(self.ui_diffparamdialog.patterns_combo.count()):
                self.pattlist[index].data_dict['option_ilo'].append(option_ilo)

        self.pattlist[index].populate_table()
        self.health_check()
        self.enable_buttons()

    def fill_wavelength(self):
        self.ui_diffparamdialog.line_ratio.setText('0.5')
        if self.ui_diffparamdialog.wav_combo.currentText() == 'CuKa1/CuKa2':
            self.ui_diffparamdialog.line_wav1.setText('1.54059')
            self.ui_diffparamdialog.line_wav2.setText('1.54431')
        elif self.ui_diffparamdialog.wav_combo.currentText() == 'CrKa1/CrKa2':
            self.ui_diffparamdialog.line_wav1.setText('2.28988')
            self.ui_diffparamdialog.line_wav2.setText('2.29428')
        elif self.ui_diffparamdialog.wav_combo.currentText() == 'AgKa1/AgKa2':
            self.ui_diffparamdialog.line_wav1.setText('0.55942')
            self.ui_diffparamdialog.line_wav2.setText('0.56380')
        elif self.ui_diffparamdialog.wav_combo.currentText() == 'MoKa1/MoKa2':
            self.ui_diffparamdialog.line_wav1.setText('0.70932')
            self.ui_diffparamdialog.line_wav2.setText('0.71360')
        elif self.ui_diffparamdialog.wav_combo.currentText() == 'NiKa1/NiKa2':
            self.ui_diffparamdialog.line_wav1.setText('1.65805')
            self.ui_diffparamdialog.line_wav2.setText('1.66199')
        elif self.ui_diffparamdialog.wav_combo.currentText() == 'FeKa1/FeKa2':
            self.ui_diffparamdialog.line_wav1.setText('1.93631')
            self.ui_diffparamdialog.line_wav2.setText('1.94043')
        elif self.ui_diffparamdialog.wav_combo.currentText() == 'CoKa1/CoKa2':
            self.ui_diffparamdialog.line_wav1.setText('1.78919')
            self.ui_diffparamdialog.line_wav2.setText('1.79321')

    def browse_irf(self):
        filename = qtw.QFileDialog.getOpenFileName(self, 'Open File', os.getcwd(), 'IRF files (*.irf)')
        irffile = os.path.normpath(filename[0])
        self.ui_diffparamdialog.irf_url.setText(irffile)

        """ Inspect Irf File and setup options (activate options depending on whether they are already included in the IRF)"""
        try:
            irf = IrfIO(irffile)
            if irf.res is not None:
                self.ui_diffparamdialog.res_combo.setEnabled(False)
                idx = self.ui_diffparamdialog.res_combo.findText(str(irf.res))
                if (idx != -1):
                    self.ui_diffparamdialog.res_combo.setCurrentIndex(idx)
            else:
                self.ui_diffparamdialog.res_combo.setEnabled(True)

            if irf.jobt is not None:
                if irf.jobt == 0 or irf.jobt == 2:
                    self.ui_diffparamdialog.button_xrd.click()
                elif irf.jobt == 1 or irf.jobt == 3:
                    self.ui_diffparamdialog.button_cw.click()
                
                for i in range(self.ui_diffparamdialog.rad_layout.count()):
                    self.ui_diffparamdialog.rad_layout.itemAt(i).widget().setEnabled(False)
            else:
                for i in range(self.ui_diffparamdialog.rad_layout.count()):
                    self.ui_diffparamdialog.rad_layout.itemAt(i).widget().setEnabled(True)

            if irf.ilo is not None:
                if irf.ilo == -2:
                    self.ui_diffparamdialog.button_deby.click()
                elif irf.ilo == 0:
                    self.ui_diffparamdialog.button_bragg.click()
                elif irf.ilo == 3:
                    self.ui_diffparamdialog.button_sync.click()
                elif irf.ilo == 1:
                    self.ui_diffparamdialog.button_psd.click()
                elif irf.ilo == 2:
                    self.ui_diffparamdialog.button_trmb.click()
                elif irf.ilo == 4:
                    self.ui_diffparamdialog.button_trmf.click()

                for i in range(self.ui_diffparamdialog.geom_layout.count()):
                    self.ui_diffparamdialog.geom_layout.itemAt(i).widget().setEnabled(False)
            else:
                for i in range(self.ui_diffparamdialog.geom_layout.count()):
                    self.ui_diffparamdialog.geom_layout.itemAt(i).widget().setEnabled(True)

            if irf.wave is not None:
                self.ui_diffparamdialog.line_wav1.setText(str(irf.wave[0]))
                self.ui_diffparamdialog.line_wav2.setText(str(irf.wave[1]))
                self.ui_diffparamdialog.line_ratio.setText(str(irf.wave[2]))
                
                self.ui_diffparamdialog.line_wav1.setEnabled(False)
                self.ui_diffparamdialog.line_wav2.setEnabled(False)
                self.ui_diffparamdialog.line_ratio.setEnabled(False)
                self.ui_diffparamdialog.wav_combo.setEnabled(False)
            else:
                self.ui_diffparamdialog.line_wav1.setEnabled(True)
                self.ui_diffparamdialog.line_wav2.setEnabled(True)
                self.ui_diffparamdialog.line_ratio.setEnabled(True)
                self.ui_diffparamdialog.wav_combo.setEnabled(True)

            self.irf = irf

        except IrfReadError as e:
            self.warning = qtw.QMessageBox()
            self.warning.setIcon(qtw.QMessageBox.Critical)
            self.warning.setText(f"Error reading the IRF file:\n\n {e}")
            self.warning.setStandardButtons(qtw.QMessageBox.Ok)
            self.warning.exec_()

    def browse_pattern(self):
        if self.ui_diffparamdialog.buffer_button.isChecked():
            self.ui_diffparamdialog.patterns_combo.clear()
            buffiles, _ = qtw.QFileDialog.getOpenFileNames(self, 'Open Files', os.getcwd(), 'BUF Files (*.buf)')

            for buf in buffiles:
                buf = os.path.normpath(buf)
                with open(buf, 'r') as f:
                    exists = []
                    notexists = []
                    for line in f:
                        if os.path.exists(line.replace('\n', '').strip()):
                            exists.append(line.replace('\n', '').strip())
                        elif os.path.exists(os.path.join(os.path.dirname(buf), line.replace('\n', '').strip())):
                            exists.append(os.path.join(os.path.dirname(buf), line.replace('\n', '').strip()))
                        else:
                            notexists.append(os.path.join(os.path.dirname(buf), line.replace('\n', '').strip()))

                msg = ''.join([f"\n - {file}\n" for file in notexists])
                if notexists:
                    self.warning = qtw.QMessageBox()
                    self.warning.setIcon(qtw.QMessageBox.Warning)
                    self.warning.setText(f"In BUF file ({buf}) the following files don't exist and they will be ignored:\n{msg}")
                    self.warning.setStandardButtons(qtw.QMessageBox.Ok)
                    self.warning.exec_()

                self.ui_diffparamdialog.patterns_combo.addItems(exists)
        else:
            self.ui_diffparamdialog.patterns_combo.clear()
            patternfiles, _ = qtw.QFileDialog.getOpenFileNames(self, 'Open Files', os.getcwd(), 'All Files (*.*)')
            patternfiles = [os.path.normpath(pattern) for pattern in patternfiles]
            self.ui_diffparamdialog.patterns_combo.addItems(patternfiles)

    def health_check(self):
        """ Check that each pattern list has no empty columns, and that the length of all pattern list is consistent
         Also check that each column has only one radiation type"""

        if any(not patt.data_dict for patt in self.pattlist):
            self.healthy = False
        else:
            rad_types = []
            geom_types = []
            for patt in self.pattlist:
                length = len(patt.data_dict['patterns'])
                if all(len(lst) == length for lst in [patt.data_dict['irf'], patt.data_dict['fmt'], patt.data_dict['res'], patt.data_dict['fmt'],
                                                      patt.data_dict['wavelength'], patt.data_dict['radiation'], patt.data_dict['geometry']]):
                    self.healthy = True
                else:
                    self.healthy = False
                    return

                if any(not os.path.isfile(path) for path in patt.data_dict['patterns']):
                    self.healthy = False
                    return

                if any(not os.path.isfile(path) for path in patt.data_dict['irf']):
                    self.healthy = False
                    return

                rad_types.append(set(patt.data_dict['radiation']))
                geom_types.append(set(patt.data_dict['geometry']))
                ndall = np.array(patt.data_dict['wavelength'], dtype=float)
                for wav in patt.data_dict['wavelength']:
                    ndwav = np.array(wav, dtype=float)
                    dif = (ndall  - ndwav) ** 2
                    dist = np.sqrt(dif)

                    if np.any(dist > 0.01):
                        self.healthy = False
                        return

            thelen = len(self.pattlist[0].data_dict['patterns'])
            if self.healthy and not all(length == thelen for length in [len(patt.data_dict['patterns']) for patt in self.pattlist]):
                self.healthy = False
            else:
                if all(len(rad_set) == 1 for rad_set in rad_types) and all(len(geom_set) == 1 for geom_set in geom_types):
                    self.healthy = True
                else:
                    self.healthy = False

    def enable_buttons(self):

        if self.healthy:
            self.ui_diffparamdialog.dialog_button.button(qtw.QDialogButtonBox.Cancel).setEnabled(True)
            self.ui_diffparamdialog.dialog_button.button(qtw.QDialogButtonBox.Ok).setEnabled(True)
        else:
            self.ui_diffparamdialog.dialog_button.button(qtw.QDialogButtonBox.Cancel).setEnabled(True)
            self.ui_diffparamdialog.dialog_button.button(qtw.QDialogButtonBox.Ok).setEnabled(False)

    def accept(self):

        self._output = [patt.data_dict for patt in self.pattlist]

        super(DiffParamDialog, self).accept()

    def get_output(self):
        return self._output

    def set_output(self, output):
        self._output = output


class OptionalIloDialog(qtw.QDialog):

    def __init__(self, geom: str, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.vlay = qtw.QVBoxLayout()
        if geom == 'sync':
            self.vlay.addWidget(qtw.QLabel('Give value of Polarization'))
        elif geom == 'psd':
            self.vlay.addWidget(qtw.QLabel('Give value of incident fixed angle (degrees)'))
        elif geom == 'trmf':
            self.vlay.addWidget(qtw.QLabel('Give value of incident angle with respect to normal (degrees)'))
            
        self.lineopt = qtw.QLineEdit()
        self.lineopt.setText('0.0')
        self.lineopt.setValidator(qtg.QDoubleValidator())
        self.vlay.addWidget(self.lineopt)

        self.buttonbox = qtw.QDialogButtonBox()
        self.buttonbox.setStandardButtons(qtw.QDialogButtonBox.Cancel | qtw.QDialogButtonBox.Ok)
        self.vlay.addWidget(self.buttonbox)
        self.setLayout(self.vlay)

        self.buttonbox.accepted.connect(self.accept)
        self.buttonbox.rejected.connect(self.reject)

    def accept(self):
        self._option = float(self.lineopt.text())
        super(OptionalIloDialog, self).accept()

    def get_output(self):
        return self._option