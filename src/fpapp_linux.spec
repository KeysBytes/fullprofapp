# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['fpapp.py'],
             pathex=[],
             binaries=[],
             datas=[('./fpgui/css', 'fpgui/css'), ('./fpgui/icons', 'fpgui/icons'),
              ('/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/crysfml_python/crysfml_forpy.so', '.'),
              ('/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/crysfml_python/crysfml08_forpy.so', '.'),
              ('/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/pymatgen/core/bond_lengths.json', 'pymatgen/core'),
              ('/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/pymatgen/core/func_groups.json', 'pymatgen/core'),
              ('/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/pymatgen/core/libxc_docs.json', 'pymatgen/core'),
              ('/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/pymatgen/core/periodic_table.json', 'pymatgen/core'),
              ('/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/pymatgen/core/quad_data.json', 'pymatgen/core'),
              ('/home/oarcelus/fullprofapp/.venv/lib/python3.10/site-packages/pymatgen/core/reconstructions_archive.json', 'pymatgen/core')],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts, 
          [],
          exclude_binaries=True,
          name='fpapp',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None,
          icon='./fpgui/icons/logo.png')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas, 
               strip=False,
               upx=True,
               upx_exclude=[],
               name='fpapp')
