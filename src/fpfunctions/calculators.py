from typing import Union, Tuple

import numpy as np
import numpy.typing as npt
import prisma.baselines as pb
from prisma.spectrum import Spectrum
from scipy.signal import find_peaks


class BackgroundCalculator:

    def calculate_background_asls(self, data: npt.NDArray[np.float64], log_lambda: float, log_p: float):
        """ It calculates the background of a diffraction signal automatically using PRISMA """
        spectrum = Spectrum(data[:, 0], data[:, 1])
        background_obj = pb.asymmetric_least_squares(spectrum=spectrum, log_lambda=log_lambda, log_p=log_p)

        return background_obj.baseline

    def calculate_background_manual(self, data: npt.NDArray[np.float64], points: npt.NDArray[np.float64]):
        """ Interpolates the initialized points to the x-values of DiffractionPattern"""
        interp_data = np.interp(data[:, 0], points[:, 0], points[:, 1])

        return interp_data


class PeakFindCalculator:

    def __init__(self):
        self.Yb = None
        self.Yr = None
        self.peaks_idx = None
        self.peaks = None

    def process_peaks(self, wavelength: Tuple[float, float, float], threshold: float = 0.005):
        """ After detecting peaks, if Ka1 - Ka2 doublets are present, it will generate them,
        if the doublet is already detected (e. g. at large 2theta), it compares peaks and
        accepts within a certain threshold """
        if abs(wavelength[0] - wavelength[1]) < 0.0001 or abs(wavelength[1]) < 0.0001:
            pass
        else:
            ka2_index = []
            for i in range(1, self.peaks.shape[0]):
                sin1 = np.sin(np.deg2rad(0.5 * self.peaks[i - 1, 0]))
                sin2 = np.sin(np.deg2rad(0.5 * self.peaks[i, 0]))
                sin_ratio = sin2/sin1
                lambda_ratio = wavelength[1]/wavelength[0]
                if np.abs(sin_ratio - lambda_ratio) < threshold:
                    """ Check intensities """
                    #int_ratio = (self.peaks[i, 1] - self.Yb[i])/(self.peaks[i - 1, 1] - self.Yb[i - 1])
                    int_ratio = self.Yr[i]/self.Yr[i - 1]
                    if np.abs(int_ratio - wavelength[2]) < 0.2:
                        ka2_index.append(i)

            tmp = np.delete(self.peaks, ka2_index, axis=0)

            tmp = np.sort(tmp)

            self.peaks = tmp

    def calculate_peaks_intens(self, data: npt.NDArray[np.float64],
                               background: npt.NDArray[np.float64], width: int, alpha: float):

        """ Subtract background """
        subdata = data[:, 1] - background
        subdata = np.abs(subdata)
        rel_intens = subdata / np.amax(data[:, 1])

        prominence = alpha*rel_intens.std()
        peaks, _ = find_peaks(rel_intens, prominence=prominence, width=width)

        X = data[:, 0][peaks]
        Y = data[:, 1][peaks]

        self.Yr = rel_intens[peaks]
        self.Yb = background[peaks]

        self.peaks_idx = peaks

        self.peaks = np.column_stack((X, Y))

