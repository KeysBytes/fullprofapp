from dataclasses import dataclass
from typing import Tuple

import numpy as np
import numpy.typing as npt

from fpfunctions.calculators import BackgroundCalculator, PeakFindCalculator


@dataclass
class DiffractionSignal:
    diffraction: npt.NDArray[np.float64] = None
    anchor_points: npt.NDArray[np.float64] = None
    fmt: str = None
    filename: str = None
    wavelength: Tuple[float, float, float] = None
    irf: str = None
    irf_res: int = None
    radiation: str = None
    geometry: str = None 
    option_ilo: float = None
    background: npt.NDArray[np.float64] = None
    peaks: npt.NDArray[np.float64] = None
    peaks_idx: npt.NDArray[np.float64] = None
    max_anchor_points: int = 200

    @classmethod
    def from_file(cls, filename: str, fmt: str):
        from fpfunctions.app_io import IODiffractionFactory

        factory = IODiffractionFactory.factory_method(fmt=fmt)
        diffraction = factory.read_fromfile(filename=filename)

        anchor_points = np.array([[diffraction[0, 0], diffraction[0, 1]],
                                  [diffraction[-1, 0], diffraction[-1, 1]]], dtype=float)

        return cls(diffraction, anchor_points, fmt, filename)

    def add_anchor_point(self, point: npt.NDArray[np.float64]):
        """ Include anchor point """
        if self.anchor_points.shape[0] < self.max_anchor_points:
            self.anchor_points = np.append(self.anchor_points, np.array([point]), axis=0)
            self.anchor_points = self.anchor_points[self.anchor_points[:, 0].argsort()]
        else:
            raise ValueError(
                f'Reached maximum number of _anchor_points ({self.max_anchor_points}), cannot add more.')

    def remove_anchor_point(self, point: npt.NDArray[np.float64]):
        """ Remove anchor point closest to the input point """

        dif = (self.anchor_points - point) ** 2
        dist = np.sum(dif, axis=1)
        dist = np.sqrt(dist)
        min_index = np.argmin(dist)

        self.anchor_points = np.delete(self.anchor_points, min_index, axis=0)

    def remove_selected_anchor_points(self, anchor: npt.NDArray[np.float64]):
        if anchor.ndim == 1:
            anchor = np.array([anchor], dtype=float)

        for point in anchor:
            dif = (self.anchor_points - point) ** 2
            dist = np.sum(dif, axis=1)
            dist = np.sqrt(dist)
            min_index = np.argmin(dist)

            self.anchor_points = np.delete(self.anchor_points, min_index, axis=0)

    def remove_anchor_points_in_range(self, xbeg: float, xfin: float):
        """ Remove anchor point closest to the input point """

        if xbeg <= xfin:
            mask1 = self.anchor_points[:, 0] < xbeg
            mask2 = self.anchor_points[:, 0] > xfin

            mask = mask1 | mask2

        else:
            mask1 = self.anchor_points[:, 0] > xbeg
            mask2 = self.anchor_points[:, 0] < xfin

            mask = mask1 | mask2

        self.anchor_points = self.anchor_points[mask]

    def select_anchor_point(self, point: npt.NDArray[np.float64]):
        dif = (self.anchor_points - point) ** 2
        dist = np.sqrt(dif)
        min_index = np.argmin(dist[:, 0])

        selected_points = self.anchor_points[min_index, :]

        return np.array([selected_points], dtype=float)

    def select_anchor_points_in_range(self, xbeg: float, xfin: float, ybeg: float, yfin: float):
        """ Remove anchor point closest to the input point """

        if xbeg <= xfin and ybeg <= yfin:
            mask1 = self.anchor_points[:, 0] > xbeg
            mask2 = self.anchor_points[:, 0] < xfin
            mask3 = self.anchor_points[:, 1] > ybeg
            mask4 = self.anchor_points[:, 1] < yfin

            mask = mask1 & mask2 & mask3 & mask4

        elif xbeg <= xfin and ybeg > yfin:
            mask1 = self.anchor_points[:, 0] > xbeg
            mask2 = self.anchor_points[:, 0] < xfin
            mask3 = self.anchor_points[:, 1] < ybeg
            mask4 = self.anchor_points[:, 1] > yfin

            mask = mask1 & mask2 & mask3 & mask4

        elif xbeg > xfin and ybeg <= yfin:
            mask1 = self.anchor_points[:, 0] < xbeg
            mask2 = self.anchor_points[:, 0] > xfin
            mask3 = self.anchor_points[:, 1] > ybeg
            mask4 = self.anchor_points[:, 1] < yfin

            mask = mask1 & mask2 & mask3 & mask4

        else:
            mask1 = self.anchor_points[:, 0] < xbeg
            mask2 = self.anchor_points[:, 0] > xfin
            mask3 = self.anchor_points[:, 1] < ybeg
            mask4 = self.anchor_points[:, 1] > yfin

            mask = mask1 & mask2 & mask3 & mask4

        select_points = self.anchor_points[mask]

        return select_points

    def get_anchor_points(self, num_points: int = 30):
        """ Extract anchor points from automatic background for more editing """
        if num_points <= self.max_anchor_points:
            xsize = self.diffraction.shape[0]
            xstep = int(xsize / num_points)

            if xsize % xstep == 0:
                X = self.diffraction[::xstep, 0]
                Y = self.background[::xstep]

                self.anchor_points = np.column_stack((X, Y))

            else:
                X = self.diffraction[::xstep, 0]
                Y = self.background[::xstep]

                X = np.append(X, self.diffraction[-1, 0])
                Y = np.append(Y, self.background[-1])

                self.anchor_points = np.column_stack((X, Y))
        else:
            raise ValueError('Number of demanded _anchor_points exceeds the maximum')

    def get_background_manual(self):
        if self.anchor_points.shape[0] > 2:
            calc = BackgroundCalculator()
            self.background = calc.calculate_background_manual(data=self.diffraction, points=self.anchor_points)

    def get_background_asls(self, log_p: float, log_lambda: float):
        calc = BackgroundCalculator()
        self.background = calc.calculate_background_asls(data=self.diffraction, log_p=log_p, log_lambda=log_lambda)

    def get_peaks_intens(self, alpha: float, width: int):
        calc = PeakFindCalculator()
        calc.calculate_peaks_intens(data=self.diffraction, background=self.background, width=width, alpha=alpha)
        calc.process_peaks(wavelength=self.wavelength)

        self.peaks = calc.peaks

    def add_peaks(self, point: npt.NDArray[np.float64]):
        self.peaks = np.append(self.peaks, np.array([point]), axis=0)
        self.peaks = self.peaks[self.peaks[:, 0].argsort()]

    def remove_peak(self, point: npt.NDArray[np.float64]):
        dif = (self.peaks - point) ** 2
        dist = np.sum(dif, axis=1)
        dist = np.sqrt(dist)
        min_index = np.argmin(dist)

        self.peaks = np.delete(self.peaks, min_index, axis=0)

    def select_peak(self, point: npt.NDArray[np.float64]):
        dif = (self.peaks - point) ** 2
        dist = np.sqrt(dif)
        min_index = np.argmin(dist[:, 0])

        selected_points = self.peaks[min_index, :]

        return np.array([selected_points], dtype=float)

    def select_peaks_in_range(self, xbeg: float, xfin: float, ybeg: float, yfin: float):
        """ Remove anchor point closest to the input point """

        if xbeg <= xfin and ybeg <= yfin:
            mask1 = self.peaks[:, 0] > xbeg
            mask2 = self.peaks[:, 0] < xfin
            mask3 = self.peaks[:, 1] > ybeg
            mask4 = self.peaks[:, 1] < yfin

            mask = mask1 & mask2 & mask3 & mask4

        elif xbeg <= xfin and ybeg > yfin:
            mask1 = self.peaks[:, 0] > xbeg
            mask2 = self.peaks[:, 0] < xfin
            mask3 = self.peaks[:, 1] < ybeg
            mask4 = self.peaks[:, 1] > yfin

            mask = mask1 & mask2 & mask3 & mask4

        elif xbeg > xfin and ybeg <= yfin:
            mask1 = self.peaks[:, 0] < xbeg
            mask2 = self.peaks[:, 0] > xfin
            mask3 = self.peaks[:, 1] > ybeg
            mask4 = self.peaks[:, 1] < yfin

            mask = mask1 & mask2 & mask3 & mask4

        else:
            mask1 = self.peaks[:, 0] < xbeg
            mask2 = self.peaks[:, 0] > xfin
            mask3 = self.peaks[:, 1] < ybeg
            mask4 = self.peaks[:, 1] > yfin

            mask = mask1 & mask2 & mask3 & mask4

        select_points = self.peaks[mask]

        return select_points

    def remove_selected_peaks(self, anchor: npt.NDArray[np.float64]):
        if anchor.ndim == 1:
            anchor = np.array([anchor], dtype=float)

        for point in anchor:
            dif = (self.peaks - point) ** 2
            dist = np.sum(dif, axis=1)
            dist = np.sqrt(dist)
            min_index = np.argmin(dist)

            self.peaks = np.delete(self.peaks, min_index, axis=0)

    def get_background_in_peak_pos(self):
        if self.peaks is not None and self.background is not None:
            peaks_idx = []
            for peak in self.peaks:
                dif = (self.diffraction[:, 0] - peak[0]) ** 2
                dist = np.sqrt(dif)
                min_index = np.argmin(dist)

                peaks_idx.append(min_index)

            self.peaks_idx = np.array(peaks_idx, dtype=int)

            backg = np.take(self.background, self.peaks_idx)

            return backg
