import os
import re
import shutil
import sys
import traceback
from enum import Enum, auto

import xml.etree.ElementTree as ET
from abc import ABC, abstractmethod
from typing import List, Union, Tuple

import decimal
from fastnumbers import fast_real, isfloat
from itertools import cycle

import numpy as np
import numpy.typing as npt
import pandas as pd
from CifFile import ReadCif, StarFile
from pymatgen.core.periodic_table import Element
from fpgui.customerrors import IrfReadError

from fpfunctions.signals import DiffractionSignal

from shutil import copyfile, move

from crysfml_python import crysfml_forpy


class DiffractionIO(ABC):
    """
    Abstract Class for reading different types of diffraction pattern data
    and transforming to DiffractionPattern class.
    """

    @staticmethod
    def check_exist(filename: str) -> bool:

        if os.path.isfile(filename):
            return True
        else:
            raise FileNotFoundError(f'File {filename} was not found!')

    @abstractmethod
    def read_fromfile(self, filename: str) -> npt.NDArray[np.float64]:
        """Read a given XRD pattern"""
        pass


class DiffractionIOXYS(DiffractionIO):
    """IO class for XYS format .dat data

        Data is formatted as:
        % Headers
        % x y s
        % ...   """

    def read_fromfile(self, filename: str) -> npt.NDArray[np.float64]:

        if super().check_exist(filename):
            with open(filename, 'rb') as f:
                """ Start loop to check for the first floating number """
                count = 0
                for line in f:
                    line = line.split()
                    try:
                        float(line[0])
                        break
                    except ValueError:
                        count += 1

                f.seek(0)

                data = np.genfromtxt(f, skip_header=count)
                return data[:, :2]


class DiffractionIOFree(DiffractionIO):
    """IO class for Free format .dat data

        Data is formatted as:
        % Headers
        % 2tteta_init, 2theta_step, 2theta_max
        % Y0, Y1, Y2, ..., YN   """

    def read_fromfile(self, filename: str) -> npt.NDArray[np.float64]:

        if super().check_exist(filename):
            with open(filename, 'rb') as f:
                """ Start loop to check for the first floating number """
                for line in f:
                    line = line.split()

                    try:
                        float(line[0])
                        scan_min = float(line[0])
                        scan_step = float(line[1])
                        scan_max = float(line[2])
                        break
                    except ValueError:
                        pass

                Y = []
                for line in f:
                    line = line.split()
                    Y.append(line)

                Y_flat = [val for sublist in Y for val in sublist]
                num = int(np.rint((scan_max - scan_min) / scan_step)) + 1
                X = np.linspace(scan_min, scan_max, num)
                Y = np.array(Y_flat, dtype=float)

                data = np.column_stack((X, Y))

                return data


class DiffractionIOSocabim(DiffractionIO):
    """IO class for Socabim format data

        Data is formatted as:
        % Headers (containing information)
        % Y0
        % Y1
        % ...
        % YN   """

    def read_fromfile(self, filename: str) -> npt.NDArray[np.float64]:
        if super().check_exist(filename):
            with open(filename, 'rb') as f:

                count = 0
                for line in f:

                    if b'_FILEVERSION' in line:
                        head, sep, tail = line.partition(b'=')
                        line1 = tail.strip()
                        fileversion = int(line1)

                    if b'_STEPSIZE' in line:
                        head, sep, tail = line.partition(b'=')
                        line1 = tail.strip()
                        scan_step = float(line1)

                    elif b'_START' in line:
                        head, sep, tail = line.partition(b'=')
                        line1 = tail.strip()
                        scan_min = float(line1)

                    try:
                        line = line.split()
                        if not line:
                            count += 1
                            continue
                        else:
                            float(line[0])
                            break

                    except ValueError:
                        count += 1

                if fileversion == 3:
                    f.seek(0)
                    Y = np.genfromtxt(f, skip_header=count)

                    scan_max = Y.shape[0] * scan_step + scan_min
                    num = int(np.rint((scan_max - scan_min) / scan_step))
                    X = np.linspace(scan_min, scan_max, num)

                    data = np.column_stack((X, Y))

                    return data

                elif fileversion == 2:
                    Y = []
                    i = 0
                    for line in f:
                        if i > count:
                            line = line.split()
                            Y.append(line)

                        i += 1

                    Y_flat = [val for sublist in Y for val in sublist]
                    Y = np.array(Y_flat, dtype=float)

                    scan_max = Y.shape[0] * scan_step + scan_min
                    num = int(np.rint((scan_max - scan_min) / scan_step))
                    X = np.linspace(scan_min, scan_max, num)

                    data = np.column_stack((X, Y))

                    return data


class DiffractionIOXrdML(DiffractionIO):
    """IO class for .xrdml data

        Data is formatted as an XML file where"""

    def read_fromfile(self, filename: str) -> npt.NDArray[np.float64]:
        if super().check_exist(filename):
            tree = ET.parse(filename)
            root = tree.getroot()

            for elem in root.iter():
                if 'positions' in elem.tag and elem.attrib['axis'] == '2Theta':
                    scan_element = root.findall(f".//{elem.tag}/[@axis='{elem.attrib['axis']}']*")
                    scan_min = float(scan_element[0].text)
                    scan_max = float(scan_element[1].text)
                elif 'intensities' in elem.tag:
                    count_element = root.find(f".//{elem.tag}")
                    Y = np.array(count_element.text.split(), dtype=float)

            X = np.linspace(scan_min, scan_max, Y.shape[0])

            data = np.column_stack((X, Y))

            return data


class DiffractionIOD1AD2B(DiffractionIO):
    """IO class for .dat data for D1A D2B neutron diffractometers"""

    def read_fromfile(self, filename: str) -> npt.NDArray[np.float64]:
        if super().check_exist(filename):
            with open(filename, 'r') as f:
                file = f.readlines()

            scan_step = float(file[1].split()[2])
            scan_min = float(file[2])

            Y = []
            for i, line in enumerate(file[4:], 4):
                if '-1000' in file[i + 1]:
                    break
                else:
                    tmp = re.findall('........', line)
                    for subtmp in tmp:
                        Y.append(float(subtmp[2:].strip()))

            scan_max = scan_min + len(Y) * scan_step
            X = np.linspace(scan_min, scan_max, len(Y))
            Y = np.array(Y, dtype=float)

            data = np.column_stack((X, Y))

            return data


class DiffractionIOD1BD20(DiffractionIO):
    """IO class for .dat data for D1B D20 neutron diffractometers"""

    def read_fromfile(self, filename: str) -> npt.NDArray[np.float64]:
        if super().check_exist(filename):
            with open(filename, 'r') as f:
                file = f.readlines()

            scan_step = float(file[3].split()[-4])
            scan_min = float(file[3].split()[2])

            Y = []
            for i, line in enumerate(file[5:], 5):
                if '-1000' in file[i + 1]:
                    break
                else:
                    tmp = re.findall('..........', line)
                    for subtmp in tmp:
                        Y.append(float(subtmp[2:].strip()))

            scan_max = scan_min + len(Y) * scan_step
            X = np.linspace(scan_min, scan_max, len(Y))
            Y = np.array(Y, dtype=float)

            data = np.column_stack((X, Y))

            return data


class DiffractionIODMC(DiffractionIO):
    """IO class for .dat data for DMC diffractometer at PSI"""

    def read_fromfile(self, filename: str) -> npt.NDArray[np.float64]:
        if super().check_exist(filename):
            with open(filename, 'r') as f:
                file = f.readlines()

            scan_min = float(file[2].split()[0])
            scan_step = float(file[2].split()[1])
            scan_max = float(file[2].split()[2])

            num = int(np.rint((scan_max - scan_min) / scan_step) + 1.005)
            Y = []
            for line in file[3:]:
                if num == len(Y):
                    break

                tmp = re.findall('........', line)
                for subtmp in tmp:
                    Y.append(float(subtmp))

            X = np.linspace(scan_min, scan_max, num)
            Y = np.array(Y, dtype=float)

            data = np.column_stack((X, Y))

            return data


class IODiffractionFactory:
    """ Factory class, it initializes Reader objects depending on the received target file format"""

    @classmethod
    def factory_method(cls, fmt: str = None):

        reader_dict = {'xys': DiffractionIOXYS(),
                       'free': DiffractionIOFree(), 'socabim': DiffractionIOSocabim(),
                       'xrdml': DiffractionIOXrdML(), 'd1ad2b': DiffractionIOD1AD2B(),
                       'd1bd20': DiffractionIOD1BD20(), 'dmc': DiffractionIODMC()}

        if fmt is not None and cls.check_implemented(fmt):
            return reader_dict[fmt]
        elif fmt is None:
            raise ValueError('Please input a format from the list')

    @staticmethod
    def check_implemented(fmt: str) -> bool:

        implemented_IO = ('xys', 'free', 'socabim', 'xrdml', 'd1ad2b', 'd1bd20', 'dmc')

        if fmt in implemented_IO:
            return True
        else:
            raise ValueError(f'Format {fmt} is not currently implemented!')


class CifReaderWriter:

    def __init__(self):
        self.data = None
        self.error = None
        self.summary = None

    @staticmethod
    def clean_cifs(dirname: str, filename: str):
        """ CIF files must be cleaned to avoid codec encoding issues
        errors usually occur with special characters in the comments so eliminate all lines that start with #
        """
        with open(os.path.join(dirname, filename), 'rb') as f:
            text = f.read()

        text = text.decode()
        text = re.sub(r'^#.*\n?', '', text, flags=re.MULTILINE)
        with open(os.path.join(dirname, filename), 'wb') as f:
            f.write(text.encode())

    @staticmethod
    def _parse_mp_hm_symbol(hm_symbol: str):
        string_list = []

        for i in hm_symbol:
            string_list.append(i)

        tmp_list1 = []
        j = 0
        for i in range(len(string_list)):

            if i + j == len(string_list):
                break

            if '_' in string_list[i + j]:
                tmp_list1.pop(-1)
                tmp_list1.append(f"{string_list[i + j - 1]}{string_list[i + j + 1]}")
                j += 1
            else:
                tmp_list1.append(string_list[i + j])

        tmp_list2 = []
        j = 0
        for i in range(len(tmp_list1)):

            if i + j == len(tmp_list1):
                break

            if '/' in tmp_list1[i + j]:
                tmp_list2.pop(-1)
                tmp_list2.append(f"{tmp_list1[i + j - 1]}{tmp_list1[i + j]}{tmp_list1[i + j + 1]}")
                j += 1
            else:
                tmp_list2.append(tmp_list1[i + j])

        tmp_list3 = []
        j = 0
        for i in range(len(tmp_list2)):

            if i + j == len(tmp_list2):
                break

            if '-' in tmp_list2[i + j]:
                tmp_list3.append(f"{tmp_list2[i + j]}{tmp_list2[i + j + 1]}")
                j += 1
            else:
                tmp_list3.append(tmp_list2[i + j])

        return ' '.join(tmp_list3)

    def read_from_dir(self, dirname: str):
        """Reads all .cif files in dir"""
        self.data = {}
        self.error = []
        for item in os.listdir(dirname):
            if os.path.isfile(os.path.join(dirname, item)) and ('.cif' in item):
                try:
                    cf = ReadCif(os.path.join(dirname, item))
                    identifier = item.replace('.cif', '')
                    self.data[identifier] = cf
                except StarFile.StarError as e:
                    self.error.append([item, e])

        self.summary = self._get_data_summary()

    def _get_data_summary(self):
        """ Get data depending on provider """
        records = []
        errors = []
        for identifier, cif in self.data.items():
            cif = cif[cif.keys()[0]]
            if ('mp' in identifier) or ('mvc' in identifier):
                provider = 'mp'
            elif ('ICSD' in identifier) or ('icsd' in identifier):
                provider = 'icsd'
            elif ('COD' in identifier) or ('cod' in identifier):
                provider = 'cod'
            else:
                provider = 'custom'

            try:
                table_dict = dict()
                table_dict["provider"] = provider
                table_dict["identifier"] = identifier
                for key in cif.keys():
                    if '_chemical_formula_sum' in key:
                        table_dict["formula"] = cif[key]

                    elif '_space_group_name_h-m' in key:
                        if 'mp' in provider:
                            table_dict["spacegroup"] = re.sub(r"\([^()]*\)", "", self._parse_mp_hm_symbol(cif[key]))
                            table_dict["FP_default"] = 'YES'
                        else:
                            table_dict["spacegroup"] = re.sub(r"\([^()]*\)", "", cif[key])
                            table_dict["FP_default"] = 'YES'

                        "COD setting keywords"
                        if ':1' in table_dict["spacegroup"]:
                            table_dict["FP_default"] = 'NO'
                            table_dict["spacegroup"] = re.sub(r":.*$", "", table_dict["spacegroup"])
                        elif ':2' in table_dict["spacegroup"]:
                            table_dict["FP_default"] = 'YES'
                            table_dict["spacegroup"] = re.sub(r":.*$", "", table_dict["spacegroup"])

                        " Hexagonal vs Rhombohedral setting COD"
                        if ':H' in table_dict["spacegroup"]:
                            table_dict["FP_default"] = 'YES'
                            table_dict["spacegroup"] = re.sub(r":.*$", "", table_dict["spacegroup"])
                        elif ':R' in table_dict["spacegroup"]:
                            table_dict["FP_default"] = 'NO'
                            table_dict["spacegroup"] = re.sub(r":.*$", "", table_dict["spacegroup"])

                        " ICSD setting values "
                        if "Z" in table_dict["spacegroup"]:
                            table_dict["FP_default"] = 'YES'
                            table_dict["spacegroup"] = re.sub(r"Z.*$", "", table_dict["spacegroup"])
                        elif "S" in table_dict["spacegroup"]:
                            table_dict["FP_default"] = 'NO'
                            table_dict["spacegroup"] = re.sub(r"S.*$", "", table_dict["spacegroup"])

                        " Hexagonal vs Rhombohedral in ICSD "
                        if 'HR' in table_dict["spacegroup"]:
                            table_dict["FP_default"] = 'NO'
                            table_dict["spacegroup"] = re.sub(r"H.*$", "", table_dict["spacegroup"])
                        elif 'H' in table_dict["spacegroup"] and ':' not in table_dict["spacegroup"]:
                            table_dict["FP_default"] = 'YES'
                            table_dict["spacegroup"] = re.sub(r"H.*$", "", table_dict["spacegroup"])

                    elif '_symmetry_int_tables_number' in key or '_space_group_it_number' in key:
                        table_dict["symnum"] = int(cif[key])

                    elif '_cell_length' in key:
                        table_dict[key] = float(re.sub(r"\([^()]*\)", "", cif[key]))

                    elif '_cell_angle' in key:
                        table_dict[key] = float(re.sub(r"\([^()]*\)", "", cif[key]))

                    else:
                        pass

                if "symnum" in table_dict:
                    r = crysfml_forpy.get_crystal_system(str(table_dict["symnum"]))
                else:
                    r = crysfml_forpy.get_crystal_system(table_dict["spacegroup"])

                table_dict["x_system_spacegroup"] = r[0]

                params = np.zeros(6, dtype=np.float32)
                params[0] = table_dict['_cell_length_a']
                params[1] = table_dict['_cell_length_b']
                params[2] = table_dict['_cell_length_c']
                params[3] = table_dict['_cell_angle_alpha']
                params[4] = table_dict['_cell_angle_beta']
                params[5] = table_dict['_cell_angle_gamma']
                r = crysfml_forpy.get_crystal_family(params)
                table_dict["x_system_cell"] = r[2]
                table_dict["x_family_cell"] = r[0]

                records.append(table_dict)

            except KeyError:
                errors.append(identifier)

        self.data = {k: v for k, v in self.data.items() if k not in errors}
        
        summary = pd.DataFrame(records)
        if records:
            summary['symnum'] = summary['symnum'].astype('Int64')

        return [summary, errors]


class IrfIO:

    def __init__(self, irf_url):
        self.res = None
        self.jobt = None
        self.wave = None
        self.thrg = None
        self.prof = None
        self.chtm = None
        self.asym = None
        self.rkk = None
        self.ilo = None
        self.optional_ilo = None
        self.irf_url = irf_url

        self.read_file()

    def read_file(self):
        try:
            self.read_res()
        except (ValueError, IndexError):
            self.res = None
        try:
            self.read_jobt()
        except (ValueError, IndexError):
            raise IrfReadError('Error at reading JOBT from IRF file')
        try:
            self.read_wavelength()
        except (ValueError, IndexError):
            raise IrfReadError('Error at reading WAVE from IRF file')
        try:
            self.read_thrg()
        except (ValueError, IndexError):
            raise IrfReadError('Error at reading THRG from IRF file')    
        try:
            self.read_prof()
        except (ValueError, IndexError):
            raise IrfReadError('Error at reading PROF from IRF file')
        try:    
            self.read_cthm()
        except (ValueError, IndexError):
            raise IrfReadError('Error at reading CTHM from IRF file')
        try:
            self.read_asym()
        except (ValueError, IndexError):
            raise IrfReadError('Error at reading ASYM from IRF file')
        try:
            self.read_rkk()
        except (ValueError, IndexError):
            raise IrfReadError('Error at reading RKK from IRF file')
        try:
            self.read_geom()
        except (ValueError, IndexError):
            raise IrfReadError('Error at reading GEOM from IRF file')
        
    def read_res(self):
        """ Sometimes IRF files specify the RES tag in the file search for it and propose to user """
        with open(self.irf_url, "r") as irf:
            for line in irf:
                if any(res in line for res in ["RES=", "RES ="]):
                    val = line.split('=')[-1].split()[0]
                    self.res = int(val)

    def read_jobt(self):
        with open(self.irf_url, "r") as irf:
            for line in irf:
                if "JOBT" in line and (line.strip()[0] != '!' and  line.strip()[0] != '#'):
                    line = line.strip().split()
                    if any(line[1] == val for val in ['xr', 'XR']):
                        self.jobt = 0
                    elif any(line[1] == val for val in ['xrc', 'XRC']):
                        self.jobt = 2
                    elif any(line[1] == val for val in ['neut', 'NEUT']):
                        self.jobt = 1
                    elif any(line[1] == val for val in ['neutc', 'NEUTC']):
                        self.jobt = 3

    def read_wavelength(self):
        with open(self.irf_url, "r") as irf:
            for line in irf:
                if "WAVE" in line and (line.strip()[0] != '!' and  line.strip()[0] != '#'):
                    line = line.strip().split()
                    wav1 = float(line[1])
                    wav2 = float(line[2])
                    wav3 = float(line[3])
                    self.wave = (wav1, wav2, wav3)

    def read_thrg(self):
        with open(self.irf_url, "r") as irf:
            for line in irf:
                if "THRG" in line and (line.strip()[0] != '!' and  line.strip()[0] != '#'):
                    line = line.strip().split()
                    self.thrg = (float(line[1]), float(line[2]), float(line[3]))
    
    def read_prof(self):
        with open(self.irf_url, "r") as irf:
            for line in irf:
                if "PROF" in line and (line.strip()[0] != '!' and  line.strip()[0] != '#'):
                    line = line.strip().split()
                    self.thrg = (int(line[1]), float(line[2]), float(line[3]), float(line[4]))
    
    def read_cthm(self):
        with open(self.irf_url, "r") as irf:
            for line in irf:
                if "CTHM" in line and (line.strip()[0] != '!' and  line.strip()[0] != '#'):
                    line = line.strip().split()
                    self.cthm = float(line[1])
    
    def read_asym(self):
        with open(self.irf_url, "r") as irf:
            for line in irf:
                if "ASYM" in line and (line.strip()[0] != '!' and  line.strip()[0] != '#'):
                    line = line.strip().split()
                    self.asym = (float(line[1]), float(line[2]))

    def read_rkk(self):
        with open(self.irf_url, "r") as irf:
            for line in irf:
                if "RKK" in line and (line.strip()[0] != '!' and  line.strip()[0] != '#'):
                    line = line.strip().split()
                    self.rkk = float(line[1])
    
    def read_geom(self):
        with open(self.irf_url, "r") as irf:
            for line in irf:
                if "GEOM" in line and (line.strip()[0] != '!' and  line.strip()[0] != '#'):
                    line = line.strip().split()
                    if any(line[1] == val for val in ['deby', 'DEBY']):
                        self.ilo = -2
                    elif any(line[1] == val for val in ['brag', 'BRAG']):
                        self.ilo = 0
                    elif any(line[1] == val for val in ['sync', 'SYNC']):
                        self.ilo = 3
                        self.optional_ilo = float(line[2])
                    elif any(line[1] == val for val in ['psd', 'PSD']):
                        self.ilo = 1
                        self.optional_ilo = float(line[2])
                    elif any(line[1] == val for val in ['trmb', 'TRMB']):
                        self.ilo = 2
                    elif any(line[1] == val for val in ['trmf', 'TRMB']):
                        self.ilo = 4
                        self.optional_ilo = float(line[2])


class BackgroundType(Enum):
    POINTS = auto()
    CHEBYSHEV = auto()
    FOURIER = auto()


class PcrIO:

    def __init__(self, path: str, jobid: str = None):
        self.custom_pcr = {}
        self.multipattern = None
        self.path = path
        self.background = {}
        if jobid is None:
            self.jobid = os.path.basename(os.path.normpath(path))
        else:
            self.jobid = jobid

        self._flag_tags = {'nph': ('params1', [0, 2]), 'job': ('params1', [0, 0]), 'nba': ('params1', [2, 3]),
                           'nex': ('params1', [3, 4]), 'res': ('params1', [8, 11]), 'ste': ('params1', [9, 12]),
                           'pcr': ('params2', [1, 4]), 'ipr': ('params2', [0, 0]), 'ppl': ('params2', [1, 1]),
                           'ins': ('params2', [7, 10]), 'nli': ('params2', [2, 8]), 'hkl': ('params2', [8, 13]),
                           'rpa': ('params2', [3, 11]), 'wav1': ('params3', [0, 0]), 'wav2': ('params3', [1, 1]),
                           'ratio': ('params3', [2, 2]), 'polar': ('params3', [8, 8]), 'ncy': ('params4', [0, 0]), 
                           'eps': ('params4', [1, 1]), 'rat': ('params4', [2, 2]), 'ran': ('params4', [3, 3]),
                           'rpr': ('params4', [4, 4]), 'rgl': ('params4', [5, 5]), 'psd': ('params4', [9, 9]), 
                           'ilo': ('params1', [7, 9]), 'prf': ('params2', [6, 9]), 'thmin': ('params4', [0, 6]),
                           'thmax': ('params4', [2, 8]), 'step': ('params4', [1, 7]), 'nre': ('params1', [3, 13])}

        self._shift_tags = {'default': {'zero': ('params5', [0, 0]), 'zerocode': ('params5', [1, 1]),
                                        'sycos': ('params5', [2, 2]), 'sycoscode': ('params5', [3, 3]),
                                        'sysin': ('params5', [4, 4]), 'sysincode': ('params5', [5, 5]),
                                        'more_zero': ('params5', [8, 8]), 'p0': ('params5more', [0, 0]),
                                        'p0code': ('params5more', [1, 1]), 'cp': ('params5more', [2, 2]),
                                        'cpcode': ('params5more', [3, 3]), 'tau': ('params5more', [4, 4]),
                                        'taucode': ('params5more', [5, 5])},
                            'tof': {'zero': ('params5', [0, 0]), 'zerocode': ('params5', [1, 1]),
                                    'ddt1': ('params5', [2, 2]), 'ddt1code': ('params5', [3, 3]),
                                    'ddt2': ('params5', [4, 4]), 'ddt2code': ('params5', [5, 5]),
                                    'ddt1d': ('params5', [6, 6]), 'ddt1dcode': ('params5', [7, 7])}}

        self._phase_tags = {'nat': (['params1global', 'params1'], [0, 0]),
                            'jbt': (['params1global', 'params1'], [3, 6]),
                            'irf': (['params1-1', 'params1'], [0, 7]),
                            'atz': (['params1', 'params1'], [7, 11]),
                            'more_phase': (['params1global', 'params1'], [9, 14]),
                            'phshift': (['params1-1', 'params1more'], [4, 12]),
                            'jdi': (['params1more', 'params1more'], [1, 1]),
                            'str': (['params1global', 'params1'], [5, 9])}

        self._profile_tags = {'default': {'scale': ('profile1', 'params', [0, 0]),
                                          'bov': ('profile1', 'params', [2, 2]),
                                          'straintag': ('profile1', 'params', [6, 6]),
                                          'u': ('profile2', 'params', [0, 0]),
                                          'v': ('profile2', 'params', [1, 1]), 'w': ('profile2', 'params', [2, 2]),
                                          'x': ('profile2', 'params', [3, 3]), 'y': ('profile2', 'params', [4, 4]),
                                          'gsz': ('profile2', 'params', [5, 5]), 'lsz': ('profile2', 'params', [6, 6]),
                                          'sizetag': ('profile2', 'params', [7, 7]),
                                          'a': ('profile3', 'params', [0, 0]), 'b': ('profile3', 'params', [1, 1]),
                                          'c': ('profile3', 'params', [2, 2]), 'alpha': ('profile3', 'params', [3, 3]),
                                          'beta': ('profile3', 'params', [4, 4]),
                                          'gamma': ('profile3', 'params', [5, 5]),
                                          'asy1': ('profile4', 'params', [2, 2]),
                                          'asy2': ('profile4', 'params', [3, 3]),
                                          'asy3': ('profile4', 'params', [4, 4]),
                                          'asy4': ('profile4', 'params', [5, 5]),
                                          'scalecode': ('profile1', 'code', [0, 0]),
                                          'bovcode': ('profile1', 'code', [2, 2]),
                                          'ucode': ('profile2', 'code', [0, 0]),
                                          'vcode': ('profile2', 'code', [1, 1]), 'wcode': ('profile2', 'code', [2, 2]),
                                          'xcode': ('profile2', 'code', [3, 3]), 'ycode': ('profile2', 'code', [4, 4]),
                                          'gszcode': ('profile2', 'code', [5, 5]),
                                          'lszcode': ('profile2', 'code', [6, 6]),
                                          'acode': ('profile3', 'code', [0, 0]),
                                          'bcode': ('profile3', 'code', [1, 1]),
                                          'ccode': ('profile3', 'code', [2, 2]),
                                          'alphacode': ('profile3', 'code', [3, 3]),
                                          'betacode': ('profile3', 'code', [4, 4]),
                                          'gammacode': ('profile3', 'code', [5, 5]),
                                          'asy1code': ('profile4', 'code', [2, 2]),
                                          'asy2code': ('profile4', 'code', [3, 3]),
                                          'asy3code': ('profile4', 'code', [4, 4]),
                                          'asy4code': ('profile4', 'code', [5, 5]),
                                          'zeroph': ('shifts', 'params', [0, 0]),
                                          'sycosph': ('shifts', 'params', [1, 1]),
                                          'sysinph': ('shifts', 'params', [2, 2]),
                                          'zerophcode': ('shifts', 'code', [0, 0]),
                                          'sycosphcode': ('shifts', 'code', [1, 1]),
                                          'sysinphcode': ('shifts', 'code', [2, 2]),
                                          'lorentzstr': ('lorentzstrain', [0, 0]),
                                          'lorentzstrcode': ('lorentzstrain', [1, 1]),},
                              'tof': {'scale': ('profile1', 'params', [0, 0]), 'bov': ('profile1', 'params', [2, 2]),
                                      'sig2': ('profile2', 'params', [0, 0]), 'sig1': ('profile2', 'params', [1, 1]),
                                      'sig0': ('profile2', 'params', [2, 2]), 'sigq': ('profile2', 'params', [3, 3]),
                                      'isogstrain': ('profile2', 'params', [4, 4]),
                                      'isogsize': ('profile2', 'params', [5, 5]),
                                      'anilsize': ('profile2', 'params', [6, 6]),
                                      'gam2': ('profile3', 'params', [0, 0]), 'gam1': ('profile3', 'params', [1, 1]),
                                      'gam0': ('profile3', 'params', [2, 2]),
                                      'isolstrain': ('profile3', 'params', [3, 3]),
                                      'isolsize': ('profile3', 'params', [4, 4]),
                                      'a': ('profile4', 'params', [0, 0]), 'b': ('profile4', 'params', [1, 1]),
                                      'c': ('profile4', 'params', [2, 2]), 'alpha': ('profile4', 'params', [3, 3]),
                                      'beta': ('profile4', 'params', [4, 4]),
                                      'gamma': ('profile4', 'params', [5, 5]),
                                      'alph0': ('profile5', 'params', [2, 2]),
                                      'alph1': ('profile5', 'params', [4, 4]),
                                      'alphq': ('profile5', 'params', [6, 6]),
                                      'beta0': ('profile5', 'params', [3, 3]),
                                      'beta1': ('profile5', 'params', [5, 5]),
                                      'betaq': ('profile5', 'params', [7, 7]),
                                      'scalecode': ('profile1', 'code', [0, 0]),
                                      'bovcode': ('profile1', 'code', [2, 2]),
                                      'sig2code': ('profile2', 'code', [0, 0]),
                                      'sig1code': ('profile2', 'code', [1, 1]),
                                      'sig0code': ('profile2', 'code', [2, 2]),
                                      'sigqcode': ('profile2', 'code', [3, 3]),
                                      'isogstraincode': ('profile2', 'code', [4, 4]),
                                      'isogsizecode': ('profile2', 'code', [5, 5]),
                                      'anilsizecode': ('profile2', 'code', [6, 6]),
                                      'gam2code': ('profile3', 'code', [0, 0]),
                                      'gam1code': ('profile3', 'code', [1, 1]),
                                      'gam0code': ('profile3', 'code', [2, 2]),
                                      'isolstraincode': ('profile3', 'code', [3, 3]),
                                      'isolsizecode': ('profile3', 'code', [4, 4]),
                                      'acode': ('profile4', 'code', [0, 0]),
                                      'bcode': ('profile4', 'code', [1, 1]),
                                      'ccode': ('profile4', 'code', [2, 2]),
                                      'alphacode': ('profile4', 'code', [3, 3]),
                                      'betacode': ('profile4', 'code', [4, 4]),
                                      'gammacode': ('profile4', 'code', [5, 5]),
                                      'alph0code': ('profile5', 'code', [2, 2]),
                                      'alph1code': ('profile5', 'code', [4, 4]),
                                      'alphqcode': ('profile5', 'code', [6, 6]),
                                      'beta0code': ('profile5', 'code', [3, 3]),
                                      'beta1code': ('profile5', 'code', [5, 5]),
                                      'betaqcode': ('profile5', 'code', [7, 7])}}

        self._site_tags = {'label': ('params', 0), 'type': ('params', 1), 'x': ('params', 2),
                           'y': ('params', 3), 'z': ('params', 4), 'biso': ('params', 5),
                           'occ': ('params', 6), 'nt': ('params', 9), 'spc': ('params', 10),
                           'xcode': ('code', 0), 'ycode': ('code', 1), 'zcode': ('code', 2),
                           'bisocode': ('code', 3), 'occcode': ('code', 4),
                           'b11': ('anisotropy', 0), 'b22': ('anisotropy', 1), 'b33': ('anisotropy', 2),
                           'b12': ('anisotropy', 3), 'b13': ('anisotropy', 4), 'b23': ('anisotropy', 5),
                           'b11code': ('codeaniso', 0), 'b22code': ('codeaniso', 1),
                           'b33code': ('codeaniso', 2), 'b12code': ('codeaniso', 3),
                           'b13code': ('codeaniso', 4), 'b23code': ('codeaniso', 5)
                           }

    def prepare_buf(self, identifiers: List[str], dirname: str):
        """ Generates PCR file from cif files in selection section. Uses CIFs_to_PCR file """

        if isinstance(identifiers, List):
            with open(os.path.join(self.path, f"{self.jobid}.buf"), 'w') as buf:
                for identifier in identifiers:
                    buf.write(f'{identifier}.cif\n')
                    try:
                        copyfile(os.path.join(dirname, f"{identifier}.cif"), os.path.join(self.path, f"{identifier}.cif"))
                    except (shutil.SameFileError, FileNotFoundError) as e:
                        pass

    def prepare_c2pcr(self, data, simulation: bool = False):
        with open(os.path.join(self.path,f"{self.jobid}.c2pcr"), 'w') as c2pcr:
            c2pcr.write(f"{self.jobid}.buf\n")
            c2pcr.write(f"NPATT {len(data)}\n")
            for patt in data:
                c2pcr.write(f"!         IRF-Type    JobType\n")
                job_dict = {'xrd': '0', 'cw': '1', 'tof': '-1'}

                c2pcr.write(f"IRF_FILE: {patt.irf}   {patt.irf_res}   {job_dict[patt.radiation]}\n")
                c2pcr.write(f"!         Instrm    Integ. Int.\n")
                if simulation:
                    c2pcr.write(f"DAT_FILE: simulation\n")
                else:
                    ins_dict = {'xys': 10, 'free': 0, 'socabim': 9, 'xrdml': 13, 'd1ad2b': 6, 'd1bd20': 3, 'dmc': 8}
                    c2pcr.write(f"DAT_FILE: {patt.filename}  {ins_dict[patt.fmt]}    0\n")


    def read_custom_pcr(self):
        """ Try to read multipattern .pcr, if it catches an error raising it will try a single pattern .pcr format
        Finally launch an error message for failed read """
        try:
            self.read_custom_pcr_multipattern()
            self.multipattern = True
            return True
        except (KeyError, ValueError, IndexError) as e:
            try:
                self.read_custom_pcr_singlepattern()
                self.multipattern = False
                return True
            except (KeyError, ValueError) as e:
                return False

    def read_custom_pcr_singlepattern(self):

        self.custom_pcr = {}
        with open(os.path.join(self.path, f"{self.jobid}.pcr"), 'r') as f:
            pcrlines = [line.replace('\n', '').strip() for line in f if '!' not in line[0] and line.strip() != '']
        
        if 'COMM' not in pcrlines[0]:
            raise ValueError(f'ERROR: Comment line is missing')
        row = 0
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['comment'] = pcrlines[row]
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['params1'] = pcrlines[row].split()
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        res = self.custom_pcr[f'params1'][11]
        if int(res) != 0:
            self.custom_pcr[f'irf'] = pcrlines[row]
            row += 1
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['params2'] = pcrlines[row].split()
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr[f'params3'] = pcrlines[row].split()
        row += 1
        if 'VARY' in pcrlines[row]:
            self.custom_pcr[f'varyglobal'] = pcrlines[row]
            row += 1

        if 'FIX' in pcrlines[row]:
            self.custom_pcr[f'fixglobal'] = pcrlines[row]
            row += 1
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['params4'] = pcrlines[row].split()
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        nbckgr = int(self.custom_pcr[f'params1'][3])
        if nbckgr > 2:
            self.custom_pcr[f'background'] = \
                np.array([line.split() for line in pcrlines[row:row + nbckgr]], dtype=float)
            row += nbckgr
        '---------------------------------------------------------------------------------------------------------'
        nex = int(self.custom_pcr[f'params1'][4])
        if nex > 0:
            self.custom_pcr[f'exclude'] = np.array([line.split() for line in pcrlines[row:row + nex]], dtype=float)
            row += nex
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['number'] = pcrlines[row].split()[0]
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr[f'params5'] = pcrlines[row].split()
        more1 = self.custom_pcr[f'params5'][-1]
        job = self.custom_pcr[f'params1'][0]
        row += 1
        if int(more1) != 0 and int(job) >= 0:
            self.custom_pcr[f'params5more'] = pcrlines[row].split()
            row += 1
        if nbckgr == -5:
            self.custom_pcr[f'background'] = np.array([line.split() for line in pcrlines[row:row + 8]], dtype=float)
            row += 8
        if nbckgr == -2:
            self.custom_pcr[f'background'] = np.array([pcrlines[row].partition('!')[0].split()], dtype=float).astype(int)
            row += 1
        '---------------------------------------------------------------------------------------------------------'
        nphase = int(self.custom_pcr[f'params1'][2])
        for i in range(nphase):
            '-----------------------------------------------------------------------------------------------------'
            self.custom_pcr[f'phase{i}'] = {'name': pcrlines[row]}
            row += 1
            '-----------------------------------------------------------------------------------------------------'
            if 'COMMANDS' in pcrlines[row]:
                row += 1
                num_vary = 1
                num_fix = 1
                num_fixspc = 1
                while 'END COMMANDS' not in pcrlines[row]:
                    if 'VARY' == pcrlines[row].split()[0]:
                        self.custom_pcr[f'phase{i}'][f'varyphase{num_vary}'] = pcrlines[row]
                        num_vary += 1

                    elif 'FIX' == pcrlines[row].split()[0]:
                        self.custom_pcr[f'phase{i}'][f'fixphase{num_fix}'] = pcrlines[row]
                        num_fix += 1

                    elif 'FIX_SPC' == pcrlines[row].split()[0]:
                        self.custom_pcr[f'phase{i}'][f'fixspc{num_fixspc}'] = pcrlines[row]
                        num_fixspc += 1

                    row += 1
                row += 1
            '-----------------------------------------------------------------------------------------------------'
            self.custom_pcr[f'phase{i}']['params1'] = pcrlines[row].split()
            more2 = self.custom_pcr[f'phase{i}']['params1'][-1]
            strf = self.custom_pcr[f'phase{i}']['params1'][9]
            row += 1
            '-----------------------------------------------------------------------------------------------------'
            if int(more2) != 0:
                self.custom_pcr[f'phase{i}']['params1more'] = pcrlines[row].split()
                row += 1
                '-------------------------------------------------------------------------------------------------'
                jdi = self.custom_pcr[f'phase{i}']['params1more'][1]
                if int(jdi) == 3 or int(jdi) == 4:
                    self.custom_pcr[f'phase{i}'][f'bvs'] = pcrlines[row].split()
                    bvs = self.custom_pcr[f'phase{i}'][f'bvs'][-1]
                    row += 1
                    '---------------------------------------------------------------------------------------------'
                    if bvs == 'BVS':
                        self.custom_pcr[f'phase{i}'][f'bvs_ions'] = {'nions': pcrlines[row].split()}
                        row += 1
                        self.custom_pcr[f'phase{i}'][f'bvs_ions']['cations'] = pcrlines[row]
                        row += 1
                        self.custom_pcr[f'phase{i}'][f'bvs_ions']['anions'] = pcrlines[row]
                        row += 1
            '-----------------------------------------------------------------------------------------------------'
            self.custom_pcr[f'phase{i}']['spacegroup'] = pcrlines[row].partition('<--')[0]
            row += 1
            '-----------------------------------------------------------------------------------------------------'
            natom = int(self.custom_pcr[f'phase{i}']['params1'][0])
            for k in range(natom):
                self.custom_pcr[f'phase{i}'][f'atom{k}'] = {'params': self.check_whitespace(pcrlines[row].split())}
                row += 1
                self.custom_pcr[f'phase{i}'][f'atom{k}']['code'] = self.check_whitespace(pcrlines[row].split())
                row += 1

                n_t = self.custom_pcr[f'phase{i}'][f'atom{k}']['params'][-2]
                if int(n_t) == 2:
                    self.custom_pcr[f'phase{i}'][f'atom{k}']['anisotropy'] = self.check_whitespace(pcrlines[row].split())
                    row += 1
                    self.custom_pcr[f'phase{i}'][f'atom{k}']['codeaniso'] = self.check_whitespace(pcrlines[row].split())
                    row += 1
            '-----------------------------------------------------------------------------------------------------'
            self.custom_pcr[f'phase{i}'][f'profile1'] = {'params': pcrlines[row].split()}
            strainmod = self.custom_pcr[f'phase{i}'][f'profile1']['params'][-1]
            row += 1
            self.custom_pcr[f'phase{i}'][f'profile1']['code'] = pcrlines[row].split()
            row += 1
            self.custom_pcr[f'phase{i}'][f'profile2'] = {'params': pcrlines[row].split()}
            sizemod = self.custom_pcr[f'phase{i}'][f'profile2']['params'][-1]
            row += 1
            self.custom_pcr[f'phase{i}'][f'profile2']['code'] = pcrlines[row].split()
            row += 1
            self.custom_pcr[f'phase{i}'][f'profile3'] = {'params': pcrlines[row].partition('#')[0].split() if int(job) >= 0 else pcrlines[row].split()}
            row += 1
            self.custom_pcr[f'phase{i}'][f'profile3']['code'] = pcrlines[row].split()
            row += 1
            self.custom_pcr[f'phase{i}'][f'profile4'] = {'params': pcrlines[row].partition('#')[0].split() if int(job) < 0 else pcrlines[row].split()}
            row += 1
            self.custom_pcr[f'phase{i}'][f'profile4']['code'] = pcrlines[row].split()
            row += 1
            if int(job) < 0:
                self.custom_pcr[f'phase{i}'][f'profile5']['params'] = {'params': pcrlines[row].split()}
                row += 1
                self.custom_pcr[f'phase{i}'][f'profile5']['code'] = pcrlines[row].split()
                row += 1
                self.custom_pcr[f'phase{i}'][f'abscorr'] = pcrlines[row].partition('ABS')[0].split()
                row += 1
            '-------------------------------------------------------------------------------------------------'
            if int(more2) != 0:
                ph_shift = self.custom_pcr[f'phase{i}']['params1more'][-2]
                if int(ph_shift) == 1:
                    self.custom_pcr[f'phase{i}']['shifts'] = {'params': pcrlines[row].split()}
                    row += 1
                    self.custom_pcr[f'phase{i}']['shifts']['code'] = pcrlines[row].split()
                    row += 1
            '-------------------------------------------------------------------------------------------------'
            iterate = 0
            if int(sizemod) == 15 or int(sizemod) == 16 or int(sizemod) == 21:
                iterate = 2
            elif int(sizemod) == 17 or int(sizemod) == 18 or int(sizemod) == 19 or int(sizemod) == 20 or int(sizemod) == 22:
                iterate = 1

            for k in range(iterate):
                self.custom_pcr[f'phase{i}'][f'sizeharmonics{k}'] = {'params': pcrlines[row].split()}
                row += 1
                self.custom_pcr[f'phase{i}'][f'sizeharmonics{k}']['code'] = pcrlines[row].split()
                row += 1
            '-------------------------------------------------------------------------------------------------'
            if int(strf) == 1:
                iterate = 0
                if int(strainmod) == 1:
                    iterate = 3
                elif int(strainmod) == 2 or int(strainmod) == -2:
                    iterate = 2
                elif 3 <= int(strainmod) <= 14:
                    iterate = 1

                for k in range(iterate):
                    self.custom_pcr[f'phase{i}'][f'strainaniso{k}'] = {'params': pcrlines[row].split()}
                    row += 1
                    self.custom_pcr[f'phase{i}'][f'strainaniso{k}']['code'] = pcrlines[row].split()
                    row += 1

                self.custom_pcr[f'phase{i}'][f'lorentzstrain'] = pcrlines[row].split()
                row += 1
        '-----------------------------------------------------------------------------------------------------'
        nre = int(self.custom_pcr['params1'][13])
        if nre > 0:
            for i in range(nre):
                self.custom_pcr[f'limits{i}'] = pcrlines[row].split()
                row += 1
        '-----------------------------------------------------------------------------------------------------'
        nli = int(self.custom_pcr['params2'][8])
        if nli > 0:
            for i in range(nli):
                self.custom_pcr[f'restraint{i}'] = {'values': pcrlines[row].partition('->')[0].split()}
                row += 1
                self.custom_pcr[f'restraint{i}']['coeff'] = pcrlines[row].split()
                row += 1

    @staticmethod
    def check_whitespace(line: List[str]):
        """
        Sometimes Uiso value fields from CIF files are really big and CIF_to_PCR jams to columns into one because of
        formatting, we try to solve this issue here. Only provisionally, this should be fixed
        """
        newline = []
        for item in line:
            if item.count('.') > 1:
                idx2 = item.find('.', item.find('.') + 1)
                newline.extend([item[:idx2-2], item[idx2-2:]])
            else:
                newline.append(item)

        return newline

    def read_custom_pcr_multipattern(self):
        self.custom_pcr = {}

        with open(os.path.join(self.path, f"{self.jobid}.pcr"), 'r') as f:
            pcrlines = [line.replace('\n', '').strip() for line in f if '!' not in line[0]]

        if 'NPATT' not in pcrlines[1] and 'W_PAT' not in pcrlines[2]:
            raise ValueError('You are probably not reading a multipattern format PCR')

        row = 0
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['comment'] = pcrlines[row]
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        line = pcrlines[row].split()
        npatt = int(line[1])
        self.custom_pcr['npatt'] = line[1:npatt + 2]
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['wpatt'] = pcrlines[row].split()[1:npatt + 1]
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['params1global'] = pcrlines[row].split()
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        for i in range(npatt):
            self.custom_pcr[f'params1patt{i}'] = pcrlines[row].partition('!->')[0].split()
            row += 1
        '---------------------------------------------------------------------------------------------------------'
        for i in range(npatt):
            self.custom_pcr[f'namepatt{i}'] = pcrlines[row]
            row += 1
        '---------------------------------------------------------------------------------------------------------'
        for i in range(npatt):
            res = self.custom_pcr[f'params1patt{i}'][8]
            if int(res) != 0:
                self.custom_pcr[f'irfpatt{i}'] = pcrlines[row]
                row += 1
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['params2global'] = pcrlines[row].split()
        row += 1
        for i in range(npatt):
            self.custom_pcr[f'params2patt{i}'] = pcrlines[row].partition('!->')[0].split()
            row += 1
        '---------------------------------------------------------------------------------------------------------'
        for i in range(npatt):
            self.custom_pcr[f'params3patt{i}'] = pcrlines[row].split()
            row += 1
            if 'VARY' in pcrlines[row]:
                self.custom_pcr[f'varyglobalpatt{i}'] = pcrlines[row]
                row += 1

            if 'FIX' in pcrlines[row]:
                self.custom_pcr[f'fixglobalpatt{i}'] = pcrlines[row]
                row += 1
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['params4global'] = pcrlines[row].split()
        row += 1
        for i in range(npatt):
            self.custom_pcr[f'params4patt{i}'] = pcrlines[row].split()
            row += 1
        '---------------------------------------------------------------------------------------------------------'
        for i in range(npatt):
            nbckgr = int(self.custom_pcr[f'params1patt{i}'][2])
            if nbckgr > 2:
                self.custom_pcr[f'backgroundpatt{i}'] = \
                    np.array([line.split() for line in pcrlines[row:row + nbckgr]], dtype=float)
                row += nbckgr

            nex = int(self.custom_pcr[f'params1patt{i}'][3])
            if nex > 0:
                self.custom_pcr[f'excludepatt{i}'] = \
                    np.array([line.split() for line in pcrlines[row:row + nex]], dtype=float)
                row += nex
        '---------------------------------------------------------------------------------------------------------'
        self.custom_pcr['number'] = pcrlines[row].split()[0]
        row += 1
        '---------------------------------------------------------------------------------------------------------'
        for i in range(npatt):
            nbckgr = int(self.custom_pcr[f'params1patt{i}'][2])
            self.custom_pcr[f'params5patt{i}'] = pcrlines[row].split()
            more = self.custom_pcr[f'params5patt{i}'][-1]
            job = self.custom_pcr[f'params1patt{i}'][0]
            row += 1
            if int(job) >= 0:
                if int(more) != 0:
                    self.custom_pcr[f'params5morepatt{i}'] = pcrlines[row].split()
                    row += 1

            if nbckgr == -5:
                self.custom_pcr[f'backgroundpatt{i}'] =  np.array([line.split() for line in pcrlines[row:row + 8]], dtype=float)
                row += 8
     
            if nbckgr == -2:
                self.custom_pcr[f'backgroundpatt{i}'] = np.array([pcrlines[row].partition('!')[0].split()], dtype=int).astype(int)
                row += 1
        '---------------------------------------------------------------------------------------------------------'
        nphase = int(self.custom_pcr[f'params1global'][0])
        for i in range(nphase):
            '-----------------------------------------------------------------------------------------------------'
            self.custom_pcr[f'phase{i}'] = {'name': pcrlines[row]}
            row += 1
            if 'COMMANDS' in pcrlines[row]:
                row += 1
                num_vary = 1
                num_fix = 1
                num_fixspc = 1
                num_pequpat = 1
                while 'END COMMANDS' not in pcrlines[row]:
                    if 'VARY' == pcrlines[row].split()[0]:
                        self.custom_pcr[f'phase{i}'][f'varyphase{num_vary}'] = pcrlines[row]
                        num_vary += 1

                    elif 'FIX' == pcrlines[row].split()[0]:
                        self.custom_pcr[f'phase{i}'][f'fixphase{num_fix}'] = pcrlines[row]
                        num_fix += 1

                    elif 'FIX_SPC' == pcrlines[row].split()[0]:
                        self.custom_pcr[f'phase{i}'][f'fixspc{num_fixspc}'] = pcrlines[row]
                        num_fixspc += 1

                    elif 'PEQU_pat' == pcrlines[row].split()[0]:
                        self.custom_pcr[f'phase{i}'][f'pequpat{num_pequpat}'] = pcrlines[row]
                        num_pequpat += 1

                    row += 1
                row += 1

            self.custom_pcr[f'phase{i}']['params1global'] = pcrlines[row].split()
            more = self.custom_pcr[f'phase{i}']['params1global'][-1]
            strf = self.custom_pcr[f'phase{i}']['params1global'][5]
            row += 1
            if int(more) != 0:
                self.custom_pcr[f'phase{i}']['params1more'] = pcrlines[row].split()
                row += 1
            '-----------------------------------------------------------------------------------------------------'
            self.custom_pcr[f'phase{i}']['contributions'] = pcrlines[row].split()
            row += 1
            '-----------------------------------------------------------------------------------------------------'
            for j in range(npatt):
                self.custom_pcr[f'phase{i}'][f'params1-1patt{j}'] = pcrlines[row].split()
                row += 1
                self.custom_pcr[f'phase{i}'][f'params1-2patt{j}'] = pcrlines[row].split()
                row += 1
            '-----------------------------------------------------------------------------------------------------'
            if int(more) != 0:
                jdi = self.custom_pcr[f'phase{i}']['params1more'][1]
                if int(jdi) == 3 or int(jdi) == 4:
                    self.custom_pcr[f'phase{i}'][f'bvs'] = pcrlines[row].split()
                    bvs = self.custom_pcr[f'phase{i}'][f'bvs'][-1]
                    row += 1
                    '---------------------------------------------------------------------------------------------'
                    if bvs == 'BVS':
                        self.custom_pcr[f'phase{i}'][f'bvs_ions'] = {'nions': pcrlines[row].split()}
                        row += 1
                        self.custom_pcr[f'phase{i}'][f'bvs_ions']['cations'] = pcrlines[row]
                        row += 1
                        self.custom_pcr[f'phase{i}'][f'bvs_ions']['anions'] = pcrlines[row]
                        row += 1
            '-----------------------------------------------------------------------------------------------------'
            self.custom_pcr[f'phase{i}']['spacegroup'] = pcrlines[row].partition('<--')[0]
            row += 1
            '-----------------------------------------------------------------------------------------------------'
            natom = int(self.custom_pcr[f'phase{i}']['params1global'][0])
            for k in range(natom):
                self.custom_pcr[f'phase{i}'][f'atom{k}'] = {'params': self.check_whitespace(pcrlines[row].split())}
                row += 1
                self.custom_pcr[f'phase{i}'][f'atom{k}']['code'] = self.check_whitespace(pcrlines[row].split())
                row += 1

                n_t = self.custom_pcr[f'phase{i}'][f'atom{k}']['params'][-2]
                if int(n_t) == 2:
                    self.custom_pcr[f'phase{i}'][f'atom{k}']['anisotropy'] = self.check_whitespace(pcrlines[row].split())
                    row += 1
                    self.custom_pcr[f'phase{i}'][f'atom{k}']['codeaniso'] = self.check_whitespace(pcrlines[row].split())
                    row += 1
            '-----------------------------------------------------------------------------------------------------'
            for j in range(npatt):
                job = int(self.custom_pcr[f'params1patt{j}'][0])

                self.custom_pcr[f'phase{i}'][f'profile1patt{j}'] = {'params': pcrlines[row].split()}
                strainmod = self.custom_pcr[f'phase{i}'][f'profile1patt{j}']['params'][-1]
                row += 1
                self.custom_pcr[f'phase{i}'][f'profile1patt{j}']['code'] = pcrlines[row].split()
                row += 1
                self.custom_pcr[f'phase{i}'][f'profile2patt{j}'] = {'params': pcrlines[row].split()}
                sizemod = self.custom_pcr[f'phase{i}'][f'profile2patt{j}']['params'][-1]
                row += 1
                self.custom_pcr[f'phase{i}'][f'profile2patt{j}']['code'] = pcrlines[row].split()
                row += 1
                self.custom_pcr[f'phase{i}'][f'profile3patt{j}'] = {'params': pcrlines[row].partition('#')[0].split() if int(job) >= 0 else pcrlines[row].split()}
                row += 1
                self.custom_pcr[f'phase{i}'][f'profile3patt{j}']['code'] = pcrlines[row].split()
                row += 1
                self.custom_pcr[f'phase{i}'][f'profile4patt{j}'] = {'params': pcrlines[row].partition('#')[0].split() if int(job) < 0 else pcrlines[row].split()}
                row += 1
                self.custom_pcr[f'phase{i}'][f'profile4patt{j}']['code'] = pcrlines[row].split()
                row += 1
                if job < 0:
                    self.custom_pcr[f'phase{i}'][f'profile5patt{j}'] = {'params': pcrlines[row].split()}
                    row += 1
                    self.custom_pcr[f'phase{i}'][f'profile5patt{j}']['code'] = pcrlines[row].split()
                    row += 1
                    self.custom_pcr[f'phase{i}'][f'abscorrpatt{j}'] = pcrlines[row].partition('ABS')[0].split()
                    row += 1
                ph_shift = self.custom_pcr[f'phase{i}'][f'params1-1patt{j}'][-1]
                if int(ph_shift) == 1:
                    self.custom_pcr[f'phase{i}'][f'shiftspatt{j}'] = {'params': pcrlines[row].split()}
                    row += 1
                    self.custom_pcr[f'phase{i}'][f'shiftspatt{j}']['code'] = pcrlines[row].split()
                    row += 1
                '-------------------------------------------------------------------------------------------------'
                iterate = 0
                if int(sizemod) == 15 or int(sizemod) == 16 or int(sizemod) == 21:
                    iterate = 2
                elif int(sizemod) == 17 or int(sizemod) == 18 or int(sizemod) == 19 or int(sizemod) == 20 or int(
                        sizemod) == 22:
                    iterate = 1

                for k in range(iterate):
                    self.custom_pcr[f'phase{i}'][f'sizeharmonics{k}patt{j}'] = {'params': pcrlines[row].split()}
                    row += 1
                    self.custom_pcr[f'phase{i}'][f'sizeharmonics{k}patt{j}']['code'] = pcrlines[row].split()
                    row += 1
                '-------------------------------------------------------------------------------------------------'
                if int(strf) == 1:
                    iterate = 0
                    if int(strainmod) == 1:
                        iterate = 3
                    elif int(strainmod) == 2 or int(strainmod) == -2:
                        iterate = 2
                    elif 3 <= int(strainmod) <= 14:
                        iterate = 1

                    for k in range(iterate):
                        self.custom_pcr[f'phase{i}'][f'strainaniso{k}patt{j}'] = {'params': pcrlines[row].split()}
                        row += 1
                        self.custom_pcr[f'phase{i}'][f'strainaniso{k}patt{j}']['code'] = pcrlines[row].split()
                        row += 1
                    self.custom_pcr[f'phase{i}'][f'lorentzstrainpatt{j}'] = pcrlines[row].split()
                    row += 1
        '-----------------------------------------------------------------------------------------------------'
        nre = int(self.custom_pcr['params1global'][3])
        if nre > 0:
            for i in range(nre):
                self.custom_pcr[f'limits{i}'] = pcrlines[row].split()
                row += 1
        '-----------------------------------------------------------------------------------------------------'
        nli = int(self.custom_pcr['params2global'][2])
        if nli > 0:
            for i in range(nli):
                self.custom_pcr[f'restraint{i}'] = {'values': pcrlines[row].split()}
                row += 1
                self.custom_pcr[f'restraint{i}']['coeff'] = pcrlines[row].split()
                row += 1

    def save_background(self, ipatt: int = None):
        thmin = self.get_flags_global('thmin', ipatt)
        step = self.get_flags_global('step', ipatt)
        thmax = self.get_flags_global('thmax', ipatt)
        background, mode = self.get_background(ipatt)
        x = np.arange(thmin, thmax + step, step)
        if mode == BackgroundType.POINTS:   
            y = np.interp(x, background[:, 0], background[:, 1])
            bac = np.column_stack((x, y))
        elif mode == BackgroundType.CHEBYSHEV:
            y = self.transform_cheb_to_background(x, ipatt)
            bac = np.column_stack((x, y))
        else:
            bac = None

        self.background[ipatt] = bac
        
    def edit_fromcif2pcr_to_custom(self, mode: BackgroundType = BackgroundType.POINTS, order: int = None, fourin: Tuple = None, excluded: List = None, ipatt: int = None, patt: DiffractionSignal = None):

        zeros = np.zeros(patt.anchor_points.shape[0], dtype=float)
        final = np.column_stack((patt.anchor_points[:, 0], patt.anchor_points[:, 1], zeros))
        self.replace_background(final, ipatt, mode = BackgroundType.POINTS)

        if mode is BackgroundType.CHEBYSHEV and order is not None:
            self.transform_anchor_to_cheb(order, ipatt)
        elif mode is BackgroundType.CHEBYSHEV and order is None:
            raise ValueError("Something when wrong, chebyshev background require you to include a value of 'order' also")
        
        if mode is BackgroundType.FOURIER and fourin is not None:
            self.save_background(ipatt)
            self.replace_background(np.array([fourin], dtype=int), ipatt, mode = BackgroundType.FOURIER)
        elif mode is BackgroundType.FOURIER and fourin is None:
            raise ValueError("Something when wrong, fourirer background require you to include window and cycle parameters")

        if excluded is not None:
            excluded = np.array(excluded, dtype=float)
            self.replace_excluded(excluded, ipatt)

        identifiers = self.read_ids()
        for identifier in identifiers:
            sites = self.read_site_labels(identifier)
            sites = sites if sites is not None else []
            for site in sites:
                self.remove_anisotropic_site(identifier, site)

        mode = {'brag': 0, 'deby': -2, 'sync': 3, 'psd': 1, 'trmb': 2, 'trmf': 4}
        self.edit_flags_global('ilo', mode[patt.geometry], ipatt)

        self.edit_flags_global('wav1', patt.wavelength[0], ipatt)
        self.edit_flags_global('wav2', patt.wavelength[1], ipatt)
        self.edit_flags_global('ratio', patt.wavelength[2], ipatt)

        if patt.geometry == 'sync':
            self.edit_flags_global('polar', patt.option_ilo, ipatt)
        elif patt.geometry == 'psd' or patt.geometry == 'trmf':
            self.edit_flags_global('psd', patt.option_ilo, ipatt)

    def write_pcr_custom(self, path: str = None):
        """ Try to read multipattern .pcr, if it catches an error raising it will try a single pattern .pcr format
        Finally launch an error message for failed read """
        if path is None:
            pcr_path = os.path.join(self.path,f"{self.jobid}.pcr")
        else:
            pcr_path = path

        try:
            self.write_pcr_custom_multipattern(pcr_path)
            return True
        except (KeyError, ValueError, IndexError) as e:
            try:
                self.write_pcr_custom_singlepattern(pcr_path)
                return True
            except (KeyError, ValueError) as e:
                return False

    def write_pcr_custom_singlepattern(self, path: str = None):

        if path is None:
            pcr_path = os.path.join(self.path,f"{self.jobid}.pcr")
        else:
            pcr_path = path

        if os.path.isfile(pcr_path):
            os.remove(pcr_path)

        with open(pcr_path, 'w') as f:
            f.write(f"{self.custom_pcr['comment']}\n")
            f.write(f"{' '.join(self.custom_pcr['params1'])}\n")
            if f'irf' in self.custom_pcr:
                f.write(f"{self.custom_pcr['irf']}\n")
            f.write(f"{' '.join(self.custom_pcr['params2'])}\n")
            f.write(f"{' '.join(self.custom_pcr['params3'])}\n")
            if 'varyglobal' in self.custom_pcr:
                f.write(f"{self.custom_pcr['varyglobal']}\n")
            if 'fixglobal' in self.custom_pcr:
                f.write(f"{self.custom_pcr['fixglobal']}\n")
            f.write(f"{' '.join(self.custom_pcr['params4'])}\n")
            nbckgr = int(self.custom_pcr['params1'][3])
            if nbckgr > 0:
                np.savetxt(f, self.custom_pcr['background'])
            nex = int(self.custom_pcr['params1'][4])
            if nex > 0:
                np.savetxt(f, self.custom_pcr['exclude'])
            f.write(f"{self.custom_pcr['number']}\n")
            f.write(f"{' '.join(self.custom_pcr['params5'])}\n")
            if 'params5more' in self.custom_pcr:
                f.write(f"{' '.join(self.custom_pcr['params5more'])}\n")
            if nbckgr < 0:
                np.savetxt(f, self.custom_pcr['background'])
            nphase = int(self.custom_pcr['params1'][2])
            for i in range(nphase):
                f.write(f"{self.custom_pcr[f'phase{i}']['name']}\n")
                f.write(f"COMMANDS\n")
                for key, val in self.custom_pcr[f'phase{i}'].items():
                    if 'varyphase' in key:
                        f.write(f"{val}\n")
                    if 'fixphase' in key:
                        f.write(f"{val}\n")
                    if 'fixspc' in key:
                        f.write(f"{val}\n")
                f.write(f"END COMMANDS\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}']['params1'])}\n")
                if 'params1more' in self.custom_pcr[f'phase{i}']:
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}']['params1more'])}\n")
                    if 'bvs' in self.custom_pcr[f'phase{i}']:
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}']['bvs'])}\n")
                        if 'bvs_ions' in self.custom_pcr[f'phase{i}']:
                            f.write(f"{' '.join(self.custom_pcr[f'phase{i}']['bvs_ions']['nions'])}\n")
                            f.write(f"{self.custom_pcr[f'phase{i}']['bvs_ions']['anions']}\n")
                            f.write(f"{self.custom_pcr[f'phase{i}']['bvs_ions']['cations']}\n")
                f.write(f"{self.custom_pcr[f'phase{i}']['spacegroup']}\n")
                natom = int(self.custom_pcr[f'phase{i}']['params1'][0])
                for j in range(natom):
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'atom{j}']['params'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'atom{j}']['code'])}\n")
                    n_t = self.custom_pcr[f'phase{i}'][f'atom{j}']['params'][-2]
                    if int(n_t) == 2:
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'atom{j}']['anisotropy'])}\n")
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'atom{j}']['codeaniso'])}\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile1']['params'])}\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile1']['code'])}\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile2']['params'])}\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile2']['code'])}\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile3']['params'])}\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile3']['code'])}\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile4']['params'])}\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile4']['code'])}\n")
                job = int(self.custom_pcr['params1'][0])
                if job < 0:
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile5']['params'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile5']['code'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'abscorr'])}\n")
                if 'params1more' in self.custom_pcr[f'phase{i}']:
                    ph_shift = self.custom_pcr[f'phase{i}']['params1more'][-2]
                    if int(ph_shift) == 1:
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}']['shifts']['params'])}\n")
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}']['shifts']['code'])}\n")
                sizemod = self.custom_pcr[f'phase{i}'][f'profile2']['params'][-1]
                iterate = 0
                if int(sizemod) == 15 or int(sizemod) == 16 or int(sizemod) == 21:
                    iterate = 2
                elif int(sizemod) == 17 or int(sizemod) == 18 or int(sizemod) == 19 or int(
                        sizemod) == 20 or int(sizemod) == 22:
                    iterate = 1
                for k in range(iterate):
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'sizeharmonics{k}']['params'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'sizeharmonics{k}']['code'])}\n")
                strf = self.custom_pcr[f'phase{i}']['params1'][9]
                strainmod = self.custom_pcr[f'phase{i}'][f'profile1']['params'][-1]
                if int(strf) == 1:
                    iterate = 0
                    if int(strainmod) == 1:
                        iterate = 3
                    elif int(strainmod) == 2 or int(strainmod) == -2:
                        iterate = 2
                    elif 3 <= int(strainmod) <= 14:
                        iterate = 1
                    for k in range(iterate):
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'strainaniso{k}']['params'])}\n")
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'strainaniso{k}']['code'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'lorentzstrain'])}\n")
            nre = int(self.custom_pcr['params1'][13])
            if nre > 0:
                for i in range(nre):
                    f.write(f"{' '.join(self.custom_pcr[f'limits{i}'])}\n")
            nli = int(self.custom_pcr['params2'][8])
            if nli > 0:
                for i in range(nli):
                    f.write(f"{' '.join(self.custom_pcr[f'restraint{i}']['values'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'restraint{i}']['coeff'])}\n")

    def write_pcr_custom_multipattern(self, path: str = None):
        if path is None:
            pcr_path = os.path.join(self.path,f"{self.jobid}.pcr")
        else:
            pcr_path = path

        if os.path.isfile(pcr_path):
            os.remove(pcr_path)

        with open(pcr_path, 'w') as f:
            f.write(f"{self.custom_pcr['comment']}\n")
            f.write(f"NPATT {' '.join(self.custom_pcr['npatt'])}\n")
            f.write(f"W_PAT {' '.join(self.custom_pcr['wpatt'])}\n")
            npatt = int(self.custom_pcr['npatt'][0])
            f.write(f"{' '.join(self.custom_pcr['params1global'])}\n")
            for i in range(npatt):
                f.write(f"{' '.join(self.custom_pcr[f'params1patt{i}'])}\n")
            for i in range(npatt):
                f.write(f"{self.custom_pcr[f'namepatt{i}']}\n")
            for i in range(npatt):
                if f'irfpatt{i}' in self.custom_pcr:
                    f.write(f"{self.custom_pcr[f'irfpatt{i}']}\n")
            f.write(f"{' '.join(self.custom_pcr['params2global'])}\n")
            for i in range(npatt):
                f.write(f"{' '.join(self.custom_pcr[f'params2patt{i}'])}\n")
            for i in range(npatt):
                f.write(f"{' '.join(self.custom_pcr[f'params3patt{i}'])}\n")
                if f'varyglobalpatt{i}' in self.custom_pcr:
                    f.write(f"{self.custom_pcr[f'varyglobalpatt{i}']}\n")
                if f'fixglobalpatt{i}' in self.custom_pcr:
                    f.write(f"{self.custom_pcr[f'fixglobalpatt{i}']}\n")
            f.write(f"{' '.join(self.custom_pcr['params4global'])}\n")
            for i in range(npatt):
                f.write(f"{' '.join(self.custom_pcr[f'params4patt{i}'])}\n")
            for i in range(npatt):
                nbckgr = int(self.custom_pcr[f'params1patt{i}'][2])
                if nbckgr > 2:
                    np.savetxt(f, self.custom_pcr[f'backgroundpatt{i}'])
                nex = int(self.custom_pcr[f'params1patt{i}'][3])
                if nex > 0:
                    np.savetxt(f, self.custom_pcr[f'excludepatt{i}'])
            f.write(f"{self.custom_pcr['number']}\n")
            for i in range(npatt):
                f.write(f"{' '.join(self.custom_pcr[f'params5patt{i}'])}\n")
                nbckgr = int(self.custom_pcr[f'params1patt{i}'][2])
                more = self.custom_pcr[f'params5patt{i}'][-1]
                job = self.custom_pcr[f'params1patt{i}'][0]
                if int(job) >= 0:
                    if int(more) != 0:
                        f.write(f"{' '.join(self.custom_pcr[f'params5morepatt{i}'])}\n")
                if nbckgr < 0:
                    np.savetxt(f, self.custom_pcr[f'backgroundpatt{i}'])
            nphase = int(self.custom_pcr[f'params1global'][0])
            for i in range(nphase):
                f.write(f"{self.custom_pcr[f'phase{i}']['name']}\n")
                f.write(f"COMMANDS\n")
                for key, val in self.custom_pcr[f'phase{i}'].items():
                    if 'varyphase' in key:
                        f.write(f"{val}\n")
                    if 'fixphase' in key:
                        f.write(f"{val}\n")
                    if 'fixspc' in key:
                        f.write(f"{val}\n")
                    if 'pequpat' in key:
                        f.write(f"{val}\n")
                f.write(f"END COMMANDS\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}']['params1global'])}\n")
                more = self.custom_pcr[f'phase{i}']['params1global'][-1]
                if int(more) != 0:
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}']['params1more'])}\n")
                f.write(f"{' '.join(self.custom_pcr[f'phase{i}']['contributions'])}\n")
                for j in range(npatt):
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'params1-1patt{j}'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'params1-2patt{j}'])}\n")
                if int(more) != 0:
                    jdi = self.custom_pcr[f'phase{i}']['params1more'][1]
                    if int(jdi) == 3 or int(jdi) == 4:
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'bvs'])}\n")
                        bvs = self.custom_pcr[f'phase{i}'][f'bvs'][-1]
                        if bvs == 'BVS':
                            f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'bvs_ions']['nions'])}\n")
                            f.write(f"{self.custom_pcr[f'phase{i}'][f'bvs_ions']['cations']}\n")
                            f.write(f"{self.custom_pcr[f'phase{i}'][f'bvs_ions']['anions']}\n")
                f.write(f"{self.custom_pcr[f'phase{i}']['spacegroup']}\n")
                natom = int(self.custom_pcr[f'phase{i}']['params1global'][0])
                for k in range(natom):
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'atom{k}']['params'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'atom{k}']['code'])}\n")
                    n_t = self.custom_pcr[f'phase{i}'][f'atom{k}']['params'][-2]
                    if int(n_t) == 2:
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'atom{k}']['anisotropy'])}\n")
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'atom{k}']['codeaniso'])}\n")
                for j in range(npatt):
                    job = int(self.custom_pcr[f'params1patt{j}'][0])
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile1patt{j}']['params'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile1patt{j}']['code'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile2patt{j}']['params'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile2patt{j}']['code'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile3patt{j}']['params'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile3patt{j}']['code'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile4patt{j}']['params'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile4patt{j}']['code'])}\n")
                    if job < 0:
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile5patt{j}']['params'])}\n")
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'profile5patt{j}']['code'])}\n")
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'abscorrpatt{j}'])}\n")
                    ph_shift = self.custom_pcr[f'phase{i}'][f'params1-1patt{j}'][-1]
                    if int(ph_shift) == 1:
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'shiftspatt{j}']['params'])}\n")
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'shiftspatt{j}']['code'])}\n")
                    sizemod = self.custom_pcr[f'phase{i}'][f'profile2patt{j}']['params'][-1]
                    iterate = 0
                    if int(sizemod) == 15 or int(sizemod) == 16 or int(sizemod) == 21:
                        iterate = 2
                    elif int(sizemod) == 17 or int(sizemod) == 18 or int(sizemod) == 19 or int(
                            sizemod) == 20 or int(sizemod) == 22:
                        iterate = 1
                    for k in range(iterate):
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'sizeharmonics{k}patt{j}']['params'])}\n")
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'sizeharmonics{k}patt{j}']['code'])}\n")
                    strf = self.custom_pcr[f'phase{i}']['params1global'][5]
                    strainmod = self.custom_pcr[f'phase{i}'][f'profile1patt{j}']['params'][-1]
                    if int(strf) == 1:
                        iterate = 0
                        if int(strainmod) == 1:
                            iterate = 3
                        elif int(strainmod) == 2 or int(strainmod) == -2:
                            iterate = 2
                        elif 3 <= int(strainmod) <= 14:
                            iterate = 1
                        for k in range(iterate):
                            f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'strainaniso{k}patt{j}']['params'])}\n")
                            f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'strainaniso{k}patt{j}']['code'])}\n")
                        f.write(f"{' '.join(self.custom_pcr[f'phase{i}'][f'lorentzstrainpatt{j}'])}\n")
            nre = int(self.custom_pcr['params1global'][3])
            if nre > 0:
                for i in range(nre):
                    f.write(f"{' '.join(self.custom_pcr[f'limits{i}'])}\n")
            nli = int(self.custom_pcr['params2global'][2])
            if nli > 0:
                for i in range(nli):
                    f.write(f"{' '.join(self.custom_pcr[f'restraint{i}']['values'])}\n")
                    f.write(f"{' '.join(self.custom_pcr[f'restraint{i}']['coeff'])}\n")

    def exclude_patterns(self, vals: npt.NDArray[int]):
        if self.multipattern:
            npatt = int(self.custom_pcr['npatt'][0])
            self.custom_pcr['npatt'][1:npatt + 1] = [str(i) for i in vals]
        else:
            pass

    def get_patt_num(self):
        if self.multipattern:
            return int(self.custom_pcr['npatt'][0])
        else:
            return 1

    def get_flags_global(self, tag: str, ipatt: int = None):
        if self.multipattern and ipatt is not None:
            key = f'{self._flag_tags[tag][0]}patt{ipatt}'
            return fast_real(self.custom_pcr[key][self._flag_tags[tag][1][0]])
        elif self.multipattern and ipatt is None:
            key = f'{self._flag_tags[tag][0]}global'
            return fast_real(self.custom_pcr[key][self._flag_tags[tag][1][0]])
        else:
            key = self._flag_tags[tag][0]
            return fast_real(self.custom_pcr[key][self._flag_tags[tag][1][1]])

    def edit_flags_global(self, tag: str, val: Union[int, float], ipatt: int = None):
        if self.multipattern and ipatt is not None:
            key = f'{self._flag_tags[tag][0]}patt{ipatt}'
            self.custom_pcr[key][self._flag_tags[tag][1][0]] = str(val)
        elif self.multipattern and ipatt is None:
            key = f'{self._flag_tags[tag][0]}global'
            self.custom_pcr[key][self._flag_tags[tag][1][0]] = str(val)
        else:
            key = self._flag_tags[tag][0]
            self.custom_pcr[key][self._flag_tags[tag][1][1]] = str(val)

    def get_shifts_global(self, tag: str, ipatt: int = None):
        job = self.get_flags_global('job', ipatt)
        if job < 0:
            tags = self._shift_tags['tof']
        else:
            tags = self._shift_tags['default']

        if self.multipattern and ipatt is not None:
            key = f'{tags[tag][0]}patt{ipatt}'
            return fast_real(self.custom_pcr[key][tags[tag][1][0]])
        else:
            key = tags[tag][0]
            return fast_real(self.custom_pcr[key][tags[tag][1][1]])

    def edit_shifts_global(self, tag: str, val: Union[int, float], ipatt: int = None):
        job = self.get_flags_global('job', ipatt)
        if job < 0:
            tags = self._shift_tags['tof']
        else:
            tags = self._shift_tags['default']

        if self.multipattern and ipatt is not None:
            key = f'{tags[tag][0]}patt{ipatt}'
            self.custom_pcr[key][tags[tag][1][0]] = str(val)
        else:
            key = tags[tag][0]
            self.custom_pcr[key][tags[tag][1][1]] = str(val)

    def replace_zero_more(self, ipatt: int = None):
        self.edit_shifts_global('more_zero', 1, ipatt)
        if self.multipattern and ipatt is not None:
            self.custom_pcr[f'params5morepatt{ipatt}'] = '0.000 0.0 0.000 0.0 0.000 0.0'.split()
        else:
            self.custom_pcr[f'params5more'] = '0.000 0.0 0.000 0.0 0.000 0.0'.split()

    def remove_zero_more(self, ipatt: int = None):
        self.edit_shifts_global('more_zero', 0, ipatt)
        if self.multipattern and ipatt is not None:
            self.custom_pcr.pop(f'params5morepatt{ipatt}', None)
        else:
            self.custom_pcr.pop(f'params5more', None)

    def get_excluded(self, ipatt: int):
        nex = self.get_flags_global('nex', ipatt)
        if nex > 0:
            if self.multipattern:
                return self.custom_pcr[f'excludepatt{ipatt}']
            else:
                return self.custom_pcr['exclude']
        else:
            return None

    def replace_excluded(self, excluded: npt.NDArray[np.float64], ipatt: int = None):
        self.edit_flags_global('nex', excluded.shape[0], ipatt)
        if self.multipattern and ipatt is not None:
            self.custom_pcr[f'excludepatt{ipatt}'] = excluded
        else:
            self.custom_pcr[f'exclude'] = excluded

    def remove_excluded(self, ipatt: int = None):
        self.edit_flags_global('nex', 0, ipatt)
        if self.multipattern and ipatt is not None:
            self.custom_pcr.pop(f'excludepatt{ipatt}', None)
        else:
            self.custom_pcr.pop(f'exclude', None)

    def get_background(self, ipatt: int = None):
        nbckgr = self.get_flags_global('nba', ipatt)
        if nbckgr > 2:
            mode = BackgroundType.POINTS
        elif nbckgr == -5:
            mode = BackgroundType.CHEBYSHEV
        elif nbckgr == -2:
            mode = BackgroundType.FOURIER
        else:
            return None

        if self.multipattern and ipatt is not None:
            return [self.custom_pcr[f'backgroundpatt{ipatt}'], mode]
        else:
            return [self.custom_pcr[f'background'], mode]

    def transform_anchor_to_cheb(self, order: int, ipatt: int = None):
        anchor, mode = self.get_background(ipatt)

        if anchor.shape[1] == 3:
            x = 2.0 * (anchor[:, 0] - 0.5 * (anchor[-1, 0] + anchor[0, 0])) / (anchor[-1, 0] - anchor[0, 0])
            background = np.polynomial.chebyshev.chebfit(x=x, y=anchor[:, 1], deg=order)
            vals = background.shape[0]
            zeros = np.zeros(24)
            zeros[:vals] = background
            background = zeros
            background = background.reshape((4, 6))
            zeros = np.zeros(np.shape(background), dtype=float)

            final = np.empty((background.shape[0] + zeros.shape[0], background.shape[1]))
            final[::2, :] = background
            final[1::2, :] = zeros

            self.replace_background(final, ipatt, mode = BackgroundType.CHEBYSHEV)

    @staticmethod
    def expand_chebyshev_polinomial(x: npt.NDArray[np.float64], order: int):
        functions = []
        for i in range(order):
            functions.append(np.nan_to_num(np.cos(i * np.arccos(x)), nan=0.999999))

        return functions
    
    def transform_cheb_to_background(self, x: npt.NDArray[np.float64], ipatt: int = None):
        coefficients, mode = self.get_background(ipatt)
        if coefficients.shape[1] == 6 and coefficients.shape[0] == 8:
            x_norm = 2.0 * (x[:] - 0.5 * (x[-1] + x[0])) / (x[-1] - x[0])
            flatcoef = coefficients[::2, :].flatten()
            flatcoef = flatcoef[np.abs(flatcoef) > 0]
            functions = np.array(self.expand_chebyshev_polinomial(x_norm, flatcoef.shape[0]))
            background = np.sum(functions * flatcoef[:, np.newaxis], axis=0)

            return background
        
        return None

    def transform_cheb_to_anchor(self, x: npt.NDArray[np.float64], ipatt: int = None, nba: int = 30):
        background = self.transform_cheb_to_background(x, ipatt)
        if background is not None:
            xsize = background.shape[0]
            xstep = int(xsize / nba)

            if xsize % xstep == 0:
                X = x[::xstep]
                Y = background[::xstep]
                zeros = np.zeros(Y.shape[0])

                anchor_points = np.column_stack((X, Y, zeros))
            else:
                X = x[::xstep]
                Y = background[::xstep]

                X = np.append(X, x[-1])
                Y = np.append(Y, background[-1])
                zeros = np.zeros(Y.shape[0])

                anchor_points = np.column_stack((X, Y, zeros))

            self.replace_background(anchor_points[1:-1, :], ipatt, mode = BackgroundType.POINTS)

    def replace_background(self, background: npt.NDArray, ipatt: int = None, mode: BackgroundType = BackgroundType.POINTS):
        if mode == BackgroundType.POINTS and background.shape[1] == 3:
            self.edit_flags_global('nba', background.shape[0], ipatt)
        elif mode == BackgroundType.POINTS and background.shape[1] != 3:
            raise ValueError('Wrong shape of for background type POINTS')
        
        if mode == BackgroundType.CHEBYSHEV and background.shape[1] == 6 and background.shape[0] == 8:
            self.edit_flags_global('nba', -5, ipatt)
        elif mode == BackgroundType.CHEBYSHEV and background.shape[1] != 6 and background.shape[0] != 8:
            raise ValueError('Wrong shape of for background type CHEBYSHEV')
        
        if mode == BackgroundType.FOURIER and background.shape[1] == 2 and background.shape[0] == 1:
            self.edit_flags_global('nba', -2, ipatt)
        elif mode == BackgroundType.FOURIER and background.shape[1] != 2 and background.shape[0] != 1:
            raise ValueError('Wrong shape of for background type FOURIER')

        if self.multipattern and ipatt is not None:
            self.custom_pcr[f'backgroundpatt{ipatt}'] = background
        else:
            self.custom_pcr[f'background'] = background

    def remove_background(self, ipatt: int = None):
        self.edit_flags_global('nba', 0, ipatt)
        if self.multipattern and ipatt is not None:
            self.custom_pcr.pop(f'backgroundpatt{ipatt}', None)
        else:
            self.custom_pcr.pop(f'background', None)

    def edit_background_code(self, opt: bool = False, ipatt: int = None):
        background, mode = self.get_background(ipatt)
        if mode == BackgroundType.POINTS:
            if opt:
                background[:, 2] = 1.0
                excluded = self.get_excluded(ipatt)
                if excluded is not None:
                    for region in excluded:
                        mask1 = background[:, 0] < region[0]
                        mask2 = background[:, 0] < region[1]
                        mask_in = mask1 ^ mask2
                        background[mask_in, 2] = 0.0
            else:
                background[:, 2] = 0.0

            self.replace_background(background, ipatt, mode = mode)

        elif mode == BackgroundType.CHEBYSHEV:
            if opt:
                size = np.where(np.abs(background[::2, :].flatten()) > 0.0000000001)[0]
                sub = background[1::2, :].flatten()
                sub[:] = 0.0
                sub[:size.shape[0]] = 1.0
                background[1::2, :] = sub.reshape((4, 6))
            else:
                background[1::2, :] = 0.0

            self.replace_background(background, ipatt, mode = mode)

    def get_irf(self, ipatt: int = None):
        if self.multipattern and ipatt is not None:
            return self.custom_pcr[f'irfpatt{ipatt}']
        else:
            return self.custom_pcr[f'irf']

    def replace_irf(self, filename: str, ipatt: int = None):
        if self.multipattern and ipatt is not None:
            self.custom_pcr[f'irfpatt{ipatt}'] = filename
        else:
            self.custom_pcr[f'irf'] = filename

    def get_names(self, ipatt: int = None):
        if self.multipattern and ipatt is not None:
            return self.custom_pcr[f'namepatt{ipatt}']
        else:
            return None

    def replace_names(self, filename: str, ipatt: int = None):
        if self.multipattern and ipatt is not None:
            self.custom_pcr[f'namepatt{ipatt}'] = filename

    def read_ids(self):
        nph = self.get_flags_global('nph')
        identifiers = []
        for i in range(nph):
            identifier = self.custom_pcr[f'phase{i}']['name']
            identifier = identifier.replace(".cif", "")
            identifiers.append(identifier)

        return identifiers

    def read_site_labels(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        nat = self.count_site_phase(identifier)
        if nat > 0:
            atoms = []
            for i in range(nat):
                atoms.append(self.custom_pcr[f'phase{idx}'][f'atom{i}']['params'][0])
            return atoms
        else:
            return None

    def read_site_elements(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        nat = self.get_flags_phase(identifier, 'nat')
        if nat > 0:
            elements = []
            for i in range(nat):
                elem = self.custom_pcr[f'phase{idx}'][f'atom{i}']['params'][1]
                if elem not in elements:
                    elements.append(elem)
            return elements
        else:
            return None

    def remove_sites(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        nat = self.count_site_phase(identifier)
        if nat > 0:
            for i in range(nat):
                self.custom_pcr[f'phase{idx}'].pop(f'atom{i}', None)

            self.edit_flags_phase(identifier, 'nat', 0)

    def get_flags_phase(self, identifier: str, tag: str, ipatt: int = None):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if self.multipattern and ipatt is not None:
            key = f'{self._phase_tags[tag][0][0]}patt{ipatt}'
            return fast_real(self.custom_pcr[f"phase{idx}"][key][self._phase_tags[tag][1][0]])
        elif self.multipattern and ipatt is None:
            key = f'{self._phase_tags[tag][0][0]}'
            return fast_real(self.custom_pcr[f"phase{idx}"][key][self._phase_tags[tag][1][0]])
        else:
            key = self._phase_tags[tag][0][1]
            return fast_real(self.custom_pcr[f"phase{idx}"][key][self._phase_tags[tag][1][1]])

    def edit_flags_phase(self, identifier: str, tag: str, val: Union[int, float], ipatt: int = None):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if self.multipattern and ipatt is not None:
            key = f'{self._phase_tags[tag][0][0]}patt{ipatt}'
            self.custom_pcr[f"phase{idx}"][key][self._phase_tags[tag][1][0]] = str(val)
        elif self.multipattern and ipatt is None:
            key = f'{self._phase_tags[tag][0][0]}'
            self.custom_pcr[f"phase{idx}"][key][self._phase_tags[tag][1][0]] = str(val)
        else:
            key = self._phase_tags[tag][0][1]
            self.custom_pcr[f"phase{idx}"][key][self._phase_tags[tag][1][1]] = str(val)

    def add_phase_more(self, identifier: str):
        self.edit_flags_phase(identifier, 'more_phase', 1)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if self.multipattern:
            self.custom_pcr[f"phase{idx}"]['params1more'] = '0   0  0   0   0   0    0'.split()
        else:
            self.custom_pcr[f"phase{idx}"]['params1more'] = '0 0 0 0 0 0 1.0000 0.0000 0.0000 0.0000 0 0 0 0'.split()

    def remove_phase_more(self, identifier: str):
        self.edit_flags_phase(identifier, 'more_phase', 0)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.custom_pcr[f"phase{idx}"].pop('params1more', None)
        self.custom_pcr[f"phase{idx}"].pop('bvs', None)
        self.custom_pcr[f"phase{idx}"].pop('bvs_ions', None)

    def get_phase_volume(self, identifier: str, ipatt: int = None):
        a = self.get_profile_phase(identifier, 'a', ipatt)
        b = self.get_profile_phase(identifier, 'b', ipatt)
        c = self.get_profile_phase(identifier, 'c', ipatt)
        alpha = self.get_profile_phase(identifier, 'alpha', ipatt)
        beta = self.get_profile_phase(identifier, 'beta', ipatt)
        gamma = self.get_profile_phase(identifier, 'gamma', ipatt)

        return crysfml_forpy.get_crystal_volume(np.array([a, b, c, alpha, beta, gamma], np.float32))[0]

    def add_phshifts(self, identifier: str, ipatt: int = None):
        self.edit_flags_phase(identifier, 'phshift', 1, ipatt)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if self.multipattern:
            self.custom_pcr[f"phase{idx}"][f'shiftspatt{ipatt}'] = {'params': '0.0000 0.0000 0.0000'.split()}
            self.custom_pcr[f"phase{idx}"][f'shiftspatt{ipatt}']['code'] = '0.0000 0.0000 0.0000'.split()
        else:
            self.custom_pcr[f"phase{idx}"][f'shifts'] = {'params': '0.0000 0.0000 0.0000'.split()}
            self.custom_pcr[f"phase{idx}"][f'shifts']['code'] = '0.0000 0.0000 0.0000'.split()

    def remove_phshifts(self, identifier: str, ipatt: int = None):
        self.edit_flags_phase(identifier, 'phshift', 0, ipatt)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if self.multipattern:
            self.custom_pcr[f"phase{idx}"].pop(f'shiftspatt{ipatt}', None)
        else:
            self.custom_pcr[f"phase{idx}"].pop(f'shifts', None)

    def add_bvs(self, identifier: str):
        self.edit_flags_phase(identifier, 'jdi', 3)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.custom_pcr[f"phase{idx}"]['bvs'] = '3.000 3.000'.split()
        self.custom_pcr[f"phase{idx}"]['bvs'].extend([''])

    def remove_bvs(self, identifier: str):
        self.edit_flags_phase(identifier, 'jdi', 0)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.custom_pcr[f"phase{idx}"].pop('bvs', None)
        self.custom_pcr[f"phase{idx}"].pop('bvs_ions', None)

    def get_init_oxistate(self, identifier: str):
        """ Load elements and obtain oxi states """
        elements = self.read_site_elements(identifier=identifier)
        if elements is not None:
            anions = []
            cations = []
            for elem in elements:
                elem_pymatgen = self.format_element_strings(elem)
                pyelem = Element(elem_pymatgen)
                oxistate = pyelem.common_oxidation_states
                if oxistate[0] < 0:
                    anions.append(f"{elem}{str(oxistate[0])}")
                else:
                    cations.append(f"{elem}+{oxistate[0]}")
            return [cations, anions]
        else:
            return None

    @staticmethod
    def format_element_strings(elem: str):
        return elem.capitalize()

    def add_bvs_ions(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.custom_pcr[f"phase{idx}"]['bvs'][-1] = 'BVS'
        oxis = self.get_init_oxistate(identifier)
        if oxis is not None:
            self.custom_pcr[f"phase{idx}"]['bvs_ions'] = {'nions': [len(oxis[0]), len(oxis[1])]}
            self.custom_pcr[f"phase{idx}"]['bvs_ions']['cations'] = ' '.join(oxis[0])
            self.custom_pcr[f"phase{idx}"]['bvs_ions']['anions'] = ' '.join(oxis[1])
            self.set_correct_spc(identifier)

    def remove_bvs_ions(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.custom_pcr[f"phase{idx}"]['bvs'][-1] = ''
        self.custom_pcr[f"phase{idx}"].pop('bvs_ions', None)
        self.set_zero_spc(identifier)

    def replace_bvs_cations(self, identifier: str, text: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.custom_pcr[f"phase{idx}"]['bvs_ions']['cations'] = text
        self.custom_pcr[f"phase{idx}"]['bvs_ions']['nions'][0] = len(text.split())

    def replace_bvs_anions(self, identifier: str, text: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.custom_pcr[f"phase{idx}"]['bvs_ions']['anions'] = text
        self.custom_pcr[f"phase{idx}"]['bvs_ions']['nions'][1] = len(text.split())

    def set_zero_spc(self, identifier: str):
        nat = self.get_flags_phase(identifier, 'nat')
        if nat > 0:
            labels = self.read_site_labels(identifier)
            for j in range(nat):
                self.edit_site_phase(identifier, labels[j], 0, 'spc')

    def set_correct_spc(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        cations = self.custom_pcr[f"phase{idx}"]['bvs_ions']['cations'].split()
        anions = self.custom_pcr[f"phase{idx}"]['bvs_ions']['anions'].split()

        listel = [els[:-2] for els in cations if '+' in els] + [els[:-2] for els in anions if '-' in els]
        nat = self.count_site_phase(identifier)
        if nat > 0:
            labels = self.read_site_labels(identifier)
            elements = self.read_site_elements(identifier)
            spcs = [0 for _ in labels]
            for elem in elements:
                idx_ions = cycle([i + 1 for i, val in enumerate(listel) if val == elem])
                idx_elem = [i for i, val in enumerate(labels) if self.get_site_phase(identifier, val, 'type') == elem]
                for i in idx_elem:
                    try:
                        spcs[i] = next(idx_ions)
                    except StopIteration:
                        pass

            for i, spc in enumerate(spcs):
                self.edit_site_phase(identifier, labels[i], spc, 'spc')

    def add_anisotropic_site(self, identifier: str, atom: str):
        self.edit_site_phase(identifier, atom, 2, 'nt')
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        ida = [i for i, val in enumerate(self.read_site_labels(identifier)) if val == atom][0]

        self.custom_pcr[f'phase{idx}'][f'atom{ida}']['anisotropy'] =\
            '0.01116  0.00087  0.00501  0.00000  0.00000   0.00040'.split()
        self.custom_pcr[f'phase{idx}'][f'atom{ida}']['codeaniso'] = \
            '0.00     0.00     0.00     0.00     0.00      0.00'.split()

    def remove_anisotropic_site(self, identifier: str, atom: str):
        self.edit_site_phase(identifier, atom, 0, 'nt')
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        ida = [i for i, val in enumerate(self.read_site_labels(identifier)) if val == atom][0]
        self.custom_pcr[f'phase{idx}'][f'atom{ida}'].pop('anisotropy', None)
        self.custom_pcr[f'phase{idx}'][f'atom{ida}'].pop('codeaniso', None)

    def get_profile_phase(self, identifier: str, tag: str, ipatt: int = None):
        job = self.get_flags_global('job', ipatt)
        if job < 0:
            tags = self._profile_tags['tof']
        else:
            tags = self._profile_tags['default']
        
        strainparams = ['s400', 's040', 's004', 's220', 's202',
                        's022', 's211', 's121', 's112', 's310',
                        's301', 's130', 's103', 's013', 's031',
                        's400code', 's040code', 's004code', 's220code', 's202code',
                        's022code', 's211code', 's121code', 's112code', 's310code',
                        's301code', 's130code', 's103code', 's013code', 's031code']
        
        sizeparams = ['y00', 'y22+', 'y22-', 'y20', 'y44+', 'y44-', 'y42+', 'y42-', 'y40',
                      'y43-', 'y60', 'y63-', 'y66+', 'y66-', 'k00', 'k41', 'k61', 'k62', 'k81',
                      'y64+', 'y64-', 'y21+', 'y21-',
                      'y00code', 'y22+code', 'y22-code', 'y20code', 'y44+code', 'y44-code',
                      'y42+code', 'y42-code', 'y40code', 'y43-code', 'y60code', 'y63-code',
                      'y66+code', 'y66-code', 'k00code', 'k41code', 'k61code', 'k62code',
                      'k81code', 'y64+code', 'y64-code', 'y21+code', 'y21-code']
        
        try:
            if tag in strainparams:
                return self._get_strain_anisotropy(identifier, tag, ipatt)
            elif tag in sizeparams:
                return self._get_size_anisotropy(identifier, tag, ipatt)
            elif tag in ['lorentzstr', 'lorentzstrcode']:
                idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
                if self.multipattern and ipatt is not None:
                    key = f'{tags[tag][0]}patt{ipatt}'
                    return fast_real(self.custom_pcr[f"phase{idx}"][key][tags[tag][1][0]])
                else:
                    key = tags[tag][0]
                    return fast_real(self.custom_pcr[f"phase{idx}"][key][tags[tag][1][1]])
            else:
                idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
                if self.multipattern and ipatt is not None:
                    key = f'{tags[tag][0]}patt{ipatt}'
                    return fast_real(self.custom_pcr[f"phase{idx}"][key][tags[tag][1]][tags[tag][2][0]])
                else:
                    key = tags[tag][0]
                    return fast_real(self.custom_pcr[f"phase{idx}"][key][tags[tag][1]][tags[tag][2][1]])
        except (IndexError, KeyError, UnboundLocalError):
            pass

    def edit_profile_phase(self, identifier: str, tag: str, val: Union[int, float], ipatt: int = None):
        job = self.get_flags_global('job', ipatt)
        if job < 0:
            tags = self._profile_tags['tof']
        else:
            tags = self._profile_tags['default']

        strainparams = ['s400', 's040', 's004', 's220', 's202',
                        's022', 's211', 's121', 's112', 's310',
                        's301', 's130', 's103', 's013', 's031',
                        's400code', 's040code', 's004code', 's220code', 's202code',
                        's022code', 's211code', 's121code', 's112code', 's310code',
                        's301code', 's130code', 's103code', 's013code', 's031code']
        
        sizeparams = ['y00', 'y22+', 'y22-', 'y20', 'y44+', 'y44-', 'y42+', 'y42-', 'y40',
                      'y43-', 'y60', 'y63-', 'y66+', 'y66-', 'k00', 'k41', 'k61', 'k62', 'k81',
                      'y64+', 'y64-', 'y21+', 'y21-', 'y43+',
                      'y00code', 'y22+code', 'y22-code', 'y20code', 'y44+code', 'y44-code',
                      'y42+code', 'y42-code', 'y40code', 'y43-code', 'y60code', 'y63-code',
                      'y66+code', 'y66-code', 'k00code', 'k41code', 'k61code', 'k62code',
                      'k81code', 'y64+code', 'y64-code', 'y21+code', 'y21-code', 'y43+code']
        try:
            if tag in strainparams:
                self._edit_strain_anisotropy(identifier, tag, val, ipatt)
            elif tag in sizeparams:
                self._edit_size_anisotropy(identifier, tag, val, ipatt)
            elif tag in ['lorentzstr', 'lorentzstrcode']:
                idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
                if self.multipattern and ipatt is not None:
                    key = f'{tags[tag][0]}patt{ipatt}'
                    self.custom_pcr[f"phase{idx}"][key][tags[tag][1][0]] = str(val)
                else:
                    key = tags[tag][0]
                    self.custom_pcr[f"phase{idx}"][key][tags[tag][1][1]] = str(val)
            else:
                idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
                if self.multipattern and ipatt is not None:
                    key = f'{tags[tag][0]}patt{ipatt}'
                    self.custom_pcr[f"phase{idx}"][key][tags[tag][1]][tags[tag][2][0]] = str(val)
                else:
                    key = tags[tag][0]
                    self.custom_pcr[f"phase{idx}"][key][tags[tag][1]][tags[tag][2][1]] = str(val)
        except (IndexError, KeyError, UnboundLocalError):
            pass

    def _get_strain_anisotropy(self, identifier: str, tag: str, ipatt: int = None):
        strf = self.get_flags_phase(identifier, 'str', ipatt)
        strainmodel = self.get_profile_phase(identifier, 'straintag', ipatt)
        if strf == 1:
            if strainmodel == 1:
                params = [['s400', 's040', 's004', 's220', 's202'],
                          ['s022', 's211', 's121', 's112', 's310'],
                          ['s301', 's130', 's103', 's013', 's031']]
                code = [['s400code', 's040code', 's004code', 's220code', 's202code'],
                        ['s022code', 's211code', 's121code', 's112code', 's310code'],
                        ['s301code', 's130code', 's103code', 's013code', 's031code']]           
            elif strainmodel == 2:
                params = [['s400', 's040', 's004', 's220', 's202'],
                          ['s022', 's121', 's301', 's103']]
                code = [['s400code', 's040code', 's004code', 's220code', 's202code'],
                        ['s022code', 's121code', 's301code', 's103code']]
            elif strainmodel == -2:
                params = [['s400', 's040', 's004', 's220', 's202'],
                          ['s022', 's112', 's310', 's130']]
                code = [['s400code', 's040code', 's004code', 's220code', 's202code'],
                        ['s022code', 's112code', 's310code', 's130code']]
            elif strainmodel == 3:
                params = [['s400', 's040', 's004', 's220', 's202', 's022']]      
                code = [['s400code', 's040code', 's004code', 's220code', 's202code', 's022code']]
            elif strainmodel == 4 or strainmodel == 5:
                params = [['s400', 's004', 's220', 's202']]      
                code = [['s400code', 's004code', 's220code', 's202code']]
            elif strainmodel == 6 or strainmodel == 7:
                params = [['s400', 's004', 's112', 's211']]      
                code = [['s400code', 's004code', 's112code', 's211code']]
            elif strainmodel == 8 or strainmodel == 9 or strainmodel == 10 or strainmodel == 11 or strainmodel == 12:
                params = [['s400', 's004', 's112']]      
                code = [['s400code', 's004code', 's112code']]
            elif strainmodel == 13 or strainmodel == 14:
                params = [['s400', 's220']]      
                code = [['s400code', 's220code']]

            idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
            idxp = next(((row, params.index(tag)) for row, params in enumerate(params) if tag in params), None)
            idxc = next(((row, code.index(tag)) for row, code in enumerate(code) if tag in code), None)

            if self.multipattern and ipatt is not None:
                if idxp is not None and idxc is None:
                    return fast_real(self.custom_pcr[f"phase{idx}"][f'strainaniso{idxp[0]}patt{ipatt}']['params'][idxp[1]])
                elif idxp is None and idxc is not None:
                    return fast_real(self.custom_pcr[f"phase{idx}"][f'strainaniso{idxc[0]}patt{ipatt}']['code'][idxc[1]])
            else:
                if idxp is not None and idxc is None:
                    return fast_real(self.custom_pcr[f"phase{idx}"][f'strainaniso{idxp[0]}']['params'][idxp[1]])
                elif idxp is None and idxc is not None:
                    return fast_real(self.custom_pcr[f"phase{idx}"][f'strainaniso{idxc[0]}']['code'][idxc[1]])

    def _edit_strain_anisotropy(self, identifier: str, tag: str, val: Union[int, float], ipatt: int = None):
        strf = self.get_flags_phase(identifier, 'str', ipatt)
        strainmodel = self.get_profile_phase(identifier, 'straintag', ipatt)
        if strf == 1:
            if strainmodel == 1:
                params = [['s400', 's040', 's004', 's220', 's202'],
                          ['s022', 's211', 's121', 's112', 's310'],
                          ['s301', 's130', 's103', 's013', 's031']]
                code = [['s400code', 's040code', 's004code', 's220code', 's202code'],
                        ['s022code', 's211code', 's121code', 's112code', 's310code'],
                        ['s301code', 's130code', 's103code', 's013code', 's031code']]           
            elif strainmodel == 2:
                params = [['s400', 's040', 's004', 's220', 's202'],
                          ['s022', 's121', 's301', 's103']]
                code = [['s400code', 's040code', 's004code', 's220code', 's202code'],
                        ['s022code', 's121code', 's301code', 's103code']]
            elif strainmodel == -2:
                params = [['s400', 's040', 's004', 's220', 's202'],
                          ['s022', 's112', 's310', 's130']]
                code = [['s400code', 's040code', 's004code', 's220code', 's202code'],
                        ['s022code', 's112code', 's310code', 's130code']]
            elif strainmodel == 3:
                params = [['s400', 's040', 's004', 's220', 's202', 's022']]      
                code = [['s400code', 's040code', 's004code', 's220code', 's202code', 's022code']]
            elif strainmodel == 4 or strainmodel == 5:
                params = [['s400', 's004', 's220', 's202']]      
                code = [['s400code', 's004code', 's220code', 's202code']]
            elif strainmodel == 6 or strainmodel == 7:
                params = [['s400', 's004', 's112', 's211']]      
                code = [['s400code', 's004code', 's112code', 's211code']]
            elif strainmodel == 8 or strainmodel == 9 or strainmodel == 10 or strainmodel == 11 or strainmodel == 12:
                params = [['s400', 's004', 's112']]      
                code = [['s400code', 's004code', 's112code']]
            elif strainmodel == 13 or strainmodel == 14:
                params = [['s400', 's220']]      
                code = [['s400code', 's220code']]

            idx = [i for i, phase in enumerate(self.read_ids()) if phase == identifier][0]
            idxp = next(((row, params.index(tag)) for row, params in enumerate(params) if tag in params), None)
            idxc = next(((row, code.index(tag)) for row, code in enumerate(code) if tag in code), None)

            if self.multipattern and ipatt is not None:
                if idxp is not None and idxc is None:
                    self.custom_pcr[f"phase{idx}"][f'strainaniso{idxp[0]}patt{ipatt}']['params'][idxp[1]] = str(val)
                elif idxp is None and idxc is not None:
                    self.custom_pcr[f"phase{idx}"][f'strainaniso{idxc[0]}patt{ipatt}']['code'][idxc[1]] = str(val)
            else:
                if idxp is not None and idxc is None:
                    self.custom_pcr[f"phase{idx}"][f'strainaniso{idxp[0]}']['params'][idxp[1]] = str(val)
                elif idxp is None and idxc is not None:
                    self.custom_pcr[f"phase{idx}"][f'strainaniso{idxc[0]}']['code'][idxc[1]] = str(val)

    def _get_size_anisotropy(self, identifier: str, tag: str, ipatt: int = None):
        sizemodel = self.get_profile_phase(identifier, 'sizetag', ipatt)
        if sizemodel == 15:
            params = [['y00', 'y22+', 'y22-', 'y20', 'y44+', 'y44-'],
                      ['y42+', 'y42-', 'y40']]
            code = [['y00code', 'y22+code', 'y22-code', 'y20code', 'y44+code', 'y44-code'],
                    ['y42+code', 'y42-code', 'y40code']]
        elif sizemodel == 16:
            params = [['y00', 'y20', 'y40', 'y43-', 'y60', 'y63-'],
                      ['y66+']]
            code = [['y00code', 'y20code', 'y40code', 'y43-code', 'y60code', 'y63-code'],
                    ['y66+code']]
        elif sizemodel == 17:
            params = [['k00', 'k41', 'k61', 'k62', 'k81']]
            code = [['k00code', 'k41code', 'k61code', 'k62code', 'k81code']]
        elif sizemodel == 18:
            params = [['y00', 'y20', 'y22+', 'y40', 'y42+', 'y44+']]
            code = [['y00code', 'y20code', 'y22+code', 'y40code', 'y42+code', 'y44+code']]
        elif sizemodel == 19:
            params = [['y00', 'y20', 'y40', 'y60', 'y66+', 'y66-']]
            code = [['y00code', 'y20code', 'y40code', 'y60code', 'y66+code', 'y66-code']]
        elif sizemodel == 20:
            params = [['y00', 'y20', 'y40', 'y43-', 'y43+']]
            code = [['y00code', 'y20code', 'y40code', 'y43-code', 'y43+code']]
        elif sizemodel == 21:
            params = [['y00', 'y20', 'y40', 'y44+', 'y44-', 'y60'],
                      ['y64+', 'y64-']]
            code = [['y00code', 'y20code', 'y40code', 'y44+code', 'y44-code', 'y60code'],
                    ['y64+code', 'y64-code']]
        elif sizemodel == 22:
            params = [['y00', 'y20', 'y21+', 'y21-', 'y22+', 'y22-']]
            code = [['y00code', 'y20code', 'y21+code', 'y21-code', 'y22+code', 'y22-code']]

        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        idxp = next(((row, params.index(tag)) for row, params in enumerate(params) if tag in params), None)
        idxc = next(((row, code.index(tag)) for row, code in enumerate(code) if tag in code), None)

        if self.multipattern and ipatt is not None:
            if idxp is not None and idxc is None:
                return fast_real(self.custom_pcr[f"phase{idx}"][f'sizeharmonics{idxp[0]}patt{ipatt}']['params'][idxp[1]])
            elif idxp is None and idxc is not None:
                return fast_real(self.custom_pcr[f"phase{idx}"][f'sizeharmonics{idxc[0]}patt{ipatt}']['code'][idxc[1]])
        else:
            if idxp is not None and idxc is None:
                return fast_real(self.custom_pcr[f"phase{idx}"][f'sizeharmonics{idxp[0]}']['params'][idxp[1]])
            elif idxp is None and idxc is not None:
                return fast_real(self.custom_pcr[f"phase{idx}"][f'sizeharmonics{idxc[0]}']['code'][idxc[1]])

    def _edit_size_anisotropy(self, identifier: str, tag: str, val: Union[int,float], ipatt: int = None):
        sizemodel = self.get_profile_phase(identifier, 'sizetag', ipatt)
        if sizemodel == 15:
            params = [['y00', 'y22+', 'y22-', 'y20', 'y44+', 'y44-'],
                      ['y42+', 'y42-', 'y40']]
            code = [['y00code', 'y22+code', 'y22-code', 'y20code', 'y44+code', 'y44-code'],
                    ['y42+code', 'y42-code', 'y40code']]
        elif sizemodel == 16:
            params = [['y00', 'y20', 'y40', 'y43-', 'y60', 'y63-'],
                      ['y66+']]
            code = [['y00code', 'y20code', 'y40code', 'y43-code', 'y60code', 'y63-code'],
                    ['y66+code']]
        elif sizemodel == 17:
            params = [['k00', 'k41', 'k61', 'k62', 'k81']]
            code = [['k00code', 'k41code', 'k61code', 'k62code', 'k81code']]
        elif sizemodel == 18:
            params = [['y00', 'y20', 'y22+', 'y40', 'y42+', 'y44+']]
            code = [['y00code', 'y20code', 'y22+code', 'y40code', 'y42+code', 'y44+code']]
        elif sizemodel == 19:
            params = [['y00', 'y20', 'y40', 'y60', 'y66+', 'y66-']]
            code = [['y00code', 'y20code', 'y40code', 'y60code', 'y66+code', 'y66-code']]
        elif sizemodel == 20:
            params = [['y00', 'y20', 'y40', 'y43-', 'y43+']]
            code = [['y00code', 'y20code', 'y40code', 'y43-code', 'y43+code']]
        elif sizemodel == 21:
            params = [['y00', 'y20', 'y40', 'y44+', 'y44-', 'y60'],
                      ['y64+', 'y64-']]
            code = [['y00code', 'y20code', 'y40code', 'y44+code', 'y44-code', 'y60code'],
                    ['y64+code', 'y64-code']]
        elif sizemodel == 22:
            params = [['y00', 'y20', 'y21+', 'y21-', 'y22+', 'y22-']]
            code = [['y00code', 'y20code', 'y21+code', 'y21-code', 'y22+code', 'y22-code']]

        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        idxp = next(((row, params.index(tag)) for row, params in enumerate(params) if tag in params), None)
        idxc = next(((row, code.index(tag)) for row, code in enumerate(code) if tag in code), None)

        if self.multipattern and ipatt is not None:
            if idxp is not None and idxc is None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics{idxp[0]}patt{ipatt}']['params'][idxp[1]] = str(val)
            elif idxp is None and idxc is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics{idxc[0]}patt{ipatt}']['code'][idxc[1]] = str(val)
        else:
            if idxp is not None and idxc is None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics{idxp[0]}']['params'][idxp[1]] = str(val)
            elif idxp is None and idxc is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics{idxc[0]}']['code'][idxc[1]] = str(val)

    def remove_size_model(self, identifier: str, ipatt: int = None):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.edit_profile_phase(identifier, 'sizetag', 0, ipatt)
        if self.multipattern and ipatt is not None:
            for i in range(2):
                self.custom_pcr[f"phase{idx}"].pop(f'sizeharmonics{i}patt{ipatt}', None)
        else:
            for i in range(2):
                self.custom_pcr[f"phase{idx}"].pop(f'sizeharmonics{i}', None)

    def add_size_model(self, identifier: str, ipatt: int = None):
        laue = self.get_laue(identifier, ipatt)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if '12/m1' == laue or '112/m' == laue:
            self.edit_profile_phase(identifier, 'sizetag', 15, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1patt{ipatt}'] = {'params': '0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1patt{ipatt}']['code'] = '0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0'] = {'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1'] = {'params': '0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1']['code'] = '0.0 0.0 0.0'.split()

        elif '-3m1' == laue or '-31m' == laue:
            self.edit_profile_phase(identifier, 'sizetag', 16, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1patt{ipatt}'] = {'params': ['0.0']}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1patt{ipatt}']['code'] = ['0.0']
            else:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1'] = {'params': ['0.0']}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1']['code'] = ['0.0']
        elif 'm-3' == laue or 'm-3m' == laue:
            self.edit_profile_phase(identifier, 'sizetag', 17, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
        elif 'mmm' == laue:
            self.edit_profile_phase(identifier, 'sizetag', 18, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
        elif '6/m' == laue or '6/mmm' == laue:
            self.edit_profile_phase(identifier, 'sizetag', 19, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
        elif '-3' == laue:
            self.edit_profile_phase(identifier, 'sizetag', 20, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
        elif '4/m' == laue or '4/mmm' == laue:
            self.edit_profile_phase(identifier, 'sizetag', 21, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1patt{ipatt}'] = {'params': '0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1patt{ipatt}']['code'] = '0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0'] = {'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1'] = {'params': '0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics1']['code'] = '0.0 0.0'.split()
        elif '-1' == laue:
            self.edit_profile_phase(identifier, 'sizetag', 22, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'sizeharmonics0']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()

    def remove_strain_model(self, identifier: str, ipatt: int = None):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.edit_profile_phase(identifier, 'straintag', 0, ipatt)
        self.edit_flags_phase(identifier, 'str', 0)
        if self.multipattern and ipatt is not None:
            for i in range(3):
                self.custom_pcr[f"phase{idx}"].pop(f'strainaniso{i}patt{ipatt}', None)
            self.custom_pcr[f"phase{idx}"].pop(f'lorentzstrainpatt{ipatt}', None)
        else:
            for i in range(3):
                self.custom_pcr[f"phase{idx}"].pop(f'strainaniso{i}', None)
            self.custom_pcr[f"phase{idx}"].pop(f'lorentzstrain', None)

    def add_strain_model(self, identifier: str, ipatt: int = None):
        laue = self.get_laue(identifier, ipatt)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.edit_flags_phase(identifier, 'str', 1)
        if '-1' == laue:
            self.edit_profile_phase(identifier, 'straintag', 1, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'strainaniso1patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso1patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'strainaniso2patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso2patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0'] = {'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'strainaniso1'] = {'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso1']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'strainaniso2'] = {'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso2']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()

        elif '12/m1' == laue or '112/m' == laue:
            if '12/m1' == laue:
                self.edit_profile_phase(identifier, 'straintag', 2, ipatt)
            elif '112/m' == laue:
                self.edit_profile_phase(identifier, 'straintag', -2, ipatt)

            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'strainaniso1patt{ipatt}'] = {'params': '0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso1patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0']['code'] = '0.0 0.0 0.0 0.0 0.0'.split()
                self.custom_pcr[f"phase{idx}"][f'strainaniso1'] = {'params': '0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso1']['code'] = '0.0 0.0 0.0 0.0'.split()
        elif 'mmm' == laue:
            self.edit_profile_phase(identifier, 'straintag', 3, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0'] = {
                    'params': '0.0 0.0 0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0']['code'] = '0.0 0.0 0.0 0.0 0.0 0.0'.split()
        elif '4/m' == laue or '4/mmm' == laue:
            if '4/m' == laue:
                self.edit_profile_phase(identifier, 'straintag', 4, ipatt)
            elif '4/mmm' == laue:
                self.edit_profile_phase(identifier, 'straintag', 5, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}'] = {'params': '0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0'] = {'params': '0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0']['code'] = '0.0 0.0 0.0 0.0'.split()
        elif '-3R' == laue or '-3mR' == laue:
            if '-3R' == laue:
                self.edit_profile_phase(identifier, 'straintag', 6, ipatt)
            elif '-3mR' == laue:
                self.edit_profile_phase(identifier, 'straintag', 7, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}'] = {'params': '0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}']['code'] = '0.0 0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0'] = {'params': '0.0 0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0']['code'] = '0.0 0.0 0.0 0.0'.split()
        elif '-3' == laue or '-3m1' == laue or '-31m' == laue or '6/m' == laue or '6/mmm' == laue:
            if '-3' == laue:
                self.edit_profile_phase(identifier, 'straintag', 8, ipatt)
            elif '-3m1' == laue:
                self.edit_profile_phase(identifier, 'straintag', 9, ipatt)
            elif '-31m' == laue:
                self.edit_profile_phase(identifier, 'straintag', 10, ipatt)
            elif '6/m' == laue:
                self.edit_profile_phase(identifier, 'straintag', 11, ipatt)
            elif '6/mmm' == laue:
                self.edit_profile_phase(identifier, 'straintag', 12, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}'] = {'params': '0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}']['code'] = '0.0 0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0'] = {'params': '0.0 0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0']['code'] = '0.0 0.0 0.0'.split()
        elif 'm-3' == laue or 'm-3m' == laue:
            if 'm-3' == laue:
                self.edit_profile_phase(identifier, 'straintag', 13, ipatt)
            elif 'm-3m' == laue:
                self.edit_profile_phase(identifier, 'straintag', 14, ipatt)
            if self.multipattern and ipatt is not None:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}'] = {'params': '0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0patt{ipatt}']['code'] = '0.0 0.0'.split()
            else:
                self.custom_pcr[f"phase{idx}"][f'strainaniso0'] = {'params': '0.0 0.0'.split()}
                self.custom_pcr[f"phase{idx}"][f'strainaniso0']['code'] = '0.0 0.0'.split()

        if self.multipattern and ipatt is not None:
            self.custom_pcr[f"phase{idx}"][f'lorentzstrainpatt{ipatt}'] = '0.0 0.0'.split()
        else:
            self.custom_pcr[f"phase{idx}"][f'lorentzstrain'] = '0.0 0.0'.split()

    def get_laue(self, identifier: str, ipatt: int = None):
        hm = self.read_hm_symbol(identifier)
        a = self.get_profile_phase(identifier, 'a', ipatt)
        b = self.get_profile_phase(identifier, 'b', ipatt)
        c = self.get_profile_phase(identifier, 'c', ipatt)
        alpha = self.get_profile_phase(identifier, 'alpha', ipatt)
        beta = self.get_profile_phase(identifier, 'beta', ipatt)
        gamma = self.get_profile_phase(identifier, 'gamma', ipatt)

        r = crysfml_forpy.get_laue_num(hm, np.array([a, b, c, alpha, beta, gamma], dtype=np.float32))
        """ If trigonal system check cell type R of H? """
        if 'Trigonal' in r[1] and 'Trigonal' in r[2]:
            if 'm' in r[0]:
                return '-3mR'
            else:
                return '-3R'

        """ If monoclinic check the type 1 2/m 1 or 1 1 2/m """
        if 'Monoclinic' in r[1]:
            hm = hm.split()
            if hm[-1] == '1' and hm[1] == '1':
                return f"1{r[0].strip()}1"
            elif hm[1] == '1' and hm[2] == '1':
                return f"11{r[0].strip()}"
            else:
                return f"1{r[0].strip()}1"

        return r[0].strip()

    def count_site_phase(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        return sum('atom' in key for key in self.custom_pcr[f'phase{idx}'].keys())

    def get_site_phase(self, identifier: str, atom: str, tag: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        ida = [i for i, val in enumerate(self.read_site_labels(identifier)) if val == atom][0]

        val = self.custom_pcr[f'phase{idx}'][f'atom{ida}'][self._site_tags[tag][0]][self._site_tags[tag][1]]
        return fast_real(val) if isfloat(val) else val

    def edit_site_phase(self, identifier: str, atom: str, val: Union[int, float, str], tag: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        ida = [i for i, val in enumerate(self.read_site_labels(identifier)) if val == atom][0]

        self.custom_pcr[f'phase{idx}'][f'atom{ida}'][self._site_tags[tag][0]][self._site_tags[tag][1]] = str(val)

    def add_site_phase(self, identifier: str, atom: str):
        nat = self.count_site_phase(identifier)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]

        self.custom_pcr[f'phase{idx}'][f'atom{nat}'] =\
            {'params': f"{atom} X 0.00000  0.00000  0.00000  0.00000   0.00000  0   0   2   0  ".split()}
        self.custom_pcr[f'phase{idx}'][f'atom{nat}']['code'] = \
            "0.00     0.00     0.00     0.00      0.00".split()
        self.custom_pcr[f'phase{idx}'][f'atom{nat}']['anisotropy'] = \
            '0.01116  0.00087  0.00501  0.00000  0.00000   0.00040'.split()
        self.custom_pcr[f'phase{idx}'][f'atom{nat}']['codeaniso'] = \
            '0.00     0.00     0.00     0.00     0.00      0.00'.split()

        self.edit_flags_phase(identifier, 'nat', nat + 1)

    def remove_site_phase(self, identifier: str, atom: str):
        nat = self.count_site_phase(identifier)
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        ida = [i for i, val in enumerate(self.read_site_labels(identifier)) if val == atom][0]
        self.custom_pcr[f'phase{idx}'].pop(f'atom{ida}', None)
        self.edit_flags_phase(identifier, 'nat', nat - 1)

    def replace_global_vary(self, vary: Union[List[str], str], ipatt: int = None):
        if self.multipattern and ipatt is not None:
            if isinstance(vary, list):
                self.custom_pcr[f'varyglobalpatt{ipatt}'] = f"VARY {' '.join(vary)}"
            else:
                self.custom_pcr[f'varyglobalpatt{ipatt}'] = vary
        else:
            if isinstance(vary, list):
                self.custom_pcr[f'varyglobal'] = f"VARY {' '.join(vary)}"
            else:
                self.custom_pcr[f'varyglobal'] = vary

    def replace_global_fix(self, fix: Union[List[str], str], ipatt: int = None):
        if self.multipattern and ipatt is not None:
            if isinstance(fix, list):
                self.custom_pcr[f'fixglobalpatt{ipatt}'] = f"FIX {' '.join(fix)}"
            else:
                self.custom_pcr[f'fixglobalpatt{ipatt}'] = fix
        else:
            if isinstance(fix, list):
                self.custom_pcr[f'fixglobal'] = f"FIX {' '.join(fix)}"
            else:
                self.custom_pcr[f'fixglobal'] = fix

    def replace_phase_vary(self, identifier: str, vary: Union[str, List[str]], num: int):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if isinstance(vary, list):
            self.custom_pcr[f'phase{idx}'][f'varyphase{num}'] = f"VARY {' '.join(vary)}"
        else:
            self.custom_pcr[f'phase{idx}'][f'varyphase{num}'] = vary

    def add_phase_vary(self, identifier: str, vary: List[str]):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        """ Remove phase number 'num' from custom_pcr dictionary """
        varykeys = [key for key in self.custom_pcr[f'phase{idx}'] if 'varyphase' in key]
        self.custom_pcr[f'phase{idx}'][f'varyphase{len(varykeys)}'] = f"VARY {' '.join(vary)}"

    def replace_phase_fix(self, identifier: str, fix: Union[str, List[str]], num: int):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if isinstance(fix, list):
            self.custom_pcr[f'phase{idx}'][f'fixphase{num}'] = f"FIX {' '.join(fix)}"
        else:
            self.custom_pcr[f'phase{idx}'][f'fixphase{num}'] = fix

    def add_phase_fix(self, identifier: str, fix: List[str]):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        """ Remove phase number 'num' from custom_pcr dictionary """
        fixkeys = [key for key in self.custom_pcr[f'phase{idx}'] if 'fixphase' in key]
        self.custom_pcr[f'phase{idx}'][f'fixphase{len(fixkeys)}'] = f"FIX {' '.join(fix)}"

    def replace_phase_fixspc(self, identifier: str, fixspc: Union[str, List[str]], num: int):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if isinstance(fixspc, list):
            self.custom_pcr[f'phase{idx}'][f'fixspc{num}'] = f"FIX_SPC {' '.join(fixspc)}"
        else:
            self.custom_pcr[f'phase{idx}'][f'fixspc{num}'] = fixspc

    def add_phase_fixspc(self, identifier: str, fixspc: List[str]):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        """ Remove phase number 'num' from custom_pcr dictionary """
        fixspckeys = [key for key in self.custom_pcr[f'phase{idx}'] if 'fixspc' in key]
        self.custom_pcr[f'phase{idx}'][f'fixspc{len(fixspckeys)}'] = f"FIX_SPC {' '.join(fixspc)}"

    def replace_phase_pequpat(self, identifier: str, pequpat: Union[str, List[str]], num: int):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        if isinstance(pequpat, list):
            self.custom_pcr[f'phase{idx}'][f'pequpat{num}'] = f"PEQU_pat {' '.join(pequpat)}"
        else:
            self.custom_pcr[f'phase{idx}'][f'pequpat{num}'] = pequpat

    def add_phase_pequpat(self, identifier: str, pequpat: List[str]):
        if self.multipattern:
            idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
            """ Remove phase number 'num' from custom_pcr dictionary """
            pequpatkeys = [key for key in self.custom_pcr[f'phase{idx}'] if 'pequpat' in key]
            self.custom_pcr[f'phase{idx}'][f'pequpat{len(pequpatkeys)}'] = f"PEQU_pat {' '.join(pequpat)}"

    def clear_fix_vary(self):
        if self.multipattern:
            npatt = int(self.custom_pcr['npatt'][0])
            for i in range(npatt):
                self.custom_pcr.pop(f'varyglobalpatt{i}', None)
                self.custom_pcr.pop(f'fixglobalpatt{i}', None)
        else:
            self.custom_pcr.pop(f'varyglobal', None)
            self.custom_pcr.pop(f'fixglobal', None)

        nph = self.get_flags_global('nph')
        for i in range(nph):
            varykeys = [key for key in self.custom_pcr[f'phase{i}'] if 'varyphase' in key]
            fixkeys = [key for key in self.custom_pcr[f'phase{i}'] if 'fixphase' in key]
            fixspckeys = [key for key in self.custom_pcr[f'phase{i}'] if 'fixspc' in key]
            pequpatkeys = [key for key in self.custom_pcr[f'phase{i}'] if 'pequpat' in key]

            for key in varykeys:self.custom_pcr[f'phase{i}'].pop(key, None)
            for key in fixkeys:self.custom_pcr[f'phase{i}'].pop(key, None)
            for key in fixspckeys:self.custom_pcr[f'phase{i}'].pop(key, None)
            for key in pequpatkeys: self.custom_pcr[f'phase{i}'].pop(key, None)

    def get_limits(self, ire: int):
        return self.custom_pcr[f'limits{ire}']

    def count_limits(self):
        return sum('limits' in key for key in self.custom_pcr.keys())

    def add_limits(self, vals: List[str]):
        nre = self.count_limits()
        self.custom_pcr[f'limits{nre}'] = vals
        self.edit_flags_global('nre', nre + 1)
    
    def replace_limits(self, ire: int, vals: str):
        self.custom_pcr[f'limits{ire}'] = vals.split()

    def remove_limits(self):
        nre = self.count_limits()
        for i in range(nre):
            self.custom_pcr.pop(f'limits{i}', None)
        self.edit_flags_global('nre', 0)

    def remove_limits_one(self):
        nre = self.count_limits()
        self.custom_pcr.pop(f'limits{range(nre)[-1]}', None)
        self.edit_flags_global('nre', nre - 1)

    def get_restraint(self, ili: int):
        return self.custom_pcr[f'restraint{ili}']

    def count_restraint(self):
        return sum('restraint' in key for key in self.custom_pcr.keys())

    def add_restraint(self, vals: List[List[str]]):
        nli = self.count_restraint()
        self.custom_pcr[f'restraint{nli}'] = {'values': vals[0]}
        self.custom_pcr[f'restraint{nli}']['coeff'] = vals[1]
        self.edit_flags_global('nli', nli + 1)

    def replace_restraint_vals(self, ili: int, vals: str):
        self.custom_pcr[f'restraint{ili}']['values'] = vals.split()

    def replace_restraint_coeff(self, ili: int, coeff: str):
        self.custom_pcr[f'restraint{ili}']['coeff'] = coeff.split()

    def remove_restraint(self):
        nli = self.count_restraint()
        for i in range(nli):
            self.custom_pcr.pop(f'restraint{i}', None)
        self.edit_flags_global('nli', 0)

    def remove_restraint_one(self):
        nli = self.count_restraint()
        self.custom_pcr.pop(f'restraint{range(nli)[-1]}', None)
        self.edit_flags_global('nli', nli - 1)

    def read_hm_symbol(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        return self.custom_pcr[f'phase{idx}']['spacegroup']

    def check_phase_exists(self, identifier: str):
        if identifier in self.read_ids():
            return True
        else:
            return False

    def remove_phase(self, identifier: str):
        nph = len(self.read_ids())
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        self.custom_pcr.pop(f'phase{idx}', None)
        self.reformat_phase_keys()
        self.edit_flags_global('nph', nph - 1)

    def get_phase(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        return self.custom_pcr[f'phase{idx}']

    def add_phase(self, phase: dict):
        nph = len(self.read_ids())
        self.custom_pcr[f'phase{nph}'] = phase
        self.edit_flags_global('nph', nph + 1)

    def reformat_phase_keys(self):
        """ After removing phases rename dictionary keys to go in increments of 1 """
        count = 0
        tmp = {}
        for keys, values in self.custom_pcr.items():
            if 'phase' in keys:
                tmp[keys] = f'phase{count}'
                count += 1

        pcr = {(tmp[k] if k in tmp else k): v for (k, v) in self.custom_pcr.items()}
        self.custom_pcr = pcr

    def reformat_atom_keys(self, identifier: str):
        idx = [i for i, val in enumerate(self.read_ids()) if val == identifier][0]
        count = 0
        tmp = {}
        for keys, values in self.custom_pcr[f'phase{idx}'].items():
            if 'atom' in keys:
                tmp[keys] = f'atom{count}'
                count += 1

        pcr = {(tmp[k] if k in tmp else k): v for (k, v) in self.custom_pcr[f'phase{idx}'].items()}
        self.custom_pcr[f'phase{idx}'] = pcr


class MicIO:

    def __init__(self, path: str, jobid: str):
        self.path = path
        self.jobid = jobid

    def read_fromfile(self, iphas: int, ipatt: int = None):

        if ipatt is None:
            filename = os.path.join(self.path, f"{self.jobid}{iphas}.mic")
        else:
            filename =  os.path.join(self.path, f"{self.jobid}{iphas}_{ipatt}.mic")

        with open(filename, 'r') as f:
            """ Start loop to check for the first floating number """
            lines = f.readlines()

        index_last = [idx for idx, line in enumerate(lines)
                      if 'Icalc' in line and 'Iobs' in line and 'Beta_TCH' in line][0]

        index_app_size = [idx for idx, line in enumerate(lines) if 'Average' in line and 'size' in line][0]
        index_app_strain = [idx for idx, line in enumerate(lines) if 'Average' in line and 'strain' in line][0]

        mic_vals = [line.split() for line in lines[index_last + 1: index_app_size - 1]
                    if 'Resolution limited' not in line]

        mic_vals = np.array(mic_vals, dtype=float)

        fwhm_info = mic_vals

        if '!' in lines[index_app_size]:
            avg_size = None
        else:
            val = re.search('(?<=:).*', lines[index_app_size])[0]

            stds = re.findall(r'\(.*?\)', val)[0]
            stds = stds.replace('(', '').replace(')', '')

            vals = re.sub(r'\([^)]*\)', '', val)

            avg_size = (float(vals), float(stds))

        if '!' in lines[index_app_strain]:
            avg_strain = None
        else:
            val = re.search('(?<=:).*', lines[index_app_strain])[0]

            stds = re.findall(r'\(.*?\)', val)[0]
            stds = stds.replace('(', '').replace(')', '')

            vals = re.sub(r'\([^)]*\)', '', val)

            avg_strain = (float(vals), float(stds))

        return [fwhm_info, avg_size, avg_strain]


class SumIO:

    def __init__(self, path: str, jobid: str = None):
        self.path = path
        if jobid is None:
            self.jobid = os.path.basename(os.path.normpath(path))
        else:
            self.jobid = jobid

    def parse_all_info(self, pcr: PcrIO):

        with open(os.path.join(self.path, f"{self.jobid}.sum"), 'r') as f:
            lines = [line.replace('\n', '').strip() for line in f]

        results = dict()
        identifiers = pcr.read_ids()
        lines_tot = len(lines)

        try:
            for iline in range(lines_tot):
                if 'Phase No.' in lines[iline]:
                    iphas = int(lines[iline].split()[3]) - 1
                    ids = identifiers[iphas]
                    nat = pcr.get_flags_phase(ids, 'nat')

                if 'ATOM PARAMETERS' in lines[iline]:
                    for atom in lines[iline + 3:iline + 3 + nat]:
                        stds = re.findall(r'\(.*?\)', atom)
                        stds = [s.replace('(', '').replace(')', '').strip() for s in stds]
                        std_numbers = [len(s) for s in stds]

                        vals = re.sub(r'\([^)]*\)', '', atom)
                        vals = [s for s in vals.split()]
                        val_decimals = [decimal.Decimal(s).as_tuple().exponent for s in vals[1:-1]]

                        fill = [abs(s + v) for s, v in zip(std_numbers, val_decimals)]
                        corrected = [f"0.{s.zfill(v + len(s))}" for s, v in zip(stds, fill)]

                        results[('Sites', 'pos_x', ids, vals[0])] = (float(vals[1]), float(corrected[0]))
                        results[('Sites', 'pos_y', ids, vals[0])] = (float(vals[2]), float(corrected[1]))
                        results[('Sites', 'pos_z', ids, vals[0])] = (float(vals[3]), float(corrected[2]))
                        results[('Sites', 'b_iso', ids, vals[0])] = (float(vals[4]), float(corrected[3]))
                        results[('Sites', 'occupations', ids, vals[0])] = (float(vals[5]), float(corrected[4]))

                if 'PROFILE PARAMETERS FOR PATTERN' in lines[iline]:
                    ipatt = int(lines[iline].split()[-1]) - 1

                if 'Cell parameters' in lines[iline]:
                    vals = (float(lines[iline + 1].split()[0]), float(lines[iline + 1].split()[1]))
                    results[(f'Pattern {ipatt}', 'cell_a', ids)] = vals
                    vals = (float(lines[iline + 2].split()[0]), float(lines[iline + 2].split()[1]))
                    results[(f'Pattern {ipatt}', 'cell_b', ids)] = vals
                    vals = (float(lines[iline + 3].split()[0]), float(lines[iline + 3].split()[1]))
                    results[(f'Pattern {ipatt}', 'cell_c', ids)] = vals
                    vals = (float(lines[iline + 4].split()[0]), float(lines[iline + 4].split()[1]))
                    results[(f'Pattern {ipatt}', 'cell_alpha', ids)] = vals
                    vals = (float(lines[iline + 5].split()[0]), float(lines[iline + 5].split()[1]))
                    results[(f'Pattern {ipatt}', 'cell_beta', ids)] = vals
                    vals = (float(lines[iline + 6].split()[0]), float(lines[iline + 6].split()[1]))
                    results[(f'Pattern {ipatt}', 'cell_gamma', ids)] = vals

                if 'overall scale factor' in lines[iline] or 'Overall scale factor' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'scale_factors', ids)] = vals

                if 'Overall temperature  factor' in lines[iline] or 'Overall tem. factor' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'boverall', ids)] = vals

                if 'Halfwidth parameters' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'u_strain', ids)] = vals
                    vals = (float(lines[iline + 1].split()[-2]), float(lines[iline + 1].split()[-1]))
                    results[(f'Pattern {ipatt}', 'v', ids)] = vals

                if 'Asymmetry parameters' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'asym1', ids)] = vals
                    vals = (float(lines[iline + 1].split()[-2]), float(lines[iline + 1].split()[-1]))
                    results[(f'Pattern {ipatt}', 'asym2', ids)] = vals
                    vals = (float(lines[iline + 2].split()[-2]), float(lines[iline + 2].split()[-1]))
                    results[(f'Pattern {ipatt}', 'asym3', ids)] = vals
                    vals = (float(lines[iline + 3].split()[-2]), float(lines[iline + 3].split()[-1]))
                    results[(f'Pattern {ipatt}', 'asym4', ids)] = vals

                if 'X and y parameters' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'x_strain', ids)] = vals
                    vals = (float(lines[iline + 1].split()[-2]), float(lines[iline + 1].split()[-1]))
                    results[(f'Pattern {ipatt}', 'y_size', ids)] = vals

                if 'Size parameters (G,L)' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'g_size', ids)] = vals

                if 'Phase-dependent shift parameters' in lines[iline]:
                    vals = (float(lines[iline + 1].split()[1]), float(lines[iline + 1].split()[2]))
                    results[(f'Pattern {ipatt}', 'phzero', ids)] = vals
                    vals = (float(lines[iline + 2].split()[1]), float(lines[iline + 2].split()[2]))
                    results[(f'Pattern {ipatt}', 'phsycos', ids)] = vals
                    vals = (float(lines[iline + 3].split()[1]), float(lines[iline + 3].split()[2]))
                    results[(f'Pattern {ipatt}', 'phsysin', ids)] = vals

                if 'Spherical Harmonics coeff.(size)' in lines[iline]:
                    laue = pcr.get_laue(ids, ipatt)
                    if '12/m1' == laue or '112/m' == laue:
                        params = 'y00 y20 y22+ y22- y40 y42+ y42- y44+ y44-'.split()     
                    elif '-3m1' == laue or '-31m' == laue:
                        params = 'y00 y20 y40 y43- y60 y63- y66+'.split()
                    elif 'm-3' == laue or 'm-3m' == laue:
                        params = 'k00 k41 k61 k62 k81'.split()
                    elif 'mmm' == laue:
                        params = 'y00 y20 y22+ y40 y42+ y44+'.split()
                    elif '6/m' == laue or '6/mmm' == laue:
                        params = 'y00 y20 y40 y60 y66+ y66-'.split()
                    elif '-3' == laue:
                        params = 'y00 y20 y40 y43- y43+'.split()
                    elif '4/m' == laue or '4/mmm' == laue:
                        params = 'y00 y20 y40 y44+ y44- y60 y64+ y64-'.split()
                    elif '-1' == laue:
                        params = 'y00 y20 y21+ y21- y22+ y22-'.split()
                    
                    numbers = [float(val) for val in lines[iline + 2].split()] + [float(val) for val in lines[iline + 3].split()] + [float(val) for val in lines[iline + 4].split()]
                    pos = 0
                    for param in params:
                        vals = (numbers[pos], numbers[pos + 1])
                        pos += 2
                        results[(f'Pattern {ipatt}', param, ids)] = vals
                
                if 'Generalized S_HKL strain parameters' in lines[iline]:
                    laue = pcr.get_laue(ids, ipatt)
                    if '-1' == laue:
                        params = 's400 s040 s004 s220 s202 s022 s211 s121 s112 s310 s301 s130 s103 s013 s031'.split()
                        rows = 3
                    elif '12/m1' == laue:
                        params = 's400 s040 s004 s220 s202 s022 s121 s301 s103'.split()
                        rows = 2
                    elif '112/m' == laue:
                        params = 's400 s040 s004 s220 s202 s022 s112 s310 s130'.split()
                        rows = 2
                    elif 'mmm' == laue:
                        params = 's400 s040 s004 s220 s202 s022'.split()
                        rows = 1
                    elif '4/m' == laue or '4/mmm' == laue:
                        params = 's400 s040 s220 s202'.split()
                        rows = 1
                    elif '-3R' == laue or '-3mR' == laue:
                        params = 's400 s004 s112 s211'.split()
                        rows = 1
                    elif '-3' == laue or '-3m1' == laue or '-31m' == laue or '6/m' == laue or '6/mmm' == laue:
                        params = 's400 s004 s112'.split()
                        rows = 1
                    elif 'm-3' == laue or 'm-3m' == laue:
                        params = 's400 s220'.split()
                        rows = 1
                    
                    res = []
                    for row in range(1, rows + 1):
                        vals = [float(val) for val in lines[iline + row*3].split()]
                        stds = [float(val) for val in lines[iline + row*3 + 1].split()]
                        res.extend(list(zip(vals, stds)))

                    for i, param in enumerate(params):
                        results[(f'Pattern {ipatt}', param, ids)] = res[i]
                    
                if 'Lorentzian-Anisotropic-Strain Mixing Parameter' in lines[iline]:
                    vals = (float(lines[iline + 1].split()[0]), float(lines[iline + 1].split()[1]))
                    results[(f'Pattern {ipatt}', 'lorentzstr', ids)] = vals

                if 'T.O.F. Gaussian variances Sig-2, Sig-1, Sig-0' in lines[iline]:
                    vals = (float(lines[iline + 1].split()[0]), float(lines[iline + 1].split()[1]))
                    results[(f'Pattern {ipatt}', 'sigma-2', ids)] = vals
                    vals = (float(lines[iline + 2].split()[0]), float(lines[iline + 2].split()[1]))
                    results[(f'Pattern {ipatt}', 'sigma-1', ids)] = vals
                    vals = (float(lines[iline + 3].split()[0]), float(lines[iline + 3].split()[1]))
                    results[(f'Pattern {ipatt}', 'sigma-0', ids)] = vals

                if 'T.O.F. Gaussian variance Sig-Q' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'sigma-q', ids)] = vals

                if 'T.O.F. Isotropic Gaussian Strain parameter (G-strain)' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'iso-gstrain', ids)] = vals

                if 'T.O.F. Isotropic Gaussian Size   parameter (G-size)' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'iso-gsize', ids)] = vals

                if 'T.O.F. Lorentzian FWHM Gam-2, Gam-1, Gam-0' in lines[iline]:
                    vals = (float(lines[iline + 1].split()[0]), float(lines[iline + 1].split()[1]))
                    results[(f'Pattern {ipatt}', 'gamma-2', ids)] = vals
                    vals = (float(lines[iline + 2].split()[0]), float(lines[iline + 2].split()[1]))
                    results[(f'Pattern {ipatt}', 'gamma-1', ids)] = vals
                    vals = (float(lines[iline + 3].split()[0]), float(lines[iline + 3].split()[1]))
                    results[(f'Pattern {ipatt}', 'gamma-0', ids)] = vals

                if 'T.O.F. Lorentzian strain and size parameters (LStr,LSiz)' in lines[iline]:
                    vals = (float(lines[iline + 1].split()[0]), float(lines[iline + 1].split()[1]))
                    results[(f'Pattern {ipatt}', 'iso-lorstrain', ids)] = vals
                    vals = (float(lines[iline + 2].split()[0]), float(lines[iline + 2].split()[1]))
                    results[(f'Pattern {ipatt}', 'iso-lorsize', ids)] = vals

                if 'T.O.F. Peak shape parameter alpha0,beta0,beta1,alphaQ,betaQ' in lines[iline]:
                    vals = (float(lines[iline + 1].split()[0]), float(lines[iline + 1].split()[1]))
                    results[(f'Pattern {ipatt}', 'alpha0', ids)] = vals
                    vals = (float(lines[iline + 1].split()[2]), float(lines[iline + 2].split()[0]))
                    results[(f'Pattern {ipatt}', 'beta0', ids)] = vals
                    vals = (float(lines[iline + 2].split()[1]), float(lines[iline + 2].split()[2]))
                    results[(f'Pattern {ipatt}', 'alpha1', ids)] = vals
                    vals = (float(lines[iline + 3].split()[0]), float(lines[iline + 3].split()[1]))
                    results[(f'Pattern {ipatt}', 'beta1', ids)] = vals
                    vals = (float(lines[iline + 3].split()[2]), float(lines[iline + 4].split()[0]))
                    results[(f'Pattern {ipatt}', 'alphaq', ids)] = vals
                    vals = (float(lines[iline + 4].split()[1]), float(lines[iline + 4].split()[2]))
                    results[(f'Pattern {ipatt}', 'betaq', ids)] = vals

                if 'GLOBAL PARAMETERS FOR PATTERN' in lines[iline]:
                    ipatt = int(lines[iline].split()[-1]) - 1

                if 'Zero-point' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'zero')] = vals

                if 'Cos( theta)-shift parameter' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'sycos')] = vals

                if 'Sin(2theta)-shift parameter' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'sysin')] = vals

                if 'T.O.F.- dtt1' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'ddt1')] = vals

                if 'T.O.F.- dtt2' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'ddt2')] = vals

                if 'T.O.F.- dtt_1OverD' in lines[iline]:
                    vals = (float(lines[iline].split()[-2]), float(lines[iline].split()[-1]))
                    results[(f'Pattern {ipatt}', 'ddt1d')] = vals

                if 'RELIABILITY FACTORS WITH ALL NON-EXCLUDED POINTS FOR PATTERN' in lines[iline]:
                    ipatt = int(lines[iline].split()[-1]) - 1

                if 'Conventional Rietveld R-factors for Pattern' in lines[iline]:
                    results[(f'Pattern {ipatt}', 'Rp')] = (float(lines[iline + 1].split()[2]), 0.0)
                    results[(f'Pattern {ipatt}', 'Rwp')] = (float(lines[iline + 1].split()[4]), 0.0)
                    results[(f'Pattern {ipatt}', 'Rexp')] = (float(lines[iline + 1].split()[6]), 0.0)
                    results[(f'Pattern {ipatt}', 'Chi2')] = (float(lines[iline + 1].split()[8]), 0.0)

                if 'BRAGG R-Factors and weight fractions for Pattern #' in lines[iline]:
                    ipatt = int(lines[iline].split()[-1]) - 1

                if '=> Phase:' in lines[iline]:
                    iphas = int(lines[iline].split()[2]) - 1
                    ids = identifiers[iphas]
                    line = lines[iline + 1].replace(
                        '=> Bragg R-factor:', '').replace('Vol:', '').replace('Fract(%):', '')
                    stds = re.findall(r'\(.*?\)', line)
                    stds = [s.replace('(', '').replace(')', '').strip() for s in stds]

                    vals = re.sub(r'\([^)]*\)', '', line)
                    vals = [s for s in vals.split()]

                    results[(f'Pattern {ipatt}', 'R-factor', ids)] = (float(vals[0]), 0.0)
                    results[(f'Pattern {ipatt}', 'cell_vol', ids)] = (float(vals[1]), float(stds[0]))
                    results[(f'Pattern {ipatt}', 'weight_fractions', ids)] = (float(vals[2]), float(stds[1]))
        except:
            return None

        return results

    def parse_cell_params(self, pcr: PcrIO, identifiers: List[str]):
        allinfo = self.parse_all_info(pcr)

        if allinfo is None:
            return None
        else:
            cell_params = {}
            names = ['cell_a', 'cell_b', 'cell_c', 'cell_alpha', 'cell_beta', 'cell_gamma']
            for key, val in allinfo.items():
                if key[1] in names and any(key[2] == ids for ids in identifiers):
                    ipatt = int(key[0].replace('Pattern', ''))
                    if ipatt in cell_params:
                        if key[2] in cell_params[ipatt]:
                            cell_params[ipatt][key[2]] = cell_params[ipatt][key[2]] | {key[1]: val[0]}
                        else:
                            cell_params[ipatt] = cell_params[ipatt] | {key[2]: {key[1]: val[0]}}
                    else:
                        cell_params = cell_params | {ipatt: {key[2]: {key[1]: val[0]}}}

            return cell_params

    def parse_rwp(self, pcr: PcrIO):
        allinfo = self.parse_all_info(pcr)
        if allinfo is None:
            return None
        else:
            rwp = {}
            for key, val in allinfo.items():
                if 'Rwp' in key[1]:
                    ipatt = int(key[0].replace('Pattern', ''))
                    rwp[ipatt] = val[0]

            return rwp

    def parse_chi2(self, pcr: PcrIO):
        allinfo = self.parse_all_info(pcr)
        if allinfo is None:
            return None
        else:
            chi2 = {}
            for key, val in allinfo.items():
                if 'Chi2' in key[1]:
                    ipatt = int(key[0].replace('Pattern', ''))
                    chi2[ipatt] = val[0]

            return chi2

    def parse_volume_fractions(self, pcr: PcrIO, identifiers: List[str]):
        allinfo = self.parse_all_info(pcr)
        if allinfo is None:
            return None
        else:
            vol_frac = {}
            for key, val in allinfo.items():
                if 'weight_fractions' in key[1] and any(key[2] == ids for ids in identifiers):
                    ipatt = int(key[0].replace('Pattern', ''))
                    if ipatt in vol_frac:
                        vol_frac[ipatt] = vol_frac[ipatt] | {key[2]: val[0]}
                    else:
                        vol_frac = vol_frac | {ipatt: {key[2]: val[0]}}

            return vol_frac

    def parse_r_bragg(self, pcr: PcrIO, identifiers: List[str]):
        allinfo = self.parse_all_info(pcr)

        if allinfo is None:
            return None
        else:
            rbragg = {}
            for key, val in allinfo.items():
                if 'R-factor' in key[1] and any(key[2] == ids for ids in identifiers):
                    ipatt = int(key[0].replace('Pattern', ''))
                    if ipatt in rbragg:
                        rbragg[ipatt] = rbragg[ipatt] | {key[2]: val[0]}
                    else:
                        rbragg = rbragg | {ipatt: {key[2]: val[0]}}

            return rbragg


class PrfIO:

    def __init__(self, path: str, jobid: str):
        self.patterns = None
        self.nphase = None
        self.reflections = None
        self.excluded = None
        self.zerovalue = None

        self.jobpath = path
        self.jobid = jobid

    def read_data(self, ipatt: int = None):

        if ipatt is None:
            filename = os.path.join(self.jobpath, f"{self.jobid}.prf")
        else:
            filename = os.path.join(self.jobpath, f"{self.jobid}_{ipatt}.prf")

        with open(filename, 'r') as f:
            f.readline()
            data = f.readline().strip().split()
            self.nphase = int(data[0])
            npts = int(data[1])
            self.zerovalue = float(data[4])

            div = self.nphase // 8
            res = self.nphase % 8
            line_phases = []
            for i in range(div):
                data = f.readline().strip().split()
                line_phases.append(data[:8])

            nexclude = int(data[-1])

            if res != 0:
                data = f.readline().strip().split()
                line_phases.append(data[:res])
                nexclude = int(data[-1])

            line_phases = [element for sublist in line_phases for element in sublist]
            line_phases = np.array(line_phases, dtype=int)

            if nexclude != 0:
                self.excluded = np.genfromtxt(f, dtype=float, max_rows=nexclude)

                if self.excluded.ndim == 1:
                    self.excluded = self.excluded[np.newaxis, :]

            f.readline()
            self.patterns = np.genfromtxt(f, dtype=float, max_rows=npts)
            reflection = {'x': [], 'h': [], 'k': [], 'l': [], 'ph': []}
            for line in f:
                lines = line.split()
                hkl = re.findall('...', re.findall('\(+(.*?)\)', line)[0])

                reflection['x'].append(float(lines[0]))
                reflection['h'].append(int(hkl[0]))
                reflection['k'].append(int(hkl[1]))
                reflection['l'].append(int(hkl[2]))
                reflection['ph'].append(int(lines[-1]))

            self.reflections = pd.DataFrame(reflection)
