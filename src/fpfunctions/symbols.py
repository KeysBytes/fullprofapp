from PyQt5 import QtGui as qtg


def custom_symbol(symbol: str, font: qtg.QFont = qtg.QFont("San Serif")):
    """Create custom symbol with font"""
    # We just want one character here, comment out otherwise
    assert len(symbol) == 1
    pg_symbol = qtg.QPainterPath()
    pg_symbol.addText(0, 0, font, symbol)
    # Scale symbol
    br = pg_symbol.boundingRect()
    scale = min(1. / br.width(), 1. / br.height())
    tr = qtg.QTransform()
    tr.scale(scale, scale)
    tr.translate(-br.x() - br.width() / 2., -br.y() - br.height() / 2.)
    return tr.map(pg_symbol)