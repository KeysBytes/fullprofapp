import logging
import os
import sys
import urllib.request
from collections import namedtuple
from typing import Dict, List, Optional, Union
from urllib.parse import urlparse, urljoin

import requests
from pymatgen.util.sequence import PBar

Provider = namedtuple("Provider", ["name", "base_url", "description", "homepage", "prefix"])

_logger = logging.getLogger(__name__)
_handler = logging.StreamHandler(sys.stdout)
_logger.addHandler(_handler)
_logger.setLevel(logging.WARNING)


class ClientOptimade:
    """ This class is a simplified version of the OptimadeRester, from Pymatgen, it just retrieves id's from queries
    TODO: Implement the optimade client but using the Client features of the optimade python tools"""

    aliases: Dict[str, str] = {
        "cod": "https://www.crystallography.net/cod/optimade/",
        "mp": "https://optimade.materialsproject.org",
    }

    def __init__(self, aliases_or_resource_urls: Optional[Union[str, List[str]]] = None, timeout: int = 5):
        self.session = requests.Session()
        self._timeout = timeout

        if isinstance(aliases_or_resource_urls, str):
            aliases_or_resource_urls = [aliases_or_resource_urls]

        self.resources = {}

        if not aliases_or_resource_urls:
            aliases_or_resource_urls = list(self.aliases.keys())
            _logger.warning(
                "Connecting to all known OPTIMADE providers, this will be slow. Please connect to only the "
                f"OPTIMADE providers you want to query. Choose from: {', '.join(self.aliases.keys())}"
            )

        for alias_or_resource_url in aliases_or_resource_urls:

            if alias_or_resource_url in self.aliases:
                self.resources[alias_or_resource_url] = self.aliases[alias_or_resource_url]

            elif self._validate_provider(alias_or_resource_url):

                # TODO: unclear what the key should be here, the "prefix" is for the root provider,
                # may need to walk back to the index for the given provider to find the correct identifier

                self.resources[alias_or_resource_url] = alias_or_resource_url

            else:
                _logger.error(f"The following is not a known alias or a valid url: {alias_or_resource_url}")

        self._providers = {url: self._validate_provider(provider_url=url) for url in self.resources.values()}

    def __repr__(self):
        return f"OptimadeRester connected to: {', '.join(self.resources.values())}"

    def __str__(self):
        return self.describe()

    def describe(self):
        """
        Provides human-readable information about the resources being searched by the OptimadeRester.
        """
        provider_text = "\n".join(map(str, (provider for provider in self._providers.values() if provider)))
        description = f"OptimadeRester connected to:\n{provider_text}"
        return description

    def _get_json(self, url):
        """
        Retrieves JSON, will attempt to (politely) try again on failure subject to a
        random delay and a maximum number of attempts.
        """
        return self.session.get(url, timeout=self._timeout).json()

    def get_id_with_filter(self, optimade_filter: str) -> Dict[str, List[str]]:
        """
        Get structure id's for later downloads
        """

        all_ids = {}

        for identifier, resource in self.resources.items():

            url = urljoin(resource, f"v1/structures?filter={optimade_filter}")

            try:

                json = self._get_json(url)

                ids = self._get_id_from_resource(json, url)

                pbar = PBar(total=json["meta"].get("data_returned", 0), desc=identifier, initial=len(ids))

                # TODO: check spec for `more_data_available` boolean, may simplify this conditional
                if ("links" in json) and ("next" in json["links"]) and (json["links"]["next"]):
                    while "next" in json["links"] and json["links"]["next"]:
                        next_link = json["links"]["next"]
                        if isinstance(next_link, dict) and "href" in next_link:
                            next_link = next_link["href"]

                        json = self._get_json(next_link)
                        additional_ids = self._get_id_from_resource(json, url)
                        ids.extend(additional_ids)
                        pbar.update(len(additional_ids))

                if ids:

                    all_ids[identifier] = ids

            except Exception as exc:

                # TODO: manually inspect failures to either (a) correct a bug or (b) raise more appropriate error

                _logger.error(
                    f"Could not retrieve required information from provider {identifier} and url {url}: {exc}"
                )

        return all_ids

    @staticmethod
    def _get_id_from_resource(json, url) -> List[str]:

        ids = []
        exceptions = set()

        for data in json["data"]:

            # TODO: check the spec! and remove this try/except (are all providers following spec?)

            try:
                ids.append(data['id'])

            # TODO: bare exception, remove...
            except Exception:
                _logger.error(f'Failed to parse returned data for {url}: {", ".join(exceptions)}')

        return ids

    def _validate_provider(self, provider_url) -> Optional[Provider]:
        """
        Checks that a given URL is indeed an OPTIMADE provider,
        returning None if it is not a provider, or the provider
        prefix if it is.
        TODO: careful reading of OPTIMADE specification required
        TODO: add better exception handling, intentionally permissive currently
        """

        def is_url(url):
            """
            Basic URL validation thanks to https://stackoverflow.com/a/52455972
            """
            try:
                result = urlparse(url)
                return all([result.scheme, result.netloc])
            except ValueError:
                return False

        if not is_url(provider_url):
            _logger.warning(f"An invalid url was supplied: {provider_url}")
            return None

        try:
            url = urljoin(provider_url, "v1/links")
            provider_info_json = self._get_json(url)
        except Exception as exc:
            _logger.warning(f"Failed to parse {url} when validating: {exc}")
            return None

        try:
            return Provider(
                name=provider_info_json["meta"].get("provider", {}).get("name", "Unknown"),
                base_url=provider_url,
                description=provider_info_json["meta"].get("provider", {}).get("description", "Unknown"),
                homepage=provider_info_json["meta"].get("provider", {}).get("homepage"),
                prefix=provider_info_json["meta"].get("provider", {}).get("prefix", "Unknown"),
            )
        except Exception as exc:
            _logger.warning(f"Failed to extract required information from {url}: {exc}")
            return None

    def _parse_provider(self, provider, provider_url) -> Dict[str, Provider]:
        """
        Used internally to update the list of providers or to
        check a given URL is valid.
        It does not raise exceptions but will instead _logger.warning and provide
        an empty dictionary in the case of invalid data.
        In future, when the specification  is sufficiently well adopted,
        we might be more strict here.
        Args:
            provider: the provider prefix
            provider_url: An OPTIMADE provider URL
        Returns:
            A dictionary of keys (in format of "provider.database") to
            Provider objects.
        """

        try:
            url = urljoin(provider_url, "v1/links")
            provider_link_json = self._get_json(url)
        except Exception as exc:
            _logger.error(f"Failed to parse {url} when following links: {exc}")
            return {}

        def _parse_provider_link(provider, provider_link_json):
            """No validation attempted."""
            ps = {}
            try:
                d = [d for d in provider_link_json["data"] if d["attributes"]["link_type"] == "child"]
                for link in d:
                    key = f"{provider}.{link['id']}" if provider != link["id"] else provider
                    if link["attributes"]["base_url"]:
                        ps[key] = Provider(
                            name=link["attributes"]["name"],
                            base_url=link["attributes"]["base_url"],
                            description=link["attributes"]["description"],
                            homepage=link["attributes"].get("homepage"),
                            prefix=link["attributes"].get("prefix"),
                        )
            except Exception:
                # print(f"Failed to parse {provider}: {exc}")
                # Not all providers parse yet.
                pass
            return ps

        return _parse_provider_link(provider, provider_link_json)

    def refresh_aliases(self, providers_url="https://providers.optimade.org/providers.json"):
        """
        Updates available OPTIMADE structure resources based on the current list of OPTIMADE
        providers.
        """
        json = self._get_json(providers_url)
        providers_from_url = {
            entry["id"]: entry["attributes"]["base_url"] for entry in json["data"] if entry["attributes"]["base_url"]
        }

        structure_providers = {}
        for provider, provider_link in providers_from_url.items():
            structure_providers.update(self._parse_provider(provider, provider_link))

        self.aliases = {alias: provider.base_url for alias, provider in structure_providers.items()}

    # TODO: revisit context manager logic here and in MPRester
    def __enter__(self):
        """
        Support for "with" context.
        """
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Support for "with" context.
        """
        self.session.close()


class Client:

    def __init__(self, provider: str, selected_elements: dict, terminal):
        self.provider = provider
        self.selected_elements = selected_elements
        self.identifiers = None
        self.terminal = terminal

    @staticmethod
    def _build_custom_optimade_filter(selected_elements: Dict[str, int]):
        """ Rewrite filter generator from PyMatgen for our needs"""
        has = []
        not_have = []
        filters = []
        for key, value in selected_elements.items():
            if isinstance(key, str):
                if value == 0:
                    not_have.append(key)
                elif value == 1:
                    pass
                elif value == 2:
                    has.append(key)
                else:
                    raise ValueError('Error, element in selection does not have a status')
            else:
                raise TypeError('Error, keys in selection dictionary are not strings')

        not_have_str = ", ".join(f'"{el}"' for el in not_have)
        has_str = ", ".join(f'"{el}"' for el in has)

        if len(has) != 0:
            filters.append(f'(elements HAS ALL {has_str})')
        if len(not_have) != 0:
            filters.append(f'NOT (elements HAS ANY {not_have_str})')

        return " AND ".join(filters)

    def _run_optimade_client(self):
        self.terminal.append(f"Connecting to the {self.provider}'s Optimade API...\n")
        opt = ClientOptimade(self.provider, timeout=240)
        self.terminal.append(f"Connected...\n")
        self.terminal.append(f"Downloading materials with the selected filters...\n")
        ids = opt.get_id_with_filter(self._build_custom_optimade_filter(self.selected_elements))
        self.terminal.append(f"Download complete\n")

        return ids

    def get_urls(self):
        urls = []
        if self.provider == 'mp':
            ids = self._run_optimade_client()

            self.identifiers = ids[self.provider]
            for identifier in self.identifiers:
                urls.append(f'https://next-gen.materialsproject.org/materials/{identifier}/cif?type=symmetrized')

        elif self.provider == 'cod':
            ids = self._run_optimade_client()

            self.identifiers = ids[self.provider]
            for identifier in self.identifiers:
                urls.append(f'https://www.crystallography.net/cod/{identifier}.cif')

        return urls
