import sys
import os

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

from fpgui.mainwindow import MainWindow


def main():
    """ TODO: Catch raised errors on runtime and show message boxes """
    qtw.QApplication.setHighDpiScaleFactorRoundingPolicy(qtc.Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    qtc.QCoreApplication.setAttribute(qtc.Qt.AA_EnableHighDpiScaling, True)
    app = qtw.QApplication(sys.argv)

    qss = os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), "fpgui/css/general.qss")
    with open(qss, "r") as f:
        app.setStyleSheet(f.read())

    if sys.platform.startswith('linux'):
        app.setWindowIcon(qtg.QIcon(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), 'fpgui/icons/logo.svg')))
    elif sys.platform.startswith('win32'):
        app.setWindowIcon(qtg.QIcon(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), 'fpgui/icons/logo.png')))
    else:
        raise ValueError("FullProfAPP is not implemented in your operating system")
    mainwindow = MainWindow()
    mainwindow.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()