Current Version
---------------
:Date: Now

* The **identifier** column in the `Queried Materials` and `Selected Materials` tables can be edited. The newly given names will rename the CIF files located in the local database folder.
* Changed behavior of :fas:`plus` and :fas:`plus-square` buttons. They now **add** the phases appearing in the `Selected Materials` to an already loaded PCR file instead of **replacing** it.
* Fixed bug with IRF files that had 'Res != 1' formats
* Included **parameter limits** in `Automatic Refinement Protocol` and `Manual Refinement` windows
* Fixed bug when clicking :fas:`plus-square` button
* Included 'Check All' button in the `Summary` graph tab
* Format changes for data exported using the `Export Data` button in the `Summary` graph tab
* Added a `Help` action bar in the Main Window, with a link to the online manual and information about the version number.

Version 1.2.10
--------------
:Date: February 9, 2024

* First pattern always checked by default
* Improved handling of PATH environmental variables

Version 1.2.9
-------------
:Date: January 18, 2024

* Issues with `Full-Profile Phase Search` due to the latest updates of CrysFML2008 are now fixed
* Fixed issues for `Manual Refinement` on linux versions.

Version 1.2.8 (broken)
----------------------
:Date: January 9, 2023

* Removed 3D renders due to unreliable use of OpenGL
* Solved bug on checked patterns table, and CIF duplication
* Included feature to load/save workflow files in JSON format in the `Automatic Refinement Protocol` window

Version 1.2.7
-------------
:Date: December 1, 2023

* Included Fourier filtering as an additional option to treat the background during Rietveld refinement

Version 1.2.6
-------------
:Date: November 14, 2023

* Improved pop-up window sizing. Dialog buttons used to hide out of the screen.
* Improved consistency of material tab behavior.
* Fixed bug where pattern combobox in `Manual Refinement` was hidden.
* Loading external PCR files in `Automatic Refinement Protocol` and `Manual Refinement` will automatically change some internal tags to target specific output file format.
* Imported CIF files must be encoded in UTF-8 or ASCII for `PyCifRW` to read. Warning messages have been implemented if file encodings are different. **This is to be changed in future versions**.
* Improved behavior of BUF files in the `Import Patterns` window.
* Exported files from the :ref:`Results <prfdat>` graphs now contain the pattern name that relates to the extracted values.

Version 1.2.5
-------------
:Date: November 9, 2023

* Bug fixed when trying to read 'NaN' values from the standard output

Version 1.2.4
-------------
:Date: October 31, 2023

* Fixed bug when trying to link a PCR file to a pattern while other PCR files existed in the working directory
* Fixed bug when substituting background points generated with `Automatic Background` in sequential mode
* Error handling in Contour Plots

Version 1.2.3
-------------
:Date: October 26, 2023

* Extended support for IRF keywords and diffractometer geometries
* Improved handling of refinement parameters in sequential runs
* Improved actions for Contour Plots
* Corrected bug with internally defined paths
* Corrected bug in exporting data from Results tab
* Correction on the transformation between the r.m.s strains and average maximum strains for the calculation of the Figure of Merit for the `Full-Profile Phase Search`.
* Corrected bug where launching ``fp2k`` in console resulted in errors due to file paths that are too long. Changing file paths to relative when executing ``fp2k`` solves the issue

Version 1.2.2
-------------
:Date: September 26, 2023

* Corrected bug in Automatic Refinement Protocol window, weight fractions were wrongly accessed
* Corrected bug in Full-Profile Phase Search protocol, timers are restarted each time a process finishes.

Version 1.2.1
-------------
:Date: September 21, 2023

* Corrected bug in Manual Refinement window, changing any value in the 'Nat' parameter line changed the number of atoms.
* Corrected bug in Full-Profile Phase Search protocol, in case processes do not finish by themselves (with or without errors), a periodic check is done every 15s looking for error messages in the standard output. If the same process is unfinished after two subsequent checks (30s), it is terminated.

Version 1.2.0
-------------
:Date: September 15, 2023

* Anisotropic Size and Strain parameters included in the `Automatic Refinement Protocol` window.
* Phase dependent Zero, SyCos, and SySin shifts included in the `Automatic Refinement Protocol` window.
* Plotting of the abovementioned parameters included in the `Results` tab.
* Corrected bug in the determination of the Laue class of the phases.

Version 1.1.1
-------------
:Date: September 3, 2023

* Corrected bug in Automatic Refinement Protocols, where weight fractions where checked for non existing phases thus raising internal exceptions.

Version 1.1.0
-------------
:Date: August 3, 2023

* Process scheduler to parallelize ``fp2k`` jobs over different patterns or candidate phases (x4 speed of execution on our tests)
* ``Reset`` button on the **Terminal** tab is removed
* Improved behavior of the `Current (.prf)`, `Current (WinPLOTR)`, and `Current Results (.sum/.mic)` actions.

Version 1.0.0
-------------
:Date: July 15, 2023

* First release of `FullProfAPP`, includes functionalities to perform Full-Profile phase searches, and highly automated refinements.
